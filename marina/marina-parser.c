/* marina-parser.c
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#include "marina-parser.h"

GType
marina_parser_get_type (void)
{
  static GType marina_parser_type = 0;
  
  if (!marina_parser_type)
    {
      const GTypeInfo marina_parser_info = {
        sizeof (MarinaParserIface),
        NULL, /* base_init */
        NULL, /* base_finalize */
        NULL,
        NULL, /* class_finalize */
        NULL, /* class_data */
        0,
        0,
        NULL
      };
      
      marina_parser_type = g_type_register_static (G_TYPE_INTERFACE, "MarinaParser",
                                                   &marina_parser_info, 0);
      g_type_interface_add_prerequisite (marina_parser_type, G_TYPE_OBJECT);
   }
  
  return marina_parser_type;
}

/**
 * marina_parser_can_parse:
 * @parser: A #MarinaParser
 * @source: A #MarinaSource
 * @stream: A #GInputStream
 *
 * Determines if the parser implementation can parse a particular stream.
 *
 * Return value: TRUE if the stream can by parsed by this parser.
 */
gboolean
marina_parser_can_parse (MarinaParser *parser,
                         MarinaSource *source,
                         GInputStream *stream)
{
  return MARINA_PARSER_GET_INTERFACE (parser)->can_parse (parser, source, stream);
}

/**
 * marina_parser_parse:
 * @parser: A #MarinaParser
 * @source: A #MarinaSource
 * @stream: A #GInputStream
 * @error: A location for a #GError
 *
 * Parses @stream from @source.
 *
 * Return value: A #MarinaParseResult which can be navigated to extract the
 *   information parsed from various items.
 */
MarinaParseResult*
marina_parser_parse (MarinaParser  *parser,
                     MarinaSource  *source,
                     GInputStream  *stream,
                     GError       **error)
{
  return MARINA_PARSER_GET_INTERFACE (parser)->
    parse (parser, source, stream, error);
}
