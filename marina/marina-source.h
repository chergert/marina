/* marina-source.h
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifndef __MARINA_SOURCE_H__
#define __MARINA_SOURCE_H__

#include <glib-object.h>
#include <gio/gio.h>

#include "xml-reader.h"
#include "xml-writer.h"

#include "marina-item.h"

G_BEGIN_DECLS

#define MARINA_TYPE_SOURCE (marina_source_get_type())

#define MARINA_SOURCE(obj)               \
  (G_TYPE_CHECK_INSTANCE_CAST((obj),     \
  MARINA_TYPE_SOURCE, MarinaSource))

#define MARINA_IS_SOURCE(obj)            \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj),     \
  MARINA_TYPE_SOURCE))

#define MARINA_SOURCE_GET_INTERFACE(obj) \
  (G_TYPE_INSTANCE_GET_INTERFACE((obj),  \
  MARINA_TYPE_SOURCE, MarinaSourceIface))

typedef struct _MarinaSource      MarinaSource;
typedef struct _MarinaSourceIface MarinaSourceIface;

struct _MarinaSourceIface
{
  GTypeInterface parent;
  
  const gchar*  (*get_id)        (MarinaSource  *source);
  
  const gchar*  (*get_title)     (MarinaSource  *source);
  void          (*set_title)     (MarinaSource  *source,
                                  const gchar   *title);
  
  gboolean      (*has_stream)    (MarinaSource  *source);
  GInputStream* (*get_stream)    (MarinaSource  *source,
                                  GError       **error);
  GList*        (*get_children)  (MarinaSource  *source);
  gboolean      (*is_transient)  (MarinaSource  *source);
  gboolean      (*observes)      (MarinaSource  *source,
                                  MarinaItem    *item);
  
  void          (*serialize)     (MarinaSource  *source,
                                  XmlWriter     *writer);
  void          (*deserialize)   (MarinaSource  *source,
                                  XmlReader     *reader);

  const gchar*  (*get_icon_name) (MarinaSource *source);

  void          (*remove)        (MarinaSource *source);

  void          (*reserved1)     (void);
  void          (*reserved2)     (void);
  void          (*reserved3)     (void);
  void          (*reserved4)     (void);
  void          (*reserved5)     (void);
  void          (*reserved6)     (void);
  void          (*reserved7)     (void);
  void          (*reserved8)     (void);
};

GType                 marina_source_get_type      (void) G_GNUC_CONST;

const gchar*          marina_source_get_id        (MarinaSource  *source);

const gchar*          marina_source_get_title     (MarinaSource  *source);
void                  marina_source_set_title     (MarinaSource  *source,
                                                   const gchar   *title);

GList*                marina_source_get_children  (MarinaSource  *source);
gboolean              marina_source_has_stream    (MarinaSource  *source);
GInputStream*         marina_source_get_stream    (MarinaSource  *source,
                                                   GError       **error);
gboolean              marina_source_is_transient  (MarinaSource  *source);
gboolean              marina_source_observes      (MarinaSource  *source,
                                                   MarinaItem    *item);

G_CONST_RETURN gchar* marina_source_get_icon_name (MarinaSource *source);

void                  marina_source_serialize     (MarinaSource  *source,
                                                   XmlWriter     *writer);
void                  marina_source_deserialize   (MarinaSource  *source,
                                                   XmlReader     *reader);

void                  marina_source_child_added   (MarinaSource  *source,
                                                   MarinaSource  *child);

void                  marina_source_remove        (MarinaSource  *source);

G_END_DECLS

#endif /* __MARINA_SOURCE_H__ */
