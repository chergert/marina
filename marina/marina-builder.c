/* marina-builder.c
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#include "marina-builder.h"

GType
marina_builder_get_type (void)
{
    static GType marina_builder_type = 0;
    
    if (!marina_builder_type)
      {
        const GTypeInfo marina_builder_info = {
            sizeof (MarinaBuilderIface),
            NULL, /* base_init */
            NULL, /* base_finalize */
            NULL,
            NULL, /* class_finalize */
            NULL, /* class_data */
            0,
            0,
            NULL
        };
        
        marina_builder_type = g_type_register_static (G_TYPE_INTERFACE, "MarinaBuilder",
                                                      &marina_builder_info, 0);
        g_type_interface_add_prerequisite (marina_builder_type, G_TYPE_OBJECT);
      }
    
    return marina_builder_type;
}

/**
 * marina_builder_build:
 * @builder: A #MarinaBuilder
 * @parse_result: A #MarinaParseResult, the output of a parse.
 * @source: A #MarinaSource or NULL
 * @item: A #MarinaItem or NULL
 *
 * Retrieves relevant information from the #MarinaParseResult and adds it
 * to either the #MarinaSource or #MarinaItem.
 *
 * Either @source or @item can be NULL as this method is used to build both
 * #MarinaSource and #MarinaItem instances.
 *
 * If @item is NULL, you should be build the @source instance.
 */
void
marina_builder_build (MarinaBuilder     *builder,
                      MarinaParseResult *parse_result,
                      MarinaSource      *source,
                      MarinaItem        *item)
{
	MARINA_BUILDER_GET_INTERFACE (builder)->build (builder, parse_result, source, item);
}

