/* marina-commands-item.c
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gtk/gtk.h>

#include "marina-app.h"
#include "marina-commands.h"
#include "marina-debug.h"
#include "marina-items.h"
#include "marina-window.h"

void
_marina_cmd_item_mark_read (GtkAction    *action,
                            MarinaWindow *window)
{
  marina_debug (DEBUG_COMMANDS);
}

void
_marina_cmd_item_move_next (GtkAction    *action,
                            MarinaWindow *window)
{
  MarinaSourceView *source_view;

  marina_debug (DEBUG_COMMANDS);

  source_view = marina_window_get_source_view (window);
  marina_source_view_move_next (source_view);
}

void
_marina_cmd_item_move_prev (GtkAction    *action,
                            MarinaWindow *window)
{
  MarinaSourceView *source_view;

  marina_debug (DEBUG_COMMANDS);

  source_view = marina_window_get_source_view (window);
  marina_source_view_move_prev (source_view);
}
