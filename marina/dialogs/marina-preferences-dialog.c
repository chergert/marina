/*
 * marina-preferences-dialog.c
 * This file is part of marina; it is based on gedit
 *
 * Copyright (C) 2001-2005 Paolo Maggi
 *               2009 Christian Hergert
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

/*
 * Modified by the gedit Team, 2001-2003. See the AUTHORS file for a 
 * list of people on the gedit Team.  
 *
 * Modified by the marina Team, 2009.
 */

#include <glib.h>
#include <glib/gi18n.h>
#include <gtk/gtk.h>

#include "marina-app.h"
#include "marina-app-private.h"
#include "marina-debug.h"
#include "marina-dirs.h"
#include "marina-message-bus.h"
#include "marina-plugins-engine.h"
#include "marina-plugin-info.h"
#include "marina-preferences-dialog.h"
#include "marina-prefs.h"
#include "marina-schemes.h"

#define PREFERENCES_DIALOG_PRIVATE(object) \
    (G_TYPE_INSTANCE_GET_PRIVATE((object), \
    MARINA_TYPE_PREFERENCES_DIALOG, \
    MarinaPreferencesDialogPrivate))

struct _MarinaPreferencesDialogPrivate
{
        GtkBuilder    *builder;
        GtkWidget     *dialog;
        
        GtkWidget     *notebook;
        
        GtkWidget     *plugins_about;
        GtkWidget     *plugins_configure;
        GtkWidget     *plugins_treeview;

        GtkWidget     *default_font_checkbutton;
        GtkWidget     *font_button;
        GtkWidget     *font_label;
        
        GtkTreeModel  *schemes;
        GtkWidget     *schemes_treeview;
};

G_DEFINE_TYPE (MarinaPreferencesDialog, marina_preferences_dialog, GTK_TYPE_DIALOG);

static void
marina_preferences_dialog_finalize (GObject *object)
{
        G_OBJECT_CLASS (marina_preferences_dialog_parent_class)->finalize (object);
}

static void
marina_preferences_dialog_class_init (MarinaPreferencesDialogClass *klass)
{
        GObjectClass *object_class = G_OBJECT_CLASS (klass);
        
        object_class->finalize = marina_preferences_dialog_finalize;

        g_type_class_add_private (object_class, sizeof (MarinaPreferencesDialogPrivate));
}

static void
dialog_response_handler (MarinaPreferencesDialog *dlg, gpointer user_data)
{
        gtk_widget_hide (GTK_WIDGET (dlg));
}

static void
default_font_toggled (GtkWidget *checkbutton,
                      MarinaPreferencesDialog *dlg)
{
  gboolean active = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (checkbutton));

  /* adjust sensitivity of the selection widgets */
  gtk_widget_set_sensitive (dlg->priv->font_label, !active);
  gtk_widget_set_sensitive (dlg->priv->font_button, !active);
  
  /* store in gconf if we want to use the default font */
  marina_prefs_set_use_default_font (active);
  
  marina_message_bus_send (marina_message_bus_get_default (),
                           "/ui",
                           "set_font",
                           "use_default_font", active,
                           "font", marina_prefs_get_font (),
                           NULL);
}

static void
plugin_text_func (GtkTreeViewColumn *tree_column,
                  GtkCellRenderer *cell,
                  GtkTreeModel *tree_model,
                  GtkTreeIter *iter,
                  gpointer data)
{
        MarinaPluginInfo *plugin = NULL;
        gchar *markup;

        gtk_tree_model_get (tree_model, iter, 0, &plugin, -1);

        if (plugin == NULL)
                return;

        markup = g_markup_printf_escaped ("<span weight=\"bold\">%s</span>\n%s",
                                          marina_plugin_info_get_name (plugin),
                                          marina_plugin_info_get_description (plugin));
        g_object_set (cell,
                      "markup", markup,
                      "sensitive", marina_plugin_info_is_available (plugin),
                      NULL);
        g_free (markup);
}

static void
plugin_active_func (GtkTreeViewColumn *tree_column,
                    GtkCellRenderer *cell,
                    GtkTreeModel *tree_model,
                    GtkTreeIter *iter,
                    gpointer data)
{
        MarinaPluginInfo *plugin = NULL;

        gtk_tree_model_get (tree_model, iter, 0, &plugin, -1);

        if (plugin == NULL)
                return;

        g_object_set (cell,
                      "active", marina_plugin_info_is_active (plugin),
                      "sensitive", marina_plugin_info_is_available (plugin),
                      NULL);
}

static void
plugin_pixbuf_func (GtkTreeViewColumn *tree_column,
                    GtkCellRenderer *cell,
                    GtkTreeModel *tree_model,
                    GtkTreeIter *iter,
                    gpointer data)
{
        MarinaPluginInfo *plugin = NULL;

        marina_debug (DEBUG_PLUGINS);

        gtk_tree_model_get (tree_model, iter, 0, &plugin, -1);

        if (plugin == NULL)
                return;

        g_object_set (cell,
                      "icon-name", marina_plugin_info_get_icon_name (plugin),
                      "sensitive", marina_plugin_info_is_available (plugin),
                      NULL);
}

static void
plugin_selection_changed_cb (GtkTreeSelection        *selection,
                             MarinaPreferencesDialog *dlg)
{
        MarinaPluginInfo *info = NULL;
        GtkTreeModel *model;
        GtkTreeIter iter;

        marina_debug (DEBUG_PLUGINS);

        if (gtk_tree_selection_get_selected (selection, &model, &iter))
                gtk_tree_model_get (model, &iter, 0, &info, -1);

        gtk_widget_set_sensitive (dlg->priv->plugins_about,
                                  info != NULL);
        gtk_widget_set_sensitive (dlg->priv->plugins_configure,
                                  (info != NULL) &&
                                  marina_plugin_info_is_configurable (info));
}

static void
active_toggled_cb (GtkCellRendererToggle   *cell,
                   gchar                   *path_str,
                   MarinaPreferencesDialog *dlg)
{
        GtkTreeIter iter;
        GtkTreePath *path;
        GtkTreeModel *model;
        MarinaPluginInfo *info;
        MarinaApp *app = marina_app_get_default ();

        marina_debug (DEBUG_PLUGINS);

        path = gtk_tree_path_new_from_string (path_str);

        model = gtk_tree_view_get_model (GTK_TREE_VIEW (dlg->priv->plugins_treeview));
        g_return_if_fail (model != NULL);

        gtk_tree_model_get_iter (model, &iter, path);

        if (&iter != NULL)
        {
                gtk_tree_model_get (model, &iter, 0, &info, -1);
                if (info != NULL)
                {
                        if (marina_plugin_info_is_active (info))
                                marina_plugins_engine_deactivate_plugin (app->priv->plugins, info);
                        else
                                marina_plugins_engine_activate_plugin (app->priv->plugins, info);
                }
        }

        gtk_tree_path_free (path);
}

static MarinaPluginInfo*
get_selected_plugin (MarinaPreferencesDialog *dlg)
{
        g_return_val_if_fail (MARINA_IS_PREFERENCES_DIALOG (dlg), NULL);

        GtkWidget        *tv;
        GtkTreeSelection *sel;
        GtkTreeModel     *model;
        GtkTreeIter       iter;

        tv = GTK_WIDGET (gtk_builder_get_object (
                GTK_BUILDER (dlg->priv->builder), "plugins_treeview"));
        g_assert (tv != NULL);

        sel = gtk_tree_view_get_selection (GTK_TREE_VIEW (tv));
        g_assert (sel != NULL);

        if (gtk_tree_selection_get_selected (sel, &model, &iter))
        {
                MarinaPluginInfo *info;
                gtk_tree_model_get (model, &iter, 0, &info, -1);
                return info;
        }

        return NULL;
}

static void
about_button_cb (GtkWidget               *button,
                 MarinaPreferencesDialog *dlg)
{
        MarinaPluginInfo *info  = NULL;
        static GtkWidget *about = NULL;

        marina_debug (DEBUG_PLUGINS);

        info = get_selected_plugin (dlg);

        g_return_if_fail (info != NULL);

        /* if there is another about dialog already open destroy it */
        if (about)
                gtk_widget_destroy (about);

        about = g_object_new (GTK_TYPE_ABOUT_DIALOG,
                "program-name", marina_plugin_info_get_name (info),
                "copyright", marina_plugin_info_get_copyright (info),
                "authors", marina_plugin_info_get_authors (info),
                "comments", marina_plugin_info_get_description (info),
                "website", marina_plugin_info_get_website (info),
                "logo-icon-name", marina_plugin_info_get_icon_name (info),
                "version", marina_plugin_info_get_version (info),
                NULL);

        gtk_window_set_destroy_with_parent (GTK_WINDOW (about), TRUE);

        g_signal_connect (about,
                          "response",
                          G_CALLBACK (gtk_widget_destroy),
                          NULL);
        g_signal_connect (about,
                          "destroy",
                          G_CALLBACK (gtk_widget_destroyed),
                          &about);

        gtk_window_set_transient_for (GTK_WINDOW (about),
                                      GTK_WINDOW (gtk_widget_get_toplevel (button)));

        gtk_widget_show (about);
}

static void
setup_plugins_page (MarinaPreferencesDialog *dlg)
{
        /* fill plugins GtkTreeModel */

        MarinaApp        *app;
        MarinaPluginInfo *plugin;
        GtkListStore     *list_store;
        const GList      *plugins;
        const GList      *tmp;
        GtkTreeIter       iter;

        marina_debug (DEBUG_PLUGINS);

        app = marina_app_get_default ();
        list_store = GTK_LIST_STORE (gtk_builder_get_object (dlg->priv->builder, "plugin_store"));
        plugins = marina_plugins_engine_get_plugin_list (app->priv->plugins);

        for (tmp = plugins; tmp; tmp = tmp->next)
        {
                plugin = MARINA_PLUGIN_INFO (tmp->data);
                g_assert (plugin != NULL);

                gtk_list_store_append (list_store, &iter);
                gtk_list_store_set (list_store, &iter, 0, plugin, -1);
        }

        /* build GtkTreeView columns */

        GtkTreeViewColumn *active_column;
        GtkTreeViewColumn *icon_column;
        GtkTreeViewColumn *text_column;

        GtkCellRenderer *toggle_cell;
        GtkCellRenderer *pixbuf_cell;
        GtkCellRenderer *text_cell;

        active_column = gtk_tree_view_column_new ();
        toggle_cell = gtk_cell_renderer_toggle_new ();
        g_object_set (toggle_cell, "xpad", 6, NULL);
        g_signal_connect (toggle_cell, "toggled",
                          G_CALLBACK (active_toggled_cb),
                          dlg);
        gtk_tree_view_column_pack_start (active_column, toggle_cell, TRUE);
        gtk_tree_view_column_set_cell_data_func (active_column, toggle_cell,
                                                 plugin_active_func,
                                                 NULL, NULL);
        gtk_tree_view_append_column (GTK_TREE_VIEW (dlg->priv->plugins_treeview),
                                     active_column);

        icon_column = gtk_tree_view_column_new ();
        pixbuf_cell = gtk_cell_renderer_pixbuf_new ();
        g_object_set (pixbuf_cell, "stock-size", GTK_ICON_SIZE_SMALL_TOOLBAR, NULL);
        gtk_tree_view_column_pack_start (icon_column, pixbuf_cell, TRUE);
        gtk_tree_view_column_set_cell_data_func (icon_column, pixbuf_cell,
                                                 plugin_pixbuf_func,
                                                 NULL, NULL);
        gtk_tree_view_append_column (GTK_TREE_VIEW (dlg->priv->plugins_treeview),
                                     icon_column);

        text_column = gtk_tree_view_column_new ();
        text_cell = gtk_cell_renderer_text_new ();
        g_object_set (text_cell, "ellipsize", PANGO_ELLIPSIZE_END, NULL);
        gtk_tree_view_column_pack_start (text_column, text_cell, TRUE);
        gtk_tree_view_column_set_cell_data_func (text_column, text_cell,
                                                 plugin_text_func,
                                                 NULL, NULL);
        gtk_tree_view_append_column (GTK_TREE_VIEW (dlg->priv->plugins_treeview),
                                     text_column);

        /* connect signals */

        GtkTreeSelection *selection = gtk_tree_view_get_selection (
                GTK_TREE_VIEW (dlg->priv->plugins_treeview));
        g_signal_connect (G_OBJECT (selection),
                          "changed",
                          G_CALLBACK (plugin_selection_changed_cb),
                          dlg);

        g_signal_connect (gtk_builder_get_object (dlg->priv->builder, "plugins_about"),
                          "clicked",
                          G_CALLBACK (about_button_cb),
                          dlg);
}

static void
load_schemes (MarinaPreferencesDialog *dlg)
{
  MarinaPreferencesDialogPrivate *priv;
  const GList *schemes;
  const GList *list;
  GtkTreeIter iter;
  const gchar *selected;
  GtkTreePath *path = NULL;
  
  g_return_if_fail (MARINA_IS_PREFERENCES_DIALOG (dlg));
  
  priv = dlg->priv;
  
  selected = marina_schemes_get_active (marina_schemes_get_default ());
  schemes = marina_schemes_get_list (marina_schemes_get_default ());
  priv->schemes = GTK_TREE_MODEL (gtk_list_store_new (1, G_TYPE_STRING));
  
  for (list = schemes; list; list = list->next)
    {
      gtk_list_store_append (GTK_LIST_STORE (priv->schemes), &iter);
      gtk_list_store_set (GTK_LIST_STORE (priv->schemes),
                          &iter,
                          0, list->data,
                          -1);
      if (selected && !path && g_str_equal (selected, list->data))
        path = gtk_tree_model_get_path (priv->schemes, &iter);
    }
  
  gtk_tree_view_set_model (GTK_TREE_VIEW (priv->schemes_treeview),
                           priv->schemes);

  GtkTreeViewColumn *column = gtk_tree_view_column_new_with_attributes (
    NULL, gtk_cell_renderer_text_new (), "text", 0, NULL);
  gtk_tree_view_append_column (GTK_TREE_VIEW (priv->schemes_treeview), column);

  GtkTreeSelection *sel = gtk_tree_view_get_selection (GTK_TREE_VIEW (priv->schemes_treeview));
  gtk_tree_selection_select_path (sel, path);
  gtk_tree_path_free (path);
}

static void
scheme_selected_cb (GtkTreeSelection *selection)
{
  GtkTreeModel *model;
  GtkTreeIter   iter;
  const gchar  *name;

  if (gtk_tree_selection_get_selected (selection, &model, &iter))
    {
      gtk_tree_model_get (model, &iter, 0, &name, -1);
      marina_schemes_set_active (marina_schemes_get_default (), name);
    }
}

static void
font_set_cb (GtkFontButton *font_button,
             MarinaPreferencesDialog *dlg)
{
  const gchar *font = NULL;
  
  g_object_get (font_button, "font-name", &font, NULL);
  
  marina_prefs_set_font (font);
  
  marina_message_bus_send (marina_message_bus_get_default (),
                           "/ui", "set_font",
                           "use_default_font", marina_prefs_get_use_default_font (),
                           "font", font,
                           NULL);
}

static gchar*
get_default_font (GtkWidget *widget)
{
  PangoFontDescription *font_desc = widget->style->font_desc;
  return g_strdup_printf ("%s %d",
                          pango_font_description_get_family (font_desc),
                          pango_font_description_get_size (font_desc) / PANGO_SCALE);
}

static void
setup_fonts_page (MarinaPreferencesDialog *dlg)
{
  GtkTreeSelection *sel;

  g_return_if_fail (MARINA_IS_PREFERENCES_DIALOG (dlg));
  
  marina_message_bus_register (marina_message_bus_get_default (),
                               "/ui",
                               "set_font",
                               0,
                               "use_default_font", G_TYPE_BOOLEAN,
                               "font", G_TYPE_STRING,
                               NULL);
  
  load_schemes (dlg);
  
  g_signal_connect (dlg->priv->default_font_checkbutton,
                    "toggled",
                    G_CALLBACK (default_font_toggled),
                    dlg);
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dlg->priv->default_font_checkbutton),
                                marina_prefs_get_use_default_font ());
  
  gchar *d = get_default_font (GTK_WIDGET (dlg));
  gchar *t = g_strdup_printf (_("_Use the system document font (%s)"), d);
  gtk_button_set_label (GTK_BUTTON (dlg->priv->default_font_checkbutton), t);
  g_free (d);
  g_free (t);
  
  gtk_font_button_set_use_font (GTK_FONT_BUTTON (dlg->priv->font_button), TRUE);
  g_signal_connect (dlg->priv->font_button, "font-set", G_CALLBACK (font_set_cb), dlg);
  gtk_font_button_set_font_name (GTK_FONT_BUTTON (dlg->priv->font_button),
                                 marina_prefs_get_font ());

  sel = gtk_tree_view_get_selection (GTK_TREE_VIEW (dlg->priv->schemes_treeview));
  gtk_tree_selection_set_mode (sel, GTK_SELECTION_BROWSE);
  g_signal_connect (sel, "changed", G_CALLBACK (scheme_selected_cb), NULL);
}

static gboolean
on_delete (GtkWidget *dlg, GdkEvent *event, gpointer user_data)
{
        /* hide the dialog instead of allowing it to dispose */
        gtk_widget_hide (dlg);
        return TRUE;
}

static void
marina_preferences_dialog_init (MarinaPreferencesDialog *dlg)
{
        dlg->priv = PREFERENCES_DIALOG_PRIVATE (dlg);
        
        marina_debug (DEBUG_PREFS);
        
        GtkBuilder *builder;
        GError *error = NULL;
        gchar *file;
        gchar *root_objects[] = {
                "notebook",
                "add_image",
                "about_image",
                "configure_image",
                "plugin_store",
                "color_scheme_store",
                "refresh_interval_store",
                "autorefresh_adjustment",
                NULL
        };

        g_signal_connect (dlg, "delete-event", G_CALLBACK (on_delete), NULL);

        /* dialog help/close buttons */
        gtk_dialog_add_buttons (GTK_DIALOG (dlg),
                                GTK_STOCK_CLOSE,
                                GTK_RESPONSE_CLOSE,
                                //GTK_STOCK_HELP,
                                //GTK_RESPONSE_HELP,
                                NULL);
        
        /* Dialog defaults */
        gtk_window_set_title (GTK_WINDOW (dlg), _("Marina Preferences"));
        gtk_window_set_resizable (GTK_WINDOW (dlg), FALSE);
        gtk_dialog_set_has_separator (GTK_DIALOG (dlg), FALSE);
        gtk_window_set_destroy_with_parent (GTK_WINDOW (dlg), TRUE);
        gtk_window_set_transient_for (GTK_WINDOW (dlg),
        GTK_WINDOW (marina_app_get_window (marina_app_get_default ())));
        gtk_window_set_modal (GTK_WINDOW (dlg), FALSE);
        gtk_widget_set_size_request (GTK_WIDGET (dlg), 400, 480);
        
        /* HIG defaults */
        gtk_container_set_border_width (GTK_CONTAINER (dlg), 5);
        gtk_box_set_spacing (GTK_BOX (GTK_DIALOG (dlg)->vbox), 2); /* 2 * 5 + 2 = 12 */
        gtk_container_set_border_width (GTK_CONTAINER (GTK_DIALOG (dlg)->action_area), 5);
        gtk_box_set_spacing (GTK_BOX (GTK_DIALOG (dlg)->action_area), 6);
        
        g_signal_connect (G_OBJECT (dlg),
                          "response",
                          G_CALLBACK (dialog_response_handler),
                          NULL);

        /* load the dialog widgets */
        file = marina_dirs_get_ui_file ("marina-preferences-dialog.ui");
        dlg->priv->builder = builder = gtk_builder_new ();
        
        if (!gtk_builder_add_objects_from_file (builder, file, root_objects, &error))
        {
                g_error ("Error creating preferences dialog: %s", error->message);
                g_error_free (error); // never reached
                error = NULL;
                return;
        }
        
        /* store references to created widgets */
        dlg->priv->notebook = GTK_WIDGET (gtk_builder_get_object (builder, "notebook"));
        dlg->priv->plugins_treeview  = GTK_WIDGET (gtk_builder_get_object (builder, "plugins_treeview"));
        dlg->priv->plugins_about = GTK_WIDGET (gtk_builder_get_object (builder, "plugins_about"));
        dlg->priv->plugins_configure = GTK_WIDGET (gtk_builder_get_object (builder, "plugins_configure"));
        dlg->priv->default_font_checkbutton = GTK_WIDGET (gtk_builder_get_object (builder, "default_font_checkbutton"));
        dlg->priv->font_button = GTK_WIDGET (gtk_builder_get_object (builder, "font_button"));
        dlg->priv->font_label = GTK_WIDGET (gtk_builder_get_object (builder, "font_label"));
        dlg->priv->schemes_treeview = GTK_WIDGET (gtk_builder_get_object (builder, "schemes_treeview"));
        
        gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dlg)->vbox),
                            dlg->priv->notebook, TRUE, TRUE, 0);
        g_object_unref (dlg->priv->notebook);
        gtk_container_set_border_width (GTK_CONTAINER (dlg->priv->notebook), 5);
        
        setup_fonts_page (dlg);
        setup_plugins_page (dlg);
}

MarinaPreferencesDialog*
marina_preferences_dialog_new (void)
{
        return g_object_new (MARINA_TYPE_PREFERENCES_DIALOG, NULL);
}

void
_marina_preferences_dialog_reset (MarinaPreferencesDialog *dlg)
{
        g_return_if_fail (MARINA_IS_PREFERENCES_DIALOG (dlg));
        gtk_notebook_set_current_page (GTK_NOTEBOOK (dlg->priv->notebook), 0);
}
