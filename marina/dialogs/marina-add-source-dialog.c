/* marina-add-source-dialog.c
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#include "marina-add-source-dialog.h"
#include "marina-app.h"
#include "marina-dirs.h"
#include "marina-sources.h"

#define GET_WIDGET(o,n) (GTK_WIDGET (gtk_builder_get_object (o->priv->builder,n)))

struct _MarinaAddSourceDialogPrivate
{
  GtkBuilder *builder;
};

G_DEFINE_TYPE (MarinaAddSourceDialog,
               marina_add_source_dialog,
               GTK_TYPE_WINDOW);

static GList* child_names = NULL;
static GList* child_funcs = NULL;
static GList* child_create_funcs = NULL;

static void
marina_add_source_dialog_finalize (GObject *object)
{
  MarinaAddSourceDialogPrivate *priv;
  
  priv = MARINA_ADD_SOURCE_DIALOG (object)->priv;
  
  g_object_unref (priv->builder);
  
  G_OBJECT_CLASS (marina_add_source_dialog_parent_class)->finalize (object);
}

static void
marina_add_source_dialog_class_init (MarinaAddSourceDialogClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  
  object_class->finalize = marina_add_source_dialog_finalize;

  g_type_class_add_private (object_class, sizeof (MarinaAddSourceDialogPrivate));
}

static gchar*
get_ui_file (void)
{
  gchar *dir;
  gchar *file;

  dir = marina_dirs_get_marina_data_dir ();
  file = g_build_filename (dir, "ui", "marina-add-source-dialog.ui", NULL);
  g_free (dir);
  return file;
}

static void
combo_changed_cb (GtkWidget             *combo,
                  MarinaAddSourceDialog *dialog)
{
  GtkWidget *notebook;
  gint page;

  g_return_if_fail (MARINA_IS_ADD_SOURCE_DIALOG (dialog));

  page = gtk_combo_box_get_active (GTK_COMBO_BOX (combo));

  notebook = GET_WIDGET (dialog, "notebook1");
  gtk_notebook_set_current_page (GTK_NOTEBOOK (notebook), page);
}

static void
create_column (GtkWidget *combo)
{
  GtkCellRenderer *ctext;

  ctext = gtk_cell_renderer_text_new ();
  gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (combo),
                              ctext, TRUE);
  gtk_cell_layout_add_attribute (GTK_CELL_LAYOUT (combo),
                                 ctext, "text", 1);
}

static void
save_cb (GtkWidget             *button,
         MarinaAddSourceDialog *dialog)
{
  MarinaAddSourceDialogCreateFunc *create_func;

  GtkWidget    *notebook,
               *child;
  gint          page;
  MarinaSource *source;

  notebook = GET_WIDGET (dialog, "notebook1");
  page = gtk_notebook_get_current_page (GTK_NOTEBOOK (notebook));

  create_func = g_list_nth_data (child_create_funcs, page);
  g_assert (create_func);

  child = gtk_notebook_get_nth_page (GTK_NOTEBOOK (notebook), page);
  source = create_func (child, dialog);

  if (!source)
    goto cleanup;

  marina_sources_add (marina_sources_get_default  (),
                      source,
                      NULL); // use method to get this later?

cleanup:
  gtk_widget_destroy (GTK_WIDGET (dialog));
}

static void
marina_add_source_dialog_init (MarinaAddSourceDialog *dialog)
{
  GtkWidget    *child,
               *title,
               *notebook,
               *combo,
               *cancel,
               *save;
  GtkTreeModel *model;
  gchar        *ui_file;
  GList        *iter,
               *niter;
  GtkTreeIter   tree_iter;
  
  dialog->priv = G_TYPE_INSTANCE_GET_PRIVATE (dialog,
                                              MARINA_TYPE_ADD_SOURCE_DIALOG,
                                              MarinaAddSourceDialogPrivate);

  /* set window defaults */
  gtk_window_set_transient_for (&dialog->window,
                                GTK_WINDOW (marina_app_get_window (
                                  marina_app_get_default ())));
  gtk_window_set_title (&dialog->window, "");
  gtk_window_set_resizable (&dialog->window, FALSE);
  gtk_window_set_modal (&dialog->window, TRUE);
  
  /* load widgets from GtkBuilder */
  dialog->priv->builder = gtk_builder_new ();
  ui_file = get_ui_file ();
  gtk_builder_add_from_file (dialog->priv->builder, ui_file, NULL);
  g_free (ui_file);

  /* move the gtk builder child widget into our window */
  child = GTK_WIDGET (gtk_builder_get_object (dialog->priv->builder, "vbox1"));
  g_assert (GTK_IS_WIDGET (child));
  gtk_widget_reparent (child, GTK_WIDGET (dialog));
  
  /* make the top title background white */
  title = GTK_WIDGET (gtk_builder_get_object (dialog->priv->builder,
                                              "eventbox1"));
  g_assert (GTK_IS_WIDGET (title));
  gtk_widget_modify_bg (title, GTK_STATE_NORMAL, &title->style->white);

  /* create the model for the type combo */
  combo = GTK_WIDGET (gtk_builder_get_object (dialog->priv->builder,
                                              "combobox1"));
  model = gtk_combo_box_get_model (GTK_COMBO_BOX (combo));
  create_column (combo);

  notebook = GTK_WIDGET (gtk_builder_get_object (dialog->priv->builder,
                                                 "notebook1"));

  if (!g_list_length (child_funcs))
    gtk_widget_set_sensitive (combo, FALSE);

  /* fill combobox with potential source types and config
   * widgets */
  gint i = 0;
  for (iter = child_funcs; iter; iter = iter->next)
    {
      MarinaAddSourceDialogFunc *func = iter->data;
      g_assert (func);
      child = func (dialog);
      g_assert (child != NULL);
      gtk_notebook_append_page (GTK_NOTEBOOK (notebook),
                                child, NULL);
      gtk_widget_show (child);
    }

  i = 0;
  for (niter = child_names; niter; niter = niter->next)
   {
      gtk_list_store_append (GTK_LIST_STORE (model), &tree_iter);
      gtk_list_store_set (GTK_LIST_STORE (model),
                          &tree_iter,
                          0, GINT_TO_POINTER (i++),
                          1, niter->data,
                          -1);
    }

  /* set to the first item */
  gtk_combo_box_set_active (GTK_COMBO_BOX (combo), 0);
  gtk_notebook_set_current_page (GTK_NOTEBOOK (notebook), 0);

  g_signal_connect (G_OBJECT (combo),
                    "changed",
                    G_CALLBACK (combo_changed_cb),
                    dialog);

  cancel = GET_WIDGET (dialog, "cancel");
  g_signal_connect_swapped (G_OBJECT (cancel),
                            "clicked",
                            G_CALLBACK (gtk_widget_destroy),
                            dialog);

  save = GET_WIDGET (dialog, "save");
  g_signal_connect (G_OBJECT (save),
                    "clicked",
                    G_CALLBACK (save_cb),
                    dialog);
}

GtkWidget*
marina_add_source_dialog_new (void)
{
  return g_object_new (MARINA_TYPE_ADD_SOURCE_DIALOG, NULL);
}

guint
marina_add_source_dialog_register (const gchar *title,
                                   MarinaAddSourceDialogFunc func,
                                   MarinaAddSourceDialogCreateFunc create_func)
{
  gchar *name;

  g_return_val_if_fail (title != NULL, 0);
  g_return_val_if_fail (func != NULL, 0);

  name = g_strdup (title);

  child_funcs = g_list_append (child_funcs, func);
  child_names = g_list_append (child_names, name);
  child_create_funcs = g_list_append (child_create_funcs, create_func);

  return g_list_index (child_names, name);
}

