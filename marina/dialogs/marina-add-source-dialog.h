/* marina-add-source-dialog.h
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifndef __MARINA_ADD_SOURCE_DIALOG_H__
#define __MARINA_ADD_SOURCE_DIALOG_H__

#include <glib-object.h>
#include <gtk/gtk.h>
#include <marina/marina-source.h>

G_BEGIN_DECLS

#define MARINA_TYPE_ADD_SOURCE_DIALOG              (marina_add_source_dialog_get_type ())
#define MARINA_ADD_SOURCE_DIALOG(obj)              (G_TYPE_CHECK_INSTANCE_CAST ((obj), MARINA_TYPE_ADD_SOURCE_DIALOG, MarinaAddSourceDialog))
#define MARINA_ADD_SOURCE_DIALOG_CONST(obj)        (G_TYPE_CHECK_INSTANCE_CAST ((obj), MARINA_TYPE_ADD_SOURCE_DIALOG, MarinaAddSourceDialog const))
#define MARINA_ADD_SOURCE_DIALOG_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), MARINA_TYPE_ADD_SOURCE_DIALOG, MarinaAddSourceDialogClass))
#define MARINA_IS_ADD_SOURCE_DIALOG(obj)           (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MARINA_TYPE_ADD_SOURCE_DIALOG))
#define MARINA_IS_ADD_SOURCE_DIALOG_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE ((klass), MARINA_TYPE_ADD_SOURCE_DIALOG))
#define MARINA_ADD_SOURCE_DIALOG_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), MARINA_TYPE_ADD_SOURCE_DIALOG, MarinaAddSourceDialogClass))

typedef struct _MarinaAddSourceDialog         MarinaAddSourceDialog;
typedef struct _MarinaAddSourceDialogClass    MarinaAddSourceDialogClass;
typedef struct _MarinaAddSourceDialogPrivate  MarinaAddSourceDialogPrivate;

typedef GtkWidget* MarinaAddSourceDialogFunc (MarinaAddSourceDialog *dialog);
typedef MarinaSource* MarinaAddSourceDialogCreateFunc (GtkWidget *widget, MarinaAddSourceDialog *dialog);

struct _MarinaAddSourceDialog
{
  GtkWindow window;
  
  MarinaAddSourceDialogPrivate *priv;
};

struct _MarinaAddSourceDialogClass
{
  GtkWindowClass window_class;
};

GType      marina_add_source_dialog_get_type       (void) G_GNUC_CONST;
GtkWidget* marina_add_source_dialog_new            (void);

void       marina_add_source_dialog_set_child_type (MarinaAddSourceDialog *dialog,
                                                    GType                  child_type);
void       marina_add_source_dialog_set_validated  (MarinaAddSourceDialog *dialog,
                                                    gboolean               validated);

/* global widget registration */

guint      marina_add_source_dialog_register       (const gchar *title,
                                                    MarinaAddSourceDialogFunc func,
                                                    MarinaAddSourceDialogCreateFunc create_func);
void       marina_add_source_dialog_unregister     (guint        add_id);



G_END_DECLS

#endif /* __MARINA_ADD_SOURCE_DIALOG_H__ */
