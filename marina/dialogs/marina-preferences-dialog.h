/*
 * marina-preferences-dialog.h
 * This file is part of marina.
 *
 * Copyright (C) 2009 - Christian Hergert
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifndef __MARINA_PREFERENCES_DIALOG_H__
#define __MARINA_PREFERENCES_DIALOG_H__

#include <glib-object.h>
#include <gtk/gtk.h>

G_BEGIN_DECLS

#define MARINA_TYPE_PREFERENCES_DIALOG			(marina_preferences_dialog_get_type ())
#define MARINA_PREFERENCES_DIALOG(obj)			(G_TYPE_CHECK_INSTANCE_CAST ((obj), MARINA_TYPE_PREFERENCES_DIALOG, MarinaPreferencesDialog))
#define MARINA_PREFERENCES_DIALOG_CONST(obj)		(G_TYPE_CHECK_INSTANCE_CAST ((obj), MARINA_TYPE_PREFERENCES_DIALOG, MarinaPreferencesDialog const))
#define MARINA_PREFERENCES_DIALOG_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST ((klass), MARINA_TYPE_PREFERENCES_DIALOG, MarinaPreferencesDialogClass))
#define MARINA_IS_PREFERENCES_DIALOG(obj)		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), MARINA_TYPE_PREFERENCES_DIALOG))
#define MARINA_IS_PREFERENCES_DIALOG_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((klass), MARINA_TYPE_PREFERENCES_DIALOG))
#define MARINA_PREFERENCES_DIALOG_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS ((obj), MARINA_TYPE_PREFERENCES_DIALOG, MarinaPreferencesDialogClass))

typedef struct _MarinaPreferencesDialog		MarinaPreferencesDialog;
typedef struct _MarinaPreferencesDialogClass	MarinaPreferencesDialogClass;
typedef struct _MarinaPreferencesDialogPrivate	MarinaPreferencesDialogPrivate;

struct _MarinaPreferencesDialog
{
	GtkDialog parent;
	
	MarinaPreferencesDialogPrivate *priv;
};

struct _MarinaPreferencesDialogClass
{
	GtkDialogClass parent_class;
};

GType                      marina_preferences_dialog_get_type   (void) G_GNUC_CONST;
MarinaPreferencesDialog*   marina_preferences_dialog_new        (void);

/* reset to main page */
void                       _marina_preferences_dialog_reset     (MarinaPreferencesDialog *self);

G_END_DECLS

#endif /* __MARINA_PREFERENCES_DIALOG_H__ */
