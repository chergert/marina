/* xml-reader.c
 *
 * Copyright (C) 2009  Christian Hergert  <chris@dronelabs.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * Author:
 *   Christian Hergert  <chris@dronelabs.com>
 */

/* After using Emmanuele Bassi's XmlWriter, I realized I wanted a
 * few more features and to be based on the xmlTextReader API. This
 * is the result of my quick hack to do so.
 *
 * You might wonder why i simply wrapped xmlTextReader from libxml2
 * as it is already a .NET like API. It's simple, I wanted a GObject
 * that was easily bindable for my plugins to use as well as my
 * core library in C.
 */

#include <string.h>
#include <libxml/xmlreader.h>

#include "xml-reader.h"

#define XML_TO_CHAR(s)  ((char *) (s))
#define CHAR_TO_XML(s)  ((unsigned char *) (s))

struct _XmlReaderPrivate
{
  xmlTextReaderPtr xml;
  gchar *cur_name;
};

G_DEFINE_TYPE (XmlReader, xml_reader, G_TYPE_OBJECT);

#define XML_NODE_TYPE_ELEMENT        1
#define XML_NODE_TYPE_END_ELEMENT   15
#define XML_NODE_TYPE_ATTRIBUTE      2

static void
xml_reader_finalize (GObject *object)
{
  G_OBJECT_CLASS (xml_reader_parent_class)->finalize (object);
}

static void
xml_reader_class_init (XmlReaderClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  
  object_class->finalize = xml_reader_finalize;

  g_type_class_add_private (object_class, sizeof (XmlReaderPrivate));
}

static void
xml_reader_init (XmlReader *reader)
{
  reader->priv = G_TYPE_INSTANCE_GET_PRIVATE (reader,
                                              XML_TYPE_READER,
                                              XmlReaderPrivate);
}

XmlReader*
xml_reader_new (void)
{
  return g_object_new (XML_TYPE_READER, NULL);
}

gboolean
xml_reader_load_from_file (XmlReader   *reader,
                           const gchar *filename)
{
  XmlReaderPrivate *priv;
  
  g_return_val_if_fail (XML_IS_READER (reader), FALSE);
  
  priv = reader->priv;
  
  if (priv->xml)
    xmlFreeTextReader (priv->xml);
  
  priv->xml = xmlNewTextReaderFilename (filename);
  
  return (priv->xml != NULL);
}

gboolean
xml_reader_load_from_data (XmlReader   *reader,
                           const gchar *data,
                           gsize        length,
                           const gchar *uri,
                           const gchar *encoding)
{
  XmlReaderPrivate *priv;
  
  g_return_val_if_fail (XML_IS_READER (reader), FALSE);
  
  priv = reader->priv;
  
  if (priv->xml)
    xmlFreeTextReader (priv->xml);
  
  if (length == -1)
    length = strlen (data);
  
  priv->xml = xmlReaderForMemory (data, length, uri, encoding, 0);
  
  return (priv->xml != NULL);
}

G_CONST_RETURN gchar*
xml_reader_get_value (XmlReader *reader)
{
  XmlReaderPrivate *priv;
  
  g_return_val_if_fail (XML_IS_READER (reader), NULL);
  
  priv = reader->priv;
  
  g_return_val_if_fail (priv->xml != NULL, NULL);
  
  return XML_TO_CHAR (xmlTextReaderConstValue (priv->xml));
}

G_CONST_RETURN gchar*
xml_reader_get_name (XmlReader *reader)
{
  XmlReaderPrivate *priv;
  
  g_return_val_if_fail (XML_IS_READER (reader), NULL);
  
  priv = reader->priv;
  
  g_return_val_if_fail (priv->xml != NULL, NULL);
  
  return XML_TO_CHAR (xmlTextReaderConstName (priv->xml));
}

gchar*
xml_reader_read_string (XmlReader *reader)
{
  XmlReaderPrivate *priv;
  
  g_return_val_if_fail (XML_IS_READER (reader), NULL);
  
  priv = reader->priv;
  
  g_return_val_if_fail (priv->xml != NULL, NULL);
  
  return XML_TO_CHAR (xmlTextReaderReadString (priv->xml));
}

gchar*
xml_reader_get_attribute (XmlReader   *reader,
                          const gchar *name)
{
  XmlReaderPrivate *priv;
  
  g_return_val_if_fail (XML_IS_READER (reader), NULL);
  
  priv = reader->priv;
  
  g_return_val_if_fail (priv->xml != NULL, NULL);
  
  return XML_TO_CHAR (xmlTextReaderGetAttribute (priv->xml, CHAR_TO_XML (name)));
}

static gboolean
read_to_type_and_name (XmlReader   *reader,
                       gint         type,
                       const gchar *name)
{
  XmlReaderPrivate *priv;
  gboolean success = FALSE;
  
  g_return_val_if_fail (XML_IS_READER (reader), FALSE);
  
  priv = reader->priv;
  
  g_return_val_if_fail (priv->xml != NULL, FALSE);
  
  
  
  while (xmlTextReaderRead (priv->xml) == 1)
    {
      if (xmlTextReaderNodeType (priv->xml) == type)
        {
          if (g_str_equal (XML_TO_CHAR (xmlTextReaderConstName (priv->xml)), name))
            {
              success = TRUE;
              break;
            }
        }
    }
  
  return success;
}

gboolean
xml_reader_read_start_element (XmlReader   *reader,
                               const gchar *name)
{
  XmlReaderPrivate *priv;
  gboolean success;
  
  g_return_val_if_fail (XML_IS_READER (reader), FALSE);
  
  priv = reader->priv;
  
  success = read_to_type_and_name (reader, XML_NODE_TYPE_ELEMENT, name);
  
  if (success)
    {
      g_free (priv->cur_name);
      priv->cur_name = g_strdup (name);
    }
  
  return success;
}

gboolean
xml_reader_read_end_element (XmlReader *reader)
{
  XmlReaderPrivate *priv;
  gboolean success = FALSE;
  
  g_return_val_if_fail (XML_IS_READER (reader), FALSE);
  
  priv = reader->priv;
  
  if (priv->cur_name)
    success = read_to_type_and_name (reader, XML_NODE_TYPE_END_ELEMENT, priv->cur_name);
  
  return success;
}

gchar*
xml_reader_read_inner_xml (XmlReader *reader)
{
  XmlReaderPrivate *priv;
  
  g_return_val_if_fail (XML_IS_READER (reader), FALSE);
  
  priv = reader->priv;
  
  return XML_TO_CHAR (xmlTextReaderReadInnerXml (priv->xml));
}

gchar*
xml_reader_read_outer_xml (XmlReader *reader)
{
  XmlReaderPrivate *priv;
  
  g_return_val_if_fail (XML_IS_READER (reader), FALSE);
  
  priv = reader->priv;
  
  return XML_TO_CHAR (xmlTextReaderReadOuterXml (priv->xml));
}

gboolean
xml_reader_read_to_next (XmlReader *reader)
{
  XmlReaderPrivate *priv;
  
  g_return_val_if_fail (XML_IS_READER (reader), FALSE);
  
  priv = reader->priv;
  
  return (xmlTextReaderNext (priv->xml) == 1);
}

gboolean
xml_reader_read (XmlReader *reader)
{
  XmlReaderPrivate *priv;
  
  g_return_val_if_fail (XML_IS_READER (reader), FALSE);
  
  priv = reader->priv;
  
  return (xmlTextReaderRead (priv->xml) == 1);
}

gboolean
xml_reader_read_to_next_sibling (XmlReader *reader)
{
  XmlReaderPrivate *priv;
  
  g_return_val_if_fail (XML_IS_READER (reader), FALSE);
  
  priv = reader->priv;
  
  xmlTextReaderMoveToElement (priv->xml);
  
  return (xmlTextReaderNextSibling (priv->xml) == 1);
}

gboolean
xml_reader_move_to_element (XmlReader *reader)
{
  XmlReaderPrivate *priv;
  
  g_return_val_if_fail (XML_IS_READER (reader), FALSE);
  
  priv = reader->priv;
  
  return (xmlTextReaderMoveToElement (priv->xml) == 1);
}

gboolean
xml_reader_move_to_attribute (XmlReader   *reader,
                              const gchar *name)
{
  XmlReaderPrivate *priv;
  
  g_return_val_if_fail (XML_IS_READER (reader), FALSE);
  
  priv = reader->priv;
  
  return (xmlTextReaderMoveToAttribute (priv->xml, CHAR_TO_XML (name)) == 1);
}

gboolean
xml_reader_move_to_first_attribute (XmlReader *reader)
{
  XmlReaderPrivate *priv;
  
  g_return_val_if_fail (XML_IS_READER (reader), FALSE);
  
  priv = reader->priv;
  
  return (xmlTextReaderMoveToFirstAttribute (priv->xml) == 1);
}

gboolean
xml_reader_move_to_next_attribute (XmlReader *reader)
{
  XmlReaderPrivate *priv;
  
  g_return_val_if_fail (XML_IS_READER (reader), FALSE);
  
  priv = reader->priv;
  
  return (xmlTextReaderMoveToNextAttribute (priv->xml) == 1);
}

gint
xml_reader_count_attributes (XmlReader *reader)
{
  XmlReaderPrivate *priv;
  
  g_return_val_if_fail (XML_IS_READER (reader), FALSE);
  
  priv = reader->priv;
  
  return xmlTextReaderAttributeCount (priv->xml);
}
