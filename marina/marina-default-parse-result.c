/* marina-default-parse-result.c
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#include "marina-debug.h"
#include "marina-item.h"
#include "marina-default-parse-result.h"

#define MAP_DOC_ATTR(name,prop)                                \
  G_STMT_START {                                               \
    if (g_str_equal(attr,name))                                \
      {                                                        \
        g_value_init (value, G_TYPE_STRING);                   \
        g_object_get_property (G_OBJECT (priv->document),      \
                               prop,                           \
                               value);                         \
        return;                                                \
      }                                                        \
    }                                                          \
  G_STMT_END

#define MAP_ITEM_ATTR(name,prop)                               \
  G_STMT_START {                                               \
    if (g_str_equal(attr,name))                                \
      {                                                        \
        g_value_init (value, G_TYPE_STRING);                   \
        g_object_get_property (G_OBJECT (item),                \
                               prop,                           \
                               value);                         \
        return;                                                \
      }                                                        \
    }                                                          \
  G_STMT_END

struct _MarinaDefaultParseResultPrivate
{
  RssDocument *document;
  GList       *items;
  GList       *current;
};

static void marina_parse_result_init (MarinaParseResultIface *iface);

G_DEFINE_TYPE_EXTENDED (MarinaDefaultParseResult,
                        marina_default_parse_result,
                        G_TYPE_OBJECT,
                        0,
                        G_IMPLEMENT_INTERFACE (MARINA_TYPE_PARSE_RESULT,
                                               marina_parse_result_init));

static void
marina_default_parse_result_finalize (GObject *object)
{
  G_OBJECT_CLASS (marina_default_parse_result_parent_class)->finalize (object);
}

static void
marina_default_parse_result_class_init (MarinaDefaultParseResultClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  
  object_class->finalize = marina_default_parse_result_finalize;
  
  g_type_class_add_private (object_class, sizeof (MarinaDefaultParseResultPrivate));
}

static void
marina_default_parse_result_init (MarinaDefaultParseResult *parse_result)
{
  parse_result->priv = G_TYPE_INSTANCE_GET_PRIVATE (parse_result,
                                                    MARINA_TYPE_DEFAULT_PARSE_RESULT,
                                                    MarinaDefaultParseResultPrivate);
}

MarinaParseResult*
marina_default_parse_result_new (RssDocument *document)
{
  MarinaDefaultParseResult *parse_result;
  
  marina_debug (DEBUG_PARSERS);
  
  g_return_val_if_fail (RSS_IS_DOCUMENT (document), NULL);
  
  parse_result = g_object_new (MARINA_TYPE_DEFAULT_PARSE_RESULT, NULL);
  
  parse_result->priv->document = document;
  parse_result->priv->items = rss_document_get_items (document);
  
  return MARINA_PARSE_RESULT (parse_result);
}

static void
get_attr (MarinaParseResult *parse_result,
          const gchar       *attr,
          GValue            *value)
{
  MarinaDefaultParseResultPrivate *priv;
  
  g_return_if_fail (MARINA_IS_DEFAULT_PARSE_RESULT (parse_result));
  
  priv = MARINA_DEFAULT_PARSE_RESULT (parse_result)->priv;
  
  if (!priv->current)
    {
      /* handle attributes for the document */
      
      MAP_DOC_ATTR ("id", "guid");
      MAP_DOC_ATTR ("title", "title");
      MAP_DOC_ATTR ("description", "description");
      MAP_DOC_ATTR ("link", "link");
      MAP_DOC_ATTR ("copyright", "copyright");
      MAP_DOC_ATTR ("image-uri", "image-url");
    }
  else
    {
      /* handle attributes for the current item */
      RssItem *item = priv->current->data;
      
      MAP_ITEM_ATTR ("id", "guid");
      MAP_ITEM_ATTR ("title", "title");
      MAP_ITEM_ATTR ("link", "link");
      MAP_ITEM_ATTR ("description", "description");
      MAP_ITEM_ATTR ("copyright", "copyright");
      MAP_ITEM_ATTR ("author", "author");
      MAP_ITEM_ATTR ("author-uri", "author-uri");
      MAP_ITEM_ATTR ("author-email", "author-email");
      MAP_ITEM_ATTR ("pub-date", "pub-date");
    }
}

static GList*
get_attrs (MarinaParseResult *parse_result)
{
  MarinaDefaultParseResultPrivate *priv;
  GList *attrs = NULL;
  
  g_return_val_if_fail (MARINA_IS_DEFAULT_PARSE_RESULT (parse_result), NULL);
  
  priv = MARINA_DEFAULT_PARSE_RESULT (parse_result)->priv;
  
  attrs = g_list_prepend (attrs, "id");
  attrs = g_list_prepend (attrs, "title");
  attrs = g_list_prepend (attrs, "link");
  attrs = g_list_prepend (attrs, "description");
  attrs = g_list_prepend (attrs, "copyright");
  
  if (!priv->current)
    {
      attrs = g_list_prepend (attrs, "image_uri");
    }
  else
    {
      attrs = g_list_prepend (attrs, "author");
      attrs = g_list_prepend (attrs, "author-uri");
      attrs = g_list_prepend (attrs, "author-email");
      attrs = g_list_prepend (attrs, "pub-date");
    }
  
  return attrs;
}

static const gchar*
get_id (MarinaParseResult *parse_result)
{
  MarinaDefaultParseResultPrivate *priv;
  
  g_return_val_if_fail (MARINA_IS_DEFAULT_PARSE_RESULT (parse_result), NULL);
  
  priv = MARINA_DEFAULT_PARSE_RESULT (parse_result)->priv;
  
  if (!priv->current)
    {
      return rss_document_get_guid (priv->document);
    }
  else
    {
      return rss_item_get_guid (priv->current->data);
    }
}

static gboolean
move_first (MarinaParseResult *parse_result)
{
  MarinaDefaultParseResultPrivate *priv;
  
  g_return_val_if_fail (MARINA_IS_DEFAULT_PARSE_RESULT (parse_result), FALSE);
  
  priv = MARINA_DEFAULT_PARSE_RESULT (parse_result)->priv;
  
  priv->current = priv->items;
  
  return priv->current != NULL;
}

static gboolean
move_next (MarinaParseResult *parse_result)
{
  MarinaDefaultParseResultPrivate *priv;
  
  g_return_val_if_fail (MARINA_IS_DEFAULT_PARSE_RESULT (parse_result), FALSE);
  
  priv = MARINA_DEFAULT_PARSE_RESULT (parse_result)->priv;
  
  if (!priv->current || !priv->current->next)
    {
      return FALSE;
    }
  else
    {
      priv->current = priv->current->next;
      return TRUE;
    }
}

static void
marina_parse_result_init (MarinaParseResultIface *iface)
{
  iface->get_attr = get_attr;
  iface->get_attrs = get_attrs;
  iface->get_id = get_id;
  iface->move_first = move_first;
  iface->move_next = move_next;
}
