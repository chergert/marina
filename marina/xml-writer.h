/* xml-writer.h: Cursor based XML writer API
 *
 * Copyright (C) 2009  Christian Hergert  <chris@dronelabs.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * Author:
 *   Christian Hergert  <chris@dronelabs.com>
 */

#ifndef __XML_WRITER_H__
#define __XML_WRITER_H__

#include <glib-object.h>

#include "xml-reader.h"

G_BEGIN_DECLS

#define XML_TYPE_WRITER             (xml_writer_get_type ())
#define XML_WRITER(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), XML_TYPE_WRITER, XmlWriter))
#define XML_WRITER_CONST(obj)       (G_TYPE_CHECK_INSTANCE_CAST ((obj), XML_TYPE_WRITER, XmlWriter const))
#define XML_WRITER_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass),  XML_TYPE_WRITER, XmlWriterClass))
#define XML_IS_WRITER(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), XML_TYPE_WRITER))
#define XML_IS_WRITER_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass),  XML_TYPE_WRITER))
#define XML_WRITER_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj),  XML_TYPE_WRITER, XmlWriterClass))

/**
 * XML_WRITER_ERROR:
 *
 * #XmlWriter #GError domain.
 */
#define XML_WRITER_ERROR           (xml_writer_error_quark ())

/**
 * XmlWriterError:
 * @XML_READER_ERROR_INVALID: Invalid writer state for command
 *
 * #XmlWriter error enumeration.
 */
typedef enum {
  XML_WRITER_ERROR_INVALID,
} XmlWriterError;

GQuark xml_writer_error_quark (void);

typedef struct _XmlWriter           XmlWriter;
typedef struct _XmlWriterClass      XmlWriterClass;
typedef struct _XmlWriterPrivate    XmlWriterPrivate;

struct _XmlWriter
{
  GObject parent;

  XmlWriterPrivate *priv;
};

struct _XmlWriterClass
{
  GObjectClass parent_class;
};

GType        xml_writer_get_type                    (void) G_GNUC_CONST;

/* Create methods */
XmlWriter*   xml_writer_new                         (void);
XmlWriter*   xml_writer_new_for_file                (const gchar  *filename);
void         xml_writer_clear                       (XmlWriter    *writer);
gboolean     xml_writer_set_file                    (XmlWriter    *writer,
                                                     const gchar  *filename,
                                                     GError      **error);

/* Streaming methods */
void         xml_writer_flush                       (XmlWriter   *writer);

/* Write methods */
void         xml_writer_write_start_document        (XmlWriter   *writer,
                                                     const gchar *version,
                                                     const gchar *encoding,
                                                     const gchar *standalone);
gsize        xml_writer_write_end_document          (XmlWriter   *writer);

void         xml_writer_write_start_element         (XmlWriter   *writer,
                                                     const gchar *name);
void         xml_writer_write_start_element_with_ns (XmlWriter   *writer, 
                                                     const gchar *name,
                                                     const gchar *prefix,
                                                     const gchar *ns);
gsize        xml_writer_write_end_element           (XmlWriter   *writer);
void         xml_writer_write_element_string        (XmlWriter   *writer,
                                                     const gchar *name,
                                                     const gchar *value);

void         xml_writer_write_start_attribute       (XmlWriter   *writer,
                                                     const gchar *name);
gsize        xml_writer_write_end_attribute         (XmlWriter   *writer);
void         xml_writer_write_attribute_string      (XmlWriter   *writer,
                                                     const gchar *name,
                                                     const gchar *value);

void         xml_writer_write_start_comment         (XmlWriter   *writer);
gsize        xml_writer_write_end_comment           (XmlWriter   *writer);
void         xml_writer_write_comment_string        (XmlWriter   *writer,
                                                     const gchar *value);

void         xml_writer_write_string                (XmlWriter   *writer,
                                                     const gchar *value);
void         xml_writer_write_boolean               (XmlWriter   *writer,
                                                     gboolean     value);
void         xml_writer_write_int                   (XmlWriter   *writer,
                                                     gint         value);
void         xml_writer_write_double                (XmlWriter   *writer,
                                                     gdouble      value);
void         xml_writer_write_float                 (XmlWriter   *writer,
                                                     gfloat       value);
void         xml_writer_write_long                  (XmlWriter   *writer,
                                                     glong        value);
void         xml_writer_write_ulong                 (XmlWriter   *writer,
                                                     gulong       value);
void         xml_writer_write_int64                 (XmlWriter   *writer,
                                                     gint64       value);
void         xml_writer_write_uint64                (XmlWriter   *writer,
                                                     guint64      value);
void         xml_writer_write_timeval               (XmlWriter   *writer,
                                                     GTimeVal    *timeval);

void         xml_writer_write_attributes            (XmlWriter   *writer,
                                                     XmlReader   *reader);

G_END_DECLS

#endif /* __XML_WRITER_H__ */
