/* marina-web-view.c
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#include <stdlib.h>
#include <webkit/webkit.h>
#include <json-glib/json-glib.h>
#include <glib/gprintf.h>
#include <glib/gi18n.h>

#include "marina-dirs.h"
#include "marina-message-bus.h"
#include "marina-prefs.h"
#include "marina-schemes.h"
#include "marina-web-view.h"
#include "marina-window.h"
#include "marina-window-private.h"

struct _MarinaWebViewPrivate
{
  GtkWidget     *web_view;
  MarinaSource  *source;
  MarinaItem    *item;
  
  GtkAdjustment *vadj;
  
  gboolean       loaded;
};

G_DEFINE_TYPE (MarinaWebView, marina_web_view, GTK_TYPE_VBOX);

static void
marina_web_view_finalize (GObject *object)
{
  G_OBJECT_CLASS (marina_web_view_parent_class)->finalize (object);
}

static void
marina_web_view_class_init (MarinaWebViewClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  
  object_class->finalize = marina_web_view_finalize;

  g_type_class_add_private (object_class, sizeof (MarinaWebViewPrivate));
}

/* Hi, this method sucks and is very much untested. However, it did
 * actually render one utf-8 persian post i looked at, so its still
 * better than just using g_strescape.
 *
 * If you have any experience with non us-english languages, you should
 * look this over and make it more robust to corner cases.
 *
 * Cheers, -- Christian
 */
static gchar*
json_strescape (const gchar *source)
{
  gchar *dest, *q;
  gunichar *ucs4;
  gint i, len;

  if (!g_utf8_validate (source, -1, NULL))
    return g_strescape (source, NULL);

  len = g_utf8_strlen (source, -1);
  dest = q = g_malloc (len * 6 + 1);

  ucs4 = g_utf8_to_ucs4_fast (source, -1, NULL);

  for (i = 0; i < len; i++)
    {
      switch (ucs4 [i]) {
      case '\\':
        *q++ = '\\';
        *q++ = '\\';
        break;
      case '"':
        *q++ = '\\';
        *q++ = '"';
        break;
      case '\b':
        *q++ = '\\';
        *q++ = 'b';
        break;
      case '\f':
        *q++ = '\\';
        *q++ = 'f';
        break;
      case '\n':
        *q++ = '\\';
        *q++ = 'n';
        break;
      case '\r':
        *q++ = '\\';
        *q++ = 'r';
        break;
      case '\t':
        *q++ = '\\';
        *q++ = 't';
        break;
      default:
        if ((ucs4 [i] >= (gunichar)0x20) || (ucs4 [i] <= (gunichar)0x1F))
          {
            g_sprintf (q, "\\u%04x", ucs4 [i]);
            q += 6;
          }
        else
          *q++ = ((gchar)ucs4 [i]);
      }
    }

  *q++ = 0;

  g_free (ucs4);

  return dest;
}

static JsonNode*
to_json_string (const gchar *str)
{
  gchar *escaped = json_strescape (str);
  JsonNode *node = json_node_new (JSON_NODE_VALUE);
  
  json_node_set_string (node, escaped);

  g_free (escaped);
  
  return node;
}

static gchar*
to_item_json (MarinaItem *item)
{
  /* NOTE: When WebKit gets GObject DOM support, we should be injecting
   *    the item directly into the dom. This will get us by for now.
   */
  
  if (!item)
    return g_strdup ("{}");
  
  gchar         *json;
  MarinaDate    *item_date = marina_item_get_published_at (item);
  JsonGenerator *generator = json_generator_new ();
  JsonNode      *root      = json_node_new (JSON_NODE_OBJECT);
  JsonObject    *object    = json_object_new ();
  JsonNode      *author;
  JsonNode      *content   = to_json_string (marina_item_get_content (item));
  JsonNode      *date      = to_json_string (marina_date_to_string (item_date));
  JsonNode      *link      = to_json_string (marina_item_get_link (item));
  JsonNode      *title;

  if (!marina_item_get_author (item) ||
      g_str_equal (marina_item_get_author (item), ""))
    author = to_json_string (_("Unknown"));
  else
    author = to_json_string (marina_item_get_author (item));
  
  if (!marina_item_get_title (item) ||
      g_str_equal (marina_item_get_title (item), ""))
    title = to_json_string (_("Untitled"));
  else
    title = to_json_string (marina_item_get_title (item));
  
  json_object_add_member (object, "author", author);
  json_object_add_member (object, "content", content);
  json_object_add_member (object, "date", date);
  json_object_add_member (object, "link", link);
  json_object_add_member (object, "title", title);
  
  json_node_take_object (root, object);
  json_generator_set_root (generator, root);
  json = json_generator_to_data (generator, NULL);
  
  json_node_free (root);
  g_object_unref (generator);
  
  return json;
}

static gchar*
to_source_json (MarinaSource *source)
{
  gchar *escaped;
  gchar *result;
  
  if (!source)
    return g_strconcat ("{title:\"\"}", NULL);
  
  escaped = g_strescape (marina_source_get_title (source), NULL);
  result = g_strconcat ("{title:\"", escaped, "\"}", NULL);
  
  g_free (escaped);
  
  return result;
}

static void
hovering_over_link (WebKitWebView *web_view,
                    const gchar   *title,
                    const gchar   *uri,
                    gpointer       user_data)
{
  MarinaWindow *window;
  static guint cid = 0;
  
  g_return_if_fail (WEBKIT_IS_WEB_VIEW (web_view));
  
  window = MARINA_WINDOW (gtk_widget_get_toplevel (GTK_WIDGET (web_view)));

  if (!cid)
    cid = gtk_statusbar_get_context_id (GTK_STATUSBAR (window->priv->statusbar), "html-link");
  
  gtk_statusbar_pop (GTK_STATUSBAR (window->priv->statusbar), cid);
  
  if (uri)
    gtk_statusbar_push (GTK_STATUSBAR (window->priv->statusbar), cid, uri);
}

static void
navigation_requested (WebKitWebView        *web_view,
                      WebKitWebFrame       *frame,
                      WebKitNetworkRequest *network_request,
                      gpointer              user_data)
{
  MarinaWebViewPrivate *priv;
  const gchar          *uri;
  gchar                *base_uri;
  gboolean              hijack;

  g_return_if_fail (MARINA_IS_WEB_VIEW (user_data));

  priv = MARINA_WEB_VIEW (user_data)->priv;

  if (frame != webkit_web_view_get_main_frame (web_view))
    {
      /* the page tried to load in another frame, kill it */
      webkit_web_frame_stop_loading (frame);
      return;
    }

  uri = webkit_network_request_get_uri (network_request);
  base_uri = g_filename_to_uri (marina_dirs_get_marina_data_dir (), NULL, NULL);
  hijack = !g_str_has_prefix (uri, base_uri);

  g_free (base_uri);
  
  /* if the request is to one of our schemes, allow it */
  if (!hijack)
    return;
  
  gtk_show_uri (NULL, uri, GDK_CURRENT_TIME, NULL);
  
  webkit_web_frame_stop_loading (frame);
}

static void
load_scheme (MarinaWebView *web_view,
             const gchar   *scheme)
{
  MarinaWebViewPrivate *priv;
  gchar                *file;
  gchar                *uri;
  
  g_return_if_fail (MARINA_IS_WEB_VIEW (web_view));
  
  priv = web_view->priv;
  
  priv->loaded = FALSE;
  
  if (!scheme)
    scheme = "Default";
  
  file = g_build_filename (marina_dirs_get_marina_data_dir (),
                           "schemes",
                           scheme,
                           "index.html",
                           NULL);

  uri = g_filename_to_uri (file, NULL, NULL);
  
  webkit_web_view_open (WEBKIT_WEB_VIEW (priv->web_view), uri);
  
  g_free (file);
  g_free (uri);
}

static void update_fonts (MarinaWebView *web_view, WebKitWebSettings *settings);

static void
load_finished_cb (MarinaWebView *web_view)
{
  MarinaWebViewPrivate *priv;
  WebKitWebSettings    *settings;

  g_return_if_fail (MARINA_IS_WEB_VIEW (web_view));

  priv = web_view->priv;
  
  priv->loaded = TRUE;
  
  settings = webkit_web_view_get_settings (WEBKIT_WEB_VIEW (web_view->priv->web_view));
  update_fonts (web_view, settings);
  
  marina_web_view_set_source (web_view, priv->source);

  if (priv->item)
    marina_web_view_set_item (web_view, priv->item);
}

static void
scheme_changed_cb (MarinaSchemes *schemes,
                   const gchar   *name,
                   MarinaWebView *web_view)
{
  MarinaWebViewPrivate *priv;

  g_return_if_fail (MARINA_IS_WEB_VIEW (web_view));

  priv = web_view->priv;

  load_scheme (web_view, name);
}

static gchar*
get_font (gchar *fullname)
{
  gchar *tmp = g_strrstr (fullname, " ");
  g_return_val_if_fail (tmp != NULL, NULL);
  *tmp = '\0';
  gchar *n = g_strdup (fullname);
  *tmp = ' ';
  return n;
}

static gint
get_size (gchar *fullname)
{
  gchar *tmp = g_strrstr (fullname, " ");
  return atoi (tmp);
}

static gchar*
get_default_font (GtkWidget *widget)
{
  PangoFontDescription *font_desc = widget->style->font_desc;
  return g_strdup_printf ("%s %d",
                          pango_font_description_get_family (font_desc),
                          pango_font_description_get_size (font_desc) / PANGO_SCALE);
}

static void
update_fonts (MarinaWebView     *web_view,
              WebKitWebSettings *settings)
{
  gchar *font;
  
  if (marina_prefs_get_use_default_font ())
    font = get_default_font (GTK_WIDGET (web_view));
  else
    font = marina_prefs_get_font ();
  
  g_object_set (settings,
                "default-font-family", get_font (font), // leaks
                "default-font-size", get_size (font),
                NULL);
  
  g_free (font);
}

static void
set_font_cb (MarinaMessageBus *message_bus,
             MarinaMessage    *message,
             MarinaWebView    *web_view)
{
  MarinaWebViewPrivate *priv;
  WebKitWebSettings *settings;
  
  g_return_if_fail (MARINA_IS_WEB_VIEW (web_view));
  
  priv = web_view->priv;
  
  settings = webkit_web_view_get_settings (WEBKIT_WEB_VIEW (priv->web_view));
  
  update_fonts (web_view, settings);
}

static void
marina_web_view_init (MarinaWebView *web_view)
{
  GtkWidget *scroller;
  WebKitWebSettings *settings;
  
  web_view->priv = G_TYPE_INSTANCE_GET_PRIVATE (web_view,
                                                MARINA_TYPE_WEB_VIEW,
                                                MarinaWebViewPrivate);
  
  scroller = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scroller), GTK_SHADOW_NONE);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scroller),
                                  GTK_POLICY_AUTOMATIC,
                                  GTK_POLICY_AUTOMATIC);
  gtk_box_pack_start (GTK_BOX (web_view), scroller, TRUE, TRUE, 0);
  gtk_widget_show (scroller);
  
  web_view->priv->vadj = gtk_scrolled_window_get_vadjustment (GTK_SCROLLED_WINDOW (scroller));
  
  web_view->priv->web_view = webkit_web_view_new ();
  gtk_container_add (GTK_CONTAINER (scroller), web_view->priv->web_view);
  gtk_widget_show (web_view->priv->web_view);
  
  /* settings defaults */
  settings = webkit_web_view_get_settings (WEBKIT_WEB_VIEW (web_view->priv->web_view));
  g_object_set (settings,
                "enable-plugins", TRUE,
                NULL);
  
  /* load the default scheme */
  const gchar *scheme = marina_schemes_get_active (marina_schemes_get_default ());
  load_scheme (web_view, scheme);
  
  /* link hover */
  g_signal_connect (web_view->priv->web_view,
                    "hovering-over-link",
                    G_CALLBACK (hovering_over_link),
                    NULL);
  
  /* navigation attempt */
  g_signal_connect (web_view->priv->web_view,
                    "navigation-requested",
                    G_CALLBACK (navigation_requested),
                    web_view);

  /* load finished (can thus load source/item) */
  g_signal_connect_swapped (web_view->priv->web_view,
                            "load-finished",
                            G_CALLBACK (load_finished_cb),
                            web_view);

  /* update scheme when it changes */
  g_signal_connect (marina_schemes_get_default (),
                    "changed",
                    G_CALLBACK (scheme_changed_cb),
                    web_view);
  
  /* adjust when font sizes change */
  marina_message_bus_connect (marina_message_bus_get_default (),
                              "/ui", "set_font",
                              (MarinaMessageCallback) set_font_cb,
                              web_view,
                              NULL);
}

GtkWidget*
marina_web_view_new (void)
{
  return g_object_new (MARINA_TYPE_WEB_VIEW, NULL);
}

void
marina_web_view_set_item (MarinaWebView *web_view,
                          MarinaItem    *item)
{
  MarinaWebViewPrivate *priv;
  gchar *json;
  gchar *script;
  
  g_return_if_fail (MARINA_IS_WEB_VIEW (web_view));
  
  priv = web_view->priv;
  
  if (priv->item)
    g_object_unref (priv->item);
  
  priv->item = item ? g_object_ref (item) : NULL;
  
  if (!priv->loaded)
    return;
  
  json = to_item_json (item);
  script = g_strconcat ("setItem(", json, ");", NULL);
  
  webkit_web_view_execute_script (WEBKIT_WEB_VIEW (priv->web_view), script);
  
  gtk_adjustment_set_value (priv->vadj, 0.0);
  
  g_free (json);
  g_free (script);
}

void
marina_web_view_set_source (MarinaWebView *web_view,
                            MarinaSource  *source)
{
  MarinaWebViewPrivate *priv;
  gchar *json;
  gchar *script;
  
  g_return_if_fail (MARINA_IS_WEB_VIEW (web_view));
  
  priv = web_view->priv;
  
  if (priv->source)
    g_object_unref (priv->source);

  priv->source = source ? g_object_ref (source) : NULL;
  
  /* this will get finished when its loaded */
  if (!priv->loaded)
    return;
  
  json = to_source_json (source);
  script = g_strconcat ("$(function(){"
                        "  setSource(", json, ");",
                        "});",
                        NULL);
  
  webkit_web_view_execute_script (WEBKIT_WEB_VIEW (priv->web_view), script);
  
  g_free (json);
  g_free (script);
}
