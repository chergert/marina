/* marina-home-source.c
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#include <glib/gi18n.h>
#include <gtk/gtk.h>

#include "marina-home-source.h"

struct _MarinaHomeSourcePrivate
{
  gpointer dummy;
};

static void marina_source_init (MarinaSourceIface *iface);

G_DEFINE_TYPE_EXTENDED (MarinaHomeSource,
                        marina_home_source,
                        G_TYPE_OBJECT,
                        0,
                        G_IMPLEMENT_INTERFACE (MARINA_TYPE_SOURCE,
                                               marina_source_init));

static void
marina_home_source_finalize (GObject *object)
{
  G_OBJECT_CLASS (marina_home_source_parent_class)->finalize (object);
}

static void
marina_home_source_class_init (MarinaHomeSourceClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  
  object_class->finalize = marina_home_source_finalize;

  g_type_class_add_private (object_class, sizeof(MarinaHomeSourcePrivate));
}

static void
marina_home_source_init (MarinaHomeSource *home_source)
{
  home_source->priv = G_TYPE_INSTANCE_GET_PRIVATE(home_source,
                                                  MARINA_TYPE_HOME_SOURCE,
                                                  MarinaHomeSourcePrivate);
}

MarinaSource*
marina_home_source_new ()
{
  return g_object_new (MARINA_TYPE_HOME_SOURCE, NULL);
}

static G_CONST_RETURN gchar*
get_title (MarinaSource *source)
{
  return _("Home");
}

static G_CONST_RETURN gchar*
get_id (MarinaSource *source)
{
  return "home";
}

static G_CONST_RETURN gchar*
get_icon_name (MarinaSource *source)
{
  return GTK_STOCK_HOME;
}

static gboolean
observes (MarinaSource *source, MarinaItem *item)
{
  return TRUE;
}

static GList*
get_children (MarinaSource *source)
{
  return NULL;
}

static void
marina_source_init (MarinaSourceIface *iface)
{
  iface->get_title = get_title;
  iface->get_id = get_id;
  iface->get_icon_name = get_icon_name;
  iface->observes = observes;
  iface->get_children = get_children;
}
