/* marina-default-builder.c
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#include "marina-builder.h"
#include "marina-debug.h"
#include "marina-default-builder.h"

#include "broken-date-parser.h"

#define MAP_SOURCE_ATTR(iprop,rprop)                          \
  G_STMT_START {                                              \
    if (marina_parse_result_has_attr (result, rprop))         \
      {                                                       \
        GValue value = {0,};                                  \
        marina_parse_result_get_attr (result, rprop, &value); \
        g_object_set_property (G_OBJECT (source),             \
                               iprop,                         \
                               &value);                       \
        g_value_unset (&value);                               \
      }                                                       \
    }                                                         \
  G_STMT_END

#define MAP_ITEM_ATTR(iprop,rprop)                            \
  G_STMT_START {                                              \
    if (marina_parse_result_has_attr (result, rprop))         \
      {                                                       \
        GValue value = {0,};                                  \
        marina_parse_result_get_attr (result, rprop, &value); \
        g_object_set_property (G_OBJECT (item),               \
                               iprop,                         \
                               &value);                       \
        g_value_unset (&value);                               \
      }                                                       \
    }                                                         \
  G_STMT_END

struct _MarinaDefaultBuilderPrivate
{
  gpointer dummy;
};

static void marina_builder_init (MarinaBuilderIface *iface);

G_DEFINE_TYPE_EXTENDED (MarinaDefaultBuilder,
                        marina_default_builder,
                        G_TYPE_OBJECT,
                        0,
                        G_IMPLEMENT_INTERFACE (MARINA_TYPE_BUILDER,
                                               marina_builder_init));

static void
marina_default_builder_finalize (GObject *object)
{
  G_OBJECT_CLASS (marina_default_builder_parent_class)->finalize (object);
}

static void
marina_default_builder_class_init (MarinaDefaultBuilderClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  
  object_class->finalize = marina_default_builder_finalize;

  g_type_class_add_private (object_class, sizeof (MarinaDefaultBuilderPrivate));
}

static void
marina_default_builder_init (MarinaDefaultBuilder *builder)
{
  builder->priv = G_TYPE_INSTANCE_GET_PRIVATE (builder,
                                               MARINA_TYPE_DEFAULT_BUILDER,
                                               MarinaDefaultBuilderPrivate);
}

MarinaDefaultBuilder*
marina_default_builder_new (void)
{
  return g_object_new (MARINA_TYPE_DEFAULT_BUILDER, NULL);
}

static void
marina_default_builder_build (MarinaBuilder     *builder,
                              MarinaParseResult *result,
                              MarinaSource      *source,
                              MarinaItem        *item)
{
  marina_debug (DEBUG_BUILDERS);
  
  g_return_if_fail (MARINA_IS_DEFAULT_BUILDER (builder));
  g_return_if_fail (MARINA_IS_PARSE_RESULT (result));
  g_return_if_fail (MARINA_IS_SOURCE (source));
  
  if (!item)
    {
      /* build the source instance */
      if (!marina_source_get_title (source))
        MAP_SOURCE_ATTR ("title", "title");
    }
  else
    {
      g_return_if_fail (MARINA_IS_ITEM (item));
      
      /* build the item instance */
      marina_item_set_source_id (item, marina_source_get_id (source));
      
      MAP_ITEM_ATTR ("item-id", "id");
      MAP_ITEM_ATTR ("title", "title");
      MAP_ITEM_ATTR ("author", "author");
      MAP_ITEM_ATTR ("content", "description");
      MAP_ITEM_ATTR ("link", "link");
      
      if (marina_parse_result_has_attr (result, "pub-date"))
        {
          GValue value = {0,};
          MarinaDate *date;

          marina_parse_result_get_attr (result, "pub-date", &value);
          date = marina_date_new_from_string (g_value_get_string (&value));

          g_value_unset (&value);

          g_object_set (item,
                        "published-at", date,
                        "updated-at", date,
                        NULL);
        }
    }
}

static void
marina_builder_init (MarinaBuilderIface *iface)
{
  iface->build = marina_default_builder_build;
}
