/* xml-reader.h
 *
 * Copyright (C) 2009  Christian Hergert  <chris@dronelabs.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * Author:
 *   Christian Hergert  <chris@dronelabs.com>
 */

/* After using Emmanuele Bassi's XmlWriter, I realized I wanted a
 * few more features and to be based on the xmlTextReader API. This
 * is the result of my quick hack to do so.
 *
 * You might wonder why i simply wrapped xmlTextReader from libxml2
 * as it is already a .NET like API. It's simple, I wanted a GObject
 * that was easily bindable for my plugins to use as well as my
 * core library in C.
 */

#ifndef __XML_READER_H__
#define __XML_READER_H__

#include <glib-object.h>

G_BEGIN_DECLS

#define XML_TYPE_READER (xml_reader_get_type ())
#define XML_READER(obj)               \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  XML_TYPE_READER, XmlReader))

#define XML_READER_CONST(obj)         \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  XML_TYPE_READER, XmlReader const))

#define XML_READER_CLASS(klass)       \
  (G_TYPE_CHECK_CLASS_CAST ((klass),  \
  XML_TYPE_READER, XmlReaderClass))

#define XML_IS_READER(obj)            \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  XML_TYPE_READER))

#define XML_IS_READER_CLASS(klass)    \
  (G_TYPE_CHECK_CLASS_TYPE ((klass),  \
  XML_TYPE_READER))

#define XML_READER_GET_CLASS(obj)     \
  (G_TYPE_INSTANCE_GET_CLASS ((obj),  \
  XML_TYPE_READER, XmlReaderClass))

typedef struct _XmlReader         XmlReader;
typedef struct _XmlReaderClass    XmlReaderClass;
typedef struct _XmlReaderPrivate  XmlReaderPrivate;

struct _XmlReader
{
  GObject parent;
  
  XmlReaderPrivate *priv;
};

struct _XmlReaderClass
{
  GObjectClass parent_class;
};

GType                 xml_reader_get_type                (void) G_GNUC_CONST;

XmlReader*            xml_reader_new                     (void);
gboolean              xml_reader_load_from_file          (XmlReader   *reader,
                                                          const gchar *filename);
gboolean              xml_reader_load_from_data          (XmlReader   *reader,
                                                          const gchar *data,
                                                          gsize        length,
                                                          const gchar *uri,
                                                          const gchar *encoding);

G_CONST_RETURN gchar* xml_reader_get_value               (XmlReader   *reader);
G_CONST_RETURN gchar* xml_reader_get_name                (XmlReader   *reader);
gchar*                xml_reader_read_string             (XmlReader   *reader);
gchar*                xml_reader_get_attribute           (XmlReader   *reader,
                                                          const gchar *name);

gboolean              xml_reader_read_start_element      (XmlReader   *reader,
                                                          const gchar *name);
gboolean              xml_reader_read_end_element        (XmlReader   *reader);

gchar*                xml_reader_read_inner_xml          (XmlReader   *reader);
gchar*                xml_reader_read_outer_xml          (XmlReader   *reader);

gboolean              xml_reader_read                    (XmlReader   *reader);
gboolean              xml_reader_read_to_next            (XmlReader   *reader);
gboolean              xml_reader_read_to_next_sibling    (XmlReader   *reader);

gboolean              xml_reader_move_to_element         (XmlReader   *reader);
gboolean              xml_reader_move_to_attribute       (XmlReader   *reader,
                                                          const gchar *name);

gboolean              xml_reader_move_to_first_attribute (XmlReader   *reader);
gboolean              xml_reader_move_to_next_attribute  (XmlReader   *reader);
gint                  xml_reader_count_attributes        (XmlReader   *reader);

G_END_DECLS

#endif /* __XML_READER_H__ */
