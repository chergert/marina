/* marina-date.c
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#include "marina-date.h"
#include "broken-date-parser.h"

GType
marina_date_get_type (void)
{
  static gint type_id = 0;
  
  if (!type_id)
    type_id = g_boxed_type_register_static (g_intern_static_string ("MarinaDate"),
                                            (GBoxedCopyFunc) marina_date_copy,
                                            (GBoxedFreeFunc) marina_date_free);

  return type_id;
}

MarinaDate*
marina_date_new (void)
{
  return g_new0 (MarinaDate, 1);
}

MarinaDate*
marina_date_new_from_time_t (time_t timet)
{
  MarinaDate* date = marina_date_new ();
  date->timet = timet;
  return date;
}

MarinaDate*
marina_date_new_from_string (const gchar *date_string)
{
  time_t timet = 0L;
  
  if (date_string)
    timet = parse_broken_date (date_string, NULL);
  
  return marina_date_new_from_time_t (timet);
}

gchar*
marina_date_format (MarinaDate  *date,
                    const gchar *format)
{
  gchar str[200];

  g_assert (date != NULL);
  
  strftime (str, sizeof (str), format, localtime (&date->timet));
  
  return g_strdup (str);
}

const gchar*
marina_date_to_string (MarinaDate *date)
{
  g_assert (date != NULL);

  if (!date->as_str)
    date->as_str = marina_date_format (date, "%c");

  return date->as_str;
}

MarinaDate*
marina_date_copy (const MarinaDate *date)
{
  return marina_date_new_from_time_t (date->timet);
}

void
marina_date_free (MarinaDate *date)
{
  if (date)
    g_free (date->as_str);
  g_free (date);
}


guint
marina_date_hash (const MarinaDate *date)
{
  return date->timet;
}

gboolean
marina_date_equal (const MarinaDate *date1,
                   const MarinaDate *date2)
{
  return date1->timet == date2->timet;
}

void
g_value_set_date (GValue           *value,
                  const MarinaDate *date)
{
  g_value_set_boxed (value, date);
}

MarinaDate*
g_value_get_date (const GValue *value)
{
  return g_value_get_boxed (value);
}
