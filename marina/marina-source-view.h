/* marina-source-view.h
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifndef __MARINA_SOURCE_VIEW_H__
#define __MARINA_SOURCE_VIEW_H__

#include <glib-object.h>
#include <gtk/gtk.h>

#include "marina-source.h"

G_BEGIN_DECLS

#define MARINA_TYPE_SOURCE_VIEW (marina_source_view_get_type())

#define MARINA_SOURCE_VIEW(obj)               \
  (G_TYPE_CHECK_INSTANCE_CAST((obj),          \
  MARINA_TYPE_SOURCE_VIEW, MarinaSourceView))

#define MARINA_IS_SOURCE_VIEW(obj)            \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj),          \
  MARINA_TYPE_SOURCE_VIEW))

#define MARINA_SOURCE_VIEW_GET_INTERFACE(obj) \
  (G_TYPE_INSTANCE_GET_INTERFACE((obj),       \
  MARINA_TYPE_SOURCE_VIEW,                    \
  MarinaSourceViewIface))

typedef struct _MarinaSourceView      MarinaSourceView;
typedef struct _MarinaSourceViewIface MarinaSourceViewIface;

struct _MarinaSourceViewIface
{
  GTypeInterface parent;
  
  void          (*set_source) (MarinaSourceView *source_view,
                               MarinaSource     *source);

  void          (*move_next)  (MarinaSourceView *source_view);
  void          (*move_prev)  (MarinaSourceView *source_view);
};

GType marina_source_view_get_type   (void) G_GNUC_CONST;

void  marina_source_view_set_source (MarinaSourceView *source_view,
                                     MarinaSource     *source);

void  marina_source_view_move_next  (MarinaSourceView *source_view);
void  marina_source_view_move_prev  (MarinaSourceView *source_view);

G_END_DECLS

#endif /* __MARINA_SOURCE_VIEW_H__ */
