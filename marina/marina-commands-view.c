/*
 * marina-commands-view.c
 * This file is part of marina; it was borrowed from gedit.
 *
 * Copyright (C) 1998, 1999 Alex Roberts, Evan Lawrence
 * Copyright (C) 2000, 2001 Chema Celorio, Paolo Maggi
 * Copyright (C) 2002-2005 Paolo Maggi
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gtk/gtk.h>

#include "marina-app.h"
#include "marina-commands.h"
#include "marina-debug.h"
#include "marina-message.h"
#include "marina-message-bus.h"
#include "marina-window.h"
#include "marina-window-private.h"

void
_marina_cmd_view_show_toolbar (GtkAction    *action,
                               MarinaWindow *window)
{
  gboolean visible;

  marina_debug (DEBUG_COMMANDS);

  visible = gtk_toggle_action_get_active (GTK_TOGGLE_ACTION (action));

  if (visible)
    gtk_widget_show (window->priv->toolbar);
  else
    gtk_widget_hide (window->priv->toolbar);
}

void
_marina_cmd_view_show_statusbar (GtkAction    *action,
                                 MarinaWindow *window)
{
  gboolean visible;

  marina_debug (DEBUG_COMMANDS);

  visible = gtk_toggle_action_get_active (GTK_TOGGLE_ACTION (action));

  if (visible)
    gtk_widget_show (window->priv->statusbar);
  else
    gtk_widget_hide (window->priv->statusbar);
}

void
_marina_cmd_view_show_side_pane (GtkAction    *action,
                                 MarinaWindow *window)
{
	GtkWidget *side_pane;
  gboolean   visible;

  marina_debug (DEBUG_COMMANDS);

  g_return_if_fail (MARINA_IS_WINDOW (window));

  side_pane = window->priv->side_pane;
  visible = gtk_toggle_action_get_active (GTK_TOGGLE_ACTION (action));

  if (visible)
    {
      gtk_widget_show (GTK_WIDGET (side_pane));
      gtk_widget_grab_focus (GTK_WIDGET (side_pane));
    }
  else
    gtk_widget_hide (GTK_WIDGET (side_pane));
}

void
_marina_cmd_view_show_bottom_pane (GtkAction    *action,
                                   MarinaWindow *window)
{
	GtkWidget *panel;
  gboolean   visible;

  marina_debug (DEBUG_COMMANDS);

  g_return_if_fail (MARINA_IS_WINDOW (window));

  panel =  window->priv->bottom_panel;
  visible = gtk_toggle_action_get_active (GTK_TOGGLE_ACTION (action));

  if (visible)
    {
      gtk_widget_show (GTK_WIDGET (panel));
      gtk_widget_grab_focus (GTK_WIDGET (panel));
    }
  else
    {
      gtk_widget_hide (GTK_WIDGET (panel));
    }
}

void
_marina_cmd_view_toggle_fullscreen_mode (GtkAction    *action,
                                         MarinaWindow *window)
{
  marina_debug (DEBUG_COMMANDS);

  if (_marina_window_is_fullscreen (window))
    _marina_window_unfullscreen (window);
  else
    _marina_window_fullscreen (window);
}

void
_marina_cmd_view_leave_fullscreen_mode (GtkAction    *action,
                                        MarinaWindow *window)
{
  GtkAction *view_action;

  view_action = gtk_action_group_get_action (window->priv->always_sensitive_action_group,
                                             "ViewFullscreen");
  g_signal_handlers_block_by_func (view_action,
                                   G_CALLBACK (_marina_cmd_view_toggle_fullscreen_mode),
                                   window);
  gtk_toggle_action_set_active (GTK_TOGGLE_ACTION (view_action), FALSE);
  _marina_window_unfullscreen (window);
  g_signal_handlers_unblock_by_func (view_action,
                                     G_CALLBACK (_marina_cmd_view_toggle_fullscreen_mode),
                                     window);
}

void
_marina_cmd_view_toggle_compact_mode (GtkAction    *action,
                                      MarinaWindow *window)
{
  gboolean active;
  
  g_object_get (G_OBJECT (action), "active", &active, NULL);
  
  marina_message_bus_send (marina_message_bus_get_default (),
                           "/window", "compact_mode",
                           "active", active,
                           NULL);
}
