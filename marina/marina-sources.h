/* marina-sources.h
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifndef __MARINA_SOURCES_H__
#define __MARINA_SOURCES_H__

#include <glib-object.h>
#include <gtk/gtk.h>

#include "marina-source.h"

G_BEGIN_DECLS

#define MARINA_TYPE_SOURCES (marina_sources_get_type ())

#define MARINA_SOURCES(obj)                 \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj),       \
  MARINA_TYPE_SOURCES, MarinaSources))

#define MARINA_SOURCES_CONST(obj)           \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj),       \
  MARINA_TYPE_SOURCES, MarinaSources const))

#define MARINA_SOURCES_CLASS(klass)         \
  (G_TYPE_CHECK_CLASS_CAST ((klass),        \
  MARINA_TYPE_SOURCES, MarinaSourcesClass))

#define MARINA_IS_SOURCES(obj)              \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj),       \
  MARINA_TYPE_SOURCES))

#define MARINA_IS_SOURCES_CLASS(klass)      \
  (G_TYPE_CHECK_CLASS_TYPE ((klass),        \
  MARINA_TYPE_SOURCES))

#define MARINA_SOURCES_GET_CLASS(obj)       \
  (G_TYPE_INSTANCE_GET_CLASS ((obj),        \
  MARINA_TYPE_SOURCES, MarinaSourcesClass))

#define MARINA_SOURCES_ERROR (marina_sources_error_quark ())

typedef enum
{
  MARINA_SOURCES_ERROR_INVALID,
} MarinaSourcesError;

typedef struct _MarinaSources           MarinaSources;
typedef struct _MarinaSourcesClass      MarinaSourcesClass;
typedef struct _MarinaSourcesPrivate    MarinaSourcesPrivate;

GQuark marina_sources_error_quark (void);

struct _MarinaSources
{
  GObject parent;
  
  MarinaSourcesPrivate *priv;
};

struct _MarinaSourcesClass
{
  GObjectClass parent_class;
};

GType            marina_sources_get_type       (void) G_GNUC_CONST;

MarinaSources*   marina_sources_get_default    (void);

void             marina_sources_remove         (MarinaSources *sources,
                                                MarinaSource  *source);

gboolean         marina_sources_contains       (MarinaSources *sources,
                                                const gchar   *source_id);

MarinaSource*    marina_sources_get            (MarinaSources *sources,
                                                const gchar   *source_id);

void             marina_sources_set            (MarinaSources *sources,
                                                MarinaSource  *source);

GtkTreeModel*    marina_sources_get_tree_model (MarinaSources *sources);

void             marina_sources_foreach        (MarinaSources *sources,
                                                GFunc          func,
                                                gpointer       user_data);

void             marina_sources_save           (MarinaSources *sources);

void             marina_sources_stat           (MarinaSources *sources);

guint            marina_sources_count_unread   (MarinaSources *sources,
                                                MarinaSource  *source);

void             marina_sources_add            (MarinaSources *sources,
                                                MarinaSource  *source,
                                                MarinaSource  *parent);

G_END_DECLS

#endif /* __MARINA_SOURCES_H__ */
