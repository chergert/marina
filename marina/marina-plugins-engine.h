/*
 * marina-plugins-engine.h
 * This file is part of marina; it was borrowed from gedit.
 *
 * Copyright (C) 2002-2005 - Paolo Maggi 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA 02111-1307, USA. 
 */
 
/*
 * Modified by the marina Team, 2002-2005. See the gedit AUTHORS file for a 
 * list of people on the gedit Team.  
 */

#ifndef __MARINA_PLUGINS_ENGINE_H__
#define __MARINA_PLUGINS_ENGINE_H__

#include <glib.h>
#include "marina-window.h"
#include "marina-plugin-info.h"
#include "marina-plugin.h"

G_BEGIN_DECLS

#define MARINA_TYPE_PLUGINS_ENGINE              (marina_plugins_engine_get_type ())
#define MARINA_PLUGINS_ENGINE(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), MARINA_TYPE_PLUGINS_ENGINE, MarinaPluginsEngine))
#define MARINA_PLUGINS_ENGINE_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass),  MARINA_TYPE_PLUGINS_ENGINE, MarinaPluginsEngineClass))
#define MARINA_IS_PLUGINS_ENGINE(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), MARINA_TYPE_PLUGINS_ENGINE))
#define MARINA_IS_PLUGINS_ENGINE_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE ((klass), MARINA_TYPE_PLUGINS_ENGINE))
#define MARINA_PLUGINS_ENGINE_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj),  MARINA_TYPE_PLUGINS_ENGINE, MarinaPluginsEngineClass))

typedef struct _MarinaPluginsEngine		MarinaPluginsEngine;
typedef struct _MarinaPluginsEnginePrivate	MarinaPluginsEnginePrivate;

struct _MarinaPluginsEngine
{
	GObject parent;
	MarinaPluginsEnginePrivate *priv;
};

typedef struct _MarinaPluginsEngineClass		MarinaPluginsEngineClass;

struct _MarinaPluginsEngineClass
{
	GObjectClass parent_class;

	void	 (* activate_plugin)		(MarinaPluginsEngine *engine,
						 MarinaPluginInfo    *info);

	void	 (* deactivate_plugin)		(MarinaPluginsEngine *engine,
						 MarinaPluginInfo    *info);
};

GType			marina_plugins_engine_get_type		(void) G_GNUC_CONST;

MarinaPluginsEngine	*marina_plugins_engine_get_default	(void);

void		 	marina_plugins_engine_garbage_collect	(MarinaPluginsEngine *engine);

const GList		*marina_plugins_engine_get_plugin_list 	(MarinaPluginsEngine *engine);

MarinaPluginInfo	*marina_plugins_engine_get_plugin_info	(MarinaPluginsEngine *engine,
							         const gchar        *name);

/* plugin load and unloading (overall, for all windows) */
gboolean 	 	marina_plugins_engine_activate_plugin 	(MarinaPluginsEngine *engine,
							         MarinaPluginInfo    *info);
gboolean 	 	marina_plugins_engine_deactivate_plugin	(MarinaPluginsEngine *engine,
							         MarinaPluginInfo    *info);

void	 	 	marina_plugins_engine_configure_plugin	(MarinaPluginsEngine *engine,
							         MarinaPluginInfo    *info,
							 	 GtkWindow          *parent);

/* plugin activation/deactivation per window, private to MarinaWindow */
void 		 	marina_plugins_engine_activate_plugins   (MarinaPluginsEngine *engine,
								  MarinaWindow        *window);
void 		 	marina_plugins_engine_deactivate_plugins (MarinaPluginsEngine *engine,
							  	  MarinaWindow        *window);
void		 	marina_plugins_engine_update_plugins_ui  (MarinaPluginsEngine *engine,
							  	  MarinaWindow        *window);

/* private for gconf notification */
void		 	marina_plugins_engine_active_plugins_changed
								(MarinaPluginsEngine *engine);

void		 	marina_plugins_engine_rescan_plugins	(MarinaPluginsEngine *engine);

G_END_DECLS

#endif  /* __MARINA_PLUGINS_ENGINE_H__ */
