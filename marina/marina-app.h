/* marina-app.h
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifndef __MARINA_APP_H__
#define __MARINA_APP_H__

#include <glib-object.h>

#include "marina-window.h"

G_BEGIN_DECLS

#define MARINA_TYPE_APP (marina_app_get_type ())

#define MARINA_APP(obj)               \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  MARINA_TYPE_APP, MarinaApp))

#define MARINA_APP_CONST(obj)         \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  MARINA_TYPE_APP, MarinaApp const))
#define MARINA_APP_CLASS(klass)       \
  (G_TYPE_CHECK_CLASS_CAST ((klass),  \
  MARINA_TYPE_APP, MarinaAppClass))

#define MARINA_IS_APP(obj)            \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  MARINA_TYPE_APP))

#define MARINA_IS_APP_CLASS(klass)    \
  (G_TYPE_CHECK_CLASS_TYPE ((klass),  \
  MARINA_TYPE_APP))

#define MARINA_APP_GET_CLASS(obj)     \
  (G_TYPE_INSTANCE_GET_CLASS ((obj),  \
  MARINA_TYPE_APP, MarinaAppClass))

typedef struct _MarinaApp         MarinaApp;
typedef struct _MarinaAppClass    MarinaAppClass;
typedef struct _MarinaAppPrivate  MarinaAppPrivate;

struct _MarinaApp
{
  GObject parent;
  
  MarinaAppPrivate *priv;
};

struct _MarinaAppClass
{
  GObjectClass parent_class;
  
  gboolean (*quit) (MarinaApp *app);
};

GType         marina_app_get_type         (void) G_GNUC_CONST;
MarinaApp*    marina_app_get_default      (void);
GtkDialog*    marina_app_get_prefs_dialog (MarinaApp *self);
MarinaWindow* marina_app_get_window       (MarinaApp *self);
void          marina_app_shutdown         (MarinaApp *self);
void          marina_app_start            (MarinaApp *self);
void          marina_app_quit             (MarinaApp *self,
                                           gboolean   allow_inhibit);

G_END_DECLS

#endif /* __MARINA_APP_H__ */
