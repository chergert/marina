/* marina-source-view.c
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#include "marina-source-view.h"

GType
marina_source_view_get_type (void)
{
  static GType marina_source_view_type = 0;
  
  if (!marina_source_view_type)
    {
      const GTypeInfo marina_source_view_info = {
          sizeof (MarinaSourceViewIface),
          NULL, /* base_init */
          NULL, /* base_finalize */
          NULL,
          NULL, /* class_finalize */
          NULL, /* class_data */
          0,
          0,
          NULL
      };

      marina_source_view_type = g_type_register_static (G_TYPE_INTERFACE, "MarinaSourceView",
                                                        &marina_source_view_info, 0);
      g_type_interface_add_prerequisite (marina_source_view_type, GTK_TYPE_WIDGET);
    }
  
  return marina_source_view_type;
}

/**
 * marina_source_view_set_source:
 * @source_view: A #MarinaSourceView
 * @source: A #MarinaSource
 *
 * Requests the #MarinaSourceView to make #MarinaSource the prominent source
 * displayed in the source view.
 */
void
marina_source_view_set_source (MarinaSourceView *source_view,
                               MarinaSource     *source)
{
  MarinaSourceViewIface *iface;

  iface = MARINA_SOURCE_VIEW_GET_INTERFACE (source_view);

  if (iface->set_source)
    iface->set_source (source_view, source);
}

/**
 * marina_source_view_move_next:
 * @source_view: A #MarinaSourceView
 *
 * If the source view supports moving items, this will move the
 * source view to the next available item.
 */
void
marina_source_view_move_next (MarinaSourceView *source_view)
{
  MarinaSourceViewIface *iface;

  iface = MARINA_SOURCE_VIEW_GET_INTERFACE (source_view);

  if (iface->move_next)
    iface->move_next (source_view);
}

/**
 * marina_source_view_move_prev:
 * @source_view: A #MarinaSourceView
 *
 * If the source view supports moving items, this will move the
 * source view to the previous item.
 */
void
marina_source_view_move_prev (MarinaSourceView *source_view)
{
  MarinaSourceViewIface *iface;

  iface = MARINA_SOURCE_VIEW_GET_INTERFACE (source_view);

  if (iface->move_prev)
    iface->move_prev (source_view);
}
