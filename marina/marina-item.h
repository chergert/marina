/* marina-item.h
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifndef __MARINA_ITEM_H__
#define __MARINA_ITEM_H__

#include <glib-object.h>

#include "marina-date.h"

G_BEGIN_DECLS

#define MARINA_TYPE_ITEM (marina_item_get_type ())

#define MARINA_ITEM(obj)              \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  MARINA_TYPE_ITEM, MarinaItem))

#define MARINA_ITEM_CONST(obj)        \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  MARINA_TYPE_ITEM, MarinaItem const))

#define MARINA_ITEM_CLASS(klass)      \
  (G_TYPE_CHECK_CLASS_CAST ((klass),  \
  MARINA_TYPE_ITEM, MarinaItemClass))

#define MARINA_IS_ITEM(obj)           \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  MARINA_TYPE_ITEM))

#define MARINA_IS_ITEM_CLASS(klass)   \
  (G_TYPE_CHECK_CLASS_TYPE ((klass),  \
  MARINA_TYPE_ITEM))

#define MARINA_ITEM_GET_CLASS(obj)    \
  (G_TYPE_INSTANCE_GET_CLASS ((obj),  \
  MARINA_TYPE_ITEM, MarinaItemClass))

typedef struct _MarinaItem           MarinaItem;
typedef struct _MarinaItemClass      MarinaItemClass;
typedef struct _MarinaItemPrivate    MarinaItemPrivate;

struct _MarinaItem
{
    GObject parent;
    
    MarinaItemPrivate *priv;
};

struct _MarinaItemClass
{
    GObjectClass parent_class;
};

GType               marina_item_get_type         (void) G_GNUC_CONST;
MarinaItem*         marina_item_new              (void);

const gchar*        marina_item_get_author       (MarinaItem *self);
void                marina_item_set_author       (MarinaItem *self, const gchar *author);

GList*              marina_item_get_data_items   (MarinaItem *self);
void                marina_item_set_data_items   (MarinaItem *self, const GList *items);

gint                marina_item_get_flag         (MarinaItem *self);
void                marina_item_set_flag         (MarinaItem *self, gint flag);

const gchar*        marina_item_get_item_id      (MarinaItem *self);
void                marina_item_set_item_id      (MarinaItem *self, const gchar *item_id);

const gchar*        marina_item_get_link         (MarinaItem *self);
void                marina_item_set_link         (MarinaItem *self, const gchar *link);

MarinaDate*         marina_item_get_published_at (MarinaItem *self);
void                marina_item_set_published_at (MarinaItem *self, const MarinaDate *published_at);

gboolean            marina_item_get_read         (MarinaItem *self);
void                marina_item_set_read         (MarinaItem *self, gboolean read);

const gchar*        marina_item_get_source_id    (MarinaItem *self);
void                marina_item_set_source_id    (MarinaItem *self, const gchar *source_id);

const gchar*        marina_item_get_title        (MarinaItem *self);
void                marina_item_set_title        (MarinaItem *self, const gchar *title);

MarinaDate*         marina_item_get_updated_at   (MarinaItem *self);
void                marina_item_set_updated_at   (MarinaItem *self, const MarinaDate *updated_at);

const gchar*        marina_item_get_content      (MarinaItem *self);
void                marina_item_set_content      (MarinaItem *self, const gchar *content);

G_END_DECLS

#endif /* __MARINA_ITEM_H__ */
