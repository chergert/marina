/* marina-progress.c
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with This; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#include "marina-progress.h"

#define PROGRESS_ANIMATION_SPEED 4

struct _MarinaProgressPrivate
{
  GtkWidget      *label;
  GtkWidget      *secondary_label;
  GtkWidget      *button_bar;
  GtkWidget      *image;
  GtkWidget      *progress;
  
  gint            dest_y;
  gboolean        animation_enter;
  guint           animation_id;
  
  GFunc           cancel_func;
  gpointer        cancel_data;
  GDestroyNotify  cancel_destroy;
};

G_DEFINE_TYPE (MarinaProgress, marina_progress, GTK_TYPE_VBOX);

static void
marina_progress_finalize (GObject *object)
{
  MarinaProgressPrivate *priv;
  
  priv = MARINA_PROGRESS (object)->priv;
  
  if (priv->cancel_destroy)
    priv->cancel_destroy (priv->cancel_data);
  
  priv->cancel_data = NULL;
  priv->cancel_func = NULL;
  priv->cancel_destroy = NULL;
  
  G_OBJECT_CLASS (marina_progress_parent_class)->finalize (object);
}

static gboolean
run_progress_animation (MarinaProgress *progress)
{
  GtkWidget *widget;
  MarinaProgressPrivate *priv;
  
  g_return_val_if_fail (MARINA_IS_PROGRESS (progress), FALSE);
  
  priv = MARINA_PROGRESS (progress)->priv;
  
  widget = GTK_WIDGET (progress);
  
  gint x, y, ty, sy;
  
  gtk_widget_get_size_request (widget, &x, &y);
  ty = priv->label->allocation.height;
  sy = priv->secondary_label->allocation.height;
  
  if (priv->animation_enter)
    {
      if (y >= priv->dest_y)
        {
          g_source_remove (priv->animation_id);
          priv->animation_id = 0;
          
          gtk_widget_show (priv->button_bar);
          
          return FALSE;
        }
      else
        {
          if (y > 30 && !GTK_WIDGET_VISIBLE (priv->secondary_label))
            gtk_widget_show (priv->secondary_label);
          else if (y > 15 && !GTK_WIDGET_VISIBLE (priv->label))
            gtk_widget_show (priv->label);
          
          gtk_widget_set_size_request (widget, x, y + 1);
        }
    }
  else
    {
      if (y <= 1)
        {
          g_source_remove (priv->animation_id);
          priv->animation_id = 0;
          
          GTK_WIDGET_CLASS (marina_progress_parent_class)->hide (widget);
          
          return FALSE;
        }
      else
        {
          if (y < ty)
            gtk_widget_hide (priv->label);
          else if (y < ty + sy)
            gtk_widget_hide (priv->secondary_label);
          
          gtk_widget_set_size_request (widget, x, y - 1);
        }
    }
  
  return TRUE;
}

static void
show (GtkWidget *widget)
{
  MarinaProgressPrivate *priv;
  GtkSettings *settings;
  gboolean enable;
  
  g_return_if_fail (MARINA_IS_PROGRESS (widget));
  
  priv = MARINA_PROGRESS (widget)->priv;
  
  gtk_widget_hide (priv->button_bar);
  gtk_widget_hide (priv->label);
  gtk_widget_hide (priv->secondary_label);
  
  priv->animation_enter = TRUE;
  
  GTK_WIDGET_CLASS (marina_progress_parent_class)->show (widget);
  
  settings = gtk_widget_get_settings (widget);

  g_object_get (settings, "gtk-enable-animations", &enable, NULL);
  
  if (enable && !priv->animation_id)
    priv->animation_id = g_timeout_add (PROGRESS_ANIMATION_SPEED,
                                        (GSourceFunc) run_progress_animation,
                                        widget);
}

static void
hide (GtkWidget *widget)
{
  MarinaProgressPrivate *priv;
  GtkSettings *settings;
  gboolean enable;
  
  g_return_if_fail (MARINA_IS_PROGRESS (widget));
  
  priv = MARINA_PROGRESS (widget)->priv;
  
  gtk_widget_hide (priv->button_bar);
  
  priv->animation_enter = FALSE;
  
  settings = gtk_widget_get_settings (widget);

  g_object_get (settings, "gtk-enable-animations", &enable, NULL);
  
  if (enable && !priv->animation_id)
    priv->animation_id = g_timeout_add (PROGRESS_ANIMATION_SPEED,
                                        (GSourceFunc) run_progress_animation,
                                        widget);
}

static void
marina_progress_class_init (MarinaProgressClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
  
  object_class->finalize = marina_progress_finalize;
  widget_class->show = show;
  widget_class->hide = hide;

  g_type_class_add_private (object_class, sizeof(MarinaProgressPrivate));
}

static void
marina_progress_init (MarinaProgress *progress)
{
  GtkWidget *hbox1;
  GtkWidget *hbox2;
  GtkWidget *alignment;
  GtkWidget *cancel;
  GtkWidget *cancel_image;
  
  progress->priv = G_TYPE_INSTANCE_GET_PRIVATE (progress,
                                                MARINA_TYPE_PROGRESS,
                                                MarinaProgressPrivate);
  
  /* we need to find a better way to get the natural height */
  progress->priv->dest_y = 70;
  
  gtk_box_set_spacing (GTK_BOX (progress), 2);
  
  hbox1 = gtk_hbox_new (FALSE, 0);
  gtk_box_pack_start (GTK_BOX (progress), hbox1, FALSE, TRUE, 0);
  gtk_widget_show (hbox1);
  
  progress->priv->label = gtk_label_new (NULL);
  gtk_misc_set_alignment (GTK_MISC (progress->priv->label), 0, 0.5);
  gtk_misc_set_padding (GTK_MISC (progress->priv->label), 3, 3);
  gtk_box_pack_start (GTK_BOX (hbox1), progress->priv->label, FALSE, TRUE, 0);
  gtk_widget_hide (progress->priv->label);
  
  progress->priv->secondary_label = gtk_label_new (NULL);
  gtk_misc_set_alignment (GTK_MISC (progress->priv->secondary_label), 0, 0.5);
  gtk_misc_set_padding (GTK_MISC (progress->priv->secondary_label), 3, 0);
  gtk_box_pack_start (GTK_BOX (progress), progress->priv->secondary_label, FALSE, TRUE, 0);
  gtk_widget_hide (progress->priv->secondary_label);
  
  alignment = gtk_alignment_new (0.5, 0.5, 1.0, 1.0);
  gtk_alignment_set_padding (GTK_ALIGNMENT (alignment), 0, 3, 0, 0);
  gtk_box_pack_start (GTK_BOX (progress), alignment, FALSE, FALSE, 0);
  gtk_widget_show (alignment);
  
  hbox2 = gtk_hbox_new (FALSE, 2);
  gtk_container_add (GTK_CONTAINER (alignment), hbox2);
  gtk_widget_show (hbox2);
  progress->priv->button_bar = hbox2;
  
  progress->priv->image = gtk_image_new ();
  gtk_misc_set_padding (GTK_MISC (progress->priv->image), 3, 0);
  gtk_box_pack_start (GTK_BOX (hbox2), progress->priv->image, FALSE, TRUE, 0);
  gtk_widget_show (progress->priv->image);
  
  progress->priv->progress = gtk_progress_bar_new ();
  gtk_widget_set_size_request (progress->priv->progress, -1, 12);
  gtk_box_pack_start (GTK_BOX (hbox2), progress->priv->progress, TRUE, TRUE, 0);
  gtk_widget_show (progress->priv->progress);
  
  gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (progress->priv->progress), 0.38);
  gtk_progress_bar_set_text (GTK_PROGRESS_BAR (progress->priv->progress), "38%");
  
  cancel = gtk_button_new ();
  gtk_button_set_relief (GTK_BUTTON (cancel), GTK_RELIEF_NONE);
  gtk_box_pack_start (GTK_BOX (hbox2), cancel, FALSE, FALSE, 0);
  gtk_widget_show (cancel);
  
  cancel_image = gtk_image_new_from_stock (GTK_STOCK_CANCEL, GTK_ICON_SIZE_MENU);
  gtk_container_add (GTK_CONTAINER (cancel), cancel_image);
  gtk_widget_show (cancel_image);
  
  g_signal_connect_swapped (G_OBJECT (cancel),
                            "clicked",
                            G_CALLBACK (marina_progress_cancel),
                            progress);
}

GtkWidget*
marina_progress_new ()
{
  return g_object_new (MARINA_TYPE_PROGRESS, NULL);
}

void
marina_progress_set_text (MarinaProgress *progress,
                          const gchar    *text)
{
  MarinaProgressPrivate *priv;
  gchar *markup;
  
  g_return_if_fail (MARINA_IS_PROGRESS (progress));
  
  priv = progress->priv;
  
  markup = g_markup_printf_escaped ("<span size='smaller' weight='bold'>%s</span>", text);
  gtk_label_set_markup (GTK_LABEL (priv->label), markup);
  g_free (markup);
}

void
marina_progress_set_secondary_text (MarinaProgress *progress,
                                    const gchar    *secondary_text)
{
  MarinaProgressPrivate *priv;
  gchar *markup;
  
  g_return_if_fail (MARINA_IS_PROGRESS (progress));
  
  priv = progress->priv;
  
  markup = g_markup_printf_escaped ("<span size='smaller'>%s</span>", secondary_text);
  gtk_label_set_markup (GTK_LABEL (priv->secondary_label), markup);
  g_free (markup);
}

void
marina_progress_set_icon_name (MarinaProgress *progress,
                               const gchar    *icon_name)
{
  MarinaProgressPrivate *priv;
  
  g_return_if_fail (MARINA_IS_PROGRESS (progress));
  
  priv = progress->priv;
  
  gtk_image_set_from_icon_name (GTK_IMAGE (priv->image), icon_name,
                                GTK_ICON_SIZE_MENU);
}

void
marina_progress_cancel (MarinaProgress *progress)
{
  MarinaProgressPrivate *priv;
  
  g_return_if_fail (MARINA_IS_PROGRESS (progress));
  
  priv = progress->priv;
  
  if (priv->cancel_func)
    priv->cancel_func (progress, priv->cancel_data);
  
  gtk_widget_hide (GTK_WIDGET (progress));
}

void
marina_progress_set_cancel_func (MarinaProgress *progress,
                                 GFunc           callback,
                                 gpointer        user_data,
                                 GDestroyNotify  destroy)
{
  MarinaProgressPrivate *priv;
  
  g_return_if_fail (MARINA_IS_PROGRESS (progress));
  
  priv = progress->priv;
  
  priv->cancel_func = callback;
  
  if (priv->cancel_destroy)
    priv->cancel_destroy (priv->cancel_data);
  
  priv->cancel_data = user_data;
  priv->cancel_destroy = destroy;
}
