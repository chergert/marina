/* marina-item.c
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#include <string.h>
#include <json-glib/json-gobject.h>

#include "marina-debug.h"
#include "marina-item.h"

struct _MarinaItemPrivate
{
  gchar      *author;
  GList      *data_items;
  gint        flag;
  gchar      *item_id;
  gchar      *link;
  MarinaDate *published_at;
  gboolean    read;
  gchar      *source_id;
  gchar      *title;
  MarinaDate *updated_at;

  gchar *content;
};

enum
{
    PROP_0,
    PROP_AUTHOR,
    PROP_FLAG,
    PROP_ITEM_ID,
    PROP_LINK,
    PROP_PUBLISHED_AT,
    PROP_READ,
    PROP_SOURCE_ID,
    PROP_TITLE,
    PROP_UPDATED_AT,

    PROP_CONTENT,
};

static void json_serializable_init (JsonSerializableIface *iface);

G_DEFINE_TYPE_EXTENDED (MarinaItem,
                        marina_item,
                        G_TYPE_OBJECT,
                        0,
                        G_IMPLEMENT_INTERFACE (JSON_TYPE_SERIALIZABLE,
                                               json_serializable_init));

static void
marina_item_get_property (GObject    *object,
                          guint       property_id,
                          GValue     *value,
                          GParamSpec *pspec)
{
  switch (property_id) {
  case PROP_AUTHOR:
    g_value_set_string (value, marina_item_get_author (MARINA_ITEM (object)));
    break;
  case PROP_FLAG:
    g_value_set_int (value, marina_item_get_flag (MARINA_ITEM (object)));
    break;
  case PROP_ITEM_ID:
    g_value_set_string (value, marina_item_get_item_id (MARINA_ITEM (object)));
    break;
  case PROP_LINK:
    g_value_set_string (value, marina_item_get_link (MARINA_ITEM (object)));
    break;
  case PROP_PUBLISHED_AT:
    g_value_set_date (value, marina_item_get_published_at (MARINA_ITEM (object)));
    break;
  case PROP_READ:
    g_value_set_boolean (value, marina_item_get_read (MARINA_ITEM (object)));
    break;
  case PROP_SOURCE_ID:
    g_value_set_string (value, marina_item_get_source_id (MARINA_ITEM (object)));
    break;
  case PROP_TITLE:
    g_value_set_string (value, marina_item_get_title (MARINA_ITEM (object)));
    break;
  case PROP_UPDATED_AT:
    g_value_set_date  (value, marina_item_get_updated_at (MARINA_ITEM (object)));
    break;
  case PROP_CONTENT:
    g_value_set_string (value, marina_item_get_content (MARINA_ITEM (object)));
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
  }
}

static void
marina_item_set_property (GObject      *object,
                          guint         property_id,
                          const GValue *value,
                          GParamSpec   *pspec)
{
  switch (property_id) {
  case PROP_AUTHOR:
    marina_item_set_author (MARINA_ITEM (object), g_value_get_string (value));
    break;
  case PROP_FLAG:
    marina_item_set_flag (MARINA_ITEM (object), g_value_get_int (value));
    break;
  case PROP_ITEM_ID:
    marina_item_set_item_id (MARINA_ITEM (object), g_value_get_string (value));
    break;
  case PROP_LINK:
    marina_item_set_link (MARINA_ITEM (object), g_value_get_string (value));
    break;
  case PROP_PUBLISHED_AT:
    marina_item_set_published_at (MARINA_ITEM (object), g_value_get_date (value));
    break;
  case PROP_READ:
    marina_item_set_read (MARINA_ITEM (object), g_value_get_boolean (value));
    break;
  case PROP_SOURCE_ID:
    marina_item_set_source_id (MARINA_ITEM (object), g_value_get_string (value));
    break;
  case PROP_TITLE:
    marina_item_set_title (MARINA_ITEM (object), g_value_get_string (value));
    break;
  case PROP_UPDATED_AT:
    marina_item_set_updated_at (MARINA_ITEM (object), g_value_get_date (value));
    break;
  case PROP_CONTENT:
    marina_item_set_content (MARINA_ITEM (object), g_value_get_string (value));
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
  }
}

static void
marina_item_finalize (GObject *object)
{
  MarinaItemPrivate *priv = MARINA_ITEM (object)->priv;

  g_free (priv->author);
  g_free (priv->item_id);
  g_free (priv->title);
  g_free (priv->link);
  g_free (priv->source_id);
  g_free (priv->content);

  marina_date_free (priv->updated_at);
  marina_date_free (priv->published_at);

  G_OBJECT_CLASS (marina_item_parent_class)->finalize (object);
}

static void
marina_item_class_init (MarinaItemClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->get_property = marina_item_get_property;
  object_class->set_property = marina_item_set_property;
  object_class->finalize = marina_item_finalize;

  g_type_class_add_private (object_class, sizeof (MarinaItemPrivate));
  
  /**
   * MarinaItem:item_id:
   *
   * The id of the item, unique to the source.
   */
  g_object_class_install_property (object_class,
                                   PROP_ITEM_ID,
                                   g_param_spec_string ("item-id",
                                                        "Item Id",
                                                        "The item guid",
                                                        NULL,
                                                        G_PARAM_READWRITE));
  
  /**
   * MarinaItem:title:
   *
   * The items title.
   */
  g_object_class_install_property (object_class,
                                   PROP_TITLE,
                                   g_param_spec_string ("title",
                                                        "Title",
                                                        "The item title",
                                                        NULL,
                                                        G_PARAM_READWRITE));

  /**
   * MarinaItem:content:
   *
   * The items content.
   */
  g_object_class_install_property (object_class,
                                   PROP_CONTENT,
                                   g_param_spec_string ("content",
                                                        "Content",
                                                        "The main content of the item",
                                                        NULL,
                                                        G_PARAM_READWRITE));
  
  /**
   * MarinaItem:link:
   *
   * The link to the item, often an http:// url.
   */
  g_object_class_install_property (object_class,
                                   PROP_LINK,
                                   g_param_spec_string ("link",
                                                        "Link",
                                                        "The link to the original content",
                                                        NULL,
                                                        G_PARAM_READWRITE));
  
  /**
   * MarinaItem:updated-at:
   *
   * The time of the most recent edit to the item by the publisher.
   */
  g_object_class_install_property (object_class,
                                   PROP_UPDATED_AT,
                                   g_param_spec_boxed ("updated-at",
                                                       "UpdatedAt",
                                                       "Item updated at",
                                                       MARINA_TYPE_DATE,
                                                       G_PARAM_READWRITE));
  
  /**
   * MarinaItem:read:
   *
   * If the item has been read.
   */
  g_object_class_install_property (object_class,
                                   PROP_READ,
                                   g_param_spec_boolean ("read",
                                                         "Read",
                                                         "Is the item read",
                                                         FALSE,
                                                         G_PARAM_READWRITE));
  
  /**
   * MarinaItem:source-id:
   *
   * The id of the source that the item originated from.
   */
  g_object_class_install_property (object_class,
                                   PROP_SOURCE_ID,
                                   g_param_spec_string ("source-id",
                                                        "SourceId",
                                                        "The id of the owning source",
                                                        NULL,
                                                        G_PARAM_READWRITE));
  
  /**
   * MarinaItem:author:
   *
   * The name of the items author.
   */
  g_object_class_install_property (object_class,
                                   PROP_AUTHOR,
                                   g_param_spec_string ("author",
                                                        "Author",
                                                        "The author of the item",
                                                        NULL,
                                                        G_PARAM_READWRITE));

  /**
   * MarinaItem:published-at:
   *
   * The time the item was originally published.
   */
  g_object_class_install_property (object_class,
                                   PROP_PUBLISHED_AT,
                                   g_param_spec_boxed ("published_at", 
                                                       "PublishedAt",
                                                       "The time the item was published",
                                                       MARINA_TYPE_DATE,
                                                       G_PARAM_READWRITE));
}

static void
marina_item_init (MarinaItem *item)
{
  item->priv = G_TYPE_INSTANCE_GET_PRIVATE (item,
                                            MARINA_TYPE_ITEM,
                                            MarinaItemPrivate);
}

static JsonNode*
time_to_string_node (time_t t)
{
  JsonNode *node;
  GTimeVal tv = {0,0};
  gchar *str;
  
  tv.tv_sec = t;
  node = json_node_new (JSON_NODE_VALUE);
  
  str = g_time_val_to_iso8601 (&tv);
  json_node_set_string (node, str);
  g_free (str);
  
  return node;
}

static MarinaDate*
string_node_to_date (JsonNode *node)
{
  GTimeVal val = {0,0};
  const gchar *str = json_node_get_string (node);
  
  g_time_val_from_iso8601 (str, &val);
  
  return marina_date_new_from_time_t (val.tv_sec);
}

static JsonNode*
to_base64_json_string (const gchar *data)
{
  JsonNode *node;
  gchar *base64;
  
  node = json_node_new (JSON_NODE_VALUE);
  
  if (!data || g_str_equal (data, ""))
    base64 = g_strdup ("");
  else
    base64 = g_base64_encode ((guchar*)data, strlen (data) + 1);
  
  json_node_set_string (node, base64);
  
  g_free (base64);
  
  return node;
}

static JsonNode*
serialize_property (JsonSerializable *serializable,
                    const gchar      *name,
                    const GValue     *value,
                    GParamSpec       *pspec)
{
  MarinaItemPrivate *priv;
  
  g_return_val_if_fail (MARINA_IS_ITEM (serializable), FALSE);
  
  priv = MARINA_ITEM (serializable)->priv;
  
  if (g_str_equal (name, "published-at"))
    {
      if (priv->published_at)
        return time_to_string_node (priv->published_at->timet);
      else
        {
          time_t t = 0;
          return time_to_string_node (t);
        }
    }
  else if (g_str_equal (name, "updated-at"))
    {
      if (priv->updated_at)
        return time_to_string_node (priv->updated_at->timet);
      else
        {
          time_t t = 0;
          return time_to_string_node (t);
        }
    }
  else if (g_str_equal (name, "content"))
    return to_base64_json_string (priv->content);
  else if (g_str_equal (name, "title"))
    return to_base64_json_string (priv->title);
  
  return NULL; /* NULL indicates default deserializer */
}

static gboolean
deserialize_property (JsonSerializable *serializable,
                      const gchar      *name,
                      GValue           *value,
                      GParamSpec       *pspec,
                      JsonNode         *node)
{
  g_assert (node != NULL);
  
  if (g_str_equal (name, "published-at") ||
      g_str_equal (name, "updated-at"))
    {
      g_value_set_date (value, string_node_to_date (node));
      return TRUE;
    }
  else if (g_str_equal (name, "content") ||
           g_str_equal (name, "title"))
    {
      const gchar *text = json_node_get_string (node);
      gsize outlen = 0;
      
      if (!text || g_str_equal (text, ""))
        g_value_set_static_string (value, "");
      else
        {
          guchar *plain = g_base64_decode (text, &outlen);
          g_value_set_string (value, (gchar*)plain);
          g_free (plain);
        }
      
      return TRUE;
    }
  
  return FALSE; /* use default deserializer */
}

static void
json_serializable_init (JsonSerializableIface *iface)
{
  iface->serialize_property = serialize_property;
  iface->deserialize_property = deserialize_property;
}

/**
 * marina_item_new:
 *
 * Creates a new instance of the #MarinaItem class
 *
 * Return value: the new #MarinaItem
 */
MarinaItem*
marina_item_new (void)
{
  return g_object_new (MARINA_TYPE_ITEM, NULL);
}

/**
 * marina_item_get_author:
 * @item: A #MarinaItem
 *
 * Return value: the author of the item
 */
const gchar*
marina_item_get_author (MarinaItem *item)
{
  MarinaItemPrivate *priv;
  
  g_return_val_if_fail (MARINA_IS_ITEM (item), NULL);
  
  priv = item->priv;

  if (!priv->author)
    return "";
  
  return priv->author;
}

/**
 * marina_item_set_author:
 * @item: A #MarinaItem
 * @author: the new author
 *
 * Sets the author for the #MarinaItem
 */
void
marina_item_set_author (MarinaItem *item, const gchar *author)
{
  MarinaItemPrivate *priv;
  
  g_return_if_fail (MARINA_IS_ITEM (item));
  
  priv = item->priv;
  
  g_free (priv->author);
  
  priv->author = g_strdup (author);
}

/**
 * marina_item_get_data_items:
 * @item: A #MarinaItem
 *
 * Retrieves the list of #MarinaDataItem<!-- -->'s associated
 * with the #MarinaItem.  The list is copied and should be freed
 * with g_list_free() when you are done with it.
 *
 * Return value: A #GList which should be freed with g_list_copy().
 */
GList*
marina_item_get_data_items (MarinaItem *item)
{
  MarinaItemPrivate *priv;
  
  g_return_val_if_fail (MARINA_IS_ITEM (item), NULL);
  
  priv = item->priv;
  
  return g_list_copy (priv->data_items);
}

/**
 * marina_item_set_data_items:
 * @item: A #MarinaItem
 * @items: a #GList of data items or NULL
 *
 * Sets the list of #MarinaDataItem<!-- -->'s associated with the
 * #MarinaItem.
 */
void
marina_item_set_data_items (MarinaItem *item, const GList *data_items)
{
  MarinaItemPrivate *priv;
  GList *list;
  
  g_return_if_fail (MARINA_IS_ITEM (item));
  
  priv = item->priv;
  list = g_list_copy ((GList*)data_items);
  
  g_list_foreach (list, (GFunc)g_object_ref, NULL);
  g_list_foreach (priv->data_items, (GFunc)g_object_unref, NULL);
  g_list_free (priv->data_items);
  
  priv->data_items = list;
}

/**
 * marina_item_get_flag:
 * @item: A #MarinaItem
 *
 * Return value: the current flag value for the item
 */
gint
marina_item_get_flag (MarinaItem *item)
{
  MarinaItemPrivate *priv;
  
  g_return_val_if_fail (MARINA_IS_ITEM (item), 0);
  
  priv = item->priv;
  
  return priv->flag;
}

/**
 * marina_item_set_flag:
 * @item: A #MarinaItem
 * @flag: the new flag
 *
 * Sets a flag for the #MarinaItem. This is used by the UI
 * to specify various flags for an item.
 */
void
marina_item_set_flag (MarinaItem *item, gint flag)
{
  MarinaItemPrivate *priv;
  
  g_return_if_fail (MARINA_IS_ITEM (item));
  
  priv = item->priv;
  
  priv->flag = flag;
}

/**
 * marina_item_get_item_id:
 * @item: A #MarinaItem
 *
 * Return value: the unique id for this item
 */
const gchar*
marina_item_get_item_id (MarinaItem *item)
{
  MarinaItemPrivate *priv;
  
  g_return_val_if_fail (MARINA_IS_ITEM (item), NULL);
  
  priv = item->priv;
  
  if (!priv->item_id)
    return "";
  
  return priv->item_id;
}

/**
 * marina_item_set_item_id:
 * @item: A #MarinaItem
 * @item_id: the new id for the #MarinaItem
 *
 * Sets the new unique id for the #MarinaItem.
 */
void
marina_item_set_item_id (MarinaItem *item, const gchar *item_id)
{
  MarinaItemPrivate *priv;
  
  g_return_if_fail (MARINA_IS_ITEM (item));
  
  priv = item->priv;
  
  g_free (priv->item_id);
  
  priv->item_id = g_strdup (item_id);
}

/**
 * marina_item_get_link:
 * @item: A #MarinaItem
 *
 * Return value: the link or uri to the @item
 */
const gchar*
marina_item_get_link (MarinaItem *item)
{
  MarinaItemPrivate *priv;
  
  g_return_val_if_fail (MARINA_IS_ITEM (item), NULL);
  
  priv = item->priv;

  if (!priv->link)
    return "";
  
  return priv->link;
}

/**
 * marina_item_set_link:
 * @item: A #MarinaItem
 * @link: A link or uri to the item
 *
 * Sets the link or uri to the item which can typically be viewed
 * within a web page. Other uri's could also be valid such as a
 * uri to a video conference call, or an email address or a
 * jabber address as long as they are in uri form.
 */
void
marina_item_set_link (MarinaItem *item, const gchar *link)
{
  MarinaItemPrivate *priv;
  
  g_return_if_fail (MARINA_IS_ITEM (item));
  
  priv = item->priv;
  
  g_free (priv->link);
  
  priv->link = g_strdup (link);
}

/**
 * marina_item_get_published_at:
 * @item: A #MarinaItem
 *
 * Return value: a #time_t containing when the item was originally
 *   published.
 */
MarinaDate*
marina_item_get_published_at (MarinaItem *item)
{
  MarinaItemPrivate *priv;
  
  g_return_val_if_fail (MARINA_IS_ITEM (item), 0);
  
  priv = item->priv;
  
  return priv->published_at;
}

/**
 * marina_item_set_published_at:
 * @item: A #MarinaItem
 * @published_at: 
 *
 * Sets the time that the item was published.
 */
void
marina_item_set_published_at (MarinaItem       *item,
                              const MarinaDate *published_at)
{
  MarinaItemPrivate *priv;
  
  g_return_if_fail (MARINA_IS_ITEM (item));
  
  priv = item->priv;
  
  if (priv->published_at)
    marina_date_free (priv->published_at);
  
  priv->published_at = marina_date_copy (published_at);
}

/**
 * marina_item_get_read:
 * @item: A #MarinaItem
 *
 * Return value: TRUE if the item as been read
 */
gboolean
marina_item_get_read (MarinaItem *item)
{
  MarinaItemPrivate *priv;
  
  g_return_val_if_fail (MARINA_IS_ITEM (item), FALSE);
  
  priv = item->priv;
  
  return priv->read;
}

/**
 * marina_item_set_read:
 * @item: A #MarinaItem
 * @read: the read status
 *
 * Sets the read property of the #MarinaItem.
 */
void
marina_item_set_read (MarinaItem *item, gboolean read)
{
  MarinaItemPrivate *priv;
  
  g_return_if_fail (MARINA_IS_ITEM (item));
  
  priv = item->priv;
  
  priv->read = read;
}

/**
 * marina_item_get_source_id:
 * @item: A #MarinaItem
 *
 * Return value: the id of the source the item belongs to
 */
const gchar*
marina_item_get_source_id (MarinaItem *item)
{
  MarinaItemPrivate *priv;
  
  g_return_val_if_fail (MARINA_IS_ITEM (item), NULL);
  
  priv = item->priv;
  
  return priv->source_id;
}

/**
 * marina_item_set_source_id:
 * @item: A #MarinaItem
 * @source_id: the new source id
 *
 * Sets the id of the source the item belongs to
 */
void
marina_item_set_source_id (MarinaItem *item, const gchar *source_id)
{
  MarinaItemPrivate *priv;
  
  g_return_if_fail (MARINA_IS_ITEM (item));
  
  priv = item->priv;
  
  g_free (priv->source_id);
  
  priv->source_id = g_strdup (source_id);
}

/**
 * marina_item_get_title:
 * @item: A #MarinaItem
 *
 * Return value: the title of the item
 */
const gchar*
marina_item_get_title (MarinaItem *item)
{
  MarinaItemPrivate *priv;
  
  g_return_val_if_fail (MARINA_IS_ITEM (item), NULL);
  
  priv = item->priv;
  
  if (!priv->title)
    return "";
  
  return priv->title;
}

/**
 * marina_item_set_title:
 * @item: A #MarinaItem
 * @title: the new title
 *
 * Sets the title for the #MarinaItem
 */
void
marina_item_set_title (MarinaItem *item, const gchar *title)
{
  MarinaItemPrivate *priv;
  
  g_return_if_fail (MARINA_IS_ITEM (item));
  
  priv = item->priv;
  
  g_free (priv->title);
  
  priv->title = g_strdup (title);
}

/**
 * marina_item_get_updated_at:
 * @item: A #MarinaItem
 *
 * Return value: 
 */
MarinaDate*
marina_item_get_updated_at (MarinaItem *item)
{
  MarinaItemPrivate *priv;
  
  g_return_val_if_fail (MARINA_IS_ITEM (item), 0);
  
  priv = item->priv;
  
  return priv->updated_at;
}

/**
 * marina_item_set_updated_at:
 * @item: A #MarinaItem
 * @updated_at: 
 *
 * Sets the time that the #MarinaItem was last updated.
 */
void
marina_item_set_updated_at (MarinaItem       *item,
                            const MarinaDate *updated_at)
{
  MarinaItemPrivate *priv;
  
  g_return_if_fail (MARINA_IS_ITEM (item));
  
  priv = item->priv;
  
  if (priv->updated_at)
    marina_date_free (priv->updated_at);
  
  priv->updated_at = marina_date_copy (updated_at);
}

/**
 * marina_item_get_content:
 * @item: A #MarinaItem
 *
 * Return value: the content for the item.
 */
const gchar*
marina_item_get_content (MarinaItem *item)
{
  MarinaItemPrivate *priv;

  g_return_val_if_fail (MARINA_IS_ITEM (item), NULL);

  priv = item->priv;
  
  if (!priv->content)
    return "";
  
  return priv->content;
}

/**
 * marina_item_set_content:
 * @item: A #MarinaItem
 * @content: a string containing the new content
 *
 * Updates the content for @item. You should persist the item back to
 * storage using marina_items_edit_item().
 */
void
marina_item_set_content (MarinaItem  *item,
                         const gchar *content)
{
  MarinaItemPrivate *priv;
  
  g_return_if_fail (MARINA_IS_ITEM (item));
  
  priv = item->priv;
  
  g_free (priv->content);
  
  priv->content = g_strdup (content);
}
