/* marina-data-item.h
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifndef __MARINA_DATA_ITEM_H__
#define __MARINA_DATA_ITEM_H__

#include <glib-object.h>

G_BEGIN_DECLS

#define MARINA_TYPE_DATA_ITEM (marina_data_item_get_type())

#define MARINA_DATA_ITEM(obj)                  \
  (G_TYPE_CHECK_INSTANCE_CAST((obj),           \
  MARINA_TYPE_DATA_ITEM, MarinaDataItem))

#define MARINA_IS_DATA_ITEM(obj)               \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj),           \
  MARINA_TYPE_DATA_ITEM))

#define MARINA_DATA_ITEM_GET_INTERFACE(obj)    \
  (G_TYPE_INSTANCE_GET_INTERFACE((obj),        \
  MARINA_TYPE_DATA_ITEM, MarinaDataItemIface))

typedef struct _MarinaDataItem      MarinaDataItem;
typedef struct _MarinaDataItemIface MarinaDataItemIface;

struct _MarinaDataItemIface
{
    GTypeInterface parent;
    
    const gchar* (*get_id) (MarinaDataItem *data_item);
};

GType        marina_data_item_get_type (void) G_GNUC_CONST;
const gchar* marina_data_item_get_id   (MarinaDataItem *data_item);

G_END_DECLS

#endif /* __MARINA_DATA_ITEM_H__ */
