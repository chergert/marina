/* marina-items.h
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifndef __MARINA_ITEMS_H__
#define __MARINA_ITEMS_H__

#include <glib-object.h>
#include <gtk/gtk.h>

#include "marina-item.h"
#include "marina-source.h"

G_BEGIN_DECLS

#define MARINA_TYPE_ITEMS (marina_items_get_type ())

#define MARINA_ITEMS(obj)                \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj),    \
  MARINA_TYPE_ITEMS, MarinaItems))

#define MARINA_ITEMS_CONST(obj)          \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj),    \
  MARINA_TYPE_ITEMS, MarinaItems const))

#define MARINA_ITEMS_CLASS(klass)        \
  (G_TYPE_CHECK_CLASS_CAST ((klass),     \
  MARINA_TYPE_ITEMS, MarinaItemsClass))

#define MARINA_IS_ITEMS(obj)             \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj),    \
  MARINA_TYPE_ITEMS))

#define MARINA_IS_ITEMS_CLASS(klass)     \
  (G_TYPE_CHECK_CLASS_TYPE ((klass),     \
  MARINA_TYPE_ITEMS))

#define MARINA_ITEMS_GET_CLASS(obj)      \
  (G_TYPE_INSTANCE_GET_CLASS ((obj),     \
  MARINA_TYPE_ITEMS, MarinaItemsClass))

typedef struct _MarinaItems         MarinaItems;
typedef struct _MarinaItemsClass    MarinaItemsClass;
typedef struct _MarinaItemsPrivate  MarinaItemsPrivate;

struct _MarinaItems
{
  GObject parent;
  
  MarinaItemsPrivate *priv;
};

struct _MarinaItemsClass
{
  GObjectClass parent_class;
};

GType           marina_items_get_type     (void) G_GNUC_CONST;

MarinaItems*    marina_items_get_default  (void);

GtkTreeModel*   marina_items_get_model    (MarinaItems *items);

void            marina_items_shutdown     (MarinaItems *items);

MarinaItem*     marina_items_lookup       (MarinaItems *items,
                                           const gchar *source_id,
                                           const gchar *id);

void            marina_items_add_item     (MarinaItems  *items,
                                           MarinaSource *source,
                                           MarinaItem   *item);
void            marina_items_edit_item    (MarinaItems *items, 
                                           MarinaItem  *item);

void            marina_items_foreach      (MarinaItems *items,
                                           GFunc        func,
                                           gpointer     data);

G_END_DECLS

#endif /* __MARINA_ITEMS_H__ */
