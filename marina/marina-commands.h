/*
 * marina-commands.h
 * This file is part of marina; it was borrowed from gedit.
 *
 * Copyright (C) 1998, 1999 Alex Roberts, Evan Lawrence
 * Copyright (C) 2000, 2001 Chema Celorio, Paolo Maggi 
 * Copyright (C) 2002-2005 Paolo Maggi 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA 02111-1307, USA. 
 */
 
/*
 * Modified by the gedit Team, 1998-2005. See the gedit AUTHORS file for a 
 * list of people on the gedit Team.
 */

#ifndef __MARINA_COMMANDS_H__
#define __MARINA_COMMANDS_H__

#include <gtk/gtk.h>
#include <marina/marina-window.h>

G_BEGIN_DECLS

void    marina_commands_init                    (void);

void    _marina_cmd_marina_add                  (GtkAction    *action,
                                                 MarinaWindow *window);
void    _marina_cmd_marina_quit                 (GtkAction    *action,
                                                 MarinaWindow *window);
void    _marina_cmd_marina_close                (GtkAction    *action,
                                                 MarinaWindow *window);

void    _marina_cmd_edit_preferences            (GtkAction    *action,
                                                 MarinaWindow *window);

void    _marina_cmd_view_show_toolbar           (GtkAction    *action,
                                                 MarinaWindow *window);
void    _marina_cmd_view_show_statusbar         (GtkAction    *action,
                                                 MarinaWindow *window);
void    _marina_cmd_view_show_side_pane         (GtkAction    *action,
                                                 MarinaWindow *window);
void    _marina_cmd_view_show_bottom_pane       (GtkAction    *action,
                                                 MarinaWindow *window);
void    _marina_cmd_view_toggle_fullscreen_mode (GtkAction    *action,
                                                 MarinaWindow *window);
void    _marina_cmd_view_leave_fullscreen_mode  (GtkAction    *action,
                                                 MarinaWindow *window);
void    _marina_cmd_view_toggle_compact_mode    (GtkAction    *action,
                                                 MarinaWindow *window);

void    _marina_cmd_search_init                 (void);
void    _marina_cmd_search_find                 (GtkAction    *action,
                                                 MarinaWindow *window);
void    _marina_cmd_search_find_next            (GtkAction    *action,
                                                 MarinaWindow *window);
void    _marina_cmd_search_find_prev            (GtkAction    *action,
                                                 MarinaWindow *window);
void    _marina_cmd_search_clear_highlight      (GtkAction    *action,
                                                 MarinaWindow *window);
void    _marina_cmd_search_incremental_search   (GtkAction    *action,
                                                 MarinaWindow *window);

void    _marina_cmd_source_refresh_all          (GtkAction    *action,
                                                 MarinaWindow *window);
void    _marina_cmd_source_refresh              (GtkAction    *action,
                                                 MarinaWindow *window);
void    _marina_cmd_source_mark_all_read        (GtkAction    *action,
                                                 MarinaWindow *window);
void    _marina_cmd_source_remove               (GtkAction    *action,
                                                 MarinaWindow *window);

void    _marina_cmd_help_contents               (GtkAction    *action,
                                                 MarinaWindow *window);
void    _marina_cmd_help_about                  (GtkAction    *action,
                                                 MarinaWindow *window);

void    _marina_cmd_item_move_next              (GtkAction    *action,
                                                 MarinaWindow *window);
void    _marina_cmd_item_move_prev              (GtkAction    *action,
                                                 MarinaWindow *window);
void    _marina_cmd_item_mark_read              (GtkAction    *action,
                                                 MarinaWindow *window);

G_END_DECLS

#endif /* __MARINA_COMMANDS_H__ */ 
