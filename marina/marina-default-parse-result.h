/* marina-default-parse-result.h
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifndef __MARINA_DEFAULT_PARSE_RESULT_H__
#define __MARINA_DEFAULT_PARSE_RESULT_H__

#include <glib-object.h>
#include <rss-glib/rss-glib.h>

#include "marina-parse-result.h"

G_BEGIN_DECLS

#define MARINA_TYPE_DEFAULT_PARSE_RESULT            \
  (marina_default_parse_result_get_type ())

#define MARINA_DEFAULT_PARSE_RESULT(obj)            \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj),               \
  MARINA_TYPE_DEFAULT_PARSE_RESULT,                 \
  MarinaDefaultParseResult))

#define MARINA_DEFAULT_PARSE_RESULT_CONST(obj)      \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj),               \
  MARINA_TYPE_DEFAULT_PARSE_RESULT,                 \
  MarinaDefaultParseResult const))

#define MARINA_DEFAULT_PARSE_RESULT_CLASS(klass)    \
  (G_TYPE_CHECK_CLASS_CAST ((klass),                \
  MARINA_TYPE_DEFAULT_PARSE_RESULT,                 \
  MarinaDefaultParseResultClass))

#define MARINA_IS_DEFAULT_PARSE_RESULT(obj)         \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj),               \
  MARINA_TYPE_DEFAULT_PARSE_RESULT))

#define MARINA_IS_DEFAULT_PARSE_RESULT_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass),                \
  MARINA_TYPE_DEFAULT_PARSE_RESULT))

#define MARINA_DEFAULT_PARSE_RESULT_GET_CLASS(obj)  \
  (G_TYPE_INSTANCE_GET_CLASS ((obj),                \
  MARINA_TYPE_DEFAULT_PARSE_RESULT,                 \
  MarinaDefaultParseResultClass))

typedef struct _MarinaDefaultParseResult        MarinaDefaultParseResult;
typedef struct _MarinaDefaultParseResultClass   MarinaDefaultParseResultClass;
typedef struct _MarinaDefaultParseResultPrivate MarinaDefaultParseResultPrivate;

struct _MarinaDefaultParseResult
{
  GObject parent;
  
  MarinaDefaultParseResultPrivate *priv;
};

struct _MarinaDefaultParseResultClass
{
  GObjectClass parent_class;
};

GType              marina_default_parse_result_get_type (void) G_GNUC_CONST;
MarinaParseResult* marina_default_parse_result_new      (RssDocument *document);

G_END_DECLS

#endif /* __MARINA_DEFAULT_PARSE_RESULT_H__ */
