/* marina-folder-source.h
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with This; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifndef __MARINA_FOLDER_SOURCE_H__
#define __MARINA_FOLDER_SOURCE_H__

#include <glib-object.h>

#include "marina-source.h"
#include "marina-source-base.h"

G_BEGIN_DECLS

#define MARINA_TYPE_FOLDER_SOURCE (marina_folder_source_get_type ())

#define MARINA_FOLDER_SOURCE(obj)              \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj),          \
  MARINA_TYPE_FOLDER_SOURCE,                   \
  MarinaFolderSource))

#define MARINA_FOLDER_SOURCE_CONST(obj)        \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj),          \
  MARINA_TYPE_FOLDER_SOURCE,                   \
  MarinaFolderSource const))

#define MARINA_FOLDER_SOURCE_CLASS(klass)      \
  (G_TYPE_CHECK_CLASS_CAST ((klass),           \
  MARINA_TYPE_FOLDER_SOURCE,                   \
  MarinaFolderSourceClass))

#define MARINA_IS_FOLDER_SOURCE(obj)          \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj),         \
  MARINA_TYPE_FOLDER_SOURCE))

#define MARINA_IS_FOLDER_SOURCE_CLASS(klass)  \
  (G_TYPE_CHECK_CLASS_TYPE ((klass),          \
  MARINA_TYPE_FOLDER_SOURCE))

#define MARINA_FOLDER_SOURCE_GET_CLASS(obj)   \
  (G_TYPE_INSTANCE_GET_CLASS ((obj),          \
  MARINA_TYPE_FOLDER_SOURCE,                  \
  MarinaFolderSourceClass))

typedef struct _MarinaFolderSource         MarinaFolderSource;
typedef struct _MarinaFolderSourceClass    MarinaFolderSourceClass;
typedef struct _MarinaFolderSourcePrivate  MarinaFolderSourcePrivate;

struct _MarinaFolderSource
{
  MarinaSourceBase parent;
  
  MarinaFolderSourcePrivate *priv;
};

struct _MarinaFolderSourceClass
{
  MarinaSourceBaseClass parent_class;
};

GType             marina_folder_source_get_type     (void) G_GNUC_CONST;
MarinaSource*     marina_folder_source_new          (void);

void              marina_folder_source_append_child (MarinaFolderSource *source,
                                                     MarinaSource       *child);
void              marina_folder_source_remove_child (MarinaFolderSource *source,
                                                     MarinaSource       *child);

G_END_DECLS

#endif /* __MARINA_FOLDER_SOURCE_H__ */
