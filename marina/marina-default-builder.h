/* marina-default-builder.h
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifndef __MARINA_DEFAULT_BUILDER_H__
#define __MARINA_DEFAULT_BUILDER_H__

#include <glib-object.h>

#include "marina-builder.h"

G_BEGIN_DECLS

#define MARINA_TYPE_DEFAULT_BUILDER (marina_default_builder_get_type ())

#define MARINA_DEFAULT_BUILDER(obj)              \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj),            \
  MARINA_TYPE_DEFAULT_BUILDER,                   \
  MarinaDefaultBuilder))

#define MARINA_DEFAULT_BUILDER_CONST(obj)        \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj),            \
  MARINA_TYPE_DEFAULT_BUILDER,                   \
  MarinaDefaultBuilder const))

#define MARINA_DEFAULT_BUILDER_CLASS(klass)      \
  (G_TYPE_CHECK_CLASS_CAST ((klass),             \
  MARINA_TYPE_DEFAULT_BUILDER,                   \
  MarinaDefaultBuilderClass))

#define MARINA_IS_DEFAULT_BUILDER(obj)          \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj),           \
  MARINA_TYPE_DEFAULT_BUILDER))

#define MARINA_IS_DEFAULT_BUILDER_CLASS(klass)  \
  (G_TYPE_CHECK_CLASS_TYPE ((klass),            \
  MARINA_TYPE_DEFAULT_BUILDER))

#define MARINA_DEFAULT_BUILDER_GET_CLASS(obj)   \
  (G_TYPE_INSTANCE_GET_CLASS ((obj),            \
  MARINA_TYPE_DEFAULT_BUILDER,                  \
  MarinaDefaultBuilderClass))

typedef struct _MarinaDefaultBuilder         MarinaDefaultBuilder;
typedef struct _MarinaDefaultBuilderClass    MarinaDefaultBuilderClass;
typedef struct _MarinaDefaultBuilderPrivate  MarinaDefaultBuilderPrivate;

struct _MarinaDefaultBuilder
{
  GObject parent;
  
  MarinaDefaultBuilderPrivate *priv;
};

struct _MarinaDefaultBuilderClass
{
  GObjectClass parent_class;
};

GType                 marina_default_builder_get_type (void) G_GNUC_CONST;
MarinaDefaultBuilder* marina_default_builder_new      (void);

G_END_DECLS

#endif /* __MARINA_DEFAULT_BUILDER_H__ */
