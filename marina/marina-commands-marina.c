/* marina-commands-marina.c
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gtk/gtk.h>

#include "dialogs/marina-add-source-dialog.h"
#include "marina-app.h"
#include "marina-commands.h"
#include "marina-debug.h"
#include "marina-window.h"

void
_marina_cmd_marina_add (GtkAction    *action,
                        MarinaWindow *window)
{
  GtkWidget *dialog;
  
  marina_debug (DEBUG_COMMANDS);
  
  dialog = marina_add_source_dialog_new ();
  gtk_widget_show (dialog);
}

void
_marina_cmd_marina_quit (GtkAction    *action,
                         MarinaWindow *window)
{
  marina_app_quit (marina_app_get_default (), FALSE);
}

void
_marina_cmd_marina_close (GtkAction    *action,
                          MarinaWindow *window)
{
  /* simply try to quit the app since we are a single
   * window application. if a plugin does not want the
   * application to exit, it can inhibit.
   */
  marina_app_quit (marina_app_get_default (), TRUE);
}
