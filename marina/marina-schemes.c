/* marina-schemes.c
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

/**
 * SECTION:marina-schemes
 * @short_description: Access to color and layout schemes for content.
 *
 * Marina schemes are used to display item content. They control the
 * colors as well as the layout itself.
 */

#include <glib/gi18n.h>

#include "marina-dirs.h"
#include "marina-prefs.h"
#include "marina-schemes.h"

struct _MarinaSchemesPrivate
{
  GList *schemes;
  gchar *active;
};

enum
{
  CHANGED,

  LAST_SIGNAL
};

static guint schemes_signals [LAST_SIGNAL];

G_DEFINE_TYPE (MarinaSchemes, marina_schemes, G_TYPE_OBJECT);

static void
marina_schemes_finalize (GObject *object)
{
  G_OBJECT_CLASS (marina_schemes_parent_class)->finalize (object);
}

static void
marina_schemes_class_init (MarinaSchemesClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  
  object_class->finalize = marina_schemes_finalize;

  g_type_class_add_private (object_class, sizeof (MarinaSchemesPrivate));

  /**
   * MarinaSchemes::changed:
   * @schemes: the #MarinaSchemes on which the signal is emitted
   * @name: the name of the new active scheme
   *
   * This signal is emitted when the active scheme is changed.
   */
  schemes_signals [CHANGED] =
    g_signal_new ("changed",
                  MARINA_TYPE_SCHEMES,
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL, NULL,
                  g_cclosure_marshal_VOID__STRING,
                  G_TYPE_NONE, 1,
                  G_TYPE_STRING);
}

static void
marina_schemes_init (MarinaSchemes *schemes)
{
  GDir *dir;
  gchar *data_dir;
  gchar *schemes_dir;
  gchar *path;
  const gchar *name;
  GList *list = NULL;
  
  schemes->priv = G_TYPE_INSTANCE_GET_PRIVATE (schemes,
                                               MARINA_TYPE_SCHEMES,
                                               MarinaSchemesPrivate);
  
  /* build path to schemes */
  data_dir = marina_dirs_get_marina_data_dir ();
  schemes_dir = g_build_filename (data_dir, "schemes", NULL);
  g_free (data_dir);
  
  /* load list of available schemes */
  dir = g_dir_open (schemes_dir, 0, NULL);
  
  if (dir)
    {
      while ((name = g_dir_read_name (dir)) != NULL)
        {
          path = g_build_filename (schemes_dir, name, NULL);
          
          if (g_file_test (path, G_FILE_TEST_IS_DIR))
            list = g_list_prepend (list, g_strdup (name));
          
          g_free (path);
        }
    }
  
  g_dir_close (dir);
  g_free (schemes_dir);
  
  schemes->priv->schemes = list;

  /* So we do not leak, look for our private string version
   * matching the default. */
  gchar *active = marina_prefs_get_scheme ();
  GList *iter;

  for (iter = schemes->priv->schemes; iter; iter = iter->next)
    {
      if (g_str_equal (active, iter->data))
        {
          schemes->priv->active = iter->data;
          break;
        }
    }
}

/**
 * marina_schemes_get_default:
 *
 * Retrieves the singleton instance of #MarinaSchemes
 *
 * Return value: The #MarinaSchemes.
 */
MarinaSchemes*
marina_schemes_get_default (void)
{
  static MarinaSchemes *instance = NULL;
  static GStaticMutex mutex  = G_STATIC_MUTEX_INIT;

  if (!instance)
    {
      g_static_mutex_lock (&mutex);
      
      if (!instance)
        instance = g_object_new (MARINA_TYPE_SCHEMES, NULL);
      
      g_static_mutex_unlock (&mutex);
    }

  return instance;
}

/**
 * marina_schemes_get_list:
 * @schemes: A #MarinaSchemes
 *
 * Retrieves the list of schemes available for the user to chose from.
 */
const GList*
marina_schemes_get_list (MarinaSchemes *schemes)
{
  MarinaSchemesPrivate *priv;
  
  g_return_val_if_fail (MARINA_IS_SCHEMES (schemes), NULL);
  
  priv = schemes->priv;
  
  return priv->schemes;
}

/**
 * marina_schemes_get_active:
 * @schemes: A #MarinaSchemes
 *
 * Retrieves the currently selected scheme. The value should not be
 * modified or freed.
 *
 * Return value: The selected scheme
 */
const gchar*
marina_schemes_get_active (MarinaSchemes *schemes)
{
  MarinaSchemesPrivate *priv;

  g_return_val_if_fail (MARINA_IS_SCHEMES (schemes), NULL);

  priv = schemes->priv;

  return priv->active;
}

/**
 * marina_schemes_set_active:
 * @schemes: A #MarinaSchemes
 *
 * Sets the active scheme.
 */
void
marina_schemes_set_active (MarinaSchemes *schemes,
                           const gchar   *active)
{
  MarinaSchemesPrivate *priv;
  GList                *iter;

  g_return_if_fail (MARINA_IS_SCHEMES (schemes));
  g_return_if_fail (active != NULL);

  priv = schemes->priv;

  for (iter = priv->schemes; iter; iter = iter->next)
    {
      if (g_str_equal (active, iter->data))
        {
          priv->active = iter->data;
          g_signal_emit (schemes, schemes_signals [CHANGED], 0, iter->data);
          if (marina_prefs_scheme_can_set ())
            marina_prefs_set_scheme (active);
          break;
        }
    }
}
