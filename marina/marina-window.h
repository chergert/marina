/* marina-window.h
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA 02111-1307, USA. 
 */

#ifndef __MARINA_WINDOW_H__
#define __MARINA_WINDOW_H__

#include <glib-object.h>
#include <gtk/gtk.h>

#include "marina-progress.h"
#include "marina-source-view.h"

G_BEGIN_DECLS

#define MARINA_TYPE_WINDOW (marina_window_get_type ())

#define MARINA_WINDOW(obj)                \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj),     \
  MARINA_TYPE_WINDOW, MarinaWindow))

#define MARINA_WINDOW_CONST(obj)          \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj),     \
  MARINA_TYPE_WINDOW, MarinaWindow const))

#define MARINA_WINDOW_CLASS(klass)        \
  (G_TYPE_CHECK_CLASS_CAST ((klass),      \
  MARINA_TYPE_WINDOW, MarinaWindowClass))

#define MARINA_IS_WINDOW(obj)             \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj),     \
  MARINA_TYPE_WINDOW))

#define MARINA_IS_WINDOW_CLASS(klass)     \
  (G_TYPE_CHECK_CLASS_TYPE ((klass),      \
  MARINA_TYPE_WINDOW))

#define MARINA_WINDOW_GET_CLASS(obj)      \
  (G_TYPE_INSTANCE_GET_CLASS ((obj),      \
  MARINA_TYPE_WINDOW, MarinaWindowClass))

typedef struct _MarinaWindow        MarinaWindow;
typedef struct _MarinaWindowClass   MarinaWindowClass;
typedef struct _MarinaWindowPrivate MarinaWindowPrivate;

struct _MarinaWindow
{
  GtkWindow window;
  
  MarinaWindowPrivate *priv;
};

struct _MarinaWindowClass
{
  GtkWindowClass parent_class;
};

GType          marina_window_get_type            (void) G_GNUC_CONST;

MarinaWindow*  marina_window_new                 (void);

GtkUIManager*  marina_window_get_ui_manager      (MarinaWindow *window);

GList*         marina_window_get_source_views    (MarinaWindow     *window);
void           marina_window_add_source_view     (MarinaWindow     *window,
                                                  MarinaSourceView *source_view);
void           marina_window_remove_source_view  (MarinaWindow     *window,
                                                  MarinaSourceView *source_view);
void           marina_window_set_source_view     (MarinaWindow     *window,
                                                  MarinaSourceView *source_view);
MarinaSourceView* marina_window_get_source_view  (MarinaWindow     *window);

GtkWidget*     marina_window_get_statusbar       (MarinaWindow     *window);

MarinaSource*  marina_window_get_selected_source (MarinaWindow     *window);
void           marina_window_set_selected_source (MarinaWindow     *window,
                                                  MarinaSource     *source);

MarinaProgress* marina_window_get_progress       (MarinaWindow     *window,
                                                  const gchar      *name);
void            marina_window_set_progress       (MarinaWindow     *window,
                                                  const gchar      *name,
                                                  MarinaProgress   *progress);

G_END_DECLS

#endif /* __MARINA_WINDOW_H__ */
