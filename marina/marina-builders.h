/* marina-builders.h
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifndef __MARINA_BUILDERS_H__
#define __MARINA_BUILDERS_H__

#include <glib-object.h>

#include "marina-builder.h"

G_BEGIN_DECLS

#define MARINA_TYPE_BUILDERS (marina_builders_get_type ())

#define MARINA_BUILDERS(obj)              \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj),     \
  MARINA_TYPE_BUILDERS, MarinaBuilders))

#define MARINA_BUILDERS_CONST(obj)        \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj),     \
  MARINA_TYPE_BUILDERS,                   \
  MarinaBuilders const))

#define MARINA_BUILDERS_CLASS(klass)      \
  (G_TYPE_CHECK_CLASS_CAST ((klass),      \
  MARINA_TYPE_BUILDERS,                   \
  MarinaBuildersClass))

#define MARINA_IS_BUILDERS(obj)          \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj),    \
  MARINA_TYPE_BUILDERS))

#define MARINA_IS_BUILDERS_CLASS(klass)  \
  (G_TYPE_CHECK_CLASS_TYPE ((klass),     \
  MARINA_TYPE_BUILDERS))

#define MARINA_BUILDERS_GET_CLASS(obj)   \
  (G_TYPE_INSTANCE_GET_CLASS ((obj),     \
  MARINA_TYPE_BUILDERS,                  \
  MarinaBuildersClass))

typedef struct _MarinaBuilders         MarinaBuilders;
typedef struct _MarinaBuildersClass    MarinaBuildersClass;
typedef struct _MarinaBuildersPrivate  MarinaBuildersPrivate;

struct _MarinaBuilders
{
  GObject parent;
  
  MarinaBuildersPrivate *priv;
};

struct _MarinaBuildersClass
{
  GObjectClass parent_class;
};

GType           marina_builders_get_type    (void) G_GNUC_CONST;
MarinaBuilders* marina_builders_get_default (void);
GList*          marina_builders_get_list    (MarinaBuilders *builders);
void            marina_builders_register    (MarinaBuilders *builders,
                                             MarinaBuilder  *builder);
void            marina_builders_unregister  (MarinaBuilders *builders,
                                             MarinaBuilder  *builder);

G_END_DECLS

#endif /* __MARINA_BUILDERS_H__ */
