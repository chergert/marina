/* marina-parse-result.h
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifndef __MARINA_PARSE_RESULT_H__
#define __MARINA_PARSE_RESULT_H__

#include <glib-object.h>

G_BEGIN_DECLS

#define MARINA_TYPE_PARSE_RESULT (marina_parse_result_get_type())

#define MARINA_PARSE_RESULT(obj)                \
  (G_TYPE_CHECK_INSTANCE_CAST((obj),            \
  MARINA_TYPE_PARSE_RESULT, MarinaParseResult))

#define MARINA_IS_PARSE_RESULT(obj)             \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj),            \
  MARINA_TYPE_PARSE_RESULT))

#define MARINA_PARSE_RESULT_GET_INTERFACE(obj)  \
  (G_TYPE_INSTANCE_GET_INTERFACE((obj),         \
  MARINA_TYPE_PARSE_RESULT,                     \
  MarinaParseResultIface))

typedef struct _MarinaParseResult      MarinaParseResult;
typedef struct _MarinaParseResultIface MarinaParseResultIface;

struct _MarinaParseResultIface
{
    GTypeInterface parent;
    
    void         (*get_attr)   (MarinaParseResult *parse_result,
                                const gchar       *attr,
                                GValue            *value);
    GList*       (*get_attrs)  (MarinaParseResult *parse_result);
    const gchar* (*get_id)     (MarinaParseResult *parse_result);
    gboolean     (*move_first) (MarinaParseResult *parse_result);
    gboolean     (*move_next)  (MarinaParseResult *parse_result);
};

GType marina_parse_result_get_type (void) G_GNUC_CONST;

void         marina_parse_result_get_attr   (MarinaParseResult *parse_result,
                                             const gchar       *attr,
                                             GValue            *value);
GList*       marina_parse_result_get_attrs  (MarinaParseResult *parse_result);
const gchar* marina_parse_result_get_id     (MarinaParseResult *parse_result);
gboolean     marina_parse_result_has_attr   (MarinaParseResult *parse_result,
                                             const gchar       *attr);
gboolean     marina_parse_result_move_first (MarinaParseResult *parse_result);
gboolean     marina_parse_result_move_next  (MarinaParseResult *parse_result);

G_END_DECLS

#endif /* __MARINA_PARSE_RESULT_H__ */
