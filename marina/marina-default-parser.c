/* marina-default-parser.c
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#include <gio/gio.h>
#include <rss-glib/rss-glib.h>

#include "marina-debug.h"
#include "marina-default-parser.h"
#include "marina-default-parse-result.h"
#include "marina-source.h"

#define CHUNK_SIZE 4096

struct _MarinaDefaultParserPrivate
{
  gpointer dummy;
};

static void marina_parser_init (MarinaParserIface *iface);

G_DEFINE_TYPE_EXTENDED (MarinaDefaultParser,
                        marina_default_parser,
                        G_TYPE_OBJECT,
                        0,
                        G_IMPLEMENT_INTERFACE (MARINA_TYPE_PARSER,
                                               marina_parser_init));

static void
marina_default_parser_finalize (GObject *object)
{
  G_OBJECT_CLASS (marina_default_parser_parent_class)->finalize (object);
}

static void
marina_default_parser_class_init (MarinaDefaultParserClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  
  object_class->finalize = marina_default_parser_finalize;

  g_type_class_add_private (object_class, sizeof (MarinaDefaultParserPrivate));
}

static gboolean
can_parse (MarinaParser *parser,
           MarinaSource *source,
           GInputStream *stream)
{
  /* NOTE: Just return true for now, but how should we really determine
   *       if we can parse the source? Reading the data increments the
   *       stream, so we will need to check to make sure its seekable.
   */
  
  return TRUE;
}

static MarinaParseResult*
parse (MarinaParser  *parser,
       MarinaSource  *source,
       GInputStream  *stream,
       GError       **error)
{
  MarinaDefaultParserPrivate *priv;
  GMemoryOutputStream        *memstream;
  RssParser                  *rss_parser;
  RssDocument                *rss_doc;
  gchar                       buffer[CHUNK_SIZE];
  gssize                      count;
  gboolean                    success = FALSE;
  
  g_return_val_if_fail (MARINA_IS_DEFAULT_PARSER (parser), NULL);
  g_return_val_if_fail (MARINA_IS_SOURCE (source), NULL);
  g_return_val_if_fail (stream != NULL, NULL);
  
  priv = MARINA_DEFAULT_PARSER (parser)->priv;
  
  /* read all of the stream to a buffer */
  memstream = G_MEMORY_OUTPUT_STREAM (g_memory_output_stream_new (NULL, 0, g_realloc, g_free));
  
  while ((count = g_input_stream_read (stream,
                                       &buffer [0],
                                       sizeof (buffer),
                                       NULL,
                                       NULL)) > 0)
    {
      if (!buffer [0])
        break;
      
      /* NOTE: If this wasn't a memory stream, we'd need to check
       *       the number of bytes written. */
      g_output_stream_write (G_OUTPUT_STREAM (memstream), &buffer [0], count, NULL, NULL);
    }
  
  rss_parser = rss_parser_new ();
  
  marina_debug_message (DEBUG_PARSERS,
                        "Memory stream length %lu",
                        g_memory_output_stream_get_size (memstream));
  
  if (!rss_parser_load_from_data (rss_parser,
                                  g_memory_output_stream_get_data (memstream),
                                  g_memory_output_stream_get_size (memstream),
                                  error))
    goto cleanup;
  
  rss_doc = rss_parser_get_document (rss_parser);
  success = TRUE;
  
cleanup:
  g_object_unref (memstream);
  g_object_unref (rss_parser);

  if (!success)
    return NULL;
  
  /* create result navigator using rss_doc */
  return marina_default_parse_result_new (rss_doc);
}

static void
marina_parser_init (MarinaParserIface *iface)
{
  iface->can_parse = can_parse;
  iface->parse = parse;
}

static void
marina_default_parser_init (MarinaDefaultParser *parser)
{
  parser->priv = G_TYPE_INSTANCE_GET_PRIVATE (parser,
                                              MARINA_TYPE_DEFAULT_PARSER,
                                              MarinaDefaultParserPrivate);
}

MarinaParser*
marina_default_parser_new (void)
{
  return MARINA_PARSER (g_object_new (MARINA_TYPE_DEFAULT_PARSER, NULL));
}
