/* marina-commands-source.c
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gtk/gtk.h>

#include "marina-app.h"
#include "marina-commands.h"
#include "marina-debug.h"
#include "marina-items.h"
#include "marina-sources.h"
#include "marina-sync.h"
#include "marina-window.h"

void
_marina_cmd_source_refresh_all (GtkAction    *action,
                                MarinaWindow *window)
{
  MarinaSources *sources;
  
	marina_debug (DEBUG_COMMANDS);
  
	sources = marina_sources_get_default ();
	marina_sources_foreach (sources, (GFunc)marina_sync_source, NULL);
}

void
_marina_cmd_source_refresh (GtkAction    *action,
                            MarinaWindow *window)
{
  MarinaSource *source;
  
	marina_debug (DEBUG_COMMANDS);
	
	source = marina_window_get_selected_source (window);
  
  if (source)
    marina_sync_source (source);
}

static void
mark_read_cb (MarinaItem   *item,
              MarinaSource *source)
{
  g_return_if_fail (item != NULL);
  g_return_if_fail (source != NULL);
  
  if ((g_strcmp0 (marina_item_get_source_id (item),
                 marina_source_get_id (source)) == 0)
      && !marina_item_get_read (item))
    {
      g_object_set (item, "read", TRUE, NULL);
      marina_items_edit_item (marina_items_get_default (), item);
    }
}

void
_marina_cmd_source_mark_all_read (GtkAction    *action,
                                  MarinaWindow *window)
{
  MarinaSource *source;
  
  source = marina_window_get_selected_source (window);
  
  if (source)
    {
      marina_items_foreach (marina_items_get_default (),
                            (GFunc)mark_read_cb, source);
      
      /* hack, update the sources stat */
      g_object_set_data (G_OBJECT (source), "sources-unread-count", NULL);
    }

  /* force update on gtk-tree-view for sources to re-render */  
  gtk_widget_queue_draw (GTK_WIDGET (window));
}

void
_marina_cmd_source_remove (GtkAction    *action,
                           MarinaWindow *window)
{
  MarinaSource *source;

  source = marina_window_get_selected_source (window);

  if (source)
    marina_sources_remove (marina_sources_get_default (), source);
}
