/* marina-debug.c
 * 
 * This file is part of marina; it was borrowed from gedit
 *
 * Copyright (C) 1998, 1999 Alex Roberts, Evan Lawrence
 * Copyright (C) 2000, 2001 Chema Celorio, Paolo Maggi
 * Copyright (C) 2002 - 2005 Paolo Maggi  
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA 02111-1307, USA.
 */

/*
 * Modified by the gedit Team, 1998-2005. See the gedit AUTHORS file for a 
 * list of people on the gedit Team.  
 *
 * Modified by the marina Team, 2009.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include "marina-debug.h"

#ifdef ENABLE_PROFILING
static GTimer *timer = NULL;
static gdouble last = 0.0;
#endif

static MarinaDebugSection debug = MARINA_NO_DEBUG;

void
marina_debug_init ()
{
  if (g_getenv ("MARINA_DEBUG") != NULL)
  {
    /* enable all debugging */
    debug = ~MARINA_NO_DEBUG;
    goto out;
  }

  if (g_getenv ("MARINA_DEBUG_SEARCH") != NULL)
    debug = debug | MARINA_DEBUG_SEARCH;
  if (g_getenv ("MARINA_DEBUG_PREFS") != NULL)
    debug = debug | MARINA_DEBUG_PREFS;
  if (g_getenv ("MARINA_DEBUG_PLUGINS") != NULL)
    debug = debug | MARINA_DEBUG_PLUGINS;
  if (g_getenv ("MARINA_DEBUG_APP") != NULL)
    debug = debug | MARINA_DEBUG_APP;
  if (g_getenv ("MARINA_DEBUG_WINDOW") != NULL)
    debug = debug | MARINA_DEBUG_WINDOW;
  if (g_getenv ("MARINA_DEBUG_SOURCES") != NULL)
    debug = debug | MARINA_DEBUG_SOURCES;
  if (g_getenv ("MARINA_DEBUG_PARSERS") != NULL)
    debug = debug | MARINA_DEBUG_PARSERS;
  if (g_getenv ("MARINA_DEBUG_BUILDERS") != NULL)
    debug = debug | MARINA_DEBUG_BUILDERS;
  if (g_getenv ("MARINA_DEBUG_ITEMS") != NULL)
    debug = debug | MARINA_DEBUG_ITEMS;

out:    

#ifdef ENABLE_PROFILING
  if (debug != MARINA_NO_DEBUG)
    timer = g_timer_new ();
#endif
  return;
}

void
marina_debug_message (MarinaDebugSection  section,
                      const gchar       *file,
                      gint               line,
                      const gchar       *function,
                      const gchar       *format, ...)
{
  if (G_UNLIKELY (debug & section))
  {  
#ifdef ENABLE_PROFILING
    gdouble seconds;
#endif

    va_list args;
    gchar *msg;

    g_return_if_fail (format != NULL);

    va_start (args, format);
    msg = g_strdup_vprintf (format, args);
    va_end (args);

#ifdef ENABLE_PROFILING
    g_return_if_fail (timer != NULL);

    seconds = g_timer_elapsed (timer, NULL);
    g_print ("[%f (%f)] %s:%d (%s) %s\n", 
       seconds, seconds - last,  file, line, function, msg);
    last = seconds;       
#else
    g_print ("%s:%d (%s) %s\n", file, line, function, msg);  
#endif

    fflush (stdout);

    g_free (msg);
  }
}

void marina_debug (MarinaDebugSection  section,
                   const gchar       *file,
                   gint               line,
                   const gchar       *function)
{
  if (G_UNLIKELY (debug & section))
  {
#ifdef ENABLE_PROFILING
    gdouble seconds;

    g_return_if_fail (timer != NULL);

    seconds = g_timer_elapsed (timer, NULL);    
    g_print ("[%f (%f)] %s:%d (%s)\n", 
       seconds, seconds - last, file, line, function);
    last = seconds;
#else
    g_print ("%s:%d (%s)\n", file, line, function);
#endif    
    fflush (stdout);
  }
}
