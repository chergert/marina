/*
 * marina.c
 * This file is part of marina.
 *
 * Copyright (C) 2009 - Christian Hergert <chris@dronelabs.com>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with This; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdlib.h>

#include <glib.h>
#include <glib/gi18n.h>
#include <gtk/gtk.h>

#include "marina-app.h"
#include "marina-debug.h"
#include "marina-dirs.h"

#include "bacon-message-connection.h"

static void show_version_and_quit (void);

static BaconMessageConnection *connection = NULL;

static const GOptionEntry options [] =
{
	{ "version", 'V', G_OPTION_FLAG_NO_ARG, G_OPTION_ARG_CALLBACK,
	  show_version_and_quit, N_("Show the application's version"), NULL },

	{ NULL }
};

static void
show_version_and_quit (void)
{
	g_print ("%s - Version %s\n", g_get_application_name (), VERSION);
	exit (EXIT_SUCCESS);
}

static void
eat_bacon (const gchar *message, gpointer data)
{
	MarinaApp    *app    = marina_app_get_default ();
	MarinaWindow *window = marina_app_get_window (app);

	/* TODO: Check for command type. Only show_window is currently
	 *   supported, so we will skip that for now.
	 */

	gtk_window_present (GTK_WINDOW (window));
}

gint
main (gint argc, gchar **argv)
{
	GOptionContext *context;
	MarinaApp *app;
	GError *error = NULL;
	gchar *dir;
	gchar *icon_dir;
	
	/* initialize threading */
	g_thread_init (NULL);
	
	/* initialize debugging */
	marina_debug_init ();
	marina_debug_message (DEBUG_APP, "Marina starting");
	
	/* initialize i18n/l10n */
	dir = marina_dirs_get_marina_data_dir ();
	bindtextdomain (GETTEXT_PACKAGE, dir);
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);
	g_free (dir);
	
	/* parse command line options */
	context = g_option_context_new (_("- Read syndicated news"));
	g_option_context_add_main_entries (context, options, GETTEXT_PACKAGE);
	g_option_context_add_group (context, gtk_get_option_group (FALSE));
	
	gtk_init (&argc, &argv);
	
	if (!g_option_context_parse (context, &argc, &argv, &error))
	{
		g_printerr (_("%s\nRun '%s --help' to see a full list of "
		            "available command line options.\n"),
		            error->message, argv [0]);
		g_error_free (error);
		return EXIT_FAILURE;
	}
	
	/* Marina is a single instance application, if we find an existing  *
	 * process lets wake him up and then exit.                          */
	marina_debug_message (DEBUG_APP, "Creating bacon connection");
	connection = bacon_message_connection_new ("marina");
	
	if (connection != NULL)
	{
		if (!bacon_message_connection_get_is_server (connection))
		{
			marina_debug_message (DEBUG_APP, "Existing process found");
			bacon_message_connection_send (connection, "show_window");
			
			/* let startup notification know we are complete */
			gdk_notify_startup_complete ();
			
			bacon_message_connection_free (connection);
			return EXIT_SUCCESS;
		}
		else
		{
			marina_debug_message (DEBUG_APP, "No existing process found");
			bacon_message_connection_set_callback (connection, eat_bacon, NULL);
		}
	}
	else
	{
		g_warning ("Could not create 'marina' message connection. "
		           "Unexpected results may occur if multiple marina "
		           "instances run concurrently.");
	}
	
	/* configure icon theming */
	marina_debug_message (DEBUG_APP, "Adjusting icon-theme paths");
	
	dir = marina_dirs_get_marina_data_dir ();
	icon_dir = g_build_filename (dir, "icons", NULL);
	gtk_icon_theme_append_search_path (gtk_icon_theme_get_default (), icon_dir);
	g_free (dir);
	g_free (icon_dir);
	
	/* set the name and icon */
	g_set_application_name ("marina");
	gtk_window_set_default_icon_name ("marina-icon");
	
	/* start the MarinaApp singleton */
	app = marina_app_get_default ();
	marina_app_start (app);
	
	gtk_main ();
	
	/* shutdown the MarinaApp to gracefully cleanup */
	marina_app_shutdown (app);
	
	return EXIT_SUCCESS;
}

