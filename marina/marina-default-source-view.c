/* marina-default-source-view.c
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#include <config.h>

#include <glib/gi18n.h>

#include "marina-app.h"
#include "marina-date.h"
#include "marina-dirs.h"
#include "marina-default-source-view.h"
#include "marina-items.h"
#include "marina-message-bus.h"
#include "marina-sources.h"
#include "marina-web-view.h"
#include "marina-window.h"
#include "marina-window-private.h"

struct _MarinaDefaultSourceViewPrivate
{
  MarinaSource *source;
  
  GtkWidget    *treeview_sw;
  GtkWidget    *treeview;
  GtkWidget    *web_view;

  GtkTreeModel *filter;
};

typedef struct
{
  MarinaDefaultSourceView *source_view;
  MarinaItem              *item;
} DelayedRead;

static void marina_source_view_init (gpointer g_iface, gpointer iface_data);

G_DEFINE_TYPE_EXTENDED (MarinaDefaultSourceView,
                        marina_default_source_view,
                        GTK_TYPE_VPANED,
                        0,
                        G_IMPLEMENT_INTERFACE (MARINA_TYPE_SOURCE_VIEW,
                                               marina_source_view_init));

static void
marina_default_source_view_finalize (GObject *object)
{
  G_OBJECT_CLASS (marina_default_source_view_parent_class)->finalize (object);
}

static void
marina_default_source_view_class_init (MarinaDefaultSourceViewClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  
  object_class->finalize = marina_default_source_view_finalize;

  g_type_class_add_private (object_class,
                            sizeof (MarinaDefaultSourceViewPrivate));
}

static MarinaItem*
get_selected (MarinaDefaultSourceView *source_view)
{
  MarinaDefaultSourceViewPrivate *priv;
  GtkTreeSelection               *sel;
  GtkTreeModel                   *model;
  GtkTreeIter                     iter;
  MarinaItem                     *item = NULL;
  
  g_return_val_if_fail (MARINA_IS_DEFAULT_SOURCE_VIEW (source_view), NULL);
  
  priv = source_view->priv;
  
  sel = gtk_tree_view_get_selection (GTK_TREE_VIEW (priv->treeview));

  if (gtk_tree_selection_get_selected (sel, &model, &iter))
    gtk_tree_model_get (model, &iter, 0, &item, -1);

  return item;
}

static void
dec_unread (gpointer object)
{
  if (!object)
    return;
  guint count = GPOINTER_TO_UINT (g_object_get_data (G_OBJECT (object), "sources-unread-count"));
  count--;
  g_object_set_data (G_OBJECT (object), "sources-unread-count", GUINT_TO_POINTER (count));
}

static gboolean
mark_item_read_cb (gpointer data)
{
  DelayedRead *dr = data;
  MarinaItem  *item;
  
  g_return_val_if_fail (MARINA_IS_DEFAULT_SOURCE_VIEW (dr->source_view), FALSE);
  g_return_val_if_fail (MARINA_IS_ITEM (dr->item), FALSE);
  
  item = get_selected (dr->source_view);
  
  if (item == dr->item)
    {
      marina_item_set_read (item, TRUE);
      marina_items_edit_item (marina_items_get_default (), item);
      
      /* HACK: decrement the count from stat() for unread items */
      MarinaSource *source = marina_sources_get (marina_sources_get_default (),
                                                 marina_item_get_source_id (item));
      dec_unread (source);
      gtk_widget_queue_draw (gtk_widget_get_toplevel (GTK_WIDGET (dr->source_view)));
    }
  
  g_object_unref (dr->item);
  g_free (dr);
  
  return FALSE;
}

static void
selection_changed_cb (GtkTreeSelection        *selection,
                      MarinaDefaultSourceView *source_view)
{
  MarinaDefaultSourceViewPrivate *priv;
  MarinaItem                     *item;
  DelayedRead                    *dr;
  
  g_return_if_fail (MARINA_IS_DEFAULT_SOURCE_VIEW (source_view));
  
  priv = source_view->priv;
  
  item = get_selected (source_view);
  
  if (item && !marina_item_get_read (item))
    {
      /* wait one second before marking read */
      dr = g_new0 (DelayedRead, 1);
      dr->source_view = source_view;
      dr->item = g_object_ref (item);
      g_timeout_add_seconds (1, mark_item_read_cb, dr);
    }
  
  marina_web_view_set_item (MARINA_WEB_VIEW (priv->web_view), item);
}

static gboolean
button_press_cb (GtkWidget      *widget,
                 GdkEventButton *event,
                 gpointer        user_data)
{
  MarinaWindow *window;
  GtkUIManager *manager;
  GtkWidget    *popup;
  
  window = marina_app_get_window (marina_app_get_default ());
  manager = marina_window_get_ui_manager (window);
  
  if (event->button == 3) /* right click */
    {
      popup = gtk_ui_manager_get_widget (manager, "/ui/ItemContextPopup");
      gtk_menu_popup (GTK_MENU (popup),
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      event->button,
                      event->time);
    }
  
  return FALSE;
}

/**
 * format_item_string:
 * @item: A #MarinaItem
 * @str: A string
 *
 * Formats a string for display based on the read status of the item.
 *
 * Return value: a new string which should be freed with g_free().
 */
static gchar*
format_item_string (MarinaItem  *item,
                    const gchar *str)
{
  if (marina_item_get_read (item))
    return g_strdup (str);

  return g_strdup_printf ("<b>%s</b>", str != NULL ? str : "");
}

static gchar*
format_item_string_escaped (MarinaItem  *item,
                            const gchar *str)
{
  gchar *escaped;
  gchar *result;
  gchar *tmp;

  // hack, need to add language to marina item
  if (!g_utf8_validate (str, strlen (str), NULL)) {
    gsize written = 0;
    tmp = g_convert (str, strlen (str), "UTF-8", "iso-8859-1", NULL, &written, NULL);
    escaped = g_markup_escape_text (tmp, written);
    g_free (tmp);
  }
  else {
    escaped = g_markup_escape_text (str, strlen (str));
  }

  result = format_item_string (item, escaped);
  g_free (escaped);

  return result;
}

static void
read_cell_data_func (GtkTreeViewColumn *tree_column,
                     GtkCellRenderer *cell,
                     GtkTreeModel *tree_model,
                     GtkTreeIter *iter,
                     gpointer data)
{
  MarinaItem *item;
  
  g_return_if_fail (MARINA_IS_DEFAULT_SOURCE_VIEW (data));
  
  gtk_tree_model_get (tree_model, iter, 0, &item, -1);
  
  if (item && !marina_item_get_read (item))
    g_object_set (cell, "stock-id", GTK_STOCK_NEW, NULL);
  else
    g_object_set (cell, "pixbuf", NULL, NULL);
}

static void
date_cell_data_func (GtkTreeViewColumn *tree_column,
                     GtkCellRenderer *cell,
                     GtkTreeModel *tree_model,
                     GtkTreeIter *iter,
                     gpointer data)
{
  MarinaItem *item;
  MarinaDate *date;
  gchar      *markup;
  
  g_return_if_fail (MARINA_IS_DEFAULT_SOURCE_VIEW (data));
  
  gtk_tree_model_get (tree_model, iter, 0, &item, -1);
  
  if (item != NULL)
    {
      g_object_get (item, "published_at", &date, NULL);
      markup = format_item_string (item, marina_date_to_string (date));
      g_object_set (cell, "markup", markup, NULL);
      g_free (markup);
    }
  else
    g_object_set (cell, "text", NULL, NULL);
}

static void
author_cell_data_func (GtkTreeViewColumn *tree_column,
                       GtkCellRenderer *cell,
                       GtkTreeModel *tree_model,
                       GtkTreeIter *iter,
                       gpointer data)
{
  MarinaItem *item;
  gchar      *markup;
  
  g_return_if_fail (MARINA_IS_DEFAULT_SOURCE_VIEW (data));
  
  gtk_tree_model_get (tree_model, iter, 0, &item, -1);
  
  if (item != NULL)
    {
      markup = format_item_string_escaped (item, marina_item_get_author (item));
      g_object_set (cell, "markup", markup, NULL);
      g_free (markup);
    }
  else
    g_object_set (cell, "text", "", NULL);
}

static void
title_cell_data_func (GtkTreeViewColumn *tree_column,
                      GtkCellRenderer   *cell,
                      GtkTreeModel      *tree_model,
                      GtkTreeIter       *iter,
                      gpointer           data)
{
  MarinaItem *item;
  gchar      *markup;
  
  g_return_if_fail (MARINA_IS_DEFAULT_SOURCE_VIEW (data));
  
  gtk_tree_model_get (tree_model, iter, 0, &item, -1);
  
  if (item != NULL)
    {
      markup = format_item_string_escaped (item, marina_item_get_title (item));
      g_object_set (cell, "markup", markup, NULL);
      g_free (markup);
    }
  else
    g_object_set (cell, "text", "", NULL);
}

static void
row_activated_cb (GtkTreeView       *tree_view,
                  GtkTreePath       *tree_path,
                  GtkTreeViewColumn *column,
                  gpointer           user_data)
{
  GtkTreeModel *model;
  GtkTreeIter   iter;
  MarinaItem   *item;
  const gchar  *link;

  model = gtk_tree_view_get_model (tree_view);
  if (gtk_tree_model_get_iter (model, &iter, tree_path))
    {
      gtk_tree_model_get (model, &iter, 0, &item, -1);
      link = marina_item_get_link (item);
      if (link)
        gtk_show_uri (NULL, link, GDK_CURRENT_TIME, NULL);
    }
}

static void
create_widgets (MarinaDefaultSourceView *source_view)
{
  MarinaDefaultSourceViewPrivate *priv;
  GtkWidget *treeview_sw;
  GtkWidget *treeview;
  GtkWidget *web_view;
  GtkWidget *item_area;
  GtkTreeViewColumn *column;
  GtkCellRenderer *cpix;
  GtkCellRenderer *ctext;
  GtkTreeSelection *selection;
  
  g_return_if_fail (MARINA_IS_DEFAULT_SOURCE_VIEW (source_view));
  
  priv = source_view->priv;
  
  /* top scroller */
  treeview_sw = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (treeview_sw),
                                       GTK_SHADOW_NONE);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (treeview_sw),
                                  GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  gtk_paned_add1 (GTK_PANED (source_view), treeview_sw);
  gtk_widget_show (treeview_sw);
  
  /* top treeview */
  treeview = gtk_tree_view_new ();
  gtk_container_add (GTK_CONTAINER (treeview_sw), treeview);
  gtk_widget_show (treeview);
  
  /* item columns */
  
  /* read */
  column = gtk_tree_view_column_new ();
  cpix = gtk_cell_renderer_pixbuf_new ();
  gtk_tree_view_column_pack_start (column, cpix, TRUE);
  gtk_tree_view_column_set_cell_data_func (column, cpix, read_cell_data_func,
                                           g_object_ref (source_view),
                                           g_object_unref);
  gtk_tree_view_append_column (GTK_TREE_VIEW (treeview), column);
  
  /* date */
  column = gtk_tree_view_column_new ();
  gtk_tree_view_column_set_title (column, _("Date"));
  gtk_tree_view_column_set_resizable (column, TRUE);
  g_object_set (column, "expand", TRUE, NULL);
  ctext = gtk_cell_renderer_text_new ();
  g_object_set (ctext, "ellipsize", PANGO_ELLIPSIZE_END, NULL);
  gtk_tree_view_column_pack_start (column, ctext, TRUE);
  gtk_tree_view_column_set_cell_data_func (column, ctext, date_cell_data_func,
                                           g_object_ref (source_view),
                                           g_object_unref);
  gtk_tree_view_append_column (GTK_TREE_VIEW (treeview), column);
  
  /* title */
  column = gtk_tree_view_column_new ();
  gtk_tree_view_column_set_resizable (column, TRUE);
  gtk_tree_view_column_set_title (column, _("Title"));
  g_object_set (column, "expand", TRUE, NULL);
  ctext = gtk_cell_renderer_text_new ();
  g_object_set (ctext, "ellipsize", PANGO_ELLIPSIZE_END, NULL);
  gtk_tree_view_column_pack_start (column, ctext, TRUE);
  gtk_tree_view_column_set_cell_data_func (column, ctext, title_cell_data_func,
                                           g_object_ref (source_view),
                                           g_object_unref);
  gtk_tree_view_append_column (GTK_TREE_VIEW (treeview), column);
  
  /* author */
  column = gtk_tree_view_column_new ();
  gtk_tree_view_column_set_title (column, _("Author"));
  gtk_tree_view_column_set_resizable (column, TRUE);
  g_object_set (column, "expand", TRUE, NULL);
  ctext = gtk_cell_renderer_text_new ();
  g_object_set (ctext, "ellipsize", PANGO_ELLIPSIZE_END, NULL);
  gtk_tree_view_column_pack_start (column, ctext, TRUE);
  gtk_tree_view_column_set_cell_data_func (column, ctext, author_cell_data_func,
                                           g_object_ref (source_view),
                                           g_object_unref);
  gtk_tree_view_append_column (GTK_TREE_VIEW (treeview), column);

  /* item area */
  item_area = gtk_vbox_new (FALSE, 0);
  gtk_paned_add2 (GTK_PANED (source_view), item_area);
  gtk_widget_show (item_area);
  
  /* web view */
  web_view = marina_web_view_new ();
  gtk_box_pack_start (GTK_BOX (item_area), web_view, TRUE, TRUE, 0);
  gtk_widget_show (web_view);
  
  priv->treeview_sw = treeview_sw;
  priv->treeview = treeview;
  priv->web_view = web_view;
  
  /* signals */
  
  /* handle selection changed in treeview */
  selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (treeview));
  g_signal_connect (selection,
                    "changed",
                    G_CALLBACK (selection_changed_cb),
                    source_view);
  
  /* handle popup menu on right click */
  g_signal_connect (G_OBJECT (treeview),
                    "button-press-event",
                    G_CALLBACK (button_press_cb),
                    NULL);

  /* handle double click/activate and open link */
  g_signal_connect (G_OBJECT (treeview),
                    "row-activated",
                    G_CALLBACK (row_activated_cb),
                    NULL);
}


static void
compact_mode_cb (MarinaMessageBus *bus,
                 MarinaMessage    *message,
                 MarinaSourceView *source_view)
{
  MarinaDefaultSourceViewPrivate *priv;
  gboolean active;
  GtkWidget *widget;
  
  GtkRcStyle *rc_style;
  gint size;
  
  g_return_if_fail (MARINA_IS_DEFAULT_SOURCE_VIEW (source_view));
  
  priv = MARINA_DEFAULT_SOURCE_VIEW (source_view)->priv;
  widget = priv->treeview;
  
  marina_message_get (message, "active", &active, NULL);
  
  rc_style = gtk_widget_get_modifier_style (widget);
  
  if (rc_style->font_desc)
    {
      pango_font_description_free (rc_style->font_desc);
      rc_style->font_desc = NULL;
    }
  
  if (active)
    {
      rc_style->font_desc = pango_font_description_copy (widget->style->font_desc);
      size = pango_font_description_get_size (rc_style->font_desc);
      pango_font_description_set_size (rc_style->font_desc, size * PANGO_SCALE_SMALL);
    }
  
  gtk_widget_modify_style (widget, rc_style);
}

static void
marina_default_source_view_init (MarinaDefaultSourceView *source_view)
{
  source_view->priv = G_TYPE_INSTANCE_GET_PRIVATE (source_view,
                                                   MARINA_TYPE_DEFAULT_SOURCE_VIEW,
                                                   MarinaDefaultSourceViewPrivate);
  
  create_widgets (source_view);
  
  gtk_paned_set_position (GTK_PANED (source_view), 150);
  
  /* connect to the compact mode signal to shrink the sources */
  marina_message_bus_connect (marina_message_bus_get_default (),
                              "/window", "compact_mode",
                              (MarinaMessageCallback) compact_mode_cb,
                              g_object_ref (source_view),
                              g_object_unref);
}

static void
marina_default_source_view_clear (MarinaDefaultSourceView *source_view)
{
  MarinaDefaultSourceViewPrivate *priv;
  
  g_return_if_fail (MARINA_IS_DEFAULT_SOURCE_VIEW (source_view));
  
  priv = source_view->priv;
  
  gtk_tree_view_set_model (GTK_TREE_VIEW (priv->treeview), NULL);
  marina_web_view_set_source (MARINA_WEB_VIEW (priv->web_view), NULL);
  marina_web_view_set_item (MARINA_WEB_VIEW (priv->web_view), NULL);
}

static gboolean
filter_func (GtkTreeModel *model,
             GtkTreeIter  *iter,
             gpointer      data)
{
  MarinaSource *source = data;
  MarinaItem *item = NULL;
  
  g_return_val_if_fail (source != NULL, FALSE);
  
  gtk_tree_model_get (model, iter, 0, &item, -1);
  
  if (item == NULL) // Just appended values could be NULL
    return FALSE;
  
  return marina_source_observes (source, item);
}

static void
marina_default_source_view_set_source (MarinaSourceView *source_view, 
                                       MarinaSource     *source)
{
  MarinaDefaultSourceViewPrivate *priv;
  GtkTreeModel                   *model;
  GtkTreeModel                   *filter = NULL;
  
  g_return_if_fail (MARINA_IS_DEFAULT_SOURCE_VIEW (source_view));
  
  priv = MARINA_DEFAULT_SOURCE_VIEW (source_view)->priv;
  
  marina_default_source_view_clear (MARINA_DEFAULT_SOURCE_VIEW (source_view));
  
  priv->source = source;
  
  if (source)
    {
      model = marina_items_get_model (marina_items_get_default ());
      filter = gtk_tree_model_filter_new (model, NULL);
      
      gtk_tree_model_filter_set_visible_func (GTK_TREE_MODEL_FILTER (filter),
                                              filter_func,
                                              g_object_ref (priv->source),
                                              g_object_unref);
      
      gtk_tree_view_set_model (GTK_TREE_VIEW (priv->treeview), filter);
      
      marina_web_view_set_source (MARINA_WEB_VIEW (priv->web_view), source);

    }

  if (priv->filter)
    g_object_unref (priv->filter);

  if (filter)
    priv->filter = g_object_ref (filter);
  else
    priv->filter = NULL;
}

static void
move_next (MarinaSourceView *source_view)
{
  MarinaDefaultSourceViewPrivate *priv;
  GtkTreePath                    *path   = NULL;
  GtkTreeViewColumn              *column = NULL;
  GtkTreeIter                     iter;

  g_return_if_fail (MARINA_IS_DEFAULT_SOURCE_VIEW (source_view));

  priv = MARINA_DEFAULT_SOURCE_VIEW (source_view)->priv;

  gtk_tree_view_get_cursor (GTK_TREE_VIEW (priv->treeview), &path, &column);

  if (!path)
    {
      path = gtk_tree_path_new_from_indices (0, -1);
      gtk_widget_grab_focus (priv->treeview);
      column = gtk_tree_view_get_column (GTK_TREE_VIEW (priv->treeview), 0);
      gtk_tree_view_set_cursor (GTK_TREE_VIEW (priv->treeview), path, column, FALSE);
      gtk_tree_path_free (path);
      return;
    }

  if (gtk_tree_model_get_iter (priv->filter, &iter, path))
    {
      if (gtk_tree_model_iter_next (priv->filter, &iter))
        {
          path = gtk_tree_model_get_path (priv->filter, &iter);
          gtk_tree_view_set_cursor (GTK_TREE_VIEW (priv->treeview), path, column, FALSE);
          gtk_tree_path_free (path);
          return;
        }
    }
}

static void
move_prev (MarinaSourceView *source_view)
{
  MarinaDefaultSourceViewPrivate *priv;
  GtkTreePath                    *path   = NULL;
  GtkTreeViewColumn              *column = NULL;
  gint                            index  = 0;

  g_return_if_fail (MARINA_IS_DEFAULT_SOURCE_VIEW (source_view));

  priv = MARINA_DEFAULT_SOURCE_VIEW (source_view)->priv;

  gtk_tree_view_get_cursor (GTK_TREE_VIEW (priv->treeview), &path, &column);

  if (!path)
    {
      //move_last (source_view);
      return;
    }

  if ((index = gtk_tree_path_get_indices (path) [0]) > 0)
    {
      path = gtk_tree_path_new_from_indices (--index, -1);
      gtk_tree_view_set_cursor (GTK_TREE_VIEW (priv->treeview), path, column, FALSE);
      gtk_tree_path_free (path);
      return;
    }
}

static void
marina_source_view_init (gpointer g_iface,
                         gpointer iface_data)
{
  MarinaSourceViewIface *source_view = g_iface;
  
  source_view->set_source = marina_default_source_view_set_source;
  source_view->move_next = move_next;
  source_view->move_prev = move_prev;
}

GtkWidget*
marina_default_source_view_new (void)
{
  return g_object_new (MARINA_TYPE_DEFAULT_SOURCE_VIEW, NULL);
}
