/* marina-items.c
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

/**
 * SECTION:marina-items
 * @short_description: Access to #MarinaItem storage
 *
 * #MarinaItems provides a centralized system to control storage and
 * manipulation of #MarinaItem objects during runtime.  Access to the
 * underlying Berkeley DB storage is done through the BdbListStore
 * implementation. BdbListStore is a #GtkTreeModel that will serialize
 * and deserialize our #MarinaItem<!-- -->'s to and from storage. It
 * also intellegently caches our items to reduce serialization overhead.
 *
 * The #GtkTreeModel is available via marina_items_get_model().  It only
 * has one column (0) which contains a #MarinaItem instance.
 */

#include <db.h>
#include <json-glib/json-gobject.h>
#include <string.h>

#include "marina-debug.h"
#include "marina-dirs.h"
#include "marina-items.h"

#include "bdb-list-store.h"

struct _MarinaItemsPrivate
{
  GtkTreeModel  *model;
  
  DB            *db;
  DB_ENV        *db_env;
};

G_DEFINE_TYPE (MarinaItems, marina_items, G_TYPE_OBJECT);

static void
marina_items_finalize (GObject *object)
{
  MarinaItemsPrivate *priv;
  
  g_return_if_fail (MARINA_IS_ITEMS (object));
  
  priv = MARINA_ITEMS (object)->priv;
  
  g_object_unref (priv->model);
  priv->model = NULL;
  
  G_OBJECT_CLASS (marina_items_parent_class)->finalize (object);
}

static void
marina_items_class_init (MarinaItemsClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  
  object_class->finalize = marina_items_finalize;

  g_type_class_add_private (object_class, sizeof (MarinaItemsPrivate));
}

/**
 * marina_items_shutdown:
 * @items: A #MarinaItems
 *
 * Shuts down the storage backend and flushes the remaining buffers to
 * disk.
 */
void
marina_items_shutdown (MarinaItems *items)
{
  MarinaItemsPrivate *priv;
  
  marina_debug (DEBUG_ITEMS);
  
  g_return_if_fail (MARINA_IS_ITEMS (items));
  
  priv = items->priv;
  
  priv->db->close (priv->db, 0);
  priv->db_env->close (priv->db_env, 0);
}

static void
load_items_db (MarinaItems *items,
               const gchar *env_dir)
{
  MarinaItemsPrivate  *priv;
  gchar               *errmsg = NULL;
  DB_ENV              *db_env;
  DB                  *db;
  gint                 ret;
  gint                 env_flags = DB_CREATE
                                 | DB_INIT_LOG
                                 | DB_INIT_LOCK
                                 | DB_INIT_MPOOL
                                 | DB_INIT_TXN
                                 | DB_PRIVATE;
  
  g_return_if_fail (MARINA_IS_ITEMS (items));
  
  priv = items->priv;
  
  if (!g_file_test (env_dir, G_FILE_TEST_IS_DIR))
      g_mkdir_with_parents (env_dir, 0755);
  
  if ((ret = db_env_create (&db_env, 0)) != 0)
    {
      errmsg = g_strdup_printf ("db_env_create: %s",
                                db_strerror (ret));
      goto error;
    }
  
  db_env->set_errpfx (db_env, "MarinaItem BDB");
  db_env->set_errfile (db_env, stderr);
  
  if ((ret = db_env->open (db_env, env_dir, env_flags, 0)) != 0)
    {
      errmsg = g_strdup_printf ("db_env_open: %s",
                                db_strerror (ret));
      goto error;
    }
  
  if ((ret = db_create (&db, db_env, 0)) != 0)
    {
      errmsg = g_strdup_printf ("db_create: %s",
                                db_strerror (ret));
      goto error;
    }
  
  /* BdbListStore needs DB_RENUMBER */
  db->set_flags (db, DB_RENUMBER);
  
  if ((ret = db->open (db, NULL, "items.db", NULL, DB_RECNO, DB_CREATE, 0)) != 0)
    {
      errmsg = g_strdup_printf ("db_open: %s",
                                db_strerror (ret));
      goto error;
    }
  
  priv->db = db;
  priv->db_env = db_env;
  
  return;
  
error:
  g_printerr ("%s\n", errmsg);
  g_free (errmsg);
}

static gboolean
json_serialize (BdbListStore  *list_store,
                GValue        *value,
                gchar        **output,
                gpointer       user_data,
                GError       **error)
{
  g_return_val_if_fail (output != NULL, FALSE);
  g_return_val_if_fail (G_VALUE_HOLDS_OBJECT (value), FALSE);
  
  *output = json_serialize_gobject (g_value_get_object (value), NULL);
  
  return TRUE;
}

static gboolean
json_deserialize (BdbListStore  *list_store,
                  GValue        *value,
                  const gchar   *input,
                  gpointer       user_data,
                  GError       **error)
{
  MarinaItem *item;
  
  g_return_val_if_fail (input != NULL, FALSE);
  
  g_value_init (value, G_TYPE_OBJECT);
  
  if (!input || g_str_equal (input, ""))
    {
      g_value_set_object (value, NULL);
      return TRUE;
    }
  
  item = MARINA_ITEM (json_construct_gobject (MARINA_TYPE_ITEM,
                                              input,
                                              strlen (input),
                                              NULL));
  g_value_take_object (value, item);
  return item != NULL;
}

static void
create_bdb_list_store (MarinaItems *items)
{
  MarinaItemsPrivate *priv;
  BdbListStore       *model;
  GError             *error = NULL;
  
  g_return_if_fail (MARINA_IS_ITEMS (items));
  
  priv = items->priv;
  
  model = bdb_list_store_new ();
  
  if (!bdb_list_store_set_db (model, priv->db, &error))
    {
      g_printerr ("%s\n", error->message);
      g_error_free (error);
      return;
    }
  
  bdb_list_store_set_serialize_func (model,
                                     json_serialize,
                                     g_object_ref (items),
                                     g_object_unref);
  
  bdb_list_store_set_deserialize_func (model,
                                       json_deserialize,
                                       g_object_ref (items),
                                       g_object_unref);
  
  priv->model = GTK_TREE_MODEL (model);
}

static gchar*
get_env_dir (void)
{
  gchar *dir;
  gchar *env_dir;
  
  dir = marina_dirs_get_user_config_dir ();
  env_dir = g_build_filename (dir, "db", "items", NULL);
  g_free (dir);
  
  return env_dir;
}

static void
marina_items_init (MarinaItems *items)
{
  gchar *env_dir;
  
  items->priv =  G_TYPE_INSTANCE_GET_PRIVATE (items,
                                              MARINA_TYPE_ITEMS,
                                              MarinaItemsPrivate);
  
  /* data dir, ~/.gnome2/marina/db/items/ */
  env_dir = get_env_dir ();
  
  /* create and load the berkeley database */
  load_items_db (items, env_dir);
  
  g_free (env_dir);
  
  /* create our bdb list store using our database */
  create_bdb_list_store (items);
}

/**
 * marina_items_get_default:
 *
 * Retrieves the singleton instance of #MarinaItems. If you wish to store a
 * reference, use g_object_ref().
 *
 * Return value: a #MarinaItems.
 */
MarinaItems*
marina_items_get_default (void)
{
  static MarinaItems *instance = NULL;
  static GStaticMutex mutex  = G_STATIC_MUTEX_INIT;

  if (!instance)
    {
      g_static_mutex_lock (&mutex);
      
      if (!instance)
        {
          marina_debug_message (DEBUG_APP, "Creating items singleton");
          instance = g_object_new (MARINA_TYPE_ITEMS, NULL);
        }
      
      g_static_mutex_unlock (&mutex);
    }

  return instance;
}

/**
 * marina_items_get_model:
 * @items: A #MarinaItems
 *
 * Retrieves the #GtkTreeModel that transparently stores #MarinaItem
 * instances to and from the Berkeley DB data store.
 */
GtkTreeModel*
marina_items_get_model (MarinaItems *items)
{
  MarinaItemsPrivate *priv;
  
  g_return_val_if_fail (MARINA_IS_ITEMS (items), NULL);
  
  priv = items->priv;
  
  return priv->model;
}

typedef struct
{
  const gchar *source_id;
  const gchar *item_id;
  MarinaItem  *item;
} LookupData;

static gboolean
lookup_cb (MarinaItem *item,
           LookupData *data)
{
  gchar *item_id,
        *source_id;

  g_return_val_if_fail (data != NULL, FALSE);

  /* item can be NULL if it was just appended to the list store but
   * has not yet had a value set. */
  if (!item)
    return FALSE;

  g_object_get (G_OBJECT (item),
                "item-id", &item_id,
                "source-id", &source_id,
                NULL);

  if (g_strcmp0 (item_id, data->item_id) == 0 &&
      g_strcmp0 (source_id, data->source_id) == 0)
    {
      data->item = item;
      return TRUE;
    }

  return FALSE;
}

/**
 * marina_items_lookup:
 * @items: A #MarinaItems
 * @source_id: the id of the source containing the item
 * @item_id: the id of the item
 *
 * Retrieves the instance of #MarinaItem that was imported from @source_id
 * and contains the unique id @item_id.
 *
 * Return value: the #MarinaItem instance or NULL
 */
MarinaItem*
marina_items_lookup (MarinaItems *items,
                     const gchar *source_id,
                     const gchar *item_id)
{
  LookupData data;

  data.source_id = source_id;
  data.item_id = item_id;
  data.item = NULL;

  /* NOTE: Ideally we would not traverse the whole tree here. It causes
   *   thrashing on the GLruCache as well as considerable deserialization
   *   overhead.  We should create a bdb related index to have quick
   *   lookups by source-id/item-id.
   */
  marina_items_foreach (items, (GFunc)lookup_cb, &data);

  return data.item;
}

/**
 * marina_items_add_item:
 * @items: A #MarinaItems
 * @source: A #MarinaSource owning @item
 * @item: A #MarinaItem
 *
 * Adds @item to the backend datastore without interacting with the
 * #GtkTreeModel directly.
 */
void
marina_items_add_item (MarinaItems  *items,
                       MarinaSource *source,
                       MarinaItem   *item)
{
  MarinaItemsPrivate *priv;
  GtkTreeIter         iter;
  GValue              value = {0,};
  
  marina_debug (DEBUG_ITEMS);
  
  g_return_if_fail (MARINA_IS_ITEMS (items));
  g_return_if_fail (MARINA_IS_SOURCE (source));
  g_return_if_fail (MARINA_IS_ITEM (item));
  
  priv = items->priv;
  
  g_value_init (&value, G_TYPE_OBJECT);
  g_value_take_object (&value, item);
  
  /* create location for the item */
  bdb_list_store_append (BDB_LIST_STORE (priv->model), &iter);
  
  /* store the item in new location, causes serialization */
  bdb_list_store_set_value (BDB_LIST_STORE (priv->model), &iter, 0, &value);
}

/**
 * marina_items_edit_item:
 * @items: A #MarinaItems
 * @item: A #MarinaItem
 *
 * Updates an item in storage. This expects that the object has originated
 * from storage so that information can be retrieved about its origin.
 */
void
marina_items_edit_item (MarinaItems *items, 
                        MarinaItem  *item)
{
  MarinaItemsPrivate *priv;
  BdbListStore       *list_store;
  GValue              value = {0,};
  gint                index;
  GtkTreePath        *path;
  GtkTreeIter         iter;
  
  marina_debug (DEBUG_ITEMS);
  
  g_return_if_fail (MARINA_IS_ITEMS (items));
  g_return_if_fail (MARINA_IS_ITEM (item));
  
  priv = items->priv;
  
  list_store = BDB_LIST_STORE (priv->model);
  
  g_value_init (&value, G_TYPE_OBJECT);
  g_value_set_object (&value, item);
  
  /* The path index is stored in the object data */
  index = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (item), "bdb-list-store-key"));

  /* the item did not originate from storage */
  g_return_if_fail (index > 0);
  
  /* update the data store */
  path = gtk_tree_path_new_from_indices (index - 1, -1);
  gtk_tree_model_get_iter (GTK_TREE_MODEL (list_store), &iter, path);
  bdb_list_store_set_value (list_store, &iter, 0, &value);
  
  g_value_unset (&value);
}

typedef struct
{
  GFunc    callback;
  gpointer data;
} ForeachInfo;

static gboolean
foreach_cb (GtkTreeModel *model,
            GtkTreePath  *path,
            GtkTreeIter  *iter,
            gpointer      user_data)
{
  ForeachInfo *info = user_data;
  MarinaItem *item;
  
  gtk_tree_model_get (model, iter, 0, &item, -1);
  
  info->callback (item, info->data);
  
  return FALSE;
}

void
marina_items_foreach (MarinaItems *items,
                      GFunc        func,
                      gpointer     data)
{
  MarinaItemsPrivate *priv;
  ForeachInfo info;
  
  g_return_if_fail (MARINA_IS_ITEMS (items));
  g_return_if_fail (func != NULL);
  
  priv = items->priv;
  
  info.callback = func;
  info.data = data;
  
  gtk_tree_model_foreach (priv->model, foreach_cb, &info);
}
