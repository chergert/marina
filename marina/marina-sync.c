/* marina-sync.c
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#include <glib/gi18n.h>
#include <iris/iris.h>

#include "marina-app.h"
#include "marina-builders.h"
#include "marina-builder.h"
#include "marina-debug.h"
#include "marina-items.h"
#include "marina-parse-result.h"
#include "marina-parsers.h"
#include "marina-progress.h"
#include "marina-source.h"
#include "marina-sources.h"
#include "marina-sync.h"
#include "marina-window.h"

#define GPOINTER_TO_BOOLEAN(i) ((((gint)(i)) == 2) ? TRUE : FALSE)
#define GBOOLEAN_TO_POINTER(i) (GINT_TO_POINTER ((i) ? 2 : 1))

static gint in_sync = 0;

/* Only use from callbacks or main thread */
static GList *syncs = NULL;

static void
cancel_cb (MarinaProgress *progress,
           gpointer        user_data)
{
  GList *iter;
  
  for (iter = syncs; iter; iter = iter->next)
    iris_task_cancel (iter->data);
}

static MarinaProgress*
get_sync_progress (void)
{
  MarinaWindow   *window;
  MarinaProgress *progress;
  gchar          *text;
  
  window = marina_app_get_window (marina_app_get_default ());
  progress = marina_window_get_progress (window, "marina-sync");
  
  if (!progress)
    {
      progress = MARINA_PROGRESS (marina_progress_new ());
      marina_progress_set_text (progress, _("Synchronizing"));
      marina_progress_set_icon_name (progress, GTK_STOCK_NETWORK);
      marina_progress_set_cancel_func (progress, (GFunc)cancel_cb, NULL, NULL);
      marina_window_set_progress (window, "marina-sync", progress);
    }
  
  text = g_strdup_printf ("Retrieving %d channel(s)", in_sync);
  marina_progress_set_secondary_text (progress, text);
  g_free (text);
  
  return progress;
}

static void
mark_updating (MarinaSource *object,
               gboolean      updating)
{
  g_object_set_data (G_OBJECT (object),
                     "updating",
                     GBOOLEAN_TO_POINTER (updating));
}

static void
get_stream (IrisTask *task, 
            gpointer  user_data)
{
  MarinaSource *source;
  GInputStream *stream;
  GError       *error = NULL;
  
  g_return_if_fail (MARINA_IS_SOURCE (user_data));
  
  source = user_data;
  
  /* NOTE: Retrieve the stream for the source. We should consider
   *       becoming an async task and adding async support to the
   *       get_stream() method. This would allow us to have massively
   *       more concurrent synchronizations as we wouldn't be
   *       restricted by the number of threads.
   *
   *       Since it is my goal to handle 1000's of feeds, we should
   *       do this in the short term.
   *
   *       As a benefit, we could also add support for progress monitoring
   *       so that we could have some sort of fancy reader board with
   *       the status of each syndicating source.
   */
  
  marina_debug_message (DEBUG_SOURCES,
                        "Retrieving stream for %s",
                        marina_source_get_id (source));
  
  mark_updating (source, TRUE);
  
  stream = marina_source_get_stream (source, &error);
  
  if (!stream)
    {
      marina_debug_message (DEBUG_SOURCES,
                            "Failed to retrieve source %s",
                            marina_source_get_id (source));
      IRIS_TASK_THROW (task, error);
    }
  
  marina_debug_message (DEBUG_SOURCES,
                        "Retrieved source %s",
                        marina_source_get_id (source));

  IRIS_TASK_RETURN_VALUE (task, G_TYPE_OBJECT, stream);
}

static void
parse_stream (IrisTask *task,
              gpointer  user_data)
{
  MarinaSource      *source;
  GInputStream      *stream;
  MarinaParseResult *parse_result;
  GList             *list, *tmp;
  GError            *error = NULL;
  GValue             value = {0,};
  
  g_return_if_fail (MARINA_IS_SOURCE (user_data));

  source = user_data;
  iris_task_get_result (task, &value);
  stream = g_value_dup_object (&value);
  g_value_unset (&value);

  g_return_if_fail (stream != NULL);
  
  list = marina_parsers_get_list (marina_parsers_get_default ());
  
  for (tmp = list; tmp; tmp = tmp->next)
    {
      if (marina_parser_can_parse (tmp->data, source, stream))
        {
          parse_result = marina_parser_parse (tmp->data, source, stream, &error);
          
          if (parse_result)
            {
              IRIS_TASK_RETURN_VALUE (task, G_TYPE_OBJECT, parse_result);
              break;
            }
          else
            IRIS_TASK_THROW (task, error);
        }
    }
}

static void
build_items (IrisTask *task,
             gpointer  user_data)
{
  MarinaParseResult *parse_result;
  MarinaSource      *source;
  GList             *items = NULL;
  GList             *builders;
  GList             *iter;
  MarinaItem        *item;
  GValue             value = {0,};
  
  g_return_if_fail (IRIS_IS_TASK (task));

  source = MARINA_SOURCE (user_data);
  iris_task_get_result (task, &value);
  parse_result = g_value_dup_object (&value);
  g_value_unset (&value);
  
  g_return_if_fail (MARINA_IS_PARSE_RESULT (parse_result));
  g_return_if_fail (MARINA_IS_SOURCE (source));
  
  builders = marina_builders_get_list (marina_builders_get_default ());
  
  /* allow builders to update source */
  for (iter = builders; iter; iter = iter->next)
    marina_builder_build (iter->data, parse_result, source, NULL);
  
  /* make sure there are items to parse */
  if (!marina_parse_result_move_first (parse_result))
    {
      IRIS_TASK_RETURN_VALUE (task, G_TYPE_POINTER, NULL);
      return;
    }
  
  /* allow builders to update each, potentially new, item */
  do
    {
      item = marina_item_new ();
      g_object_ref (item);
      for (iter = builders; iter; iter = iter->next)
        marina_builder_build (iter->data, parse_result, source, item);
      g_assert (MARINA_IS_ITEM (item));
      items = g_list_prepend (items, item);
    }
  while (marina_parse_result_move_next (parse_result));
  
  items = g_list_reverse (items);
  
  IRIS_TASK_RETURN_VALUE (task, G_TYPE_POINTER, items);
}

static void
merge_items (IrisTask *task,
             gpointer  user_data)
{
  MarinaSource *source;
  MarinaItem   *prev;
  GList        *items;
  GList        *tmp;
  GValue        value = {0,};
  
  g_return_if_fail (IRIS_IS_TASK (task));
  g_return_if_fail (MARINA_IS_SOURCE (user_data));
  
  source = MARINA_SOURCE (user_data);
  iris_task_get_result (task, &value);
  items = g_value_get_pointer (&value);
  g_value_unset (&value);
  
  for (tmp = items; tmp; tmp = tmp->next)
    {
      /* look for a previous version of this item */
      prev = marina_items_lookup (marina_items_get_default (),
                                  marina_item_get_source_id (tmp->data),
                                  marina_item_get_item_id (tmp->data));
      
      if (!prev)
        {
          /* create a new entry */
          marina_items_add_item (marina_items_get_default (),
                                 source,
                                 tmp->data);
        }
      else
        {
          /* copy the real bdb offset from the original version
           * of the object so it can be edited quickly by record
           * number.
           */
          g_object_set_data (G_OBJECT (tmp->data),
                             "bdb-list-store-key",
                             g_object_get_data (G_OBJECT (prev),
                                                "bdb-list-store-key"));

          /* update the existing entry */
          marina_items_edit_item (marina_items_get_default (), prev);
        }
    }
}

static void
notify_success (IrisTask *task,
                gpointer  user_data)
{
  MarinaProgress *progress;

  g_return_if_fail (IRIS_IS_TASK (task));

  mark_updating (MARINA_SOURCE (user_data), FALSE);
  
  if (g_atomic_int_dec_and_test (&in_sync))
    {
      progress = get_sync_progress ();
      gtk_widget_hide (GTK_WIDGET (progress));
    }
  
  marina_sources_stat (marina_sources_get_default ());
  
  syncs = g_list_remove (syncs, task);
}

static void
notify_error (IrisTask *task,
              gpointer  user_data)
{
  MarinaProgress *progress;
  
  mark_updating (MARINA_SOURCE (user_data), FALSE);
  
  if (g_atomic_int_dec_and_test (&in_sync))
    {
      progress = get_sync_progress ();
      gtk_widget_hide (GTK_WIDGET (progress));
    }
  
  syncs = g_list_remove (syncs, task);
}

void
marina_sync_source (MarinaSource *source)
{
  MarinaProgress *progress;
  IrisTask       *task;
  
  marina_debug (DEBUG_SOURCES);
  
  g_return_if_fail (MARINA_IS_SOURCE (source));
  
  marina_debug_message (DEBUG_SOURCES, "Syncing %s",
                        marina_source_get_id (source));
  
  /* make sure this is a stream providing source */
  if (!marina_source_has_stream (source))
    return;
  
  g_atomic_int_inc (&in_sync);
  
  progress = get_sync_progress ();
  if (!GTK_WIDGET_VISIBLE (progress))
    gtk_widget_show (GTK_WIDGET (progress));
  
  /* get the stream inside a task */
  task = iris_task_new_with_func (get_stream,
                                  g_object_ref (source),
                                  g_object_unref);
  
  /* after getting the stream, parse it */
  iris_task_add_callback (task,
                          parse_stream,
                          g_object_ref (source),
                          g_object_unref);
  
  /* after parsing the stream, build items */
  iris_task_add_callback (task,
                          build_items,
                          g_object_ref (source),
                          g_object_unref);
  
  /* after building the items, merge them into the database */
  iris_task_add_callback (task,
                          merge_items,
                          g_object_ref (source),
                          g_object_unref);
  
  /* final task handler to update the ui */
  iris_task_add_both (task,
                      notify_success,
                      notify_error,
                      g_object_ref (source),
                      g_object_unref);
  
  syncs = g_list_prepend (syncs, task);
  
  iris_task_run (task);
}
