/* marina-app.c
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with This; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#include "marina-app.h"
#include "marina-app-private.h"
#include "marina-commands.h"
#include "marina-debug.h"
#include "marina-marshal.h"
#include "marina-plugins-engine.h"
#include "marina-prefs.h"
#include "marina-sources.h"
#include "marina-sources-private.h"
#include "marina-window.h"
#include "marina-window-private.h"

#include "dialogs/marina-preferences-dialog.h"

G_DEFINE_TYPE (MarinaApp, marina_app, G_TYPE_OBJECT);

enum
{
  QUIT,
  
  LAST_SIGNAL
};

static guint app_signals[LAST_SIGNAL];

static void
marina_app_finalize (GObject *object)
{
  G_OBJECT_CLASS (marina_app_parent_class)->finalize (object);
}

static gboolean
quit_accumulator (GSignalInvocationHint *ihint,
                  GValue                *return_accu,
                  const GValue          *handler_return,
                  gpointer               data)
{
  if (g_value_get_boolean (handler_return))
    {
      g_value_set_boolean (return_accu, TRUE);
      return TRUE;
    }
  
  return FALSE;
}

static void
marina_app_class_init (MarinaAppClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = marina_app_finalize;

  g_type_class_add_private (object_class, sizeof (MarinaAppPrivate));
  
  app_signals [QUIT] = g_signal_new ("quit",
                                     MARINA_TYPE_APP,
                                     G_SIGNAL_RUN_LAST,
                                     G_STRUCT_OFFSET (MarinaAppClass, quit),
                                     quit_accumulator,
                                     NULL,
                                     marina_marshal_BOOLEAN__VOID,
                                     G_TYPE_BOOLEAN,
                                     0);
}

static void
window_delete_event_cb (MarinaWindow *window)
{
  marina_app_quit (marina_app_get_default (), TRUE);
}

static void
marina_app_init (MarinaApp *app)
{
  app->priv = G_TYPE_INSTANCE_GET_PRIVATE (app,
                                           MARINA_TYPE_APP,
                                           MarinaAppPrivate);

  /* initialize preferences */
  if (!marina_prefs_init ())
    g_error ("Could not initialize the preferences engine (GConf)");
  /* create the main window */
  app->priv->window = marina_window_new ();
  
  /* make sure we get a callback on close to notify the bus */
  g_signal_connect (app->priv->window,
                    "delete-event",
                    G_CALLBACK (window_delete_event_cb),
                    NULL);
  
  /* steal the reference to the plugins engine */
  app->priv->plugins = marina_plugins_engine_get_default ();
}

/**
 * marina_app_get_default:
 *
 * Return value: The default singleton instance of #MarinaApp.
 */
MarinaApp*
marina_app_get_default (void)
{
  static MarinaApp *instance = NULL;
  static GStaticMutex mutex  = G_STATIC_MUTEX_INIT;

  if (!instance)
    {
      g_static_mutex_lock (&mutex);
      
      if (!instance)
        {
          marina_debug_message (DEBUG_APP, "Creating app singleton");
          instance = g_object_new (MARINA_TYPE_APP, NULL);
        }
      
      g_static_mutex_unlock (&mutex);
    }

  return instance;
}

/**
 * marina_app_get_window:
 * @app: A #MarinaApp
 *
 * Retrieves the window for the instance of the application. There is
 * only one #MarinaApp per process, so it is mostly an easy way to
 * access the singleton window.
 *
 * Return value: The #MarinaWindow for the process
 */
MarinaWindow*
marina_app_get_window (MarinaApp *app)
{
  g_return_val_if_fail (MARINA_IS_APP (app), NULL);
  return app->priv->window;
}

/**
 * marina_app_start:
 * @app: A #MarinaApp
 *
 * Starts the marina application. This should only be called once, from
 * the main thread during application startup.
 */
void
marina_app_start (MarinaApp *app)
{
  MarinaAppPrivate *priv;
  MarinaSources    *sources;
  GError           *error = NULL;
  
  g_return_if_fail (MARINA_IS_APP (app));
  
  marina_debug (DEBUG_APP);
  
  priv = app->priv;
  
  /* initialize the commands subsystem */
  marina_commands_init ();
  
  /* enable the active plugins, do this before loading sources
   * so that plugins can provide MarinaSource implementations.
   */
  marina_plugins_engine_activate_plugins (priv->plugins, priv->window);
  
  sources = marina_sources_get_default ();
  g_assert (sources != NULL);
  
  /* start loading the sources */
  if (!marina_sources_load (sources, &error))
    g_printerr ("Could not load data sources: %s", error->message);
  
  /* steal the ownership reference */
  priv->sources = sources;
  
  /* load our items database */
  priv->items = marina_items_get_default ();
  
  /* select the first item in the sources view */
  _marina_window_select_first (MARINA_WINDOW (priv->window));
  
  gtk_widget_show (GTK_WIDGET (priv->window));
}

/**
 * marina_app_shutdown:
 * @app: A #MarinaApp
 *
 * This method gracefully shuts down the various components in the
 * application. It is called after the gtk_main() loop has completed
 * so that the application is left in a consisitent state.
 */
void
marina_app_shutdown (MarinaApp *app)
{
  MarinaAppPrivate *priv;
  
  g_return_if_fail (MARINA_IS_APP (app));
  
  priv = app->priv;
  
  marina_debug (DEBUG_APP);

  marina_items_shutdown (marina_items_get_default ());

  marina_sources_save (marina_sources_get_default ());

  marina_prefs_shutdown ();
  
  g_object_unref (priv->sources);
}

/**
 * marina_app_quit:
 * @app: A #MarinaApp
 * @allow_inhibit: If plugins can inhibit the quit command
 *
 * This method will begin the exit process for the application. It will
 * first check with all of the plugins to make sure it is safe to exit
 * the application. If allowed, the gtk_main() loop will be exited.
 */
void
marina_app_quit (MarinaApp *app,
                 gboolean   allow_inhibit)
{
  gboolean inhibit = FALSE;
  
  g_return_if_fail (MARINA_IS_APP (app));
  
  if (allow_inhibit)
    g_signal_emit (G_OBJECT (app), app_signals [QUIT], 0, &inhibit);
  
  if (!inhibit)
    gtk_main_quit ();
}

/**
 * marina_app_get_prefs_dialog:
 * @app: A #MarinaApp
 *
 * Retrieves the singleton instance of the preferences dialog for the
 * application. If the dialog is not currently visible, it will be reset
 * to the front page.
 *
 * Return value: A #GtkDialog
 */
GtkDialog*
marina_app_get_prefs_dialog (MarinaApp *app)
{
  g_return_val_if_fail (MARINA_IS_APP (app), NULL);

  if (!app->priv->prefs_dialog)
    app->priv->prefs_dialog = marina_preferences_dialog_new ();

  /* if the dialog isnt already visible, reset it to the first page */
  if (!GTK_WIDGET_VISIBLE (GTK_WIDGET (app->priv->prefs_dialog)))
    _marina_preferences_dialog_reset (app->priv->prefs_dialog);

  return GTK_DIALOG (app->priv->prefs_dialog);
}
