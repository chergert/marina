/*
 * marina-sources-private.h
 * This file is part of marina
 *
 * Copyright (C) 2009 - Christian Hergert
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifndef __MARINA_SOURCES_PRIVATE_H__
#define __MARINA_SOURCES_PRIVATE_H__

#include <glib.h>
#include <gtk/gtk.h>

struct _MarinaSourcesPrivate
{
  GHashTable   *hash_table;
  GtkTreeStore *tree_store;
};

gboolean  marina_sources_load    (MarinaSources *self, GError **error);
void      marina_sources_save    (MarinaSources *self);

#endif /* __MARINA_SOURCES_PRIVATE_H__ */

