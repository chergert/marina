/* marina-schemes.h
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifndef __MARINA_SCHEMES_H__
#define __MARINA_SCHEMES_H__

#include <glib-object.h>

G_BEGIN_DECLS

#define MARINA_TYPE_SCHEMES  (marina_schemes_get_type ())

#define MARINA_SCHEMES(obj)                   \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj),         \
  MARINA_TYPE_SCHEMES, MarinaSchemes))

#define MARINA_SCHEMES_CONST(obj)             \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj),         \
  MARINA_TYPE_SCHEMES, MarinaSchemes const))

#define MARINA_SCHEMES_CLASS(klass)           \
  (G_TYPE_CHECK_CLASS_CAST ((klass),          \
  MARINA_TYPE_SCHEMES, MarinaSchemesClass))

#define MARINA_IS_SCHEMES(obj)                \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj),         \
  MARINA_TYPE_SCHEMES))

#define MARINA_IS_SCHEMES_CLASS(klass)        \
  (G_TYPE_CHECK_CLASS_TYPE ((klass),          \
  MARINA_TYPE_SCHEMES))

#define MARINA_SCHEMES_GET_CLASS(obj)         \
  (G_TYPE_INSTANCE_GET_CLASS ((obj),          \
  MARINA_TYPE_SCHEMES, MarinaSchemesClass))

typedef struct _MarinaSchemes         MarinaSchemes;
typedef struct _MarinaSchemesClass    MarinaSchemesClass;
typedef struct _MarinaSchemesPrivate  MarinaSchemesPrivate;

struct _MarinaSchemes
{
  GObject parent;
  
  MarinaSchemesPrivate *priv;
};

struct _MarinaSchemesClass
{
  GObjectClass parent_class;
};

GType          marina_schemes_get_type    (void) G_GNUC_CONST;
MarinaSchemes* marina_schemes_get_default (void);
const GList*   marina_schemes_get_list    (MarinaSchemes *schemes);
const gchar*   marina_schemes_get_active  (MarinaSchemes *schemes);
void           marina_schemes_set_active  (MarinaSchemes *schemes,
                                           const gchar   *scheme);

G_END_DECLS

#endif /* __MARINA_SCHEMES_H__ */
