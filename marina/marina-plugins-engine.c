/*
 * marina-plugins-engine.c
 * This file is part of marina; it was borrowed from gedit.
 *
 * Copyright (C) 2002-2005 Paolo Maggi 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA 02111-1307, USA. 
 */

/*
 * Modified by the marina Team, 2002-2005. See the gedit AUTHORS file for a 
 * list of people on the gedit Team.  
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>

#include <glib/gi18n.h>

#include "marina-app.h"
#include "marina-debug.h"
#include "marina-dirs.h"
#include "marina-object-module.h"
#include "marina-prefs.h"
#include "marina-plugin.h"
#include "marina-plugin-info-priv.h"
#include "marina-plugin-loader.h"
#include "marina-plugins-engine.h"

#define MARINA_PLUGINS_ENGINE_BASE_KEY "/apps/marina/plugins"
#define MARINA_PLUGINS_ENGINE_KEY MARINA_PLUGINS_ENGINE_BASE_KEY "/active-plugins"

#define PLUGIN_EXT	".marina-plugin"
#define LOADER_EXT	G_MODULE_SUFFIX

typedef struct
{
	MarinaPluginLoader *loader;
	MarinaObjectModule *module;
} LoaderInfo;

/* Signals */
enum
{
	ACTIVATE_PLUGIN,
	DEACTIVATE_PLUGIN,
	LAST_SIGNAL
};

static guint signals[LAST_SIGNAL];

G_DEFINE_TYPE(MarinaPluginsEngine, marina_plugins_engine, G_TYPE_OBJECT)

struct _MarinaPluginsEnginePrivate
{
	GList *plugin_list;
	GHashTable *loaders;

	gboolean activate_from_prefs;
};

MarinaPluginsEngine *default_engine = NULL;

static void	marina_plugins_engine_activate_plugin_real (MarinaPluginsEngine *engine,
							    MarinaPluginInfo    *info);
static void	marina_plugins_engine_deactivate_plugin_real (MarinaPluginsEngine *engine,
							      MarinaPluginInfo    *info);

typedef gboolean (*LoadDirCallback)(MarinaPluginsEngine *engine, const gchar *filename, gpointer userdata);

static gboolean
load_dir_real (MarinaPluginsEngine *engine,
	       const gchar        *dir,
	       const gchar        *suffix,
	       LoadDirCallback     callback,
	       gpointer            userdata)
{
	GError *error = NULL;
	GDir *d;
	const gchar *dirent;
	gboolean ret = TRUE;

	g_return_val_if_fail (dir != NULL, TRUE);

	marina_debug_message (DEBUG_PLUGINS, "DIR: %s", dir);

	d = g_dir_open (dir, 0, &error);
	if (!d)
	{
		g_warning ("%s", error->message);
		g_error_free (error);
		return TRUE;
	}
	
	while ((dirent = g_dir_read_name (d)))
	{
		gchar *filename;

		if (!g_str_has_suffix (dirent, suffix))
			continue;

		filename = g_build_filename (dir, dirent, NULL);

		ret = callback (engine, filename, userdata);

		g_free (filename);

		if (!ret)
			break;
	}

	g_dir_close (d);
	return ret;
}

static gboolean
load_plugin_info (MarinaPluginsEngine *engine,
		  const gchar        *filename,
		  gpointer            userdata)
{
	MarinaPluginInfo *info;
	
	info = _marina_plugin_info_new (filename);

	if (info == NULL)
		return TRUE;

	/* If a plugin with this name has already been loaded
	 * drop this one (user plugins override system plugins) */
	if (marina_plugins_engine_get_plugin_info (engine, marina_plugin_info_get_module_name (info)) != NULL)
	{
		marina_debug_message (DEBUG_PLUGINS, "Two or more plugins named '%s'. "
		                      "Only the first will be considered.\n",
		                      marina_plugin_info_get_module_name (info));

		_marina_plugin_info_unref (info);

		return TRUE;
	}

	engine->priv->plugin_list = g_list_prepend (engine->priv->plugin_list, info);

	marina_debug_message (DEBUG_PLUGINS, "Plugin %s loaded", info->name);
	return TRUE;
}

static void
load_all_plugins (MarinaPluginsEngine *engine)
{
	gchar *plugin_dir;
	const gchar *pdirs_env = NULL;

	/* load user plugins */
	plugin_dir = marina_dirs_get_user_plugins_dir ();
	if (g_file_test (plugin_dir, G_FILE_TEST_IS_DIR))
	{
		load_dir_real (engine,
			       plugin_dir,
			       PLUGIN_EXT,
			       load_plugin_info,
			       NULL);

	}
	g_free (plugin_dir);

	/* load system plugins */
	pdirs_env = g_getenv ("MARINA_PLUGINS_PATH");

	marina_debug_message (DEBUG_PLUGINS, "MARINA_PLUGINS_PATH=%s", pdirs_env);

	if (pdirs_env != NULL)
	{
		gchar **pdirs;
		gint i;

		pdirs = g_strsplit (pdirs_env, G_SEARCHPATH_SEPARATOR_S, 0);

		for (i = 0; pdirs[i] != NULL; i++)
		{
			if (!load_dir_real (engine,
					    pdirs[i],
					    PLUGIN_EXT,
					    load_plugin_info,
					    NULL))
			{
				break;
			}
		}

		g_strfreev (pdirs);
	}
	else
	{
		plugin_dir = marina_dirs_get_marina_plugins_dir ();

		load_dir_real (engine,
			       plugin_dir,
			       PLUGIN_EXT,
			       load_plugin_info,
			       NULL);

		g_free (plugin_dir);
	}
}

static guint
hash_lowercase (gconstpointer data)
{
	gchar *lowercase;
	guint ret;
	
	lowercase = g_ascii_strdown ((const gchar *)data, -1);
	ret = g_str_hash (lowercase);
	g_free (lowercase);
	
	return ret;
}

static gboolean
equal_lowercase (gconstpointer a, gconstpointer b)
{
	return g_ascii_strcasecmp ((const gchar *)a, (const gchar *)b) == 0;
}

static void
loader_destroy (LoaderInfo *info)
{
	if (!info)
		return;
	
	if (info->loader)
		g_object_unref (info->loader);
	
	g_free (info);
}

static void
add_loader (MarinaPluginsEngine *engine,
	    const gchar        *loader_id,
	    MarinaObjectModule  *module)
{
	LoaderInfo *info;

	info = g_new (LoaderInfo, 1);
	info->loader = NULL;
	info->module = module;

	g_hash_table_insert (engine->priv->loaders, g_strdup (loader_id), info);
}

static void
marina_plugins_engine_init (MarinaPluginsEngine *engine)
{
	marina_debug (DEBUG_PLUGINS);

	if (!g_module_supported ())
	{
		g_warning ("marina is not able to initialize the plugins engine.");
		return;
	}

	engine->priv = G_TYPE_INSTANCE_GET_PRIVATE (engine,
						    MARINA_TYPE_PLUGINS_ENGINE,
						    MarinaPluginsEnginePrivate);

	load_all_plugins (engine);

	/* make sure that the first reactivation will read active plugins
	   from the prefs */
	engine->priv->activate_from_prefs = TRUE;

	/* mapping from loadername -> loader object */
	engine->priv->loaders = g_hash_table_new_full (hash_lowercase,
						       equal_lowercase,
						       (GDestroyNotify)g_free,
						       (GDestroyNotify)loader_destroy);
}

static void
loader_garbage_collect (const char *id, LoaderInfo *info)
{
	if (info->loader)
		marina_plugin_loader_garbage_collect (info->loader);
}

void
marina_plugins_engine_garbage_collect (MarinaPluginsEngine *engine)
{
	g_hash_table_foreach (engine->priv->loaders,
			      (GHFunc) loader_garbage_collect,
			      NULL);
}

static void
marina_plugins_engine_finalize (GObject *object)
{
	MarinaPluginsEngine *engine = MARINA_PLUGINS_ENGINE (object);
	GList *item;
	
	marina_debug (DEBUG_PLUGINS);

	/* Firs deactivate all plugins */
	for (item = engine->priv->plugin_list; item; item = item->next)
	{
		MarinaPluginInfo *info = MARINA_PLUGIN_INFO (item->data);
		
		if (marina_plugin_info_is_active (info))
			marina_plugins_engine_deactivate_plugin_real (engine, info);
	}
	
	/* unref the loaders */	
	g_hash_table_destroy (engine->priv->loaders);

	/* and finally free the infos */
	for (item = engine->priv->plugin_list; item; item = item->next)
	{
		MarinaPluginInfo *info = MARINA_PLUGIN_INFO (item->data);

		_marina_plugin_info_unref (info);
	}

	g_list_free (engine->priv->plugin_list);
}

static void
marina_plugins_engine_class_init (MarinaPluginsEngineClass *klass)
{
	GType the_type = G_TYPE_FROM_CLASS (klass);
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	object_class->finalize = marina_plugins_engine_finalize;
	klass->activate_plugin = marina_plugins_engine_activate_plugin_real;
	klass->deactivate_plugin = marina_plugins_engine_deactivate_plugin_real;

	signals[ACTIVATE_PLUGIN] =
		g_signal_new ("activate-plugin",
			      the_type,
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (MarinaPluginsEngineClass, activate_plugin),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__BOXED,
			      G_TYPE_NONE,
			      1,
			      MARINA_TYPE_PLUGIN_INFO | G_SIGNAL_TYPE_STATIC_SCOPE);

	signals[DEACTIVATE_PLUGIN] =
		g_signal_new ("deactivate-plugin",
			      the_type,
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (MarinaPluginsEngineClass, deactivate_plugin),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__BOXED,
			      G_TYPE_NONE,
			      1,
			      MARINA_TYPE_PLUGIN_INFO | G_SIGNAL_TYPE_STATIC_SCOPE);

	g_type_class_add_private (klass, sizeof (MarinaPluginsEnginePrivate));
}

static gboolean
load_loader (MarinaPluginsEngine *engine,
	     const gchar        *filename,
	     gpointer		 data)
{
	MarinaObjectModule *module;
	gchar *base;
	gchar *path;
	const gchar *id;
	GType type;
	
	/* try to load in the module */
	path = g_path_get_dirname (filename);
	base = g_path_get_basename (filename);

	module = marina_object_module_new (base, path, "register_marina_plugin_loader");
	
	g_free (base);
	g_free (path);

	/* make sure to load the type definition */
	if (!g_type_module_use (G_TYPE_MODULE (module)))
	{
		g_object_unref (module);
		g_warning ("Plugin loader module `%s' could not be loaded", filename);

		return TRUE;
	}

	/* get the exported type and check the name as exported by the 
	 * loader interface */
	type = marina_object_module_get_object_type (module);
	id = marina_plugin_loader_type_get_id (type);
	
	add_loader (engine, id, module);	
	g_type_module_unuse (G_TYPE_MODULE (module));

	return TRUE;
}

static void
ensure_loader (LoaderInfo *info)
{
	if (info->loader == NULL && info->module != NULL)
	{
		/* create a new loader object */
		MarinaPluginLoader *loader;
		loader = (MarinaPluginLoader *)marina_object_module_new_object (info->module, NULL);
	
		if (loader == NULL || !MARINA_IS_PLUGIN_LOADER (loader))
		{
			g_warning ("Loader object is not a valid MarinaPluginLoader instance");
		
			if (loader != NULL && G_IS_OBJECT (loader))
				g_object_unref (loader);
		}
		else
		{
			info->loader = loader;
		}
	}
}

static MarinaPluginLoader *
get_plugin_loader (MarinaPluginsEngine *engine, MarinaPluginInfo *info)
{
	const gchar *loader_id;
	LoaderInfo *loader_info;

	loader_id = info->loader;

	loader_info = (LoaderInfo *)g_hash_table_lookup (
			engine->priv->loaders, 
			loader_id);

	if (loader_info == NULL)
	{
		gchar *loader_dir;

		loader_dir = marina_dirs_get_marina_plugin_loaders_dir ();

		/* loader could not be found in the hash, try to find it by 
		   scanning */
		load_dir_real (engine, 
			       loader_dir,
			       LOADER_EXT,
			       (LoadDirCallback)load_loader,
			       NULL);
		g_free (loader_dir);
		
		loader_info = (LoaderInfo *)g_hash_table_lookup (
				engine->priv->loaders, 
				loader_id);
	}

	if (loader_info == NULL)
	{
		/* cache non-existent so we don't scan again */
		add_loader (engine, loader_id, NULL);
		return NULL;
	}
	
	ensure_loader (loader_info);
	return loader_info->loader;
}

MarinaPluginsEngine *
marina_plugins_engine_get_default (void)
{
	if (default_engine != NULL)
		return default_engine;

	default_engine = MARINA_PLUGINS_ENGINE (g_object_new (MARINA_TYPE_PLUGINS_ENGINE, NULL));
	g_object_add_weak_pointer (G_OBJECT (default_engine),
				   (gpointer) &default_engine);
	return default_engine;
}

const GList *
marina_plugins_engine_get_plugin_list (MarinaPluginsEngine *engine)
{
	marina_debug (DEBUG_PLUGINS);

	return engine->priv->plugin_list;
}

static gint
compare_plugin_info_and_name (MarinaPluginInfo *info,
			      const gchar *module_name)
{
	return strcmp (marina_plugin_info_get_module_name (info), module_name);
}

MarinaPluginInfo *
marina_plugins_engine_get_plugin_info (MarinaPluginsEngine *engine,
				      const gchar        *name)
{
	GList *l = g_list_find_custom (engine->priv->plugin_list,
				       name,
				       (GCompareFunc) compare_plugin_info_and_name);

	return l == NULL ? NULL : (MarinaPluginInfo *) l->data;
}

static void
save_active_plugin_list (MarinaPluginsEngine *engine)
{
	GSList *active_plugins = NULL;
	GList *l;

	for (l = engine->priv->plugin_list; l != NULL; l = l->next)
	{
		MarinaPluginInfo *info = (MarinaPluginInfo *) l->data;

		if (marina_plugin_info_is_active (info))
		{
			active_plugins = g_slist_prepend (active_plugins,
							  (gpointer)marina_plugin_info_get_module_name (info));
		}
	}

	marina_prefs_set_active_plugins (active_plugins);

	g_slist_free (active_plugins);
}

static gboolean
load_plugin (MarinaPluginsEngine *engine,
	     MarinaPluginInfo    *info)
{
	MarinaPluginLoader *loader;
	gchar *path;

	if (marina_plugin_info_is_active (info))
		return TRUE;
	
	if (!marina_plugin_info_is_available (info))
		return FALSE;

	loader = get_plugin_loader (engine, info);
	
	if (loader == NULL)
	{
		g_warning ("Could not find loader `%s' for plugin `%s'", info->loader, info->name);
		info->available = FALSE;
		return FALSE;
	}
	
	path = g_path_get_dirname (info->file);
	g_return_val_if_fail (path != NULL, FALSE);

	info->plugin = marina_plugin_loader_load (loader, info, path);
	
	g_free (path);
	
	if (info->plugin == NULL)
	{
		g_warning ("Error loading plugin '%s'", info->name);
		info->available = FALSE;
		return FALSE;
	}
	
	return TRUE;
}

static void
marina_plugins_engine_activate_plugin_real (MarinaPluginsEngine *engine,
					   MarinaPluginInfo *info)
{
	if (!load_plugin (engine, info))
		return;

	MarinaWindow *window = marina_app_get_window (marina_app_get_default ());
	marina_plugin_activate (info->plugin, window);
}

gboolean
marina_plugins_engine_activate_plugin (MarinaPluginsEngine *engine,
				      MarinaPluginInfo    *info)
{
	marina_debug (DEBUG_PLUGINS);

	g_return_val_if_fail (info != NULL, FALSE);

	if (!marina_plugin_info_is_available (info))
		return FALSE;
		
	if (marina_plugin_info_is_active (info))
		return TRUE;

	g_signal_emit (engine, signals[ACTIVATE_PLUGIN], 0, info);

	if (marina_plugin_info_is_active (info))
		save_active_plugin_list (engine);

	return marina_plugin_info_is_active (info);
}

static void
call_plugin_deactivate (MarinaPlugin *plugin, 
			MarinaWindow *window)
{
	marina_plugin_deactivate (plugin, window);

	/* ensure update of ui manager, because we suspect it does something
	   with expected static strings in the type module (when unloaded the
	   strings don't exist anymore, and ui manager updates in an idle
	   func) */
	gtk_ui_manager_ensure_update (marina_window_get_ui_manager (window));
}

static void
marina_plugins_engine_deactivate_plugin_real (MarinaPluginsEngine *engine,
					     MarinaPluginInfo *info)
{
	MarinaPluginLoader *loader;

	if (!marina_plugin_info_is_active (info) || 
	    !marina_plugin_info_is_available (info))
		return;

	MarinaWindow *window = marina_app_get_window (marina_app_get_default ());
	call_plugin_deactivate (info->plugin, window);

	/* first unref the plugin (the loader still has one) */
	g_object_unref (info->plugin);
	
	/* find the loader and tell it to gc and unload the plugin */
	loader = get_plugin_loader (engine, info);
	
	marina_plugin_loader_garbage_collect (loader);
	marina_plugin_loader_unload (loader, info);
	
	info->plugin = NULL;
}

gboolean
marina_plugins_engine_deactivate_plugin (MarinaPluginsEngine *engine,
					MarinaPluginInfo    *info)
{
	marina_debug (DEBUG_PLUGINS);

	g_return_val_if_fail (info != NULL, FALSE);

	if (!marina_plugin_info_is_active (info))
		return TRUE;

	g_signal_emit (engine, signals[DEACTIVATE_PLUGIN], 0, info);
	if (!marina_plugin_info_is_active (info))
		save_active_plugin_list (engine);

	return !marina_plugin_info_is_active (info);
}

void
marina_plugins_engine_activate_plugins (MarinaPluginsEngine *engine,
					MarinaWindow        *window)
{
	GSList *active_plugins = NULL;
	GList *pl;

	marina_debug (DEBUG_PLUGINS);

	g_return_if_fail (MARINA_IS_PLUGINS_ENGINE (engine));
	g_return_if_fail (MARINA_IS_WINDOW (window));

	/* the first time, we get the 'active' plugins from gconf */
	if (engine->priv->activate_from_prefs)
	{
		active_plugins = marina_prefs_get_active_plugins ();
	}

	for (pl = engine->priv->plugin_list; pl; pl = pl->next)
	{
		MarinaPluginInfo *info = (MarinaPluginInfo*)pl->data;
		
		if (engine->priv->activate_from_prefs && 
		    g_slist_find_custom (active_plugins,
					 marina_plugin_info_get_module_name (info),
					 (GCompareFunc)strcmp) == NULL)
			continue;
		
		/* If plugin is not active, don't try to activate/load it */
		if (!engine->priv->activate_from_prefs && 
		    !marina_plugin_info_is_active (info))
			continue;

		if (load_plugin (engine, info))
			marina_plugin_activate (info->plugin,
					       window);
	}
	
	if (engine->priv->activate_from_prefs)
	{
		g_slist_foreach (active_plugins, (GFunc) g_free, NULL);
		g_slist_free (active_plugins);
		engine->priv->activate_from_prefs = FALSE;
	}
	
	marina_debug_message (DEBUG_PLUGINS, "End");

	/* also call update_ui after activation */
	marina_plugins_engine_update_plugins_ui (engine, window);
}

void
marina_plugins_engine_deactivate_plugins (MarinaPluginsEngine *engine,
					  MarinaWindow        *window)
{
	GList *pl;
	
	marina_debug (DEBUG_PLUGINS);

	g_return_if_fail (MARINA_IS_PLUGINS_ENGINE (engine));
	g_return_if_fail (MARINA_IS_WINDOW (window));
	
	for (pl = engine->priv->plugin_list; pl; pl = pl->next)
	{
		MarinaPluginInfo *info = (MarinaPluginInfo*)pl->data;
		
		/* check if the plugin is actually active */
		if (!marina_plugin_info_is_active (info))
			continue;
		
		/* call deactivate for the plugin for this window */
		marina_plugin_deactivate (info->plugin, window);
	}
	
	marina_debug_message (DEBUG_PLUGINS, "End");
}

void
marina_plugins_engine_update_plugins_ui (MarinaPluginsEngine *engine,
					 MarinaWindow        *window)
{
	GList *pl;

	marina_debug (DEBUG_PLUGINS);

	g_return_if_fail (MARINA_IS_PLUGINS_ENGINE (engine));
	g_return_if_fail (MARINA_IS_WINDOW (window));

	/* call update_ui for all active plugins */
	for (pl = engine->priv->plugin_list; pl; pl = pl->next)
	{
		MarinaPluginInfo *info = (MarinaPluginInfo*)pl->data;

		if (!marina_plugin_info_is_active (info))
			continue;
			
	       	marina_debug_message (DEBUG_PLUGINS, "Updating UI of %s", info->name);
		marina_plugin_update_ui (info->plugin, window);
	}
}

void 	 
marina_plugins_engine_configure_plugin (MarinaPluginsEngine *engine,
				        MarinaPluginInfo    *info,
				        GtkWindow           *parent)
{
	GtkWidget *conf_dlg;
	
	GtkWindowGroup *wg;
	
	marina_debug (DEBUG_PLUGINS);

	g_return_if_fail (info != NULL);

	conf_dlg = marina_plugin_create_configure_dialog (info->plugin);
	g_return_if_fail (conf_dlg != NULL);
	gtk_window_set_transient_for (GTK_WINDOW (conf_dlg),
				      parent);

	wg = parent->group;		      
	if (wg == NULL)
	{
		wg = gtk_window_group_new ();
		gtk_window_group_add_window (wg, parent);
	}
			
	gtk_window_group_add_window (wg,
				     GTK_WINDOW (conf_dlg));
		
	gtk_window_set_modal (GTK_WINDOW (conf_dlg), TRUE);		     
	gtk_widget_show (conf_dlg);
}

void 
marina_plugins_engine_active_plugins_changed (MarinaPluginsEngine *engine)
{
	gboolean to_activate;
	GSList *active_plugins;
	GList *pl;

	marina_debug (DEBUG_PLUGINS);

	active_plugins = marina_prefs_get_active_plugins ();

	for (pl = engine->priv->plugin_list; pl; pl = pl->next)
	{
		MarinaPluginInfo *info = (MarinaPluginInfo*)pl->data;

		if (!marina_plugin_info_is_available (info))
			continue;

		to_activate = (g_slist_find_custom (active_plugins,
						    marina_plugin_info_get_module_name (info),
						    (GCompareFunc)strcmp) != NULL);

		if (!marina_plugin_info_is_active (info) && to_activate)
			g_signal_emit (engine, signals[ACTIVATE_PLUGIN], 0, info);
		else if (marina_plugin_info_is_active (info) && !to_activate)
			g_signal_emit (engine, signals[DEACTIVATE_PLUGIN], 0, info);
	}

	g_slist_foreach (active_plugins, (GFunc) g_free, NULL);
	g_slist_free (active_plugins);
}

void
marina_plugins_engine_rescan_plugins (MarinaPluginsEngine *engine)
{
	marina_debug (DEBUG_PLUGINS);
	
	load_all_plugins (engine);
}
