/* marina-vfs-source.c
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#include "marina-vfs-source.h"
#include "marina-source-base.h"

struct _MarinaVfsSourcePrivate
{
  gchar *uri;
};

enum
{
  PROP_0,
  PROP_URI,
};

static void marina_source_init (gpointer g_iface, gpointer iface_data);

G_DEFINE_TYPE_EXTENDED (MarinaVfsSource,
                        marina_vfs_source,
                        MARINA_TYPE_SOURCE_BASE,
                        0,
                        G_IMPLEMENT_INTERFACE (MARINA_TYPE_SOURCE,
                                               marina_source_init));

MarinaVfsSource*
marina_vfs_source_new (void)
{
  return g_object_new (MARINA_TYPE_VFS_SOURCE, NULL);
}

static gboolean
marina_vfs_source_has_stream (MarinaSource *source)
{
  return TRUE;
}

static GInputStream*
marina_vfs_source_get_stream (MarinaSource  *source,
                              GError       **error)
{
  MarinaVfsSourcePrivate *priv;
  GFile *file;
  GInputStream *stream = NULL;
  
  g_return_val_if_fail (MARINA_IS_VFS_SOURCE (source), NULL);
  
  priv = MARINA_VFS_SOURCE (source)->priv;
  
  if (priv->uri)
    {
      file = g_file_new_for_uri (priv->uri);
      stream = (GInputStream*)g_file_read (file, NULL, error);
      g_object_unref (file);
    }
  
  return stream;
}

const gchar*
marina_vfs_source_get_uri (MarinaVfsSource *vfs_source)
{
  MarinaVfsSourcePrivate *priv;
  
  g_return_val_if_fail (MARINA_IS_VFS_SOURCE (vfs_source), NULL);
  
  priv = vfs_source->priv;
  
  return priv->uri;
}

void
marina_vfs_source_set_uri (MarinaVfsSource *vfs_source,
                           const gchar     *uri)
{
  MarinaVfsSourcePrivate *priv;
  
  g_return_if_fail (MARINA_IS_VFS_SOURCE (vfs_source));
  
  priv = vfs_source->priv;
  
  g_free (priv->uri);
  
  priv->uri = g_strdup (uri);
}

static void
marina_vfs_source_finalize (GObject *object)
{
  G_OBJECT_CLASS (marina_vfs_source_parent_class)->finalize (object);
}

static void
marina_vfs_source_get_property (GObject    *object,
                                guint       property_id,
                                GValue     *value,
                                GParamSpec *pspec)
{
  switch (property_id) {
  case PROP_URI:
    g_value_set_string (value, marina_vfs_source_get_uri (MARINA_VFS_SOURCE (object)));
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
  }
}

static void
marina_vfs_source_set_property (GObject      *object,
                                guint         property_id,
                                const GValue *value,
                                GParamSpec   *pspec)
{
  switch (property_id) {
  case PROP_URI:
    marina_vfs_source_set_uri (MARINA_VFS_SOURCE (object), g_value_get_string (value));
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
  }
}

static void
marina_vfs_source_class_init (MarinaVfsSourceClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  
  object_class->get_property = marina_vfs_source_get_property;
  object_class->set_property = marina_vfs_source_set_property;
  object_class->finalize = marina_vfs_source_finalize;

  g_type_class_add_private (object_class, sizeof (MarinaVfsSourcePrivate));
  
  g_object_class_install_property (object_class,
                                   PROP_URI,
                                   g_param_spec_string ("uri",
                                                        "Uri",
                                                        "The vfs source URI",
                                                        NULL,
                                                        G_PARAM_READWRITE));
}

static void
marina_vfs_source_init (MarinaVfsSource *vfs_source)
{
  vfs_source->priv = G_TYPE_INSTANCE_GET_PRIVATE (vfs_source,
                                                  MARINA_TYPE_VFS_SOURCE,
                                                  MarinaVfsSourcePrivate);
}

static void
marina_source_init (gpointer g_iface,
                    gpointer iface_data)
{
  MarinaSourceIface *iface = g_iface;
  
  iface->get_stream = marina_vfs_source_get_stream;
  iface->has_stream = marina_vfs_source_has_stream;
}
