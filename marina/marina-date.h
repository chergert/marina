/* marina-date.h
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

/**
 * SECTION:marina-date
 * @short_description: A boxed date abstraction for Marina
 *
 * #MarinaDate provides a boxed data type that is friendlier for both
 * 32-bit and 64-bit platforms than working with time_t or other dates
 * directly (such as #GTimeVal). It has the ability to cache the local
 * generated string for the date and re-use it. This is ideal for
 * use in #GtkTreeView<!-- -->'s where the string would be generated
 * frequently.
 */

#ifndef __MARINA_DATE_TIME_H__
#define __MARINA_DATE_TIME_H__

#include <time.h>
#include <glib-object.h>

G_BEGIN_DECLS

#define MARINA_TYPE_DATE (marina_date_get_type ())
#define MARINA_DATE(boxed) ((MarinaDate*)boxed)

typedef struct
{
  time_t  timet;
  gchar  *as_str;
} MarinaDate;

GType        marina_date_get_type        (void);

MarinaDate*  marina_date_new             (void);
MarinaDate*  marina_date_new_from_time_t (time_t timet);
MarinaDate*  marina_date_new_from_string (const gchar *date_string);

const gchar* marina_date_to_string       (MarinaDate  *date);
gchar*       marina_date_format          (MarinaDate  *date,
                                          const gchar *format);

MarinaDate*  marina_date_copy            (const MarinaDate *date);
void         marina_date_free            (MarinaDate *date);
guint        marina_date_hash            (const MarinaDate *date);
gboolean     marina_date_equal           (const MarinaDate *date1,
                                         const MarinaDate *date2);

MarinaDate*  g_value_get_date            (const GValue     *value);
void         g_value_set_date            (GValue           *value,
                                         const MarinaDate *date);

G_END_DECLS

#endif /* __MARINA_DATE_TIME_H__ */
