/* marina-prefs.c
 * 
 * This file is part of marina; it was borrowed from gedit
 *
 * Copyright (C) 2002  Paolo Maggi
 * Copyright (C) 2009  Christian Hergert <chris@dronelabs.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA 02111-1307, USA. 
 */
 
/*
 * Modified by the gedit Team, 2002. See the gedit AUTHORS file for a 
 * list of people on the marina Team.  
 *
 * Modified by the marina Team, 2009.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>

#include <glib/gi18n.h>
#include <gconf/gconf-value.h>
#include <gconf/gconf-client.h>

#include "marina-prefs.h"
#include "marina-debug.h"

static GConfClient *gconf_client = NULL;

#define DEFINE_BOOL_PREF(name, key, def) gboolean                             \
marina_prefs_get_ ## name (void)                                              \
{                                                                             \
        marina_debug (DEBUG_PREFS);                                           \
                                                                              \
        return marina_prefs_get_bool (key,                                    \
                                      (def));                                 \
}                                                                             \
                                                                              \
void                                                                          \
marina_prefs_set_ ## name (gboolean v)                                        \
{                                                                             \
        marina_debug (DEBUG_PREFS);                                           \
                                                                              \
        marina_prefs_set_bool (key,                                           \
                               v);                                            \
}                                                                             \
                                                                              \
gboolean                                                                      \
marina_prefs_ ## name ## _can_set (void)                                      \
{                                                                             \
        marina_debug (DEBUG_PREFS);                                           \
                                                                              \
        return marina_prefs_key_is_writable (key);                            \
}



#define DEFINE_INT_PREF(name, key, def) gint                                  \
marina_prefs_get_ ## name (void)                                              \
{                                                                             \
        marina_debug (DEBUG_PREFS);                                           \
                                                                              \
        return marina_prefs_get_int (key,                                     \
                                     (def));                                  \
}                                                                             \
                                                                              \
void                                                                          \
marina_prefs_set_ ## name (gint v)                                            \
{                                                                             \
        marina_debug (DEBUG_PREFS);                                           \
                                                                              \
        marina_prefs_set_int (key,                                            \
                              v);                                             \
}                                                                             \
                                                                              \
gboolean                                                                      \
marina_prefs_ ## name ## _can_set (void)                                      \
{                                                                             \
        marina_debug (DEBUG_PREFS);                                           \
                                                                              \
        return marina_prefs_key_is_writable (key);                            \
}                



#define DEFINE_STRING_PREF(name, key, def) gchar*                             \
marina_prefs_get_ ## name (void)                                              \
{                                                                             \
        marina_debug (DEBUG_PREFS);                                           \
                                                                              \
        return marina_prefs_get_string (key,                                  \
                                        def);                                 \
}                                                                             \
                                                                              \
void                                                                          \
marina_prefs_set_ ## name (const gchar* v)                                    \
{                                                                             \
        marina_debug (DEBUG_PREFS);                                           \
                                                                              \
        marina_prefs_set_string (key,                                         \
                                 v);                                          \
}                                                                             \
                                                                              \
gboolean                                                                      \
marina_prefs_ ## name ## _can_set (void)                                      \
{                                                                             \
        marina_debug (DEBUG_PREFS);                                           \
                                                                              \
        return marina_prefs_key_is_writable (key);                            \
}                

static gboolean     gconf_client_get_bool_with_default          (GConfClient* client, 
                                                                 const gchar* key, 
                                                                 gboolean def, 
                                                                 GError** err);

static gchar       *gconf_client_get_string_with_default (GConfClient* client, 
                                                          const gchar* key,
                                                          const gchar* def, 
                                                          GError** err);

static gint         gconf_client_get_int_with_default    (GConfClient* client, 
                                                          const gchar* key,
                                                          gint def, 
                                                          GError** err);

static gboolean     marina_prefs_get_bool        (const gchar* key, 
                                                  gboolean def);

static gint         marina_prefs_get_int         (const gchar* key, 
                                                  gint def);

static gchar       *marina_prefs_get_string      (const gchar* key, 
                                                  const gchar* def);

gboolean
marina_prefs_init (void)
{
  marina_debug (DEBUG_PREFS);

  if (gconf_client == NULL)
    {
      gconf_client = gconf_client_get_default ();
      if (gconf_client == NULL)
        {
              g_warning (_("Cannot initialize preferences manager."));
              return FALSE; /* Causes MarinaApp to abort execution */
        }
    }

  return gconf_client != NULL;
}

void
marina_prefs_shutdown ()
{
  marina_debug (DEBUG_PREFS);

  g_return_if_fail (gconf_client != NULL);

  g_object_unref (gconf_client);
  gconf_client = NULL;
}

static gboolean                 
marina_prefs_get_bool (const gchar* key, gboolean def)
{
  marina_debug (DEBUG_PREFS);

  g_return_val_if_fail (gconf_client != NULL, def);

  return gconf_client_get_bool_with_default (gconf_client,
                                             key,
                                             def,
                                             NULL);
}

static gint 
marina_prefs_get_int (const gchar* key, gint def)
{
  marina_debug (DEBUG_PREFS);

  g_return_val_if_fail (gconf_client != NULL, def);

  return gconf_client_get_int_with_default (gconf_client,
                                            key,
                                            def,
                                            NULL);
}        

static gchar *
marina_prefs_get_string (const gchar* key, const gchar* def)
{
  marina_debug (DEBUG_PREFS);

  g_return_val_if_fail (gconf_client != NULL, 
                        def ? g_strdup (def) : NULL);

  return gconf_client_get_string_with_default (gconf_client,
                                               key,
                                               def,
                                               NULL);
}        

static void                 
marina_prefs_set_bool (const gchar* key, gboolean value)
{
  marina_debug (DEBUG_PREFS);

  g_return_if_fail (gconf_client != NULL);
  g_return_if_fail (gconf_client_key_is_writable (gconf_client, key, NULL));
  gconf_client_set_bool (gconf_client, key, value, NULL);
}

static void                 
marina_prefs_set_int (const gchar* key, gint value)
{
  marina_debug (DEBUG_PREFS);

  g_return_if_fail (gconf_client != NULL);
  g_return_if_fail (gconf_client_key_is_writable (gconf_client, key, NULL));
  gconf_client_set_int (gconf_client, key, value, NULL);
}

static void                 
marina_prefs_set_string (const gchar* key, const gchar* value)
{
  marina_debug (DEBUG_PREFS);

  g_return_if_fail (value != NULL);
  
  g_return_if_fail (gconf_client != NULL);
  g_return_if_fail (gconf_client_key_is_writable (gconf_client, key, NULL));
  gconf_client_set_string (gconf_client, key, value, NULL);
}

static gboolean 
marina_prefs_key_is_writable (const gchar* key)
{
  marina_debug (DEBUG_PREFS);

  g_return_val_if_fail (gconf_client != NULL, FALSE);
  return gconf_client_key_is_writable (gconf_client, key, NULL);
}

/* Window size */
DEFINE_INT_PREF (window_width,
                 MPM_WINDOW_WIDTH,
                 MPM_DEFAULT_WINDOW_WIDTH)

DEFINE_INT_PREF (window_height,
                 MPM_WINDOW_HEIGHT,
                 MPM_DEFAULT_WINDOW_HEIGHT)

/* Window position */
DEFINE_INT_PREF (window_x_pos,
                 MPM_WINDOW_X_POS,
                 MPM_DEFAULT_WINDOW_X_POS)

DEFINE_INT_PREF (window_y_pos,
                 MPM_WINDOW_Y_POS,
                 MPM_DEFAULT_WINDOW_Y_POS)

/* Statusbar visiblity */
DEFINE_BOOL_PREF (statusbar_visible,
                  MPM_STATUSBAR_VISIBLE,
                  MPM_DEFAULT_STATUSBAR_VISIBLE)
                  
/* Side Pane visiblity */
DEFINE_BOOL_PREF (side_pane_visible,
                  MPM_SIDE_PANE_VISIBLE,
                  MPM_DEFAULT_SIDE_PANE_VISIBLE)
                  
/* Bottom Panel visiblity */
DEFINE_BOOL_PREF (bottom_panel_visible,
                  MPM_BOTTOM_PANEL_VISIBLE,
                  MPM_DEFAULT_BOTTOM_PANEL_VISIBLE)                                    

/* Network Refresh */
DEFINE_INT_PREF (network_refresh,
                 MPM_NETWORK_REFRESH,
                 MPM_DEFAULT_NETWORK_REFRESH)

/* Network use-proxy */
DEFINE_BOOL_PREF (network_use_proxy,
                  MPM_NETWORK_USE_PROXY,
                  MPM_DEFAULT_NETWORK_USE_PROXY)

/* Network proxy uri */
DEFINE_STRING_PREF (network_proxy_uri,
                    MPM_NETWORK_PROXY_URI,
                    MPM_DEFAULT_NETWORK_PROXY_URI)

/* Toolbar visibility */
DEFINE_BOOL_PREF (toolbar_visible,
                  MPM_TOOLBAR_VISIBLE,
                  MPM_DEFAULT_TOOLBAR_VISIBLE)

/* Scheme */
DEFINE_STRING_PREF (scheme,
                    MPM_SCHEME,
                    MPM_DEFAULT_SCHEME)

/* Compact mode */
DEFINE_BOOL_PREF (compact_mode,
                  MPM_COMPACT_MODE,
                  MPM_DEFAULT_COMPACT_MODE)

/* Use default font */
DEFINE_BOOL_PREF (use_default_font,
                  MPM_USE_DEFAULT_FONT,
                  MPM_DEFAULT_USE_DEFAULT_FONT)

/* Font name */
DEFINE_STRING_PREF (font,
                    MPM_FONT,
                    MPM_DEFAULT_FONT)

/* Plugins: we just store/return a list of strings, all the magic has to
 * happen in the plugin engine */

GSList *
marina_prefs_get_active_plugins (void)
{
  GSList *plugins;

  marina_debug (DEBUG_PREFS);

  g_return_val_if_fail (gconf_client != NULL, NULL);

  plugins = gconf_client_get_list (gconf_client,
                                   MPM_ACTIVE_PLUGINS,
                                   GCONF_VALUE_STRING, 
                                   NULL);

  return plugins;
}

void
marina_prefs_set_active_plugins (const GSList *plugins)
{        
  g_return_if_fail (gconf_client != NULL);
  g_return_if_fail (marina_prefs_active_plugins_can_set ());

  gconf_client_set_list (gconf_client,
                         MPM_ACTIVE_PLUGINS,
                         GCONF_VALUE_STRING,
                         (GSList *) plugins,
                         NULL);
}

gboolean
marina_prefs_active_plugins_can_set (void)
{
  marina_debug (DEBUG_PREFS);

  return marina_prefs_key_is_writable (MPM_ACTIVE_PLUGINS);
}

/* The following functions are taken from gconf-client.c 
 * and partially modified. 
 * The licensing terms on these is: 
 *
 * 
 * GConf
 * Copyright (C) 1999, 2000, 2000 Red Hat Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */


static const gchar* 
gconf_value_type_to_string(GConfValueType type)
{
  switch (type)
    {
    case GCONF_VALUE_INT:
      return "int";
      break;
    case GCONF_VALUE_STRING:
      return "string";
      break;
    case GCONF_VALUE_FLOAT:
      return "float";
      break;
    case GCONF_VALUE_BOOL:
      return "bool";
      break;
    case GCONF_VALUE_SCHEMA:
      return "schema";
      break;
    case GCONF_VALUE_LIST:
      return "list";
      break;
    case GCONF_VALUE_PAIR:
      return "pair";
      break;
    case GCONF_VALUE_INVALID:
      return "*invalid*";
      break;
    default:
      g_return_val_if_reached (NULL);
      break;
    }
}

/* Emit the proper signals for the error, and fill in err */
static gboolean
handle_error (GConfClient* client, GError* error, GError** err)
{
  if (error != NULL)
    {
      gconf_client_error(client, error);
      
      if (err == NULL)
        {
          gconf_client_unreturned_error(client, error);

          g_error_free(error);
        }
      else
        *err = error;

      return TRUE;
    }
  else
    return FALSE;
}

static gboolean
check_type (const gchar* key, GConfValue* val, GConfValueType t, GError** err)
{
  if (val->type != t)
    {
      /*
      gconf_set_error(err, GCONF_ERROR_TYPE_MISMATCH,
                      _("Expected `%s' got `%s' for key %s"),
                      gconf_value_type_to_string(t),
                      gconf_value_type_to_string(val->type),
                      key);
      */
      g_set_error (err, GCONF_ERROR, GCONF_ERROR_TYPE_MISMATCH,
                     _("Expected `%s' got `%s' for key %s"),
                   gconf_value_type_to_string(t),
                   gconf_value_type_to_string(val->type),
                   key);
              
      return FALSE;
    }
  else
    return TRUE;
}

static gboolean
gconf_client_get_bool_with_default (GConfClient* client, const gchar* key,
                                    gboolean def, GError** err)
{
  GError* error = NULL;
  GConfValue* val;

  g_return_val_if_fail (err == NULL || *err == NULL, def);

  val = gconf_client_get (client, key, &error);

  if (val != NULL)
    {
      gboolean retval = def;

      g_return_val_if_fail (error == NULL, retval);
      
      if (check_type (key, val, GCONF_VALUE_BOOL, &error))
        retval = gconf_value_get_bool (val);
      else
        handle_error (client, error, err);

      gconf_value_free (val);

      return retval;
    }
  else
    {
      if (error != NULL)
        handle_error (client, error, err);
      return def;
    }
}

static gchar*
gconf_client_get_string_with_default (GConfClient* client, const gchar* key,
                                      const gchar* def, GError** err)
{
  GError* error = NULL;
  gchar* val;

  g_return_val_if_fail (err == NULL || *err == NULL, def ? g_strdup (def) : NULL);

  val = gconf_client_get_string (client, key, &error);

  if (val != NULL)
    {
      g_return_val_if_fail (error == NULL, def ? g_strdup (def) : NULL);
      
      return val;
    }
  else
    {
      if (error != NULL)
        handle_error (client, error, err);
      return def ? g_strdup (def) : NULL;
    }
}

static gint
gconf_client_get_int_with_default (GConfClient* client, const gchar* key,
                                   gint def, GError** err)
{
  GError* error = NULL;
  GConfValue* val;

  g_return_val_if_fail (err == NULL || *err == NULL, def);

  val = gconf_client_get (client, key, &error);

  if (val != NULL)
    {
      gint retval = def;

      g_return_val_if_fail (error == NULL, def);
      
      if (check_type (key, val, GCONF_VALUE_INT, &error))
        retval = gconf_value_get_int(val);
      else
        handle_error (client, error, err);

      gconf_value_free (val);

      return retval;
    }
  else
    {
      if (error != NULL)
        handle_error (client, error, err);
      return def;
    }
}

