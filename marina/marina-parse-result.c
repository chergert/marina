/* marina-parse-result.c
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#include "marina-parse-result.h"

GType
marina_parse_result_get_type (void)
{
  static GType marina_parse_result_type = 0;
  
  if (!marina_parse_result_type)
    {
      const GTypeInfo marina_parse_result_info = {
        sizeof (MarinaParseResultIface),
        NULL, /* base_init */
        NULL, /* base_finalize */
        NULL,
        NULL, /* class_finalize */
        NULL, /* class_data */
        0,
        0,
        NULL
      };
      
      marina_parse_result_type = g_type_register_static (G_TYPE_INTERFACE, "MarinaParseResult",
                                                         &marina_parse_result_info, 0);
      g_type_interface_add_prerequisite (marina_parse_result_type, G_TYPE_OBJECT);
    }
  
  return marina_parse_result_type;
}

/**
 * marina_parse_result_get_attr:
 * @parse_result: A #MarinaParseResult
 * @attr: the attribute name
 * @value: a location to store the attribute
 *
 * Retrieves the value for a given attribute. You should check to
 * make sure the attribute exists with marina_parse_result_has_attr().
 */
void
marina_parse_result_get_attr (MarinaParseResult *parse_result,
                              const gchar       *attr,
                              GValue            *value)
{
  return MARINA_PARSE_RESULT_GET_INTERFACE (parse_result)->get_attr (parse_result, attr, value);
}

/**
 * marina_parse_result_get_attrs:
 * @parse_result: A #MarinaParseResult
 *
 * Retrieves a list of strings with the names of the available
 * attributse. The strings should not be modified or freed.
 * You need to free the list with g_list_free().
 *
 * Return value: A GList of strings that should be freed with g_list_free().
 */
GList*
marina_parse_result_get_attrs (MarinaParseResult *parse_result)
{
  return MARINA_PARSE_RESULT_GET_INTERFACE (parse_result)->get_attrs (parse_result);
}

/**
 * marina_parse_result_get_id:
 * @parse_result: A #MarinaParseResult
 *
 * Return value: The id for the parse instance. This should be the guid
 *   of the parsed source if discovered.
 */
const gchar*
marina_parse_result_get_id (MarinaParseResult *parse_result)
{
  return MARINA_PARSE_RESULT_GET_INTERFACE (parse_result)->get_id (parse_result);
}

/**
 * marina_parse_result_has_attr:
 * @parse_result: A #MarinaParseResult
 * @attr: the attribute name
 *
 * Checks to see if @attr exists in the current set of attributes.
 *
 * Return value: TRUE if it exists.
 */
gboolean
marina_parse_result_has_attr (MarinaParseResult *parse_result,
                              const gchar       *attr)
{
  GList *list;
  GList *tmp;
  gboolean success = FALSE;
  
  list = marina_parse_result_get_attrs (parse_result);
  for (tmp = list; tmp; tmp = tmp->next)
    {
      if (g_str_equal (attr, tmp->data))
        {
          success = TRUE;
          break;
        }
    }

  g_list_free (list);
  
  return success;
}

/**
 * marina_parse_result_move_first:
 * @parse_result: A #MarinaParseResult
 *
 * Moves the iterator to the first item that was parsed.
 *
 * Return value: TRUE if an item exists and is now the current item.
 */
gboolean
marina_parse_result_move_first (MarinaParseResult *parse_result)
{
  return MARINA_PARSE_RESULT_GET_INTERFACE (parse_result)->move_first (parse_result);
}

/**
 * marina_parse_result_move_next:
 * @parse_result: A #MarinaParseResult
 *
 * Moves the iterator to the next item.
 *
 * Return value: TRUE if there was another item to move to and was successful
 */
gboolean
marina_parse_result_move_next (MarinaParseResult *parse_result)
{
  return MARINA_PARSE_RESULT_GET_INTERFACE (parse_result)->move_next (parse_result);
}
