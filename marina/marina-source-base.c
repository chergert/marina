/* marina-source-base.c
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#include <stdlib.h>
#include <glib/gprintf.h>

#include "marina-source-base.h"

struct _MarinaSourceBasePrivate
{
  gchar *id;
  gchar *title;
};

enum
{
  PROP_0,
  PROP_ID,
  PROP_TITLE,
};

static void marina_source_init (gpointer g_iface, gpointer iface_data);

G_DEFINE_ABSTRACT_TYPE_WITH_CODE (MarinaSourceBase,
                                  marina_source_base,
                                  G_TYPE_OBJECT,
                                  G_IMPLEMENT_INTERFACE (MARINA_TYPE_SOURCE,
                                                         marina_source_init));

static void
marina_source_base_get_property (GObject    *object,
                                 guint       property_id,
                                 GValue     *value,
                                 GParamSpec *pspec)
{
  switch (property_id) {
  case PROP_ID:
    g_value_set_string (value, marina_source_get_id (MARINA_SOURCE (object)));
    break;
  case PROP_TITLE:
    g_value_set_string (value, marina_source_get_title (MARINA_SOURCE (object)));
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
  }
}

static void
marina_source_base_set_property (GObject      *object,
                                 guint         property_id,
                                 const GValue *value,
                                 GParamSpec   *pspec)
{
  switch (property_id) {
  case PROP_TITLE:
    marina_source_set_title (MARINA_SOURCE (object), g_value_get_string (value));
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
  }
}

static void
marina_source_base_finalize (GObject *object)
{
  G_OBJECT_CLASS (marina_source_base_parent_class)->finalize (object);
}

static void
marina_source_base_class_init (MarinaSourceBaseClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  
  object_class->finalize = marina_source_base_finalize;
  object_class->get_property = marina_source_base_get_property;
  object_class->set_property = marina_source_base_set_property;
  
  g_object_class_install_property (object_class,
                                   PROP_ID,
                                   g_param_spec_string ("id",
                                                        "Id",
                                                        "The source unique id",
                                                        NULL,
                                                        G_PARAM_READABLE));
  
  g_object_class_install_property (object_class,
                                   PROP_TITLE,
                                   g_param_spec_string ("title",
                                                        "Title",
                                                        "The source title",
                                                        NULL,
                                                        G_PARAM_READWRITE));
}

static void
marina_source_base_init (MarinaSourceBase *source)
{
}

static const gchar*
marina_source_base_get_id (MarinaSource *source)
{
  g_return_val_if_fail (MARINA_IS_SOURCE_BASE (source), NULL);
  
  return MARINA_SOURCE_BASE (source)->id;
}

static GList*
marina_source_base_get_children (MarinaSource *source)
{
  return NULL;
}

static gboolean
marina_source_base_is_transient (MarinaSource *source)
{
  return FALSE;
}

static gboolean
marina_source_base_observes (MarinaSource *source,
                             MarinaItem   *item)
{
  g_return_val_if_fail (MARINA_IS_SOURCE_BASE (source), FALSE);
  g_return_val_if_fail (source != NULL && item != NULL, FALSE);
  
  return (g_strcmp0 (marina_source_get_id (source),
                     marina_item_get_source_id (item)) == 0);
}

static void
marina_source_base_serialize (MarinaSource *source,
                              XmlWriter    *writer)
{
  GParamSpec **props;
  guint i, n;
  GType ptype;
  const gchar *pname;
  GValue value = {0,};
  
  g_return_if_fail (MARINA_IS_SOURCE_BASE (source));
  g_return_if_fail (XML_IS_WRITER (writer));
  
  /* start <source> */
  xml_writer_write_start_element (writer, "source");
  
  /* type="MarinaVfsSource" */
  xml_writer_write_attribute_string (writer, "type",
                                     g_type_name (G_TYPE_FROM_INSTANCE (source)));
  
  /* id="DEADBEEF" */
  xml_writer_write_attribute_string (writer, "id",
                                     marina_source_get_id (source));
  
  props = g_object_class_list_properties (G_OBJECT_GET_CLASS (source), &n);
  
  for (i = 0; i < n; i++)
    {
      if (!((props [i]->flags & G_PARAM_READWRITE) == G_PARAM_READWRITE))
        continue;
      
      ptype = G_PARAM_SPEC_VALUE_TYPE (props [i]);
      pname = g_param_spec_get_name (props [i]);
      
      g_value_init (&value, ptype);
      g_object_get_property (G_OBJECT (source), pname, &value);
      
      xml_writer_write_start_attribute (writer, pname);
      
      if (ptype == G_TYPE_BOOLEAN)
        {
          xml_writer_write_boolean (writer,
                                    g_value_get_boolean (&value));
        }
      else if (ptype == G_TYPE_STRING)
        {
          xml_writer_write_string (writer,
                                   g_value_get_string (&value));
        }
      else if (ptype == G_TYPE_DOUBLE)
        {
          xml_writer_write_double (writer,
                                   g_value_get_double (&value));
        }
      else if (ptype == G_TYPE_INT)
        {
          xml_writer_write_int (writer,
                                g_value_get_int (&value));
        }
      else if (ptype == G_TYPE_FLOAT)
        {
          xml_writer_write_float (writer,
                                  g_value_get_float (&value));
        }
      else if (ptype == G_TYPE_LONG)
        {
          xml_writer_write_long (writer,
                                 g_value_get_long (&value));
        }
      else if (ptype == G_TYPE_ULONG)
        {
          xml_writer_write_ulong (writer,
                                  g_value_get_ulong (&value));
        }
      else if (ptype == G_TYPE_INT64)
        {
          xml_writer_write_int64 (writer,
                                  g_value_get_int64 (&value));
        }
      else if (ptype == G_TYPE_UINT64)
        {
          xml_writer_write_uint64 (writer,
                                   g_value_get_uint64 (&value));
        }
      
      xml_writer_write_end_attribute (writer);

      g_value_unset (&value);
    }
    
    /* finish </source> */
    xml_writer_write_end_element (writer);
}

static void
marina_source_base_deserialize (MarinaSource *source,
                                XmlReader    *reader)
{
  MarinaSourceBase *source_base;
  GParamSpec *pspec;
  const gchar *name;
  const gchar *value;
  GType ptype;
  GValue gvalue = {0,};
  gint n;
  
  g_return_if_fail (MARINA_IS_SOURCE_BASE (source));
  g_return_if_fail (XML_IS_READER (reader));
  
  source_base = MARINA_SOURCE_BASE (source);
  
  n = xml_reader_count_attributes (reader);
  
  if (xml_reader_move_to_first_attribute (reader))
    {
      do
        {
          name = xml_reader_get_name (reader);
          
          if (g_str_equal (name, "type"))
            {
              continue;
            }
          else if (g_str_equal (name, "id"))
            {
              value = xml_reader_get_value (reader);
              
              g_free (source_base->id);
              source_base->id = g_strdup (value);
              
              continue;
            }
          
          value = xml_reader_get_value (reader);
          
          pspec = g_object_class_find_property (G_OBJECT_GET_CLASS (source), name);
          
          if (pspec && (pspec->flags & G_PARAM_READWRITE) == G_PARAM_READWRITE)
            {
              ptype = G_PARAM_SPEC_VALUE_TYPE (pspec);
              g_value_init (&gvalue, ptype);
              
              if (ptype == G_TYPE_BOOLEAN)
                {
                  if (g_str_equal (value, "true") ||
                      g_str_equal (value, "1")    ||
                      g_str_equal (value, "True") ||
                      g_str_equal (value, "TRUE"))
                    g_value_set_boolean (&gvalue, TRUE);
                  else
                    g_value_set_boolean (&gvalue, FALSE);
                }
              else if (ptype == G_TYPE_STRING)
                {
                  g_value_set_string (&gvalue, value);
                }
              else if (ptype == G_TYPE_DOUBLE)
                {
                  g_value_set_double (&gvalue, strtod (value, NULL));
                }
              else if (ptype == G_TYPE_INT)
                {
                  g_value_set_int (&gvalue, atoi (value));
                }
              else if (ptype == G_TYPE_FLOAT)
                {
                  g_value_set_float (&gvalue, atof (value));
                }
              else if (ptype == G_TYPE_LONG)
                {
                  g_value_set_long (&gvalue, atol (value));
                }
              else if (ptype == G_TYPE_ULONG)
                {
                  g_value_set_ulong (&gvalue, strtoul (value, NULL, 0));
                }
              else if (ptype == G_TYPE_INT64)
                {
                  g_value_set_int64 (&gvalue,
                                     g_ascii_strtoll (value, NULL, 0));
                }
              else if (ptype == G_TYPE_UINT64)
                {
                  g_value_set_uint64 (&gvalue,
                                      g_ascii_strtoull (value, NULL, 0));
                }
              else continue;
              
              g_object_set_property (G_OBJECT (source), name, &gvalue);
              g_value_unset (&gvalue);
            }
          else g_warning ("Invalid property \"%s\"", name);
        }
      while (xml_reader_move_to_next_attribute (reader));
    }
}

static const gchar*
marina_source_base_get_title (MarinaSource *source)
{
  g_return_val_if_fail (MARINA_IS_SOURCE_BASE (source), NULL);
  
  return MARINA_SOURCE_BASE (source)->title;
}

static void
marina_source_base_set_title (MarinaSource *source,
                              const gchar  *title)
{
  MarinaSourceBase *source_base;
  
  g_return_if_fail (MARINA_IS_SOURCE_BASE (source));
  
  source_base = MARINA_SOURCE_BASE (source);
  
  g_free (source_base->title);
  
  source_base->title = g_strdup (title);
}

static G_CONST_RETURN gchar*
marina_source_base_get_icon_name (MarinaSource *source)
{
  return NULL;
}

static void
marina_source_init (gpointer g_iface,
                    gpointer iface_data)
{
  MarinaSourceIface *iface = g_iface;
  
  iface->get_id        = marina_source_base_get_id;
  iface->get_title     = marina_source_base_get_title;
  iface->set_title     = marina_source_base_set_title;
  iface->get_children  = marina_source_base_get_children;
  iface->is_transient  = marina_source_base_is_transient;
  iface->observes      = marina_source_base_observes;
  iface->serialize     = marina_source_base_serialize;
  iface->deserialize   = marina_source_base_deserialize;
  iface->get_icon_name = marina_source_base_get_icon_name;
}
