/* marina-parsers.h
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifndef __MARINA_PARSERS_H__
#define __MARINA_PARSERS_H__

#include <glib-object.h>

#include "marina-parser.h"

G_BEGIN_DECLS

#define MARINA_TYPE_PARSERS (marina_parsers_get_type ())

#define MARINA_PARSERS(obj)              \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj),    \
  MARINA_TYPE_PARSERS, MarinaParsers))

#define MARINA_PARSERS_CONST(obj)        \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj),    \
  MARINA_TYPE_PARSERS,                   \
  MarinaParsers const))

#define MARINA_PARSERS_CLASS(klass)      \
  (G_TYPE_CHECK_CLASS_CAST ((klass),     \
  MARINA_TYPE_PARSERS,                   \
  MarinaParsersClass))

#define MARINA_IS_PARSERS(obj)          \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj),   \
  MARINA_TYPE_PARSERS))

#define MARINA_IS_PARSERS_CLASS(klass)  \
  (G_TYPE_CHECK_CLASS_TYPE ((klass),    \
  MARINA_TYPE_PARSERS))

#define MARINA_PARSERS_GET_CLASS(obj)   \
  (G_TYPE_INSTANCE_GET_CLASS ((obj),    \
  MARINA_TYPE_PARSERS,                  \
  MarinaParsersClass))

typedef struct _MarinaParsers         MarinaParsers;
typedef struct _MarinaParsersClass    MarinaParsersClass;
typedef struct _MarinaParsersPrivate  MarinaParsersPrivate;

struct _MarinaParsers
{
  GObject parent;
  
  MarinaParsersPrivate *priv;
};

struct _MarinaParsersClass
{
  GObjectClass parent_class;
};

GType          marina_parsers_get_type    (void) G_GNUC_CONST;
MarinaParsers* marina_parsers_get_default (void);
GList*         marina_parsers_get_list    (MarinaParsers *parsers);
void           marina_parsers_register    (MarinaParsers *parsers,
                                           MarinaParser  *parser);
void           marina_parsers_unregister  (MarinaParsers *parsers,
                                           MarinaParser  *parser);

G_END_DECLS

#endif /* __MARINA_PARSERS_H__ */
