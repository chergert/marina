/* marina-source-base.h
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifndef __MARINA_SOURCE_BASE_H__
#define __MARINA_SOURCE_BASE_H__

#include <glib-object.h>

#include "marina-source.h"

G_BEGIN_DECLS

#define MARINA_TYPE_SOURCE_BASE (marina_source_base_get_type ())

#define MARINA_SOURCE_BASE(obj)            \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj),      \
  MARINA_TYPE_SOURCE_BASE,                 \
  MarinaSourceBase))

#define MARINA_SOURCE_BASE_CONST(obj)      \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj),      \
  MARINA_TYPE_SOURCE_BASE,                 \
  MarinaSourceBase const))

#define MARINA_SOURCE_BASE_CLASS(klass)    \
  (G_TYPE_CHECK_CLASS_CAST ((klass),       \
  MARINA_TYPE_SOURCE_BASE,                 \
  MarinaSourceBaseClass))

#define MARINA_IS_SOURCE_BASE(obj)         \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj),      \
  MARINA_TYPE_SOURCE_BASE))

#define MARINA_IS_SOURCE_BASE_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass),       \
  MARINA_TYPE_SOURCE_BASE))

#define MARINA_SOURCE_BASE_GET_CLASS(obj)  \
  (G_TYPE_INSTANCE_GET_CLASS ((obj),       \
  MARINA_TYPE_SOURCE_BASE,                 \
  MarinaSourceBaseClass))

typedef struct _MarinaSourceBase        MarinaSourceBase;
typedef struct _MarinaSourceBaseClass   MarinaSourceBaseClass;

struct _MarinaSourceBase
{
  GObject parent;
  
  /*< private >*/
  
  gchar *id;
  gchar *title;
};

struct _MarinaSourceBaseClass
{
  GObjectClass parent_class;
};

GType marina_source_base_get_type (void) G_GNUC_CONST;

G_END_DECLS

#endif /* __MARINA_SOURCE_BASE_H__ */
