/* this file was borrowed from gedit.
 * please see gedit for additional copyright information */

#ifndef __MARINA_MESSAGE_H__
#define __MARINA_MESSAGE_H__

#include <glib-object.h>
#include <stdarg.h>

G_BEGIN_DECLS

#define MARINA_TYPE_MESSAGE			(marina_message_get_type ())
#define MARINA_MESSAGE(obj)			(G_TYPE_CHECK_INSTANCE_CAST ((obj), MARINA_TYPE_MESSAGE, MarinaMessage))
#define MARINA_MESSAGE_CONST(obj)		(G_TYPE_CHECK_INSTANCE_CAST ((obj), MARINA_TYPE_MESSAGE, MarinaMessage const))
#define MARINA_MESSAGE_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST ((klass), MARINA_TYPE_MESSAGE, MarinaMessageClass))
#define MARINA_IS_MESSAGE(obj)			(G_TYPE_CHECK_INSTANCE_TYPE ((obj), MARINA_TYPE_MESSAGE))
#define MARINA_IS_MESSAGE_CLASS(klass)		(G_TYPE_CHECK_CLASS_TYPE ((klass), MARINA_TYPE_MESSAGE))
#define MARINA_MESSAGE_GET_CLASS(obj)		(G_TYPE_INSTANCE_GET_CLASS ((obj), MARINA_TYPE_MESSAGE, MarinaMessageClass))

typedef struct _MarinaMessage		MarinaMessage;
typedef struct _MarinaMessageClass	MarinaMessageClass;
typedef struct _MarinaMessagePrivate	MarinaMessagePrivate;

struct _MarinaMessage {
	GObject parent;
	
	MarinaMessagePrivate *priv;
};

struct _MarinaMessageClass {
	GObjectClass parent_class;
};

GType marina_message_get_type (void) G_GNUC_CONST;

struct _MarinaMessageType marina_message_get_message_type (MarinaMessage *message);

void marina_message_get			(MarinaMessage	 *message,
					 ...) G_GNUC_NULL_TERMINATED;
void marina_message_get_valist		(MarinaMessage	 *message,
					 va_list 	  var_args);
void marina_message_get_value		(MarinaMessage	 *message,
					 const gchar	 *key,
					 GValue		 *value);

void marina_message_set			(MarinaMessage	 *message,
					 ...) G_GNUC_NULL_TERMINATED;
void marina_message_set_valist		(MarinaMessage	 *message,
					 va_list	  	  var_args);
void marina_message_set_value		(MarinaMessage	 *message,
					 const gchar 	 *key,
					 GValue		 *value);
void marina_message_set_valuesv		(MarinaMessage	 *message,
					 const gchar	**keys,
					 GValue		 *values,
					 gint		  n_values);

const gchar *marina_message_get_object_path (MarinaMessage	*message);
const gchar *marina_message_get_method	(MarinaMessage	 *message);

gboolean marina_message_has_key		(MarinaMessage	 *message,
					 const gchar     *key);

GType marina_message_get_key_type 	(MarinaMessage    *message,
			    		 const gchar     *key);

gboolean marina_message_validate		(MarinaMessage	 *message);


G_END_DECLS

#endif /* __MARINA_MESSAGE_H__ */

// ex:ts=8:noet:
