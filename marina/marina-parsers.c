/* marina-parsers.c
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

/**
 * SECTION:marina-parsers
 * @short_description: Collection of #MarinaParser<!-- -->'s
 *
 * #MarinaParsers is a singleton collection of registered #MarinaParser
 * that are used to parse syndications.  During the parsing phase, each
 * parser registered in the collection will be checked to see if it can
 * parse a particular incoming stream.
 *
 * Register your parser instance from a plugin using
 * marina_parsers_register(). When your plugin unloads, make sure you
 * unregister the instance with marina_parsers_unregister().
 */

#include "marina-debug.h"
#include "marina-parsers.h"
#include "marina-default-parser.h"

struct _MarinaParsersPrivate
{
  GList *parsers;
};

G_DEFINE_TYPE (MarinaParsers, marina_parsers, G_TYPE_OBJECT);

static void
marina_parsers_finalize (GObject *object)
{
  G_OBJECT_CLASS (marina_parsers_parent_class)->finalize (object);
}

static void
marina_parsers_class_init (MarinaParsersClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  
  object_class->finalize = marina_parsers_finalize;

  g_type_class_add_private (object_class, sizeof (MarinaParsersPrivate));
}

static void
marina_parsers_init (MarinaParsers *parsers)
{
  parsers->priv = G_TYPE_INSTANCE_GET_PRIVATE (parsers,
                                               MARINA_TYPE_PARSERS,
                                               MarinaParsersPrivate);

  /* register our core parsers */
  marina_parsers_register (parsers, marina_default_parser_new ());
}

MarinaParsers*
marina_parsers_get_default (void)
{
  static MarinaParsers *instance = NULL;
  static GStaticMutex mutex  = G_STATIC_MUTEX_INIT;

  if (!instance)
    {
      g_static_mutex_lock (&mutex);
      
      if (!instance)
        {
          marina_debug_message (DEBUG_PARSERS, "Creating parsers singleton");
          instance = g_object_new (MARINA_TYPE_PARSERS, NULL);
        }
      
      g_static_mutex_unlock (&mutex);
    }

  return instance;
}

/**
 * marina_parsers_get_list:
 * @parsers: A #MarinaParsers
 *
 * Retrieves the list of registered #MarinaParser<!-- -->'s. The list is
 * copied and should be freed with g_list_free().
 *
 * Return value: a copied #GList.
 */
GList*
marina_parsers_get_list (MarinaParsers *parsers)
{
  MarinaParsersPrivate *priv;
  
  g_return_val_if_fail (MARINA_IS_PARSERS (parsers), NULL);
  
  priv = parsers->priv;
  
  return g_list_copy (priv->parsers);
}

/**
 * marina_parsers_register:
 * @parsers: A #MarinaParsers
 * @parser: A #MarinaParser
 *
 * Registers a new instance of #MarinaParser to be used during the parsing
 * phase.  If the parser already has been added, it will not be added again.
 */
void
marina_parsers_register (MarinaParsers *parsers,
                         MarinaParser  *parser)
{
  MarinaParsersPrivate *priv;
  
  g_return_if_fail (MARINA_IS_PARSERS (parsers));
  g_return_if_fail (MARINA_IS_PARSER (parser));
  
  priv = parsers->priv;
  
  if (g_list_find (priv->parsers, parser))
    return;

  priv->parsers = g_list_prepend (priv->parsers,
                                  g_object_ref (parser));
}

/**
 * marina_parsers_unregister:
 * @parsers: A #MarinaParsers
 * @parser: A #MarinaParser
 *
 * Unregisters a registered #MarinaParser from being executed during the
 * parsing phase.
 */
void
marina_parsers_unregister (MarinaParsers *parsers,
                           MarinaParser  *parser)
{
  MarinaParsersPrivate *priv;
  
  g_return_if_fail (MARINA_IS_PARSERS (parsers));
  g_return_if_fail (MARINA_IS_PARSER (parser));
  
  priv = parsers->priv;
  
  if (g_list_find (priv->parsers, parser))
    {
      priv->parsers = g_list_remove (priv->parsers, parser);
      g_object_unref (parser);
    }
}

