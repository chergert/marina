/* marina-ui.h
 * 
 * This file is part of marina; it was borrowed from gedit
 *
 * Copyright (C) 2005 - Paolo Maggi 
 *               2009 - Christian Hergert <chris@dronelabs.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA 02111-1307, USA.
 */
 
/*
 * Modified by the gedit Team, 2005. See the gedit AUTHORS file for a 
 * list of people on the gedit Team.  
 * 
 * Modified by the marina Team, 2009.
 */

#ifndef __MARINA_UI_H__
#define __MARINA_UI_H__

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gtk/gtk.h>

#include "marina-commands.h"

G_BEGIN_DECLS

static const GtkActionEntry marina_always_sensitive_menu_entries[] =
{
  /* Toplevel */
  { "Marina", NULL, N_("_Marina") },
  { "Edit", NULL, N_("_Edit") },
  { "View", NULL, N_("_View") },
  { "Search", NULL, N_("_Search") },
  { "Source", NULL, N_("_Channel") },
  { "Item", NULL, N_("_Item") },
  { "Help", NULL, N_("_Help") },

  /* Marina Menu */
  { "MarinaNew", GTK_STOCK_NEW, NULL, "<control>N",
    N_("Add new source"), G_CALLBACK (_marina_cmd_marina_add) },
  { "MarinaQuit", GTK_STOCK_QUIT, NULL, "<control>Q",
    N_("Quit the program"), G_CALLBACK (_marina_cmd_marina_quit) },
  { "MarinaClose", GTK_STOCK_CLOSE, NULL, "<control>W",
    N_("Close the window"), G_CALLBACK (_marina_cmd_marina_close) },

  /* Edit menu */
  { "EditPreferences", GTK_STOCK_PREFERENCES, N_("Pr_eferences"), NULL,
    N_("Configure the application"), G_CALLBACK (_marina_cmd_edit_preferences) },

  /* Source menu */
  { "RefreshAll", GTK_STOCK_REFRESH, N_("Refresh _All"), "<control>F5",
    N_("Refresh all channels with recent information"),
    G_CALLBACK (_marina_cmd_source_refresh_all) },

  /* Help menu */
  {"HelpContents", GTK_STOCK_HELP, N_("_Contents"), "F1",
   N_("Open the marina manual"), G_CALLBACK (_marina_cmd_help_contents) },
  { "HelpAbout", GTK_STOCK_ABOUT, NULL, NULL,
   N_("About this application"), G_CALLBACK (_marina_cmd_help_about) },

  /* Fullscreen toolbar */
  { "LeaveFullscreen", GTK_STOCK_LEAVE_FULLSCREEN, NULL,
    NULL, N_("Leave fullscreen mode"),
    G_CALLBACK (_marina_cmd_view_leave_fullscreen_mode) }
};

static const GtkActionEntry marina_menu_entries[] =
{
  /* Search menu */
  { "SearchFind", GTK_STOCK_FIND, N_("_Find..."), "<control>F",
    N_("Search for text"), G_CALLBACK (_marina_cmd_search_find) },
  { "SearchFindNext", NULL, N_("Find Ne_xt"), "<control>G",
    N_("Search forwards for the same text"), G_CALLBACK (_marina_cmd_search_find_next) },
  { "SearchFindPrevious", NULL, N_("Find Pre_vious"), "<shift><control>G",
    N_("Search backwards for the same text"), G_CALLBACK (_marina_cmd_search_find_prev) },
  { "SearchClearHighlight", NULL, N_("_Clear Highlight"), "<shift><control>K",
    N_("Clear highlighting of search matches"), G_CALLBACK (_marina_cmd_search_clear_highlight) },
  { "SearchIncrementalSearch", GTK_STOCK_FIND, N_("_Incremental Search..."), NULL,
    N_("Incrementally search for text"), G_CALLBACK (_marina_cmd_search_incremental_search) },

  /* Source menu */
  { "Refresh", GTK_STOCK_REFRESH, N_("_Refresh"), "F5",
    N_("Refresh the current channel with recent information"),
    G_CALLBACK (_marina_cmd_source_refresh) },
  { "MarkAllRead", NULL, N_("_Mark All Items Read"), "<control><shift>m",
    N_("Mark all of the items in the selected channel as read"),
    G_CALLBACK (_marina_cmd_source_mark_all_read) },
  { "RemoveSource", GTK_STOCK_REMOVE, N_("Remove"), NULL,
    N_("Remove the selected syndication"),
    G_CALLBACK (_marina_cmd_source_remove) },
  
  /* item menu */
  { "ItemMoveNext", GTK_STOCK_GO_DOWN, N_("Move Next"), "<control>Page_Down",
    N_("Move to the next available item"), G_CALLBACK (_marina_cmd_item_move_next) },
  { "ItemMovePrev", GTK_STOCK_GO_UP, N_("Move Previous"), "<control>Page_Up",
    N_("Move to the previous item"), G_CALLBACK (_marina_cmd_item_move_prev) },
  { "ItemMarkRead", NULL, N_("Mark as Read"), NULL,
    N_("Mark the current item as read"), G_CALLBACK (_marina_cmd_item_move_next) },
};

static const GtkToggleActionEntry marina_always_sensitive_toggle_menu_entries[] =
{
  { "ViewToolbar", NULL, N_("_Toolbar"), NULL,
    N_("Show or hide the toolbar in the current window"),
    G_CALLBACK (_marina_cmd_view_show_toolbar), TRUE },
  { "ViewStatusbar", NULL, N_("_Statusbar"), NULL,
    N_("Show or hide the statusbar in the current window"),
    G_CALLBACK (_marina_cmd_view_show_statusbar), TRUE },
  { "ViewSidePane", NULL, N_("Side _Pane"), "F9",
    N_("Show or hide the side pane in the current window"),
    G_CALLBACK (_marina_cmd_view_show_side_pane), FALSE },
  { "ViewFullscreen", GTK_STOCK_FULLSCREEN, NULL, "F11",
    N_("Read articles at fullscreen"),
    G_CALLBACK (_marina_cmd_view_toggle_fullscreen_mode), FALSE },
  { "ViewCompactMode", NULL, N_("Compact Mode"), "F7",
    N_("Shrink font sizes to make room for more content"),
    G_CALLBACK (_marina_cmd_view_toggle_compact_mode), FALSE},
};

static const GtkRadioActionEntry marina_view_radio_menu_entries[] = {
  { "ViewClassic", NULL, N_("Classic View"), NULL,
    N_("Show item content below the item list"), 0 },
  { "ViewVertical", NULL, N_("Vertical View"), NULL,
    N_("Show item content side-by-side with the item list"), 1 },
};

static const GtkToggleActionEntry marina_toggle_menu_entries[] =
{
  { "ViewBottomPane", NULL, N_("_Bottom Pane"), "<control>F9",
    N_("Show or hide the bottom pane in the current window"),
    G_CALLBACK (_marina_cmd_view_show_bottom_pane), FALSE }
};

G_END_DECLS

#endif  /* __MARINA_UI_H__  */
