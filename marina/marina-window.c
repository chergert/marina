/* marina-window.c
 * 
 * This file is part of marina; it is based on gedit
 *
 * Copyright (C) 2005 - Paolo Maggi
 *               2009 - Christian Hergert
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA 02111-1307, USA. 
 */

#include <glib.h>
#include <gtk/gtk.h>
#include <glib/gi18n.h>

#include "marina-debug.h"
#include "marina-default-source-view.h"
#include "marina-dirs.h"
#include "marina-message-bus.h"
#include "marina-outlook-source-view.h"
#include "marina-prefs.h"
#include "marina-progress.h"
#include "marina-sources.h"
#include "marina-ui.h"
#include "marina-window.h"
#include "marina-window-private.h"

#include "gtkcellrendererbubble.h"

#define MARINA_UIFILE "marina-ui.xml"
#define FULLSCREEN_ANIMATION_SPEED 4
#define APP_BUS (marina_message_bus_get_default ())

#define GPOINTER_TO_BOOLEAN(i) ((GPOINTER_TO_INT (i) == 1) ? TRUE : FALSE)
#define GBOOLEAN_TO_POINTER(i) (GINT_TO_POINTER ((i) ? 1 : 0))

G_DEFINE_TYPE (MarinaWindow, marina_window, GTK_TYPE_WINDOW);

static void
marina_window_finalize (GObject *object)
{
  G_OBJECT_CLASS (marina_window_parent_class)->finalize (object);
}

static void
marina_window_dispose (GObject *object)
{
  MarinaWindow *window;
  
  g_return_if_fail (MARINA_IS_WINDOW (object));
  
  window = MARINA_WINDOW (object);
  
  if (window->priv->fullscreen_animation_timeout_id != 0)
	{
		g_source_remove (window->priv->fullscreen_animation_timeout_id);
		window->priv->fullscreen_animation_timeout_id = 0;
	}

	if (window->priv->fullscreen_controls != NULL)
	{
		gtk_widget_destroy (window->priv->fullscreen_controls);
		
		window->priv->fullscreen_controls = NULL;
	}
}

static void
marina_window_class_init (MarinaWindowClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  
  object_class->dispose = marina_window_dispose;
  object_class->finalize = marina_window_finalize;
  
  g_type_class_add_private (object_class, sizeof (MarinaWindowPrivate));
}

static void
fullscreen_controls_show (MarinaWindow *window)
{
	GdkScreen *screen;
	GdkRectangle fs_rect;
	gint w, h;

	screen = gtk_window_get_screen (GTK_WINDOW (window));
	gdk_screen_get_monitor_geometry (screen,
					 gdk_screen_get_monitor_at_window (screen, GTK_WIDGET (window)->window),
					 &fs_rect);

	gtk_window_get_size (GTK_WINDOW (window->priv->fullscreen_controls), &w, &h);

	gtk_window_resize (GTK_WINDOW (window->priv->fullscreen_controls),
			   fs_rect.width, h);

	gtk_window_move (GTK_WINDOW (window->priv->fullscreen_controls),
			 fs_rect.x, fs_rect.y - h + 1);

	gtk_widget_show_all (window->priv->fullscreen_controls);
}

static gboolean
run_fullscreen_animation (gpointer data)
{
	MarinaWindow *window = MARINA_WINDOW (data);
	GdkScreen *screen;
	GdkRectangle fs_rect;
	gint x, y;
	
	screen = gtk_window_get_screen (GTK_WINDOW (window));
	gdk_screen_get_monitor_geometry (screen,
					 gdk_screen_get_monitor_at_window (screen, GTK_WIDGET (window)->window),
					 &fs_rect);
					 
	gtk_window_get_position (GTK_WINDOW (window->priv->fullscreen_controls),
				 &x, &y);
	
	if (window->priv->fullscreen_animation_enter)
	{
		if (y == fs_rect.y)
		{
			window->priv->fullscreen_animation_timeout_id = 0;
			return FALSE;
		}
		else
		{
			gtk_window_move (GTK_WINDOW (window->priv->fullscreen_controls),
					 x, y + 1);
			return TRUE;
		}
	}
	else
	{
		gint w, h;
	
		gtk_window_get_size (GTK_WINDOW (window->priv->fullscreen_controls),
				     &w, &h);
	
		if (y == fs_rect.y - h + 1)
		{
			window->priv->fullscreen_animation_timeout_id = 0;
			return FALSE;
		}
		else
		{
			gtk_window_move (GTK_WINDOW (window->priv->fullscreen_controls),
					 x, y - 1);
			return TRUE;
		}
	}
}

static void
show_hide_fullscreen_toolbar (MarinaWindow *window,
                              gboolean     show,
                              gint         height)
{
	GtkSettings *settings;
	gboolean enable_animations;

	settings = gtk_widget_get_settings (GTK_WIDGET (window));
	g_object_get (G_OBJECT (settings),
                "gtk-enable-animations",
                &enable_animations,
                NULL);

	if (enable_animations)
	{
		window->priv->fullscreen_animation_enter = show;

		if (window->priv->fullscreen_animation_timeout_id == 0)
		{
			window->priv->fullscreen_animation_timeout_id =
				g_timeout_add (FULLSCREEN_ANIMATION_SPEED,
                       (GSourceFunc) run_fullscreen_animation,
                       window);
		}
	}
	else
	{
		GdkRectangle fs_rect;
		GdkScreen *screen;

		screen = gtk_window_get_screen (GTK_WINDOW (window));
		gdk_screen_get_monitor_geometry (screen,
						 gdk_screen_get_monitor_at_window (screen, GTK_WIDGET (window)->window),
						 &fs_rect);

		if (show)
			gtk_window_move (GTK_WINDOW (window->priv->fullscreen_controls),
				 fs_rect.x, fs_rect.y);
		else
			gtk_window_move (GTK_WINDOW (window->priv->fullscreen_controls),
					 fs_rect.x, fs_rect.y - height + 1);
	}

}

static gboolean
on_fullscreen_controls_enter_notify_event (GtkWidget        *widget,
                                           GdkEventCrossing *event,
                                           MarinaWindow      *window)
{
	show_hide_fullscreen_toolbar (window, TRUE, 0);

	return FALSE;
}

static gboolean
on_fullscreen_controls_leave_notify_event (GtkWidget        *widget,
                                           GdkEventCrossing *event,
                                           MarinaWindow      *window)
{
	GdkDisplay *display;
	GdkScreen *screen;
	gint w, h;
	gint x, y;

	display = gdk_display_get_default ();
	screen = gtk_window_get_screen (GTK_WINDOW (window));

	gtk_window_get_size (GTK_WINDOW (window->priv->fullscreen_controls), &w, &h);
	gdk_display_get_pointer (display, &screen, &x, &y, NULL);
	
	/* gtk seems to emit leave notify when clicking on tool items,
	 * work around it by checking the coordinates
	 */
	if (y >= h)
	{
		show_hide_fullscreen_toolbar (window, FALSE, h);
	}

	return FALSE;
}

static void
set_non_homogeneus (GtkWidget *widget, gpointer data)
{
	gtk_tool_item_set_homogeneous (GTK_TOOL_ITEM (widget), FALSE);
}

static void
fullscreen_controls_build (MarinaWindow *window)
{
	MarinaWindowPrivate *priv = window->priv;
	GtkWidget *toolbar;
	GtkAction *action;

	if (priv->fullscreen_controls != NULL)
		return;
	
	priv->fullscreen_controls = gtk_window_new (GTK_WINDOW_POPUP);
	
	gtk_window_set_transient_for (GTK_WINDOW (priv->fullscreen_controls),
				                        &window->window);
	
	/* popup toolbar */
	toolbar = gtk_ui_manager_get_widget (priv->manager, "/FullscreenToolBar");
	gtk_container_add (GTK_CONTAINER (priv->fullscreen_controls),
			               toolbar);
  	
	action = gtk_action_group_get_action (priv->always_sensitive_action_group,
					                              "LeaveFullscreen");
	g_object_set (action, "is-important", TRUE, NULL);

	gtk_container_foreach (GTK_CONTAINER (toolbar),
			                   (GtkCallback)set_non_homogeneus,
			                   NULL);

	/* Set the toolbar style */
	gtk_toolbar_set_style (GTK_TOOLBAR (toolbar), GTK_TOOLBAR_BOTH_HORIZ);

	g_signal_connect (priv->fullscreen_controls, "enter-notify-event",
			              G_CALLBACK (on_fullscreen_controls_enter_notify_event),
			              window);
	g_signal_connect (priv->fullscreen_controls, "leave-notify-event",
			              G_CALLBACK (on_fullscreen_controls_leave_notify_event),
			              window);
}

static void
menu_item_select_cb (GtkMenuItem *proxy,
                     MarinaWindow *window)
{
  GtkAction *action;
  gchar *message;

  action = g_object_get_data (G_OBJECT (proxy),  "gtk-action");
  g_return_if_fail (action != NULL);

  g_object_get (G_OBJECT (action), "tooltip", &message, NULL);
  if (message)
    {
      gtk_statusbar_push (GTK_STATUSBAR (window->priv->statusbar),
                          window->priv->tip_message_cid, message);
      g_free (message);
    }
}

static void
menu_item_deselect_cb (GtkMenuItem  *proxy,
                       MarinaWindow *window)
{
  gtk_statusbar_pop (GTK_STATUSBAR (window->priv->statusbar),
                     window->priv->tip_message_cid);
}

static void
connect_proxy_cb (GtkUIManager *manager,
                  GtkAction *action,
                  GtkWidget *proxy,
                  MarinaWindow *window)
{
  if (GTK_IS_MENU_ITEM (proxy))
    {
      g_signal_connect (proxy, "select",
                        G_CALLBACK (menu_item_select_cb), window);
      g_signal_connect (proxy, "deselect",
                        G_CALLBACK (menu_item_deselect_cb), window);
    }
}

static void
disconnect_proxy_cb (GtkUIManager *manager,
                     GtkAction *action,
                     GtkWidget *proxy,
                     MarinaWindow *window)
{
  if (GTK_IS_MENU_ITEM (proxy))
    {
      g_signal_handlers_disconnect_by_func (proxy, G_CALLBACK (menu_item_select_cb), window);
      g_signal_handlers_disconnect_by_func (proxy, G_CALLBACK (menu_item_deselect_cb), window);
    }
}

static void
toolbar_visibility_changed (GtkWidget   *toolbar,
                            MarinaWindow *window)
{
  gboolean visible;
  GtkAction *action;

  visible = GTK_WIDGET_VISIBLE (toolbar);

  if (marina_prefs_toolbar_visible_can_set ())
    marina_prefs_set_toolbar_visible (visible);

  action = gtk_action_group_get_action (window->priv->always_sensitive_action_group,
                                        "ViewToolbar");

  if (gtk_toggle_action_get_active (GTK_TOGGLE_ACTION (action)) != visible)
    gtk_toggle_action_set_active (GTK_TOGGLE_ACTION (action), visible);
}

static void
statusbar_visibility_changed (GtkWidget   *statusbar,
                              MarinaWindow *window)
{
  gboolean visible;
  GtkAction *action;

  visible = GTK_WIDGET_VISIBLE (statusbar);

  if (marina_prefs_statusbar_visible_can_set ())
    marina_prefs_set_statusbar_visible (visible);

  action = gtk_action_group_get_action (window->priv->always_sensitive_action_group,
                                        "ViewStatusbar");

  if (gtk_toggle_action_get_active (GTK_TOGGLE_ACTION (action)) != visible)
    gtk_toggle_action_set_active (GTK_TOGGLE_ACTION (action), visible);
}

static void
side_pane_visibility_changed (GtkWidget    *side_pane,
                              MarinaWindow *window)
{
  gboolean visible;
  GtkAction *action;

  visible = GTK_WIDGET_VISIBLE (side_pane);

  if (marina_prefs_side_pane_visible_can_set ())
    marina_prefs_set_side_pane_visible (visible);

  action = gtk_action_group_get_action (window->priv->always_sensitive_action_group,
                                        "ViewSidePane");

  if (gtk_toggle_action_get_active (GTK_TOGGLE_ACTION (action)) != visible)
    gtk_toggle_action_set_active (GTK_TOGGLE_ACTION (action), visible);
}

static void
source_view_radio_cb (GtkRadioAction *action,
                      GtkRadioAction *radio_action,
                      MarinaWindow   *window)
{
  MarinaWindowPrivate *priv;
  MarinaSourceView    *source_view;

  g_return_if_fail (GTK_IS_RADIO_ACTION (action));
  g_return_if_fail (GTK_IS_RADIO_ACTION (radio_action));
  g_return_if_fail (MARINA_IS_WINDOW (window));

  priv = window->priv;

  source_view = g_list_nth_data (priv->source_views,
                                 gtk_radio_action_get_current_value (radio_action));

  if (!MARINA_IS_SOURCE_VIEW (source_view))
    return;

  marina_window_set_source_view (window, source_view);
}

static void
create_menu_bar_and_toolbar (MarinaWindow *window,
                             GtkWidget    *main_box)
{
  GtkActionGroup *action_group;
  GtkUIManager   *manager;
  GtkWidget      *menubar;
  gchar          *ui_file;
  GError         *error = NULL;

  marina_debug (DEBUG_WINDOW);

  manager = gtk_ui_manager_new ();
  window->priv->manager = manager;

  gtk_window_add_accel_group (&window->window,
                              gtk_ui_manager_get_accel_group (manager));

  action_group = gtk_action_group_new ("MarinaWindowAlwaysSensitiveActions");
  gtk_action_group_set_translation_domain (action_group, NULL);
  gtk_action_group_add_actions (action_group,
                                marina_always_sensitive_menu_entries,
                                G_N_ELEMENTS (marina_always_sensitive_menu_entries),
                                window);
  gtk_action_group_add_toggle_actions (action_group,
                                       marina_always_sensitive_toggle_menu_entries,
                                       G_N_ELEMENTS (marina_always_sensitive_toggle_menu_entries),
                                       window);
  gtk_action_group_add_radio_actions (action_group,
                                      marina_view_radio_menu_entries,
                                      G_N_ELEMENTS (marina_view_radio_menu_entries),
                                      0,
                                      G_CALLBACK (source_view_radio_cb),
                                      window);

  gtk_ui_manager_insert_action_group (manager, action_group, 0);
  g_object_unref (action_group);
  window->priv->always_sensitive_action_group = action_group;

  action_group = gtk_action_group_new ("MarinaWindowActions");
  gtk_action_group_set_translation_domain (action_group, NULL);
  gtk_action_group_add_actions (action_group,
                                marina_menu_entries,
                                G_N_ELEMENTS (marina_menu_entries),
                                window);
  gtk_action_group_add_toggle_actions (action_group,
                                       marina_toggle_menu_entries,
                                       G_N_ELEMENTS (marina_toggle_menu_entries),
                                       window);

  gtk_ui_manager_insert_action_group (manager, action_group, 0);
  g_object_unref (action_group);
  window->priv->action_group = action_group;

  /* now load the ui definition */
  ui_file = marina_dirs_get_ui_file (MARINA_UIFILE);
  gtk_ui_manager_add_ui_from_file (manager, ui_file, &error);
  if (error != NULL)
    {
      g_warning ("Could not merge %s: %s", ui_file, error->message);
      g_error_free (error);
    }
  g_free (ui_file);

  /* show tooltips in the statusbar */
  g_signal_connect (manager,
                    "connect_proxy",
                    G_CALLBACK (connect_proxy_cb),
                    window);
  g_signal_connect (manager,
                    "disconnect_proxy",
                    G_CALLBACK (disconnect_proxy_cb),
                    window);

  menubar = gtk_ui_manager_get_widget (manager, "/MenuBar");
  gtk_box_pack_start (GTK_BOX (main_box),
                      menubar,
                      FALSE,
                      FALSE,
                      0);

  window->priv->toolbar = gtk_ui_manager_get_widget (manager, "/ToolBar");
  gtk_box_pack_start (GTK_BOX (main_box),
                      window->priv->toolbar,
                      FALSE,
                      FALSE,
                      0);

  g_signal_connect_after (G_OBJECT (window->priv->toolbar),
                          "show",
                          G_CALLBACK (toolbar_visibility_changed),
                          window);
  g_signal_connect_after (G_OBJECT (window->priv->toolbar),
                          "hide",
                          G_CALLBACK (toolbar_visibility_changed),
                          window);
}

/* Returns TRUE if status bar is visible */
static gboolean
set_statusbar_style (MarinaWindow *window,
                     MarinaWindow *origin)
{
  GtkAction *action;

  gboolean visible;

  if (origin == NULL)
    visible = marina_prefs_get_statusbar_visible ();
  else
    visible = GTK_WIDGET_VISIBLE (origin->priv->statusbar);

  if (visible)
    gtk_widget_show (window->priv->statusbar);
  else
    gtk_widget_hide (window->priv->statusbar);

  action = gtk_action_group_get_action (window->priv->always_sensitive_action_group,
                                        "ViewStatusbar");

  if (gtk_toggle_action_get_active (GTK_TOGGLE_ACTION (action)) != visible)
    gtk_toggle_action_set_active (GTK_TOGGLE_ACTION (action), visible);

  return visible;
}

static gboolean
set_side_pane_style (MarinaWindow *window)
{
  GtkAction *action;

  gboolean visible;

  visible = GTK_WIDGET_VISIBLE (window->priv->side_panel);

  if (visible)
    gtk_widget_show (window->priv->side_panel);
  else
    gtk_widget_hide (window->priv->side_panel);

  action = gtk_action_group_get_action (window->priv->always_sensitive_action_group,
                                        "ViewSidePane");

  if (gtk_toggle_action_get_active (GTK_TOGGLE_ACTION (action)) != visible)
    gtk_toggle_action_set_active (GTK_TOGGLE_ACTION (action), visible);

  return visible;
}

static void
create_statusbar (MarinaWindow *window,
                  GtkWidget    *main_box)
{
  MarinaWindowPrivate *priv;
  
  g_return_if_fail (MARINA_IS_WINDOW (window));
  
  priv = window->priv;
  
  marina_debug (DEBUG_WINDOW);

  priv->statusbar = gtk_statusbar_new ();

  priv->generic_message_cid = gtk_statusbar_get_context_id
          (GTK_STATUSBAR (priv->statusbar), "generic_message");
  priv->tip_message_cid = gtk_statusbar_get_context_id
          (GTK_STATUSBAR (priv->statusbar), "tip_message");

  gtk_box_pack_end (GTK_BOX (main_box),
                    priv->statusbar,
                    FALSE,
                    TRUE,
                    0);
  
  g_signal_connect_after (G_OBJECT (priv->statusbar),
                          "show",
                          G_CALLBACK (statusbar_visibility_changed),
                          window);
  g_signal_connect_after (G_OBJECT (priv->statusbar),
                          "hide",
                          G_CALLBACK (statusbar_visibility_changed),
                          window);
  
  set_statusbar_style (window, NULL);
}

static void
store_size (MarinaWindow *window)
{
  static gint last_x = 0, last_y = 0;
  gint x, y;
  
  g_return_if_fail (MARINA_IS_WINDOW (window));

  gtk_window_get_size (&window->window, &x, &y);

  if (last_x && (x != last_x))
    marina_prefs_set_window_width (x);

  if (last_y && (y != last_y))
    marina_prefs_set_window_height (y);

  last_x = x;
  last_y = y;
}

static void
store_position (MarinaWindow *window)
{
  static gint last_x_pos = 0, last_y_pos = 0;
  gint x_pos, y_pos;
  
  g_return_if_fail (MARINA_IS_WINDOW (window));

  gtk_window_get_position (&window->window, &x_pos, &y_pos);

  if (last_x_pos && (x_pos != last_x_pos))
    marina_prefs_set_window_x_pos (x_pos);

  if (last_y_pos && (y_pos != last_y_pos))
    marina_prefs_set_window_y_pos (y_pos);

  last_x_pos = x_pos;
  last_y_pos = y_pos;
}

static gboolean
configure_event_cb (MarinaWindow      *window,
                    GdkEventConfigure *event,
                    gpointer           user_data)
{
  store_size (window);
  store_position (window);
  return FALSE;
}

static void
create_main_area (MarinaWindow *window,
                  GtkWidget    *main_box)
{
  MarinaWindowPrivate *priv;
  GtkWidget *hpaned;
  GtkWidget *vbox;
  
  g_return_if_fail (MARINA_IS_WINDOW (window));
  g_return_if_fail (GTK_IS_WIDGET (main_box));
  
  priv = window->priv;
  
  /* the main horiz paned */
  hpaned = gtk_hpaned_new ();
  gtk_box_pack_start (GTK_BOX (main_box), hpaned, TRUE, TRUE, 0);
  gtk_paned_set_position (GTK_PANED (hpaned), 200);
  gtk_widget_show (hpaned);
  priv->hpaned = hpaned;
  
  /* vbox containing side panel and progress items */
  vbox = gtk_vbox_new (FALSE, 0);
  gtk_paned_add1 (GTK_PANED (hpaned), vbox);
  priv->side_pane = vbox;
  gtk_widget_show (vbox);
  
  /* create the side panel */
  priv->side_panel = gtk_notebook_new ();
  gtk_notebook_set_tab_pos (GTK_NOTEBOOK (priv->side_panel), GTK_POS_BOTTOM);
  gtk_notebook_set_show_border (GTK_NOTEBOOK (priv->side_panel), FALSE);
  gtk_notebook_set_show_tabs (GTK_NOTEBOOK (priv->side_panel), FALSE);
  gtk_box_pack_start (GTK_BOX (vbox), priv->side_panel, TRUE, TRUE, 0);
  gtk_widget_show (priv->side_panel);
  
  /* create the content area */
  priv->source_view_notebook = gtk_notebook_new ();
  gtk_notebook_set_show_border (GTK_NOTEBOOK (priv->source_view_notebook), FALSE);
  gtk_notebook_set_show_tabs (GTK_NOTEBOOK (priv->source_view_notebook), FALSE);
  gtk_paned_add2 (GTK_PANED (hpaned), priv->source_view_notebook);
  gtk_widget_show (priv->source_view_notebook);

  /* create the bottom panel */
  priv->bottom_panel = gtk_notebook_new ();
  
  g_signal_connect_after (G_OBJECT (window->priv->side_panel),
                          "show",
                          G_CALLBACK (side_pane_visibility_changed),
                          window);
  g_signal_connect_after (G_OBJECT (window->priv->side_panel),
                          "hide",
                          G_CALLBACK (side_pane_visibility_changed),
                          window);
  
  set_side_pane_style (window);
}

static void
source_title_func (GtkTreeViewColumn *tree_column,
                   GtkCellRenderer *cell,
                   GtkTreeModel *tree_model,
                   GtkTreeIter *iter,
                   gpointer data)
{
  MarinaWindowPrivate *priv;
  MarinaSource *source;
  const gchar *title = NULL;
  
  g_return_if_fail (MARINA_IS_WINDOW (data));
  
  priv = MARINA_WINDOW (data)->priv;
  
  gtk_tree_model_get (tree_model, iter, 0, &source, -1);
  
  if (MARINA_IS_SOURCE (source))
    title = marina_source_get_title (source);
  
  g_object_set (cell, "text", title, NULL);
}

static void
source_pixbuf_func (GtkTreeViewColumn *tree_column,
                    GtkCellRenderer *cell,
                    GtkTreeModel *tree_model,
                    GtkTreeIter *iter,
                    gpointer data)
{
  MarinaWindowPrivate *priv;
  MarinaSource *source;
  
  g_return_if_fail (MARINA_IS_WINDOW (data));
  
  priv = MARINA_WINDOW (data)->priv;
  
  gtk_tree_model_get (tree_model, iter, 0, &source, -1);
  
  /* render refresh icon if the source is refreshing */
  if (GPOINTER_TO_BOOLEAN (g_object_get_data (G_OBJECT (source), "updating")))
    g_object_set (cell, "stock-id", GTK_STOCK_REFRESH, NULL);
  else if (!source || !marina_source_get_icon_name (source))
    g_object_set (cell, "pixbuf", NULL, NULL);
  else
    g_object_set (cell, "icon-name", marina_source_get_icon_name (source), NULL);
}

static void
source_unread_func (GtkTreeViewColumn *tree_column,
                    GtkCellRenderer *cell,
                    GtkTreeModel *tree_model,
                    GtkTreeIter *iter,
                    gpointer data)
{
  MarinaWindowPrivate *priv;
  MarinaSource *source;
  gchar *text;
  guint unread;
  
  g_return_if_fail (MARINA_IS_WINDOW (data));
  
  priv = MARINA_WINDOW (data)->priv;
  
  gtk_tree_model_get (tree_model, iter, 0, &source, -1);
  
  unread = marina_sources_count_unread (marina_sources_get_default (), source);
  
  g_object_set (cell, "show-bubble", unread > 0, "text", "", NULL);
  
  if (!unread)
    return;
  
  if (unread < 10)
    text = g_strdup_printf (" %d ", unread);
  else
    text = g_strdup_printf ("%d", unread);
    
  g_object_set (cell, "text", text, NULL);
  
  g_free (text);
}

static void
source_changed (GtkTreeSelection *selection,
                MarinaWindow     *window)
{
  MarinaWindowPrivate *priv;
  MarinaSourceView *source_view;
  GtkTreeModel *model;
  GtkTreeIter iter;
  MarinaSource *source = NULL;
  
  g_return_if_fail (GTK_IS_TREE_SELECTION (selection));
  g_return_if_fail (MARINA_IS_WINDOW (window));
  
  priv = window->priv;
  
  source_view = MARINA_SOURCE_VIEW (priv->source_view);
  
  if (source_view)
    {
      if (gtk_tree_selection_get_selected (selection, &model, &iter))
        gtk_tree_model_get (model, &iter, 0, &source, -1);
      marina_source_view_set_source (source_view, source);
    }
}

static gboolean
treeview_button_press_cb (GtkWidget      *widget,
                          GdkEventButton *event,
                          gpointer        user_data)
{
  MarinaWindowPrivate *priv;
  GtkWidget *popup;
  
  g_return_val_if_fail (MARINA_IS_WINDOW (user_data), FALSE);
  
  priv = MARINA_WINDOW (user_data)->priv;
  
  if (event->button == 3) /* right click */
    {
      popup = gtk_ui_manager_get_widget (priv->manager, "/ui/SourceContextPopup");
      gtk_menu_popup (GTK_MENU (popup),
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      event->button,
                      event->time);
    }
  
  return FALSE;
}

static void
create_sources_tree_view (MarinaWindow *window)
{
  MarinaWindowPrivate *priv;
  GtkWidget *scroller;
  GtkWidget *treeview;
  GtkWidget *notebook;
  
  g_return_if_fail (MARINA_IS_WINDOW (window));
  
  priv = window->priv;
  
  notebook = priv->side_panel;
  g_assert (GTK_IS_NOTEBOOK (notebook));
  
  /* scrolled window */
  
  scroller = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scroller),
                                  GTK_POLICY_NEVER,
                                  GTK_POLICY_AUTOMATIC);
  gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scroller),
                                       GTK_SHADOW_NONE);
  gtk_notebook_append_page (GTK_NOTEBOOK (notebook), scroller, NULL);
  gtk_widget_show (scroller);
  
  /* treeview */
  
  priv->source_treeview = treeview = gtk_tree_view_new ();
  gtk_tree_view_set_headers_visible (GTK_TREE_VIEW (treeview), FALSE);
  gtk_container_add (GTK_CONTAINER (scroller), treeview);
  gtk_widget_show (treeview);
  
  MarinaSources *sources = marina_sources_get_default ();
  GtkTreeModel *model = marina_sources_get_tree_model (sources);
  gtk_tree_view_set_model (GTK_TREE_VIEW (treeview), model);
  
  /* column */
  
  /* icon */
  GtkTreeViewColumn *column = gtk_tree_view_column_new ();
  gtk_tree_view_column_set_title (column, _("Sources"));
  GtkCellRenderer *cpix = gtk_cell_renderer_pixbuf_new ();
  gtk_tree_view_column_pack_start (column, cpix, FALSE);
  gtk_tree_view_column_set_cell_data_func (column,
                                           cpix,
                                           source_pixbuf_func,
                                           g_object_ref (window),
                                           g_object_unref);
  
  /* title */
  GtkCellRenderer *ctext = gtk_cell_renderer_text_new ();
  g_object_set (ctext, "ellipsize", PANGO_ELLIPSIZE_END, NULL);
  gtk_tree_view_column_pack_start (column, ctext, TRUE);
  gtk_tree_view_column_set_cell_data_func (column,
                                           ctext,
                                           source_title_func,
                                           g_object_ref (window),
                                           g_object_unref);
  
  /* bubble */
  GtkCellRenderer *cbubble = gtk_cell_renderer_bubble_new ();
  g_object_set (cbubble,
                "scale", 0.75,
                "width-chars", 3,
                "xalign", 0.5,
                NULL);
  gtk_tree_view_column_pack_start (column, cbubble, FALSE);
  gtk_tree_view_column_set_cell_data_func (column,
                                           cbubble,
                                           source_unread_func,
                                           g_object_ref (window),
                                           g_object_unref);
  
  gtk_tree_view_append_column (GTK_TREE_VIEW (treeview), column);
  
  
  
  /* selection signals */
  
  GtkTreeSelection *sel = gtk_tree_view_get_selection (GTK_TREE_VIEW (treeview));
  
  g_signal_connect (sel,
                    "changed",
                    G_CALLBACK (source_changed),
                    window);
  
  /* handle popup menu on right click */
  g_signal_connect (G_OBJECT (treeview),
                    "button-press-event",
                    G_CALLBACK (treeview_button_press_cb),
                    window);
}

static void
create_source_views (MarinaWindow *window)
{
  MarinaWindowPrivate *priv;
  GtkWidget *notebook;
  GtkWidget *default_source_view,
            *outlook_source_view;
  
  g_return_if_fail (MARINA_IS_WINDOW (window));
  
  priv = window->priv;
  notebook = priv->source_view_notebook;
  
  /* MarinaDefaultSourceView (Must be item 0) */
  default_source_view = marina_default_source_view_new ();
  marina_window_add_source_view (window, MARINA_SOURCE_VIEW (default_source_view));
  gtk_widget_show (default_source_view);

  /* MarinaOutlookSourceView (Must be item 1) */
  outlook_source_view = marina_outlook_source_view_new ();
  marina_window_add_source_view (window, MARINA_SOURCE_VIEW (outlook_source_view));
  gtk_widget_show (outlook_source_view);
}

static gchar*
get_icon_file (void)
{
  gchar *dir = marina_dirs_get_marina_data_dir ();
  gchar *result = g_build_filename (dir, "icons", "marina-icon.png", NULL);
  g_free (dir);
  return result;
}

static gboolean
expand_all (GtkTreeView *tree_view)
{
  /* show all the available sources */
  gtk_tree_view_expand_all (tree_view);
  return FALSE;
}

static void
set_compact_mode (MarinaWindow *window,
                  gboolean      active)
{
  MarinaWindowPrivate *priv;
  GtkWidget *widget;
  GtkRcStyle *rc_style;
  gint size;
  
  g_return_if_fail (MARINA_IS_WINDOW (window));
  
  priv = window->priv;
  widget = priv->source_treeview;
  
  rc_style = gtk_widget_get_modifier_style (widget);
  
  if (rc_style->font_desc)
    {
      pango_font_description_free (rc_style->font_desc);
      rc_style->font_desc = NULL;
    }
  
  if (active)
    {
      rc_style->font_desc = pango_font_description_copy (widget->style->font_desc);
      size = pango_font_description_get_size (rc_style->font_desc);
      pango_font_description_set_size (rc_style->font_desc, size * PANGO_SCALE_SMALL);
    }
  
  gtk_widget_modify_style (widget, rc_style);
  
  if (marina_prefs_compact_mode_can_set ())
    marina_prefs_set_compact_mode (active);
}

static void
compact_mode_cb (MarinaMessageBus *bus,
                 MarinaMessage    *message,
                 MarinaWindow     *window)
{
  gboolean active;
  
  marina_message_get (message, "active", &active, NULL);
  
  set_compact_mode (window, active);
}

static void
register_messages (MarinaWindow *window)
{
  /* create message bus item for compact mode */
  marina_message_bus_register (APP_BUS,
                               "/window", "compact_mode", 0,
                               "active", G_TYPE_BOOLEAN,
                               NULL);
  
  /* connect to the compact mode signal to shrink the sources */
  marina_message_bus_connect (APP_BUS,
                              "/window", "compact_mode",
                              (MarinaMessageCallback) compact_mode_cb,
                              g_object_ref (window),
                              g_object_unref);
}

static void
marina_window_init (MarinaWindow *window)
{
  GtkWidget *main_box;
  gint width = marina_prefs_get_window_width ();
  gint height = marina_prefs_get_window_height ();
  gchar *icon_file;
  
  window->priv = WINDOW_PRIVATE (window);
  
  window->priv->progresses = g_hash_table_new (g_str_hash, g_str_equal);
  
  register_messages (window);
  
  /* set window settings */
  gtk_window_set_title (&window->window, N_("Marina"));
  gtk_window_set_default_size (&window->window, width, height);
  
  /* window icon */
  icon_file = get_icon_file ();
  gtk_window_set_icon_from_file (&window->window, icon_file, NULL);
  g_free (icon_file);

  g_signal_connect (G_OBJECT (window),
                    "configure-event",
                    G_CALLBACK (configure_event_cb),
                    NULL);

  /* outmost container */
  main_box = gtk_vbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER (window), main_box);
  gtk_widget_show (main_box);

  /* create and insert our menu and toolbar */
  create_menu_bar_and_toolbar (window, main_box);

  /* create and insert our status bar */
  create_statusbar (window, main_box);
  
  /* create the main area */
  create_main_area (window, main_box);
  
  /* add the sources view */
  create_sources_tree_view (window);
  
  /* add our source views */
  create_source_views (window);
  
  /* update compact mode */
  GtkAction *action;
  action = gtk_action_group_get_action (window->priv->always_sensitive_action_group,
                                        "ViewCompactMode");
  gtk_toggle_action_set_active (GTK_TOGGLE_ACTION (action), marina_prefs_get_compact_mode ());
  
  /* something prevents us from calling gtk_tree_view_expand_all()
   * here, so we have to do it in a main loop callback. I suspect
   * the treeview hasn't updated itself yet.
   */
  g_timeout_add (0, (GSourceFunc)expand_all, window->priv->source_treeview);
}

void
_marina_window_select_first (MarinaWindow *window)
{
  MarinaSources *sources = marina_sources_get_default ();
  GtkTreeModel *model = marina_sources_get_tree_model (sources);
  GtkTreeIter iter;
  
  if (gtk_tree_model_get_iter_first (model, &iter))
      gtk_tree_selection_select_iter (
        gtk_tree_view_get_selection (GTK_TREE_VIEW (window->priv->source_treeview)),
        &iter);
}

MarinaWindow*
marina_window_new (void)
{
  return g_object_new (MARINA_TYPE_WINDOW, NULL);
}

/**
 * marina_window_get_ui_manager:
 * @window: A #MarinaWindow
 *
 * Retrieves the #GtkUIManager that was associated with the window.
 * Use this to attach accellerators, extend the ui, and add menus.
 *
 * Return value: The #GtkUIManager associated with @window
 */
GtkUIManager*
marina_window_get_ui_manager (MarinaWindow *window)
{
  g_return_val_if_fail (MARINA_IS_WINDOW (window), NULL);
  return window->priv->manager;
}

GList*
marina_window_get_source_views (MarinaWindow *window)
{
  MarinaWindowPrivate *priv;
  
  g_return_val_if_fail (MARINA_IS_WINDOW (window), NULL);
  
  priv = window->priv;
  
  return g_list_copy (priv->source_views);
}

void
marina_window_add_source_view (MarinaWindow     *window,
                               MarinaSourceView *source_view)
{
  MarinaWindowPrivate *priv;
  
  g_return_if_fail (MARINA_IS_WINDOW (window));
  
  priv = window->priv;
  
  if (g_list_find (priv->source_views, source_view))
    return;
  
  priv->source_views = g_list_append (priv->source_views, source_view);
  gtk_notebook_append_page (GTK_NOTEBOOK (priv->source_view_notebook),
                            GTK_WIDGET (source_view), NULL);
  
  if (!priv->source_view)
    marina_window_set_source_view (window, source_view);
}

void
marina_window_remove_source_view (MarinaWindow     *window,
                                  MarinaSourceView *source_view)
{
  MarinaWindowPrivate *priv;
  MarinaSourceView *new_child;
  
  g_return_if_fail (MARINA_IS_WINDOW (window));
  
  priv = window->priv;
  
  if (source_view == MARINA_SOURCE_VIEW (priv->source_view))
    {
      marina_source_view_set_source (source_view, NULL);
      gtk_container_remove (GTK_CONTAINER (priv->source_view_notebook),
                            GTK_WIDGET (source_view));
      priv->source_view = NULL;
    }
  
  priv->source_views = g_list_remove (priv->source_views, source_view);
  
  if (priv->source_view == NULL && g_list_length (priv->source_views))
    {
      new_child = g_list_first (priv->source_views)->data;
      marina_window_set_source_view (window, new_child);
    }
}

void
marina_window_set_source_view (MarinaWindow     *window,
                               MarinaSourceView *source_view)
{
  MarinaWindowPrivate *priv;
  GtkNotebook         *notebook;
  GtkWidget           *old_source_view;
  gint                 n_pages, i;
  
  g_return_if_fail (MARINA_IS_WINDOW (window));
  g_return_if_fail (MARINA_IS_SOURCE_VIEW (source_view));
  
  priv = window->priv;
  
  if (source_view == MARINA_SOURCE_VIEW (priv->source_view))
    return;
  
  old_source_view = priv->source_view;
  notebook = GTK_NOTEBOOK (priv->source_view_notebook);
  
  n_pages = gtk_notebook_get_n_pages (notebook);
  
  for (i = 0; i < n_pages; i++)
    {
      if (GTK_WIDGET (source_view) == gtk_notebook_get_nth_page (notebook, i))
        {
          gtk_notebook_set_current_page (notebook, i);
          gtk_widget_show (GTK_WIDGET (source_view));
          break;
        }
    }
  
  priv->source_view = g_object_ref (source_view);
  
  if (old_source_view)
    {
      gtk_widget_hide (old_source_view);
      marina_source_view_set_source (MARINA_SOURCE_VIEW (old_source_view), NULL);
      g_object_unref (old_source_view);
    }

  marina_source_view_set_source (source_view,
                                 marina_window_get_selected_source (window));
}

GtkWidget*
marina_window_get_statusbar (MarinaWindow *window)
{
  MarinaWindowPrivate *priv;
  
  g_return_val_if_fail (MARINA_IS_WINDOW (window), NULL);
  
  priv = window->priv;
  
  return priv->statusbar;
}

gboolean
_marina_window_is_fullscreen (MarinaWindow *window)
{
	g_return_val_if_fail (MARINA_IS_WINDOW (window), FALSE);

	return window->priv->is_fullscreen;
}

void
_marina_window_fullscreen (MarinaWindow *window)
{
	g_return_if_fail (MARINA_IS_WINDOW (window));

	if (_marina_window_is_fullscreen (window))
		return;

	/* Go to fullscreen mode and hide bars */
	gtk_window_fullscreen (&window->window);
	
	gtk_widget_hide (gtk_ui_manager_get_widget (window->priv->manager, "/MenuBar"));
	
	g_signal_handlers_block_by_func (window->priv->toolbar,
					                         toolbar_visibility_changed,
					                         window);
	gtk_widget_hide (window->priv->toolbar);
	
	g_signal_handlers_block_by_func (window->priv->statusbar,
					                         statusbar_visibility_changed,
					                         window);
	gtk_widget_hide (window->priv->statusbar);

	fullscreen_controls_build (window);
	fullscreen_controls_show (window);
	
	window->priv->is_fullscreen = TRUE;
}

void
_marina_window_unfullscreen (MarinaWindow *window)
{
	gboolean visible;
	GtkAction *action;

	g_return_if_fail (MARINA_IS_WINDOW (window));

	if (!_marina_window_is_fullscreen (window))
		return;

	/* Unfullscreen and show bars */
	gtk_window_unfullscreen (&window->window);
	gtk_widget_show (gtk_ui_manager_get_widget (window->priv->manager, "/MenuBar"));
	
	action = gtk_action_group_get_action (window->priv->always_sensitive_action_group,
                        					      "ViewToolbar");
	visible = gtk_toggle_action_get_active (GTK_TOGGLE_ACTION (action));
	if (visible)
		gtk_widget_show (window->priv->toolbar);
	g_signal_handlers_unblock_by_func (window->priv->toolbar,
					                           toolbar_visibility_changed,
					                           window);
	
	action = gtk_action_group_get_action (window->priv->always_sensitive_action_group,
					                              "ViewStatusbar");
	visible = gtk_toggle_action_get_active (GTK_TOGGLE_ACTION (action));
	if (visible)
		gtk_widget_show (window->priv->statusbar);
	g_signal_handlers_unblock_by_func (window->priv->statusbar,
					                           statusbar_visibility_changed,
					                           window);

	gtk_widget_hide (window->priv->fullscreen_controls);
	
	window->priv->is_fullscreen = FALSE;
}

MarinaSource*
marina_window_get_selected_source (MarinaWindow *window)
{
  MarinaWindowPrivate *priv;
  MarinaSource *source = NULL;
  GtkTreeSelection *sel;
  GtkTreeModel *model;
  GtkTreeIter iter;
  
  g_return_val_if_fail (MARINA_IS_WINDOW (window), NULL);
  
  priv = window->priv;
  
  sel = gtk_tree_view_get_selection (GTK_TREE_VIEW (priv->source_treeview));
  
  if (gtk_tree_selection_get_selected (sel, &model, &iter))
    gtk_tree_model_get (model, &iter, 0, &source, -1);
  
  return source;
}

MarinaProgress*
marina_window_get_progress (MarinaWindow *window,
                            const gchar  *name)
{
  MarinaWindowPrivate *priv;
  
  g_return_val_if_fail (MARINA_IS_WINDOW (window), NULL);
  
  priv = window->priv;
  
  return g_hash_table_lookup (priv->progresses, name);
}

void
marina_window_set_progress (MarinaWindow   *window,
                            const gchar    *name,
                            MarinaProgress *progress)
{
  MarinaWindowPrivate *priv;
  
  g_return_if_fail (MARINA_IS_WINDOW (window));
  g_return_if_fail (progress != NULL);
  
  priv = window->priv;
  
  gtk_box_pack_start (GTK_BOX (priv->side_pane), GTK_WIDGET (progress), FALSE, TRUE, 0);
  g_hash_table_insert (priv->progresses, g_strdup (name), progress);
}

MarinaSourceView*
marina_window_get_source_view (MarinaWindow *window)
{
  MarinaWindowPrivate *priv;

  g_return_val_if_fail (MARINA_IS_WINDOW (window), NULL);

  priv = window->priv;

  return MARINA_SOURCE_VIEW (priv->source_view);
}
