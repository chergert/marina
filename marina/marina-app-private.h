/* marina-app-private.h
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with This; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifndef __MARINA_APP_PRIVATE_H__
#define __MARINA_APP_PRIVATE_H__

#include "marina-items.h"
#include "marina-plugins-engine.h"
#include "marina-sources.h"
#include "marina-window.h"

#include "dialogs/marina-preferences-dialog.h"

struct _MarinaAppPrivate
{
  MarinaWindow            *window;
  MarinaPluginsEngine     *plugins;
  MarinaPreferencesDialog *prefs_dialog;
  MarinaSources           *sources;
  MarinaItems             *items;
};

#endif /* __MARINA_APP_PRIVATE_H__ */
