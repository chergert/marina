/* marina-data-item.c
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#include "marina-data-item.h"

GType
marina_data_item_get_type (void)
{
  static GType marina_data_item_type = 0;

  if (!marina_data_item_type)
    {
      const GTypeInfo marina_data_item_info = {
          sizeof (MarinaDataItemIface),
          NULL, /* base_init */
          NULL, /* base_finalize */
          NULL,
          NULL, /* class_finalize */
          NULL, /* class_data */
          0,
          0,
          NULL
      };
      
      marina_data_item_type = g_type_register_static (G_TYPE_INTERFACE, "MarinaDataItem",
                                                      &marina_data_item_info, 0);
      g_type_interface_add_prerequisite (marina_data_item_type, G_TYPE_OBJECT);
    }

  return marina_data_item_type;
}

/**
 * marina_data_item_get_id:
 * @data_item: A #MarinaDataItem
 *
 * The id for the item. It needs to only be unique per #MarinaItem that it
 * is to be attached to. For example, if you had an instance such as
 * #MarinaLinkDataItem the id might be "links" and the object would contain
 * a list of links for the associted #MarinaItem.
 *
 * Return value: the id of the data item.
 */
const gchar*
marina_data_item_get_id (MarinaDataItem *self)
{
  return MARINA_DATA_ITEM_GET_INTERFACE (self)->get_id (self);
}
