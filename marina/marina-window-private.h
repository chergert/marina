/*
 * marina-window-private.h
 * This file is part of marina.
 *
 * Copyright (C) 2009  Christian Hergert
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA 02111-1307, USA. 
 */

#ifndef __MARINA_WINDOW_PRIVATE_H__
#define __MARINA_WINDOW_PRIVATE_H__

#include <glib-object.h>
#include <gtk/gtk.h>

#define WINDOW_PRIVATE(object) (G_TYPE_INSTANCE_GET_PRIVATE((object), \
                                MARINA_TYPE_WINDOW,                   \
                                MarinaWindowPrivate))

struct _MarinaWindowPrivate
{
  GtkWidget                *menubar;
  GtkWidget                *toolbar;

  GtkWidget                *hpaned;
  GtkWidget                *side_pane;
  GtkWidget                *side_panel;
  GtkWidget                *bottom_panel;

  GtkUIManager             *manager;
  
  GtkActionGroup           *action_group;
  GtkActionGroup           *always_sensitive_action_group;

  GtkWidget                *statusbar;
  guint                     tip_message_cid;
  guint                     generic_message_cid;
  
  GList                    *source_views;
  GtkWidget                *source_view_notebook;
  GtkWidget                *source_view;
  
  GtkWidget                *source_treeview;
        
  /* Widgets for fullscreen mode */
	GtkWidget                *fullscreen_controls;
	guint                     fullscreen_animation_timeout_id;
	gboolean                  fullscreen_animation_enter;
	gboolean                  is_fullscreen;
	
	GHashTable               *progresses;
};

void     _marina_window_select_first    (MarinaWindow *window);
gboolean _marina_window_is_fullscreen   (MarinaWindow *window);
void     _marina_window_fullscreen      (MarinaWindow *window);
void     _marina_window_unfullscreen    (MarinaWindow *window);

#endif /* __MARINA_WINDOW_PRIVATE_H__ */
