/* this file was borrowed from gedit.
 * please see gedit for additional copyright information */

#ifndef __MARINA_MESSAGE_BUS_H__
#define __MARINA_MESSAGE_BUS_H__

#include <glib-object.h>
#include <marina/marina-message.h>
#include <marina/marina-message-type.h>

G_BEGIN_DECLS

#define MARINA_TYPE_MESSAGE_BUS			(marina_message_bus_get_type ())
#define MARINA_MESSAGE_BUS(obj)			(G_TYPE_CHECK_INSTANCE_CAST ((obj), MARINA_TYPE_MESSAGE_BUS, MarinaMessageBus))
#define MARINA_MESSAGE_BUS_CONST(obj)		(G_TYPE_CHECK_INSTANCE_CAST ((obj), MARINA_TYPE_MESSAGE_BUS, MarinaMessageBus const))
#define MARINA_MESSAGE_BUS_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST ((klass), MARINA_TYPE_MESSAGE_BUS, MarinaMessageBusClass))
#define MARINA_IS_MESSAGE_BUS(obj)		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), MARINA_TYPE_MESSAGE_BUS))
#define MARINA_IS_MESSAGE_BUS_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((klass), MARINA_TYPE_MESSAGE_BUS))
#define MARINA_MESSAGE_BUS_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS ((obj), MARINA_TYPE_MESSAGE_BUS, MarinaMessageBusClass))

typedef struct _MarinaMessageBus		MarinaMessageBus;
typedef struct _MarinaMessageBusClass	MarinaMessageBusClass;
typedef struct _MarinaMessageBusPrivate	MarinaMessageBusPrivate;

struct _MarinaMessageBus {
	GObject parent;
	
	MarinaMessageBusPrivate *priv;
};

struct _MarinaMessageBusClass {
	GObjectClass parent_class;
	
	void (*dispatch)		(MarinaMessageBus  *bus,
					 MarinaMessage     *message);
	void (*registered)		(MarinaMessageBus  *bus,
					 MarinaMessageType *message_type);
	void (*unregistered)		(MarinaMessageBus  *bus,
					 MarinaMessageType *message_type);
};

typedef void (* MarinaMessageCallback) 	(MarinaMessageBus *bus,
					 MarinaMessage	 *message,
					 gpointer	  userdata);

typedef void (* MarinaMessageBusForeach) (MarinaMessageType *message_type,
					 gpointer	   userdata);

GType marina_message_bus_get_type (void) G_GNUC_CONST;

MarinaMessageBus *marina_message_bus_get_default	(void);
MarinaMessageBus *marina_message_bus_new		(void);

/* registering messages */
MarinaMessageType *marina_message_bus_lookup	(MarinaMessageBus 	*bus,
						 const gchar		*object_path,
						 const gchar		*method);
MarinaMessageType *marina_message_bus_register	(MarinaMessageBus		*bus,
					   	 const gchar 		*object_path,
					  	 const gchar		*method,
					  	 guint		 	 num_optional,
					  	 ...) G_GNUC_NULL_TERMINATED;

void marina_message_bus_unregister	  (MarinaMessageBus	*bus,
					   MarinaMessageType	*message_type);

void marina_message_bus_unregister_all	  (MarinaMessageBus	*bus,
					   const gchar		*object_path);

gboolean marina_message_bus_is_registered  (MarinaMessageBus	*bus,
					   const gchar		*object_path,
					   const gchar		*method);

void marina_message_bus_foreach		  (MarinaMessageBus        *bus,
					   MarinaMessageBusForeach  func,
					   gpointer		   userdata);


/* connecting to message events */		   
guint marina_message_bus_connect	 	  (MarinaMessageBus	*bus, 
					   const gchar		*object_path,
					   const gchar		*method,
					   MarinaMessageCallback	 callback,
					   gpointer		 userdata,
					   GDestroyNotify        destroy_data);

void marina_message_bus_disconnect	  (MarinaMessageBus	*bus,
					   guint		 id);

void marina_message_bus_disconnect_by_func (MarinaMessageBus	*bus,
					   const gchar		*object_path,
					   const gchar		*method,
					   MarinaMessageCallback	 callback,
					   gpointer		 userdata);

/* blocking message event callbacks */
void marina_message_bus_block		  (MarinaMessageBus	*bus,
					   guint		 id);
void marina_message_bus_block_by_func	  (MarinaMessageBus	*bus,
					   const gchar		*object_path,
					   const gchar		*method,
					   MarinaMessageCallback	 callback,
					   gpointer		 userdata);

void marina_message_bus_unblock		  (MarinaMessageBus	*bus,
					   guint		 id);
void marina_message_bus_unblock_by_func	  (MarinaMessageBus	*bus,
					   const gchar		*object_path,
					   const gchar		*method,
					   MarinaMessageCallback	 callback,
					   gpointer		 userdata);

/* sending messages */
void marina_message_bus_send_message	  (MarinaMessageBus	*bus,
					   MarinaMessage		*message);
void marina_message_bus_send_message_sync  (MarinaMessageBus	*bus,
					   MarinaMessage		*message);
					  
void marina_message_bus_send		  (MarinaMessageBus	*bus,
					   const gchar		*object_path,
					   const gchar		*method,
					   ...) G_GNUC_NULL_TERMINATED;
MarinaMessage *marina_message_bus_send_sync (MarinaMessageBus	*bus,
					   const gchar		*object_path,
					   const gchar		*method,
					   ...) G_GNUC_NULL_TERMINATED;

G_END_DECLS

#endif /* __MARINA_MESSAGE_BUS_H__ */

// ex:ts=8:noet:
