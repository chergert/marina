/* marina-search-source.h
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with This; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifndef __MARINA_SEARCH_SOURCE_H__
#define __MARINA_SEARCH_SOURCE_H__

#include <glib-object.h>

#include "marina-source.h"
#include "marina-source-base.h"

G_BEGIN_DECLS

#define MARINA_TYPE_SEARCH_SOURCE (marina_search_source_get_type ())

#define MARINA_SEARCH_SOURCE(obj)             \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj),         \
  MARINA_TYPE_SEARCH_SOURCE,                  \
  MarinaSearchSource))

#define MARINA_SEARCH_SOURCE_CONST(obj)       \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj),         \
  MARINA_TYPE_SEARCH_SOURCE,                  \
  MarinaSearchSource const))

#define MARINA_SEARCH_SOURCE_CLASS(klass)     \
  (G_TYPE_CHECK_CLASS_CAST ((klass),          \
  MARINA_TYPE_SEARCH_SOURCE,                  \
  MarinaSearchSourceClass))

#define MARINA_IS_SEARCH_SOURCE(obj)          \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj),         \
  MARINA_TYPE_SEARCH_SOURCE))

#define MARINA_IS_SEARCH_SOURCE_CLASS(klass)  \
  (G_TYPE_CHECK_CLASS_TYPE ((klass),          \
  MARINA_TYPE_SEARCH_SOURCE))

#define MARINA_SEARCH_SOURCE_GET_CLASS(obj)   \
  (G_TYPE_INSTANCE_GET_CLASS ((obj),          \
  MARINA_TYPE_SEARCH_SOURCE,                  \
  MarinaSearchSourceClass))

typedef struct _MarinaSearchSource         MarinaSearchSource;
typedef struct _MarinaSearchSourceClass    MarinaSearchSourceClass;
typedef struct _MarinaSearchSourcePrivate  MarinaSearchSourcePrivate;

struct _MarinaSearchSource
{
  MarinaSourceBase parent;
  
  MarinaSearchSourcePrivate *priv;
};

struct _MarinaSearchSourceClass
{
  MarinaSourceBaseClass parent_class;
};

GType          marina_search_source_get_type     (void) G_GNUC_CONST;
MarinaSource*  marina_search_source_new          (void);

const gchar*   marina_search_source_get_keywords (MarinaSearchSource *search_source);
void           marina_search_source_set_keywords (MarinaSearchSource *search_source,
                                                  const gchar        *keywords);

G_END_DECLS

#endif /* __MARINA_SEARCH_SOURCE_H__ */
