/* marina-debug.h
 * 
 * This file is part of marina; it was borrowed from gedit
 *
 * Copyright (C) 1998, 1999 Alex Roberts, Evan Lawrence
 * Copyright (C) 2000, 2001 Chema Celorio, Paolo Maggi
 * Copyright (C) 2002 - 2005 Paolo Maggi  
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA 02111-1307, USA.
 */
 
/*
 * Modified by the gedit Team, 1998-2005. See the gedit AUTHORS file for a 
 * list of people on the marina Team.  
 *
 * Modified by the marina Team, 2009.
 */

#ifndef __MARINA_DEBUG_H__
#define __MARINA_DEBUG_H__

#include <glib.h>

/*
 * Set an environmental var of the same name to turn on
 * debugging output. Setting MARINA_DEBUG will turn on all
 * sections.
 */
typedef enum {
  MARINA_NO_DEBUG       = 0,
  MARINA_DEBUG_APP      = 1 << 1,
  MARINA_DEBUG_WINDOW   = 1 << 2,
  MARINA_DEBUG_PLUGINS  = 1 << 3,
  MARINA_DEBUG_PREFS    = 1 << 4,
  MARINA_DEBUG_SEARCH   = 1 << 5,
  MARINA_DEBUG_COMMANDS = 1 << 6,
  MARINA_DEBUG_SOURCES  = 1 << 7,
  MARINA_DEBUG_PARSERS  = 1 << 8,
  MARINA_DEBUG_BUILDERS = 1 << 9,
  MARINA_DEBUG_ITEMS    = 1 << 10,
} MarinaDebugSection;


#define  DEBUG_APP       MARINA_DEBUG_APP,     __FILE__, __LINE__, G_STRFUNC
#define  DEBUG_WINDOW    MARINA_DEBUG_WINDOW,  __FILE__, __LINE__, G_STRFUNC
#define  DEBUG_PLUGINS   MARINA_DEBUG_PLUGINS, __FILE__, __LINE__, G_STRFUNC
#define  DEBUG_PREFS     MARINA_DEBUG_PREFS,   __FILE__, __LINE__, G_STRFUNC
#define  DEBUG_SEARCH    MARINA_DEBUG_SEARCH,  __FILE__, __LINE__, G_STRFUNC
#define  DEBUG_COMMANDS  MARINA_DEBUG_COMMANDS,__FILE__, __LINE__, G_STRFUNC
#define  DEBUG_SOURCES   MARINA_DEBUG_SOURCES, __FILE__, __LINE__, G_STRFUNC
#define  DEBUG_PARSERS   MARINA_DEBUG_PARSERS, __FILE__, __LINE__, G_STRFUNC
#define  DEBUG_BUILDERS  MARINA_DEBUG_BUILDERS,__FILE__, __LINE__, G_STRFUNC
#define  DEBUG_ITEMS     MARINA_DEBUG_ITEMS,   __FILE__, __LINE__, G_STRFUNC

void marina_debug_init (void);

void marina_debug (MarinaDebugSection  section,
                   const gchar       *file,
                   gint               line,
                   const gchar       *function);

void marina_debug_message (MarinaDebugSection  section,
                           const gchar       *file,
                           gint               line,
                           const gchar       *function,
                           const gchar       *format, ...) G_GNUC_PRINTF(5, 6);


#endif /* __MARINA_DEBUG_H__ */
