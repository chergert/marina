/* marina-sources.c
 * This file is part of marina
 *
 * Copyright (C) 2009  Christian Hergert
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#include <string.h>

#include "marina-debug.h"
#include "marina-dirs.h"
#include "marina-items.h"
#include "marina-source.h"
#include "marina-sources.h"
#include "marina-sources-private.h"

#include "marina-folder-source.h"
#include "marina-home-source.h"
#include "marina-search-source.h"
#include "marina-soup-source.h"
#include "marina-vfs-source.h"

#include "xml-reader.h"
#include "xml-writer.h"

G_DEFINE_TYPE (MarinaSources, marina_sources, G_TYPE_OBJECT);

static void
marina_sources_finalize (GObject *object)
{
  MarinaSourcesPrivate *priv;
  
  marina_debug (DEBUG_SOURCES);
  
  priv = MARINA_SOURCES (object)->priv;
  
  g_hash_table_destroy (priv->hash_table);
  g_object_unref (priv->tree_store);
  
  G_OBJECT_CLASS (marina_sources_parent_class)->finalize (object);
}

static void
marina_sources_class_init (MarinaSourcesClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  
  object_class->finalize = marina_sources_finalize;
  
  g_type_class_add_private (object_class, sizeof (MarinaSourcesPrivate));
}

static void
marina_sources_init (MarinaSources *sources)
{
  GtkTreeStore *tree_store;

  g_return_if_fail (MARINA_IS_SOURCES (sources));

  marina_debug (DEBUG_SOURCES);

  sources->priv = G_TYPE_INSTANCE_GET_PRIVATE (sources,
                                               MARINA_TYPE_SOURCES,
                                               MarinaSourcesPrivate);

  tree_store = gtk_tree_store_new (1, MARINA_TYPE_SOURCE);
	
  /*
  g_signal_connect (store, "row-deleted", row_deleted_cb, self);
  g_signal_connect (store, "row-inserted", row_inserted_cb, self);
  g_signal_connect (store, "rows-reordered", rows_reordered_cb, self);
  */
	
  sources->priv->tree_store = tree_store;
  
  /* force loading of our core types */
  g_type_name (MARINA_TYPE_VFS_SOURCE);
  g_type_name (MARINA_TYPE_SOUP_SOURCE);
  g_type_name (MARINA_TYPE_FOLDER_SOURCE);
  g_type_name (MARINA_TYPE_SEARCH_SOURCE);
  g_type_name (MARINA_TYPE_HOME_SOURCE);
}

/**
 * marina_sources_get_default:
 *
 * Retrieves the singleton instance for the #MarinaSources collection.
 *
 * Returns: the instance. Use g_object_ref() if you intend to hold a
 *   reference to the object.
 */
MarinaSources*
marina_sources_get_default (void)
{
  static MarinaSources *instance = NULL;
  static GStaticMutex mutex = G_STATIC_MUTEX_INIT;
  
  if (!instance)
    {
      g_static_mutex_lock (&mutex);
      
      if (!instance)
        {
          marina_debug_message (DEBUG_SOURCES, "Creating sources singleton");
          instance = g_object_new (MARINA_TYPE_SOURCES, NULL);
        }
      
      g_static_mutex_unlock (&mutex);
    }
  
  return instance;
}

static gchar*
get_user_sources_xml (void)
{
	gchar *dir, *file;

	dir = marina_dirs_get_user_config_dir ();
	file = g_build_filename (dir, "sources.xml", NULL);
	g_free (dir);

	return file;
}

static gchar*
get_global_sources_xml (void)
{
  gchar *dir, *file;

  dir = marina_dirs_get_marina_data_dir ();
  file = g_build_filename (dir, "sources.xml", NULL);
  g_free (dir);

  return file;
}

typedef struct {
  GtkTreePath  *path;
  MarinaSource *source;
} TreeLookup;

static gboolean
find_iter_cb (GtkTreeModel *model,
              GtkTreePath  *path,
              GtkTreeIter  *iter,
              gpointer      data)
{
  TreeLookup *lookup = data;
  MarinaSource  *source;
  
  gtk_tree_model_get (model, iter, 0, &source, -1);
  
  if (source == lookup->source)
    lookup->path = gtk_tree_path_copy (path);
  
  return lookup->path != NULL;
}

static void
add_to_tree_store (MarinaSource *source,
                   GtkTreeStore *store,
                   GtkTreeIter  *parent);

static void
add_to_hash_table (MarinaSource *source,
                   GHashTable   *hash_table);

static void
child_added_cb (MarinaSource *source,
                MarinaSource *child)
{
  MarinaSources *sources;
  TreeLookup lookup = {0,0};
  GtkTreeIter parent;
  
  g_return_if_fail (MARINA_IS_SOURCE (source));
  g_return_if_fail (MARINA_IS_SOURCE (child));
  
  sources = marina_sources_get_default ();
  
  lookup.source = source;
  
  /* get the iter for the parent */
  gtk_tree_model_foreach (GTK_TREE_MODEL (sources->priv->tree_store),
                          (GtkTreeModelForeachFunc) find_iter_cb,
                          &lookup);
  
  if (lookup.path)
    {
      if (gtk_tree_model_get_iter (GTK_TREE_MODEL (sources->priv->tree_store),
                                   &parent, lookup.path))
        add_to_tree_store (child, sources->priv->tree_store, &parent);
      
      gtk_tree_path_free (lookup.path);
    }
  
  add_to_hash_table (child, sources->priv->hash_table);
}

static void
add_to_tree_store (MarinaSource *source,
                   GtkTreeStore *store,
                   GtkTreeIter  *parent)
{
  GtkTreeIter tree_iter;
  GList *list;
  GList *iter;
  
  g_return_if_fail (source != NULL);
  g_return_if_fail (MARINA_IS_SOURCE (source));
  g_return_if_fail (store != NULL);
  
  gtk_tree_store_append (store, &tree_iter, parent);
  gtk_tree_store_set (store, &tree_iter, 0, source, -1);
  
  /* get a callback when a child is added so we can add it */
  g_signal_connect (source,
                    "child-added",
                    G_CALLBACK (child_added_cb),
                    NULL);
  
  list = marina_source_get_children (source);
  
  for (iter = list; iter; iter = iter->next)
    add_to_tree_store (iter->data, store, &tree_iter);
  
  g_list_free (list);
}

static void
add_to_hash_table (MarinaSource *source,
                   GHashTable   *hash_table)
{
  GList *list;
  GList *iter;
  const gchar *key;
  
  g_return_if_fail (MARINA_IS_SOURCE (source));
  g_return_if_fail (hash_table != NULL);
  
  key = marina_source_get_id (source);
  
  if (!key)
    return;
  
  g_hash_table_insert (hash_table, g_strdup (key), source);
  
  list = marina_source_get_children (source);
  
  for (iter = list; iter; iter = iter->next)
    add_to_hash_table (iter->data, hash_table);
  
  g_list_free (list);
}

void
marina_sources_add (MarinaSources *sources,
                    MarinaSource  *source,
                    MarinaSource  *parent)
{
  g_return_if_fail (MARINA_IS_SOURCES (sources));
  g_return_if_fail (MARINA_IS_SOURCE (source));

  if (!parent)
    {
      add_to_hash_table (source, sources->priv->hash_table);
      add_to_tree_store (source, sources->priv->tree_store, NULL);
    }
  else
    child_added_cb (parent, source);
}

static void
load (MarinaSources  *sources,
      const gchar    *filename,
      GError        **error)
{
  MarinaSourcesPrivate *priv;
  XmlReader *reader;
  GList *list = NULL;
  GList *iter;
  
  g_return_if_fail (MARINA_IS_SOURCES (sources));
  
  priv = sources->priv;
  
  reader = xml_reader_new ();
  
  if (!xml_reader_load_from_file (reader, filename))
    return; // todo: set error
  
  /* look for the outer <sources> element */
  if (xml_reader_read_start_element (reader, "sources"))
    {
      /* we only parse top-most <source> elements. implementations
       * handle any children themselves via marina_source_deserialize().
       */
      while (xml_reader_read_start_element (reader, "source"))
        {
          MarinaSource *source;
          const gchar *type;
          GType gtype;
          
          if ((type = xml_reader_get_attribute (reader, "type")) != NULL)
            {
              /* get the desired GType to instantiate */
              gtype = g_type_from_name (type);
              
              if (!gtype)
                continue;
              
              /* instantiate the source */
              source = g_object_new (gtype, NULL);
              
              /* make sure the type was resolved and created */
              if (source == NULL)
                continue;
              
              /* allow the source to deserialize itself */
              marina_source_deserialize (source, reader);
              
              list = g_list_prepend (list, source);
            }
        }
    }
  
  list = g_list_reverse (list);
  
  /* create our internal hashtable for lookups */
  priv->hash_table = g_hash_table_new_full (g_str_hash, g_str_equal,
                                            g_free, g_object_unref);
  
  add_to_tree_store (marina_home_source_new (), priv->tree_store, NULL);
  
  /* populate the hashtable (method is recursive) */
  for (iter = list; iter; iter = iter->next)
    {
      add_to_hash_table (iter->data, priv->hash_table);
      add_to_tree_store (iter->data, priv->tree_store, NULL);
    }
  
  g_list_free (list);
  g_object_unref (reader);
}

gboolean
marina_sources_load (MarinaSources  *sources,
                     GError        **error)
{
  MarinaSourcesPrivate *priv;
  gchar *file;
  gboolean success;
  
	marina_debug (DEBUG_SOURCES);
  
  g_return_val_if_fail (MARINA_IS_SOURCES (sources), FALSE);
  
  priv = sources->priv;
	file = get_user_sources_xml ();

  if (!g_file_test (file, G_FILE_TEST_IS_REGULAR))
    {
      g_free (file);
      file = get_global_sources_xml ();
    }
  
	if (g_file_test (file, G_FILE_TEST_IS_REGULAR))
	  load (sources, file, error);
  else
    g_error ("Incomplete marina install.");
	
	g_free (file);
  
	success = (priv->hash_table != NULL);
	
	if (!success)
	  g_set_error (error, MARINA_SOURCES_ERROR,
	               MARINA_SOURCES_ERROR_INVALID,
	               "There was an error parsing the data sources");
	
	marina_sources_stat (sources);
	
	return success;
}

void
marina_sources_remove (MarinaSources *sources,
                       MarinaSource  *source)
{
  MarinaSourcesPrivate *priv;
  TreeLookup lookup = {0,0};
  GtkTreeIter iter;
  const gchar *key;
  
	marina_debug (DEBUG_SOURCES);
	
	g_return_if_fail (MARINA_IS_SOURCES (sources));
	g_return_if_fail (MARINA_IS_SOURCE (source));
	
	priv = sources->priv;
	key = marina_source_get_id (source);
	
	marina_debug_message (DEBUG_SOURCES, "Removing source %s", key);
	
  /* remove from hash table */
	g_hash_table_remove (priv->hash_table, key);

  /* remove from tree store */
  lookup.source = source;

  gtk_tree_model_foreach (GTK_TREE_MODEL (sources->priv->tree_store),
                          (GtkTreeModelForeachFunc) find_iter_cb,
                          &lookup);
  
  if (lookup.path)
    {
      if (gtk_tree_model_get_iter (GTK_TREE_MODEL (sources->priv->tree_store),
                                   &iter, lookup.path))
        gtk_tree_store_remove (sources->priv->tree_store, &iter);
      
      gtk_tree_path_free (lookup.path);
    }
}

MarinaSource*
marina_sources_get (MarinaSources *sources,
                    const gchar   *source_id)
{
  MarinaSourcesPrivate *priv;
  
	marina_debug (DEBUG_SOURCES);
	
	g_return_val_if_fail (MARINA_IS_SOURCES (sources), NULL);
	g_return_val_if_fail (source_id != NULL, NULL);
	
	priv = sources->priv;
	
	return g_hash_table_lookup (priv->hash_table, source_id);
}

void
marina_sources_set (MarinaSources *sources,
                    MarinaSource  *source)
{
  MarinaSourcesPrivate *priv;
  const gchar *key;

  marina_debug (DEBUG_SOURCES);

  g_return_if_fail (MARINA_IS_SOURCES (sources));
  g_return_if_fail (source != NULL);

  priv = sources->priv;

  key = marina_source_get_id (source);
  
  g_hash_table_insert (priv->hash_table, g_strdup (key), source);

  /* TODO: Merge with tree_store */
}

gboolean
marina_sources_contains (MarinaSources *sources,
                         const gchar   *source_id)
{
  MarinaSourcesPrivate *priv;

  marina_debug (DEBUG_SOURCES);

  g_return_val_if_fail (MARINA_IS_SOURCES (sources), FALSE);
  g_return_val_if_fail (source_id != NULL, FALSE);
  
  priv = sources->priv;

  return g_hash_table_lookup (priv->hash_table, source_id) != NULL;
}

GtkTreeModel*
marina_sources_get_tree_model (MarinaSources *sources)
{
  MarinaSourcesPrivate *priv;
  
  g_return_val_if_fail (MARINA_IS_SOURCES (sources), NULL);
  
  priv = sources->priv;
  
  return GTK_TREE_MODEL (priv->tree_store);
}

void
marina_sources_foreach (MarinaSources *sources,
                        GFunc          func,
                        gpointer       user_data)
{
  MarinaSourcesPrivate *priv;
  GHashTableIter iter;
  gpointer key, value;
  
  g_return_if_fail (MARINA_IS_SOURCES (sources));
  g_return_if_fail (func != NULL);
  
  priv = sources->priv;
  
  g_hash_table_iter_init (&iter, priv->hash_table);
  
  while (g_hash_table_iter_next (&iter, &key, &value))
      func (value, user_data);
}

GQuark
marina_sources_error_quark (void)
{
  return g_quark_from_static_string ("marina-sources-error-quark");
}

void
marina_sources_save (MarinaSources *sources)
{
  MarinaSourcesPrivate *priv;
  GtkTreeModel *model;
  GtkTreeIter iter;
  XmlWriter *writer;
  gchar *filename;
  MarinaSource *source;

  g_return_if_fail (MARINA_IS_SOURCES (sources));

  priv = sources->priv;
  model = GTK_TREE_MODEL (priv->tree_store);

  filename = get_user_sources_xml ();
  writer = xml_writer_new_for_file (filename);
  g_free (filename);

  if (!writer)
    g_error ("Could not open %s for writing", filename);

  xml_writer_write_start_document (writer, NULL, NULL, NULL);
  xml_writer_write_start_element (writer, "sources");

  if (gtk_tree_model_get_iter_first (model, &iter))
    {
      do
        {
          gtk_tree_model_get (model, &iter, 0, &source, -1);
          if (G_OBJECT_TYPE (source) != MARINA_TYPE_HOME_SOURCE)
            marina_source_serialize (source, writer);
        }
      while (gtk_tree_model_iter_next (model, &iter));
    }

  xml_writer_write_end_element (writer);
  xml_writer_write_end_document (writer);

  g_object_unref (writer);
}

guint
marina_sources_count_unread (MarinaSources *sources,
                             MarinaSource  *source)
{
  MarinaSourcesPrivate *priv;
  
  g_return_val_if_fail (MARINA_IS_SOURCES (sources), 0);
  
  priv = sources->priv;
  
  return GPOINTER_TO_UINT (g_object_get_data (G_OBJECT (source), "sources-unread-count"));
}

static gboolean
stat_cb (GtkTreeModel *model,
         GtkTreePath  *path,
         GtkTreeIter  *iter,
         gpointer      data)
{
  MarinaSources *sources = data;
  MarinaItem *item;
  MarinaSource *source;
  guint count;
  
  gtk_tree_model_get (model, iter, 0, &item, -1);
  
  if (!item)
    return FALSE;
  
  source = marina_sources_get (sources, marina_item_get_source_id (item));
  
  if (!source)
    return FALSE;
  
  count = GPOINTER_TO_UINT (g_object_get_data (G_OBJECT (source), "sources-unread-count"));
  
  if (!marina_item_get_read (item))
    g_object_set_data (G_OBJECT (source), "sources-unread-count", GUINT_TO_POINTER (++count));
  
  return FALSE;
}

static gboolean
clear_cb (GtkTreeModel *model,
          GtkTreePath  *path,
          GtkTreeIter  *iter,
          gpointer      data)
{
  MarinaSource *source;
  
  gtk_tree_model_get (model, iter, 0, &source, -1);
  
  if (source)
    g_object_set_data (G_OBJECT (source), "sources-unread-count", NULL);
  
  return FALSE;
}

void
marina_sources_stat (MarinaSources *sources)
{
  MarinaSourcesPrivate *priv;
  GtkTreeModel *model;
  
  g_return_if_fail (MARINA_IS_SOURCES (sources));
  
  priv = sources->priv;
  
  gtk_tree_model_foreach (GTK_TREE_MODEL (priv->tree_store), clear_cb, NULL);
  
  model = marina_items_get_model (marina_items_get_default ());
  
  gtk_tree_model_foreach (model, stat_cb, sources);
}
