/* marina-web-view.h
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifndef __MARINA_WEB_VIEW_H__
#define __MARINA_WEB_VIEW_H__

#include <glib-object.h>
#include <gtk/gtk.h>

#include "marina-item.h"
#include "marina-source.h"

G_BEGIN_DECLS

#define MARINA_TYPE_WEB_VIEW (marina_web_view_get_type ())

#define MARINA_WEB_VIEW(obj)            \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj),   \
  MARINA_TYPE_WEB_VIEW, MarinaWebView))

#define MARINA_WEB_VIEW_CONST(obj)      \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj),   \
  MARINA_TYPE_WEB_VIEW,                 \
  MarinaWebView const))

#define MARINA_WEB_VIEW_CLASS(klass)    \
  (G_TYPE_CHECK_CLASS_CAST ((klass),    \
  MARINA_TYPE_WEB_VIEW,                 \
  MarinaWebViewClass))

#define MARINA_IS_WEB_VIEW(obj)         \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj),   \
  MARINA_TYPE_WEB_VIEW))

#define MARINA_IS_WEB_VIEW_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass),    \
  MARINA_TYPE_WEB_VIEW))

#define MARINA_WEB_VIEW_GET_CLASS(obj)  \
  (G_TYPE_INSTANCE_GET_CLASS ((obj),    \
  MARINA_TYPE_WEB_VIEW,                 \
  MarinaWebViewClass))

typedef struct _MarinaWebView         MarinaWebView;
typedef struct _MarinaWebViewClass    MarinaWebViewClass;
typedef struct _MarinaWebViewPrivate  MarinaWebViewPrivate;

struct _MarinaWebView
{
  GtkVBox parent;
  
  MarinaWebViewPrivate *priv;
};

struct _MarinaWebViewClass
{
  GtkVBoxClass parent_class;
};

GType         marina_web_view_get_type    (void) G_GNUC_CONST;
GtkWidget*    marina_web_view_new         (void);

void          marina_web_view_set_item    (MarinaWebView *web_view,
                                           MarinaItem    *item);
void          marina_web_view_set_source  (MarinaWebView *web_view,
                                           MarinaSource  *source);
void          marina_web_view_clear       (MarinaWebView *web_view);

G_END_DECLS

#endif /* __MARINA_WEB_VIEW_H__ */
