/* marina-object-module.h
 *
 * This file is part of marina; this was borrowed from gedit
 *
 * Copyright (C) 2005 - Paolo Maggi
 * Copyright (C) 2008 - Jesse van den Kieboom
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA 02111-1307, USA. 
 */
 
/* This is a modified version of gedit-module.h from Epiphany source code.
 * Here the original copyright assignment:
 *
 *  Copyright (C) 2003 Marco Pesenti Gritti
 *  Copyright (C) 2003, 2004 Christian Persch
 *
 */

/*
 * Modified by the gedit Team, 2005. See the gedit AUTHORS file for a 
 * list of people on the gedit Team.  
 *
 * Modified by the marina Team, 2009.
 */

#ifndef __MARINA_OBJECT_MODULE_H__
#define __MARINA_OBJECT_MODULE_H__

#include <glib-object.h>
#include <gmodule.h>
#include <stdarg.h>

G_BEGIN_DECLS

#define MARINA_TYPE_OBJECT_MODULE (marina_object_module_get_type ())

#define MARINA_OBJECT_MODULE(obj)            \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj),        \
  MARINA_TYPE_OBJECT_MODULE,                 \
  MarinaObjectModule))

#define MARINA_OBJECT_MODULE_CLASS(klass)    \
  (G_TYPE_CHECK_CLASS_CAST ((klass),         \
  MARINA_TYPE_OBJECT_MODULE,                 \
  MarinaObjectModuleClass))
  
#define MARINA_IS_OBJECT_MODULE(obj)         \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj),        \
  MARINA_TYPE_OBJECT_MODULE))

#define MARINA_IS_OBJECT_MODULE_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass),         \
  MARINA_TYPE_OBJECT_MODULE))

#define MARINA_OBJECT_MODULE_GET_CLASS(obj)  \
  (G_TYPE_INSTANCE_GET_CLASS((obj),          \
  MARINA_TYPE_OBJECT_MODULE,                 \
  MarinaObjectModuleClass))

typedef struct _MarinaObjectModule        MarinaObjectModule;
typedef struct _MarinaObjectModulePrivate MarinaObjectModulePrivate;
typedef struct _MarinaObjectModuleClass   MarinaObjectModuleClass;

struct _MarinaObjectModule
{
  GTypeModule parent;

  MarinaObjectModulePrivate *priv;
};

struct _MarinaObjectModuleClass
{
  GTypeModuleClass parent_class;

  /* Virtual class methods */
  void (* garbage_collect) ();
};

GType               marina_object_module_get_type        (void) G_GNUC_CONST;
MarinaObjectModule* marina_object_module_new             (const gchar *module_name,
                                                          const gchar *path,
                                                          const gchar *type_registration);

GObject*            marina_object_module_new_object      (MarinaObjectModule *module, 
                                                          const gchar     *first_property_name,
                                                          ...);

GType         marina_object_module_get_object_type       (MarinaObjectModule *module);
const gchar*  marina_object_module_get_path              (MarinaObjectModule *module);
const gchar*  marina_object_module_get_module_name       (MarinaObjectModule *module);
const gchar*  marina_object_module_get_type_registration (MarinaObjectModule *module);

G_END_DECLS

#endif
