/* marina-default-parser.h
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifndef __MARINA_DEFAULT_PARSER_H__
#define __MARINA_DEFAULT_PARSER_H__

#include <glib-object.h>

#include "marina-parser.h"

G_BEGIN_DECLS

#define MARINA_TYPE_DEFAULT_PARSER (marina_default_parser_get_type ())

#define MARINA_DEFAULT_PARSER(obj)            \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj),         \
  MARINA_TYPE_DEFAULT_PARSER,                 \
  MarinaDefaultParser))

#define MARINA_DEFAULT_PARSER_CONST(obj)      \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj),         \
  MARINA_TYPE_DEFAULT_PARSER,                 \
  MarinaDefaultParser const))

#define MARINA_DEFAULT_PARSER_CLASS(klass)    \
  (G_TYPE_CHECK_CLASS_CAST ((klass),          \
  MARINA_TYPE_DEFAULT_PARSER,                 \
  MarinaDefaultParserClass))

#define MARINA_IS_DEFAULT_PARSER(obj)         \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj),         \
  MARINA_TYPE_DEFAULT_PARSER))

#define MARINA_IS_DEFAULT_PARSER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass),          \
  MARINA_TYPE_DEFAULT_PARSER))

#define MARINA_DEFAULT_PARSER_GET_CLASS(obj)  \
  (G_TYPE_INSTANCE_GET_CLASS ((obj),          \
  MARINA_TYPE_DEFAULT_PARSER,                 \
  MarinaDefaultParserClass))

typedef struct _MarinaDefaultParser         MarinaDefaultParser;
typedef struct _MarinaDefaultParserClass    MarinaDefaultParserClass;
typedef struct _MarinaDefaultParserPrivate  MarinaDefaultParserPrivate;

struct _MarinaDefaultParser
{
  GObject parent;
  
  MarinaDefaultParserPrivate *priv;
};

struct _MarinaDefaultParserClass
{
  GObjectClass parent_class;
};

GType         marina_default_parser_get_type (void) G_GNUC_CONST;
MarinaParser* marina_default_parser_new      (void);

G_END_DECLS

#endif /* __MARINA_DEFAULT_PARSER_H__ */
