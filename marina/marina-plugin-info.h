/* marina-plugin-info.h
 * 
 * This file is part of marina; it was borrowed from gedit
 *
 * Copyright (C) 2002-2005 - Paolo Maggi 
 * Copyright (C) 2007 - Paolo Maggi, Steve Frécinaux
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */
 
/*
 * Modified by the marina Team, 2002-2007. See the gedit AUTHORS file for a
 * list of people on the gedit Team.
 */

#ifndef __MARINA_PLUGIN_INFO_H__
#define __MARINA_PLUGIN_INFO_H__

#include <glib-object.h>

G_BEGIN_DECLS

#define MARINA_TYPE_PLUGIN_INFO    (marina_plugin_info_get_type ())
#define MARINA_PLUGIN_INFO(obj)    ((MarinaPluginInfo *) (obj))

typedef struct _MarinaPluginInfo      MarinaPluginInfo;

GType         marina_plugin_info_get_type        (void) G_GNUC_CONST;

gboolean      marina_plugin_info_is_active       (MarinaPluginInfo *info);
gboolean      marina_plugin_info_is_available    (MarinaPluginInfo *info);
gboolean      marina_plugin_info_is_configurable (MarinaPluginInfo *info);

const gchar*  marina_plugin_info_get_module_name (MarinaPluginInfo *info);

const gchar*  marina_plugin_info_get_name        (MarinaPluginInfo *info);
const gchar*  marina_plugin_info_get_description (MarinaPluginInfo *info);
const gchar*  marina_plugin_info_get_icon_name   (MarinaPluginInfo *info);
const gchar** marina_plugin_info_get_authors     (MarinaPluginInfo *info);
const gchar*  marina_plugin_info_get_website     (MarinaPluginInfo *info);
const gchar*  marina_plugin_info_get_copyright   (MarinaPluginInfo *info);
const gchar*  marina_plugin_info_get_version     (MarinaPluginInfo *info);

G_END_DECLS

#endif /* __MARINA_PLUGIN_INFO_H__ */

