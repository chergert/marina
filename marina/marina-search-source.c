/* marina-search-source.c
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with This; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <string.h>
#include <gtk/gtk.h>

#include "marina-debug.h"
#include "marina-search-source.h"

struct _MarinaSearchSourcePrivate
{
  gchar  *keywords;
  gchar **keywords_s;
};

enum
{
  PROP_0,
  PROP_KEYWORDS,
};

static void marina_source_init (MarinaSourceIface *iface);

G_DEFINE_TYPE_EXTENDED (MarinaSearchSource,
                        marina_search_source,
                        MARINA_TYPE_SOURCE_BASE,
                        0,
                        G_IMPLEMENT_INTERFACE (MARINA_TYPE_SOURCE,
                                               marina_source_init));

static void
marina_search_source_finalize (GObject *object)
{
  G_OBJECT_CLASS (marina_search_source_parent_class)->finalize (object);
}

static void
marina_search_source_get_property (GObject    *object,
                                   guint       property_id,
                                   GValue     *value,
                                   GParamSpec *pspec)
{
  switch (property_id) {
  case PROP_KEYWORDS:
    g_value_set_string (value, marina_search_source_get_keywords (MARINA_SEARCH_SOURCE (object)));
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
  }
}

static void
marina_search_source_set_property (GObject      *object,
                                   guint         property_id,
                                   const GValue *value,
                                   GParamSpec   *pspec)
{
  switch (property_id) {
  case PROP_KEYWORDS:
    marina_search_source_set_keywords (MARINA_SEARCH_SOURCE (object), g_value_get_string (value));
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
  }
}

static void
marina_search_source_class_init (MarinaSearchSourceClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  
  object_class->get_property = marina_search_source_get_property;
  object_class->set_property = marina_search_source_set_property;
  object_class->finalize = marina_search_source_finalize;

  g_type_class_add_private (object_class, sizeof (MarinaSearchSourcePrivate));
  
  g_object_class_install_property (object_class,
                                   PROP_KEYWORDS,
                                   g_param_spec_string ("keywords",
                                                        "Keywords",
                                                        "The search keywords",
                                                        NULL,
                                                        G_PARAM_READWRITE));
}

static void
marina_search_source_init (MarinaSearchSource *source)
{
  source->priv = G_TYPE_INSTANCE_GET_PRIVATE (source,
                                              MARINA_TYPE_SEARCH_SOURCE,
                                              MarinaSearchSourcePrivate);
}

MarinaSource*
marina_search_source_new (void)
{
  return g_object_new (MARINA_TYPE_SEARCH_SOURCE, NULL);
}

static gboolean
observes (MarinaSource *source,
          MarinaItem   *item)
{
  MarinaSearchSourcePrivate *priv;
  gchar *title;
  gchar *author;
  gchar *content;
  gchar *str;
  gint i;
  
  g_return_val_if_fail (MARINA_IS_SEARCH_SOURCE (source), FALSE);
  
  priv = MARINA_SEARCH_SOURCE (source)->priv;
  
  if (!priv->keywords)
    return FALSE;
  
  g_object_get (G_OBJECT (item),
                "title", &title,
                "author", &author,
                "content", &content,
                NULL);
  
  for (i = 0; i < g_strv_length (priv->keywords_s); i++)
    {
      str = priv->keywords_s [i];
      
      if (strcasestr (title, str))
        return TRUE;
      else if (strcasestr (author, str))
        return TRUE;
      else if (strcasestr (content, str))
        return TRUE;
    }
  
  return FALSE;
}

static gboolean
has_stream (MarinaSource *source)
{
  return FALSE;
}

static GInputStream*
get_stream (MarinaSource  *source,
            GError       **error)
{
  return NULL;
}

static G_CONST_RETURN gchar*
get_icon_name (MarinaSource *source)
{
  return "folder-saved-search";
}

static void
marina_source_init (MarinaSourceIface *iface)
{
  iface->get_stream = get_stream;
  iface->has_stream = has_stream;
  iface->get_icon_name = get_icon_name;
  iface->observes = observes;
}

const gchar*
marina_search_source_get_keywords (MarinaSearchSource *search_source)
{
  MarinaSearchSourcePrivate *priv;
  
  g_return_val_if_fail (MARINA_IS_SEARCH_SOURCE (search_source), NULL);
  
  priv = search_source->priv;
  
  return priv->keywords;
}

void
marina_search_source_set_keywords (MarinaSearchSource *search_source,
                                   const gchar        *keywords)
{
  MarinaSearchSourcePrivate *priv;
  
  g_return_if_fail (MARINA_IS_SEARCH_SOURCE (search_source));
  
  priv = search_source->priv;
  
  g_free (priv->keywords);
  g_free (priv->keywords_s);
  
  priv->keywords = g_strdup (keywords);
  priv->keywords_s = g_strsplit_set (priv->keywords, " ,", 0);
}
