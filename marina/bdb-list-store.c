/* bdb-list-store.c
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with This; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 *
 * Should you wish to use BdbListStore under another license
 * please contact me.
 */

#include <glib.h>
#include <string.h>

#include "bdb-list-store.h"
#include "g-lru-cache.h"

#define CLEAR_DBT(dbt)   (memset(&(dbt), 0, sizeof(dbt)))
#define FREE_DBT(dbt)    if ((dbt.flags & (DB_DBT_MALLOC|DB_DBT_REALLOC)) && \
                              dbt.data != NULL) { g_free(dbt.data); dbt.data = NULL; }

static void tree_model_init (GtkTreeModelIface *iface);

G_DEFINE_TYPE_EXTENDED (BdbListStore,
                        bdb_list_store,
                        G_TYPE_OBJECT,
                        0,
                        G_IMPLEMENT_INTERFACE (GTK_TYPE_TREE_MODEL,
                                               tree_model_init));

struct _BdbListStorePrivate
{
  gint                stamp;
  GType               g_type;
  DB                 *db;
  gboolean            dirty;
  gint                n_keys;
  
  GLruCache          *lru;
  
  BdbSerializeFunc    serialize_func;
  gpointer            serialize_data;
  GDestroyNotify      serialize_destroy;
  
  BdbDeserializeFunc  deserialize_func;
  gpointer            deserialize_data;
  GDestroyNotify      deserialize_destroy;
};

static void
bdb_list_store_finalize (GObject *object)
{
  G_OBJECT_CLASS (bdb_list_store_parent_class)->finalize (object);
}

static void
bdb_list_store_class_init (BdbListStoreClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  g_type_class_add_private (klass, sizeof (BdbListStorePrivate));

  object_class->finalize = bdb_list_store_finalize;
}

/**
 * bdb_list_store_get_n_keys:
 * @list_store: A #BdbListStore.
 *
 * Retreives the number of keys within the current DB instance. If the
 * cached value is not stale, it will be returned saving a trip to
 * the database.
 *
 * Return value: an integer containing the number of keys.
 */
static gint
bdb_list_store_get_n_keys (BdbListStore *list_store)
{
  BdbListStorePrivate *priv;
  DB_BTREE_STAT       *stat;

  g_return_val_if_fail (BDB_IS_LIST_STORE (list_store), 0);
  
  priv = list_store->priv;
  
  if (!priv->dirty)
    return priv->n_keys;
  
  g_return_val_if_fail (priv->db != NULL, 0);
  
  if (priv->db->stat (priv->db, NULL, &stat, DB_FAST_STAT) == 0)
    {
      priv->n_keys = stat->bt_nkeys;
      priv->dirty = FALSE;
    }
  
  g_free (stat);
  
  return priv->n_keys;
}

/**
 * bdb_list_store_get_flags:
 * @tree_model: A #BdbListStore
 *
 * #BdbListStore does not support tree like heirarchys. Therefore, it
 * always returns #GTK_TREE_MODEL_LIST_ONLY.
 *
 * Return value: #GTK_TREE_MODEL_LIST_ONLY
 */
static GtkTreeModelFlags
bdb_list_store_get_flags (GtkTreeModel *tree_model)
{
  return GTK_TREE_MODEL_LIST_ONLY;
}

/**
 * bdb_list_store_get_iter:
 * @tree_model: A #BdbListStore
 * @iter: a #GtkTreeIter
 * @path: A #GtkTreePath
 * 
 * Creates a #GtkTreeIter that points to the row specified by @path.
 *
 * Return value: TRUE if the iter is valid.
 */
static gboolean
bdb_list_store_get_iter (GtkTreeModel *tree_model,
                         GtkTreeIter  *iter,
                         GtkTreePath  *path)
{
  BdbListStorePrivate *priv;
  gint                *indices;
  gint                 depth;
  gint                 n_keys;
  
  g_return_val_if_fail (BDB_IS_LIST_STORE (tree_model), FALSE);
  
  priv = BDB_LIST_STORE (tree_model)->priv;
  indices = gtk_tree_path_get_indices (path);
  depth = gtk_tree_path_get_depth (path);
  
  if (depth == 0)
    return FALSE;
  
  n_keys = bdb_list_store_get_n_keys (BDB_LIST_STORE (tree_model));
  if (n_keys <= indices[0])
    return FALSE;
  
  iter->stamp = priv->stamp;
  iter->user_data = GINT_TO_POINTER (indices[0] + 1);
  
  return TRUE;
}

/**
 * bdb_list_store_get_n_columns:
 * @tree_model: A #BdbListStore
 * 
 * Return value: the number of columns. This is always one as #BdbListStore
 *   only supports a single unserialized column from data storage.
 */
static gint
bdb_list_store_get_n_columns (GtkTreeModel *tree_model)
{
  return 1;
}

/**
 * bdb_list_store_get_column_type:
 * @tree_model: A #BdbListStore
 * @index: the column index
 *
 * #BdbListStore only supports 1 column, so index should ALWAYS be 0.
 * You can specify the #GType for the column with
 * bdb_list_store_set_g_type().
 *
 * Return value: The GType of the column
 */
static GType
bdb_list_store_get_column_type (GtkTreeModel *tree_model,
                                gint          index)
{
  BdbListStorePrivate *priv;
  
  g_return_val_if_fail (BDB_IS_LIST_STORE (tree_model), G_TYPE_INVALID);
  
  priv = BDB_LIST_STORE (tree_model)->priv;

  return priv->g_type;
}

/**
 * bdb_list_store_iter_next:
 * @tree_model: A #BdbListStore
 * @iter: A #GtkTreeIter
 *
 * Increments the iter to the next key in the #BdbListStore database.
 *
 * Return value: TRUE if the iter is still valid and the last key has not
 *   been reached.
 */
static gboolean
bdb_list_store_iter_next (GtkTreeModel *tree_model,
                          GtkTreeIter  *iter)
{
  BdbListStorePrivate *priv;
  gint                 n_keys;
  gint                 next;
  
  g_return_val_if_fail (BDB_IS_LIST_STORE (tree_model), FALSE);
  
  priv = BDB_LIST_STORE (tree_model)->priv;
  
  g_return_val_if_fail (iter->stamp == priv->stamp, FALSE);
  
  n_keys = bdb_list_store_get_n_keys (BDB_LIST_STORE (tree_model));
  next = GPOINTER_TO_INT (iter->user_data) + 1;
  
  if (n_keys < next)
    return FALSE;
  
  iter->user_data = GINT_TO_POINTER (next);
  
  return TRUE;
}

/**
 * bdb_list_store_iter_nth_child:
 * @tree_model: A #GtkTreeModel
 * @iter: A #GtkTreeIter
 * @parent: A #GtkTreeIter
 * @n: the child offset
 *
 * Retreives an iter for the @n<!-- -->th child row. Since #BdbListStore
 * does not support heirarchies, @parent MUST ALWAYS be #NULL.
 *
 * Return value: TRUE if @iter is valid.
 */
static gboolean
bdb_list_store_iter_nth_child (GtkTreeModel *tree_model,
                               GtkTreeIter  *iter,
                               GtkTreeIter  *parent,
                               gint          n)
{
  BdbListStorePrivate *priv;
  gint                 n_keys = 0;
  
  g_return_val_if_fail (BDB_IS_LIST_STORE (tree_model), FALSE);
  
  priv = BDB_LIST_STORE (tree_model)->priv;
  
  if (parent)
    return FALSE;
  
  n_keys = bdb_list_store_get_n_keys (BDB_LIST_STORE (tree_model));
  
  if (n_keys <= n)
    return FALSE;
  
  iter->stamp = priv->stamp;
  iter->user_data = GINT_TO_POINTER (n);
  
  return TRUE;
}

/**
 * bdb_list_store_serialize:
 * @list_store: A #BdbListStore
 * @value: A #GValue
 * @output: A location to store the serialized buffer
 * @error: A location to store a #GError
 *
 * Attempts to serialize the current value object using the serialization
 * method provided when creating the #BdbListStore. If there was an error
 * and @value could not be serialized, FALSE is returned and @error will
 * be set.
 *
 * Return value: TRUE if @value was serialized. FALSE if there was an
 *   error and @error is set.
 */
static gboolean
bdb_list_store_serialize (BdbListStore  *list_store,
                          GValue        *value,
                          gchar        **output,
                          GError       **error)
{
  BdbListStorePrivate *priv;
  
  g_return_val_if_fail (BDB_IS_LIST_STORE (list_store), FALSE);
  
  priv = list_store->priv;
  
  if (priv->serialize_func)
      return priv->serialize_func (list_store,
                                   value,
                                   output,
                                   priv->serialize_data,
                                   error);
  
  return FALSE;
}

/**
 * bdb_list_store_deserialize:
 * @list_store: A #BdbListStore
 * @value: A #GValue
 * @input: the buffer to be deserialized
 * @error: A location for a #GError
 *
 * Attempts to deserialize the data from @input using the deserialization
 * method provided when the #BdbListStore was created. If there was an
 * error, FALSE is returned and @error will be set.
 *
 * Return value: TRUE if the value was successfully deserialized. FALSE if
 *   there was an error and @error is set.
 */
static gboolean
bdb_list_store_deserialize (BdbListStore  *list_store,
                            GValue        *value,
                            const gchar   *input,
                            GError       **error)
{
  BdbListStorePrivate *priv;
  
  g_return_val_if_fail (BDB_IS_LIST_STORE (list_store), FALSE);
  
  priv = list_store->priv;
  
  if (priv->deserialize_func)
      return priv->deserialize_func (list_store,
                                     value,
                                     input,
                                     priv->deserialize_data,
                                     error);
  
  return FALSE;
}

/**
 * bdb_list_store_get_value:
 * @tree_model: A #BdbListStore
 * @iter: A #GtkTreeIter
 * @column: the column offset
 * @value: A #GValue to store the columns value
 *
 * Retreives the value from cache or raw storage.  In the case of raw
 * storage, the value is deserialized and cached for future use.
 */
static void
bdb_list_store_get_value (GtkTreeModel *tree_model,
                          GtkTreeIter  *iter,
                          gint          column,
                          GValue       *value)
{
  BdbListStorePrivate *priv;
  GValue *cache_value;
  
  g_return_if_fail (column == 0);
  g_return_if_fail (BDB_IS_LIST_STORE (tree_model));
  
  priv = BDB_LIST_STORE (tree_model)->priv;
  
  g_return_if_fail (priv->stamp == iter->stamp);
  
  /* get the gvalue pointer from cache */
  cache_value = g_lru_cache_get (priv->lru, &iter->user_data);
  
  /* dup the value type */
  g_value_init (value, G_VALUE_TYPE (cache_value));
  
  /* dup the gvalue to the caller value */
  g_value_copy (cache_value, value);
}

/**
 * bdb_list_store_get_value_real:
 * @tree_model: A #BdbListStore
 * @recno: the record offset
 * @value: A #GValue to store the deserialized value
 *
 * Retrieves a record from raw storage and deserializes it. This is
 * used to retreive a value that can later be cached.
 */
static void
bdb_list_store_get_value_real (GtkTreeModel *tree_model,
                               guint         recno,
                               GValue       *value)
{
  BdbListStorePrivate *priv;
  DBT                  key, data;
  db_recno_t           keydata;
  DB_TXN              *txn = NULL;
  gint                 flags = 0;
  gint                 ret;
  
  g_return_if_fail (BDB_IS_LIST_STORE (tree_model));
  
  priv = BDB_LIST_STORE (tree_model)->priv;
  
  CLEAR_DBT (key);
  CLEAR_DBT (data);
  
  keydata = recno;
  key.data = &keydata;
  key.size = sizeof (gint);
  key.ulen = key.size;
  key.flags = DB_DBT_USERMEM;
  
  data.flags = DB_DBT_MALLOC;
  
  if ((ret = priv->db->get (priv->db, txn, &key, &data, flags)) != 0)
    {
      g_warning ("bdb_list_store_get_value: %s", db_strerror (ret));
      return;
    }
  
  if (!bdb_list_store_deserialize (BDB_LIST_STORE (tree_model),
                                   value,
                                   data.data,
                                   NULL))
    g_warning ("Could not deserialize value");
  
  /* Store the items offset */
  if (G_VALUE_HOLDS_OBJECT (value) && g_value_peek_pointer (value))
    {
      g_object_set_data (g_value_get_object (value),
                         "bdb-list-store-key",
                         GINT_TO_POINTER (recno));
    }
  
  FREE_DBT (key);
  FREE_DBT (data);
}

/**
 * bdb_list_store_iter_children:
 * @tree_model: A #BdbListStore
 * @iter: A #GtkTreeIter
 * @parent: A #GtkTreeIter
 *
 * #BdbListStore does not support hierarchies, therefore it may never
 * have children. This will always return FALSE.
 *
 * Return value: FALSE
 */
static gboolean
bdb_list_store_iter_children (GtkTreeModel *tree_model,
                              GtkTreeIter  *iter,
                              GtkTreeIter  *parent)
{
  return FALSE;
}

/**
 * bdb_list_store_iter_has_child:
 * @tree_model: A #BdbListStore
 * @iter: A #GtkTreeIter
 *
 * #BdbListStore does not support hierarchies, therefore it may never
 * have children. This will always return FALSE.
 *
 * Return value: FALSE
 */
static gboolean
bdb_list_store_iter_has_child (GtkTreeModel *tree_model,
                               GtkTreeIter  *iter)
{
  return FALSE;
}

/**
 * bdb_list_store_iter_n_children:
 * @tree_model: A #BdbListStore
 * @iter: A #GtkTreeIter
 *
 * #BdbListStore does not support hierarchies, therefore it may never
 * have children. This will always return 0.
 *
 * Return value: 0.
 */
static int
bdb_list_store_iter_n_children (GtkTreeModel *tree_model,
                                GtkTreeIter  *iter)
{
  g_return_val_if_fail (BDB_IS_LIST_STORE (tree_model), 0);
  
  if (!iter)
    return bdb_list_store_get_n_keys (BDB_LIST_STORE (tree_model));
  
  return 0;
}

/**
 * bdb_list_store_iter_parent:
 * @tree_model: A #BdbListStore
 * @iter: A #GtkTreeIter
 * @parent: A #GtkTreeIter
 *
 * #BdbListStore does not support hierarchies, therefore it may never
 * have children. This will always return FALSE.
 *
 * Return value: FALSE
 */
static gboolean
bdb_list_store_iter_parent (GtkTreeModel *tree_model,
                            GtkTreeIter  *iter,
                            GtkTreeIter  *parent)
{
  return FALSE;
}

/**
 * bdb_list_store_get_path:
 * @tree_model: A #BdbListStore
 * @iter: A #GtkTreeIter
 *
 * Creates a new #GtkTreePath for the row found at @iter.  This #GtkTreePath
 * should be freed using gtk_tree_path_free().
 *
 * Return value: A new #GtkTreePath or NULL
 */
static GtkTreePath*
bdb_list_store_get_path (GtkTreeModel *tree_model, GtkTreeIter *iter)
{
  BdbListStorePrivate *priv;
  gint                 index;
  
  g_return_val_if_fail (BDB_IS_LIST_STORE (tree_model), NULL);
  
  priv = BDB_LIST_STORE (tree_model)->priv;
  
  g_return_val_if_fail (priv->stamp == iter->stamp, NULL);
  
  /* record offsets are 1 based */
  index = GPOINTER_TO_INT (iter->user_data);
  
  return gtk_tree_path_new_from_indices (index - 1, -1);
}

/**
 * bdb_list_store_lookup_cb:
 * @key: The cache key
 * @user_data: the callback data
 *
 * Calling of this method indicates a cache-miss. We provide the
 * mechanism to lookup the value from storage.
 *
 * Return value: A pointer to the newly created value. The #GValue should
 *   be freed with g_free().
 */
static gpointer
bdb_list_store_lookup_cb (gpointer key,
                          gpointer user_data)
{
  BdbListStorePrivate *priv;
  GValue *value;
  
  g_return_val_if_fail (BDB_IS_LIST_STORE (user_data), NULL);
  
  priv = BDB_LIST_STORE (user_data)->priv;
  
  /* create a new value on the heap */
  value = g_new0 (GValue, 1);
  
  /* get the real value, deserialized */
  bdb_list_store_get_value_real (GTK_TREE_MODEL (user_data),
                                 *(gint*)key,
                                 value);
  
  return value;
}

/**
 * bdb_list_store_value_destroy_cb:
 * @data: the data to be destroyed
 *
 * Calling this method means the object has been removed from cache. We will
 * unset the #GValue which will ideally cause any possible reference counted
 * type be freed as well. I say ideally as the program may be using the object
 * somewhere else, thus preventing the freeing from memory.
 */
static void
bdb_list_store_value_destroy_cb (gpointer data)
{
  GValue *value = data;
  
  g_value_unset (value);
  
  g_free (value);
}

/*
 * tree_model_init:
 * @iface: A #GtkTreeModelIface
 *
 * Initializes the #GtkTreeModelIface with all the callbacks necessary for
 * implementing #BdbListStore.
 */
static void
tree_model_init (GtkTreeModelIface *iface)
{
  iface->get_flags        = bdb_list_store_get_flags;
  iface->get_iter         = bdb_list_store_get_iter;
  iface->get_n_columns    = bdb_list_store_get_n_columns;
  iface->get_column_type  = bdb_list_store_get_column_type;
  iface->iter_next        = bdb_list_store_iter_next;
  iface->iter_nth_child   = bdb_list_store_iter_nth_child;
  iface->get_value        = bdb_list_store_get_value;
  iface->iter_children    = bdb_list_store_iter_children;
  iface->iter_has_child   = bdb_list_store_iter_has_child;
  iface->iter_n_children  = bdb_list_store_iter_n_children;
  iface->iter_parent      = bdb_list_store_iter_parent;
  iface->get_path         = bdb_list_store_get_path;
}

/*
 * bdb_list_store_key_copy_cb:
 * @key: the key
 * @user_data: the user supplied data
 * 
 * This method copies the key used by the cache. Since the db_recno_t key used
 * in bdb is integer based, we simply get new memory for the integer on the
 * heap.
 */
static gpointer
bdb_list_store_key_copy_cb (gconstpointer key,
                            gpointer      user_data)
{
  const gint *oldkey;
  gint       *newkey;
  
  oldkey = key;
  newkey = g_new0 (gint, 1);
  
  *newkey = *oldkey;
  
  return newkey;
}

static void
bdb_list_store_init (BdbListStore *list_store)
{
  list_store->priv = G_TYPE_INSTANCE_GET_PRIVATE (list_store,
                                                  BDB_TYPE_LIST_STORE,
                                                  BdbListStorePrivate);
  
  list_store->priv->g_type = G_TYPE_STRING;
  list_store->priv->stamp = g_random_int ();
  list_store->priv->n_keys = 0;
  list_store->priv->dirty = TRUE;
  
  /* use our primitive GLruCache to keep a minimal number of items
   * in memory and reduce the deserialization overhead.
   */
  
  list_store->priv->lru = g_lru_cache_new (g_int_hash,
                                           g_int_equal,
                                           bdb_list_store_key_copy_cb,
                                           bdb_list_store_lookup_cb,
                                           g_free,
                                           bdb_list_store_value_destroy_cb,
                                           g_object_ref (list_store),
                                           g_object_unref);
}

/**
 * bdb_list_store_new:
 *
 * Creates a #BdbListStore.
 *
 * Return value: A new #BdbListStore.
 */
BdbListStore*
bdb_list_store_new (void)
{
  return g_object_new (BDB_TYPE_LIST_STORE, NULL);
}

/**
 * bdb_list_store_get_db:
 * @list_store: A #BdbListStore
 *
 * Return value: the DB that is currently being used by the #BdbListStore.
 */
DB*
bdb_list_store_get_db (BdbListStore *list_store)
{
  BdbListStorePrivate *priv;
  
  g_return_val_if_fail (BDB_IS_LIST_STORE (list_store), NULL);
  
  priv = list_store->priv;
  
  return priv->db;
}

/**
 * bdb_list_store_set_db:
 * @list_store: A #BdbListStore
 * @db: A Berkeley DB pointer
 * @error: A location for a #GError
 *
 * Sets the DB for the #BdbListStore. This may only be called once
 * during the lifetime of a #BdbListStore.
 *
 * Return value: TRUE of the #BdbListStore successfully attached the DB.
 *   FALSE if there was an error and @error is set.
 */
gboolean
bdb_list_store_set_db (BdbListStore *list_store, DB *db, GError **error)
{
  BdbListStorePrivate *priv;
  guint                flags = 0;
  gint                 ret   = 0;
  
  g_return_val_if_fail (BDB_IS_LIST_STORE (list_store), FALSE);
  g_return_val_if_fail (db != NULL, FALSE);
  
  priv = list_store->priv;
  
  /* verify we do not already have an attached DB */
  if (priv->db != NULL)
    {
      g_set_error (error,
                   BDB_LIST_STORE_ERROR,
                   BDB_LIST_STORE_ERROR_INVALID,
                   "DB already set");
      return FALSE;
    }
  
  /* Make sure the DB is in record number format */
  if (db->type != DB_RECNO)
    {
      g_set_error (error,
                   BDB_LIST_STORE_ERROR,
                   BDB_LIST_STORE_ERROR_INVALID,
                   "DB must be of type DB_RECNO");
      return FALSE;
    }
  
  /* Retreive the flags for the DB to verify them */
  if ((ret = db->get_flags (db, &flags)) != 0)
    {
      g_set_error (error,
                   BDB_LIST_STORE_ERROR,
                   BDB_LIST_STORE_ERROR_INVALID,
                   "Cannot retrieve db flags");
      return FALSE;
    }
  
  /* Verify that RENUMBER flag is set on the database */
  if (!(flags & DB_RENUMBER))
    {
      g_set_error (error,
                   BDB_LIST_STORE_ERROR,
                   BDB_LIST_STORE_ERROR_INVALID,
                   "Please enable DB_RENUMBER on your database");
      return FALSE;
    }
  
  priv->db = db;
  
  return TRUE;
}

/**
 * bdb_list_store_append:
 * @list_store: A #BdbListStore
 * @iter: A #GtkTreeiter
 *
 * Appends an item to the #BdbListStore and updates the value of
 * @iter. The item is not cached until it has be retrieved from
 * the DB.
 */
void
bdb_list_store_append (BdbListStore *list_store,
                       GtkTreeIter  *iter)
{
  BdbListStorePrivate *priv;
  DBT                  key, data;
  DB_TXN              *txn = NULL;
  db_recno_t           recno;
  gint                 ret;
  GtkTreePath         *path;
  
  g_return_if_fail (BDB_IS_LIST_STORE (list_store));
  
  priv = list_store->priv;
  
  CLEAR_DBT (key);
  CLEAR_DBT (data);
  
  recno = 0;
  key.data = &recno;
  key.size = sizeof (db_recno_t);
  key.ulen = key.size;
  key.flags = DB_DBT_USERMEM;
  
  data.data = "";
  data.size = strlen ("") + 1;
  key.ulen = key.size;
  data.flags = DB_DBT_USERMEM;
  
  if ((ret = priv->db->put (priv->db, txn, &key, &data, DB_APPEND)) != 0)
    g_warning ("bdb_list_store_append: %s", db_strerror (ret));
  
  priv->dirty = TRUE;
  
  iter->stamp = priv->stamp;
  iter->user_data = GINT_TO_POINTER (bdb_list_store_get_n_keys (list_store));
  
  FREE_DBT (key);
  FREE_DBT (data);
  
  /* create the GtkTreePath to use in the signal */
  path = gtk_tree_model_get_path (GTK_TREE_MODEL (list_store),
                                  iter);
  
  /* signal a row has been added */
  gtk_tree_model_row_inserted (GTK_TREE_MODEL (list_store),
                               path,
                               iter);
  
  
  gtk_tree_path_free (path);
}

/**
 * bdb_list_store_set_value:
 * @list_store: A #BdbListStore
 * @iter: A #GtkTreeIter
 * @column: the column
 * @value: A #GValue containing the new value
 *
 * Updates a value in storage with a serialized version of @value.
 * Any cached version of @value will be evicted.
 */
void
bdb_list_store_set_value (BdbListStore *list_store,
                          GtkTreeIter  *iter,
                          gint          column,
                          GValue       *value)
{
  BdbListStorePrivate *priv;
  GtkTreePath         *path;
  gchar               *output = NULL;
  db_recno_t           recno;
  DB_TXN              *txn = NULL;
  DBT                  key;
  DBT                  data;
  gint                 ret = 0;
  
  g_return_if_fail (BDB_IS_LIST_STORE (list_store));
  
  priv = list_store->priv;
  
  g_return_if_fail (priv->stamp == iter->stamp);
  
  recno = GPOINTER_TO_INT (iter->user_data);
  
  if (!bdb_list_store_serialize (list_store, value, &output, NULL))
    {
      g_warning ("Could not serialize value");
      return;
    }
  
  CLEAR_DBT (key);
  CLEAR_DBT (value);
  
  key.data = &recno;
  key.size = sizeof (db_recno_t);
  key.flags = DB_DBT_USERMEM;
  
  data.data = (void*)output;
  data.size = strlen (output) + 1;
  data.flags = DB_DBT_USERMEM;
  
  if ((ret = priv->db->put (priv->db, txn, &key, &data, 0)) != 0)
    g_warning ("bdb_list_store_set_value: %s", db_strerror (ret));
  else
    priv->dirty = TRUE;
  
  g_free (output);
  
  FREE_DBT (key);
  FREE_DBT (data);
  
  /* evict any previous version */
  g_lru_cache_evict (priv->lru, &recno);
  
  /* create a GtkTreePath for use in the signal */
  path = gtk_tree_model_get_path (GTK_TREE_MODEL (list_store), iter);
  
  /* signal the row being changed */
  gtk_tree_model_row_changed (GTK_TREE_MODEL (list_store), path, iter);
  
  gtk_tree_path_free (path);
}

/**
 * bdb_list_store_remove:
 * @list_store: A #BdbListStore
 * @iter: A #GtkTreeIter
 *
 * Removes a row from the DB. This causes a shift of record numbers
 * in the data store and renumbering of all keys.
 * 
 * Return value: TRUE if the row was removed.
 */
gboolean
bdb_list_store_remove (BdbListStore *list_store, GtkTreeIter *iter)
{
  BdbListStorePrivate *priv;
  DBT                  key;
  DB_TXN              *txn = NULL;
  gint                 ret = 0;
  gint                 flags = 0;
  GtkTreePath         *path;
  
  g_return_val_if_fail (BDB_IS_LIST_STORE (list_store), FALSE);
  
  priv = list_store->priv;
  
  g_return_val_if_fail (priv->stamp == iter->stamp, FALSE);
  g_return_val_if_fail (priv->db != NULL, FALSE);
  
  CLEAR_DBT (key);
  
  db_recno_t recno = GPOINTER_TO_INT (iter->user_data);
  key.data = &recno;
  key.size = sizeof (db_recno_t);
  key.ulen = key.size;
  key.flags = DB_DBT_USERMEM;
  
  if ((ret = priv->db->del (priv->db, txn, &key, flags)) != 0)
    g_warning ("Could not remove ");
  
  priv->dirty = TRUE;
  path = bdb_list_store_get_path (GTK_TREE_MODEL (list_store), iter);
  
  gboolean is_valid = FALSE;
  
  if (bdb_list_store_get_n_keys (list_store) >= GPOINTER_TO_INT (iter->user_data))
    is_valid = TRUE;
  else
    {
      iter->stamp = 0;
      iter->user_data = NULL;
    }
  
  /* we really have two choices here. we can go through the cache removing
   * ever item that has a key >= to that of our recno or we can simply
   * clear it. lets just clear it for now.
   */
  
  g_lru_cache_clear (priv->lru);
  
  /* signal the row has been deleted */
  gtk_tree_model_row_deleted (GTK_TREE_MODEL (list_store), path);
  
  gtk_tree_path_free (path);
  
  FREE_DBT (key);
  
  return is_valid;
}

/**
 * bdb_list_store_set_serialization_func:
 * @list_store: A #BdbListStore
 * @func: A #BdbSerializeFunc
 * @user_data: user supplied data for callback
 * @destroy_func: a callback to free @user_data
 *
 * Sets the serialization method used by #BdbListStore to serialize
 * #GValue<!-- -->'s to the DB.
 */
void
bdb_list_store_set_serialize_func (BdbListStore     *list_store,
                                   BdbSerializeFunc  func,
                                   gpointer          user_data,
                                   GDestroyNotify    destroy_func)
{
  BdbListStorePrivate *priv;
  
  g_return_if_fail (BDB_IS_LIST_STORE (list_store));
  
  priv = list_store->priv;
  
  priv->serialize_func = func;
  priv->serialize_data = user_data;
  priv->serialize_destroy = destroy_func;
}

/**
 * bdb_list_store_set_deserialization_func:
 * @list_store: A #BdbListStore
 * @func: A #BdbDeserializeFunc
 * @user_data: user supplied data for callback
 * @destroy_func: a callback to free @user_data
 *
 * Sets the deserialization method used by #BdbListStore to deserialize
 * the DB retrieved data into #GValue<!-- -->'s.
 */
void
bdb_list_store_set_deserialize_func (BdbListStore       *list_store,
                                     BdbDeserializeFunc  func,
                                     gpointer            user_data,
                                     GDestroyNotify      destroy_func)
{
  BdbListStorePrivate *priv;
  
  g_return_if_fail (BDB_IS_LIST_STORE (list_store));
  
  priv = list_store->priv;
  
  priv->deserialize_func = func;
  priv->deserialize_data = user_data;
  priv->deserialize_destroy = destroy_func;
}

GQuark
bdb_list_store_error_quark (void)
{
  return g_quark_from_static_string ("bdb-list-store-error-quark");
}
