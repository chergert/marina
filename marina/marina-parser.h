/* marina-parser.h
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifndef __MARINA_PARSER_H__
#define __MARINA_PARSER_H__

#include <glib-object.h>
#include <gio/gio.h>

#include "marina-source.h"
#include "marina-parse-result.h"

G_BEGIN_DECLS

#define MARINA_TYPE_PARSER (marina_parser_get_type())

#define MARINA_PARSER(obj)               \
  (G_TYPE_CHECK_INSTANCE_CAST((obj),     \
  MARINA_TYPE_PARSER, MarinaParser))

#define MARINA_IS_PARSER(obj)            \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj),     \
  MARINA_TYPE_PARSER))

#define MARINA_PARSER_GET_INTERFACE(obj) \
  (G_TYPE_INSTANCE_GET_INTERFACE((obj),  \
  MARINA_TYPE_PARSER, MarinaParserIface))

typedef struct _MarinaParser      MarinaParser;
typedef struct _MarinaParserIface MarinaParserIface;

struct _MarinaParserIface
{
    GTypeInterface parent;
    
    gboolean           (*can_parse) (MarinaParser  *parser,
                                     MarinaSource  *source,
                                     GInputStream  *stream);
    
    MarinaParseResult* (*parse)     (MarinaParser  *parser,
                                     MarinaSource  *source,
                                     GInputStream  *stream,
                                     GError       **error);
};

GType              marina_parser_get_type  (void) G_GNUC_CONST;

gboolean           marina_parser_can_parse (MarinaParser  *parser,
                                            MarinaSource  *source,
                                            GInputStream  *stream);

MarinaParseResult* marina_parser_parse     (MarinaParser  *parser,
                                            MarinaSource  *source,
                                            GInputStream  *stream,
                                            GError       **error);

G_END_DECLS

#endif /* __MARINA_PARSER_H__ */
