/* marina-outlook-source-view.h
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifndef __MARINA_OUTLOOK_SOURCE_VIEW_H__
#define __MARINA_OUTLOOK_SOURCE_VIEW_H__

#include <gtk/gtk.h>

#include "marina-source-view.h"

G_BEGIN_DECLS

#define MARINA_TYPE_OUTLOOK_SOURCE_VIEW (marina_outlook_source_view_get_type ())

#define MARINA_OUTLOOK_SOURCE_VIEW(obj)             \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj),               \
  MARINA_TYPE_OUTLOOK_SOURCE_VIEW,                  \
  MarinaOutlookSourceView))

#define MARINA_OUTLOOK_SOURCE_VIEW_CONST(obj)       \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj),               \
  MARINA_TYPE_OUTLOOK_SOURCE_VIEW,                  \
  MarinaOutlookSourceView const))

#define MARINA_OUTLOOK_SOURCE_VIEW_CLASS(klass)     \
  (G_TYPE_CHECK_CLASS_CAST ((klass),                \
  MARINA_TYPE_OUTLOOK_SOURCE_VIEW,                  \
  MarinaOutlookSourceViewClass))

#define MARINA_IS_OUTLOOK_SOURCE_VIEW(obj)          \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj),               \
  MARINA_TYPE_OUTLOOK_SOURCE_VIEW))

#define MARINA_IS_OUTLOOK_SOURCE_VIEW_CLASS(klass)  \
  (G_TYPE_CHECK_CLASS_TYPE ((klass),                \
  MARINA_TYPE_OUTLOOK_SOURCE_VIEW))

#define MARINA_OUTLOOK_SOURCE_VIEW_GET_CLASS(obj)   \
  (G_TYPE_INSTANCE_GET_CLASS ((obj),                \
  MARINA_TYPE_OUTLOOK_SOURCE_VIEW,                  \
  MarinaOutlookSourceViewClass))

typedef struct _MarinaOutlookSourceView         MarinaOutlookSourceView;
typedef struct _MarinaOutlookSourceViewClass    MarinaOutlookSourceViewClass;
typedef struct _MarinaOutlookSourceViewPrivate  MarinaOutlookSourceViewPrivate;

struct _MarinaOutlookSourceView
{
  GtkHPaned parent;
  
  MarinaOutlookSourceViewPrivate *priv;
};

struct _MarinaOutlookSourceViewClass
{
  GtkHPanedClass parent_class;
};

GType      marina_outlook_source_view_get_type (void) G_GNUC_CONST;
GtkWidget* marina_outlook_source_view_new      (void);

G_END_DECLS

#endif /* __MARINA_OUTLOOK_SOURCE_VIEW_H__ */
