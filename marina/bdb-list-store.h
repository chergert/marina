/* bdb-list-store.h
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with This; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 *
 * Should you wish to use BdbListStore under another license
 * please contact me.
 */

#ifndef __BDB_LIST_STORE_H__
#define __BDB_LIST_STORE_H__

#include <glib-object.h>
#include <gtk/gtktreemodel.h>
#include <db.h>

G_BEGIN_DECLS

#define BDB_TYPE_LIST_STORE bdb_list_store_get_type()

#define BDB_LIST_STORE(obj)               \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj),     \
  BDB_TYPE_LIST_STORE,                    \
  BdbListStore))

#define BDB_LIST_STORE_CLASS(klass)       \
  (G_TYPE_CHECK_CLASS_CAST ((klass),      \
  BDB_TYPE_LIST_STORE,                    \
  BdbListStoreClass))

#define BDB_IS_LIST_STORE(obj)            \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj),     \
  BDB_TYPE_LIST_STORE))

#define BDB_IS_LIST_STORE_CLASS(klass)    \
  (G_TYPE_CHECK_CLASS_TYPE ((klass),      \
  BDB_TYPE_LIST_STORE))

#define BDB_LIST_STORE_GET_CLASS(obj)     \
  (G_TYPE_INSTANCE_GET_CLASS ((obj),      \
  BDB_TYPE_LIST_STORE,                    \
  BdbListStoreClass))

/**
 * BDB_LIST_STORE_ERROR:
 *
 * #BdbListStore #GError domain.
 */
#define BDB_LIST_STORE_ERROR (bdb_list_store_error_quark ())

/**
 * BdbListStoreError:
 * @BDB_LIST_STORE_ERROR_INVALID: Invalid state for command
 *
 * #BdbListStore error enumeration.
 */
typedef enum
{
  BDB_LIST_STORE_ERROR_INVALID,
} BdbListStoreError;

GQuark bdb_list_store_error_quark (void);

typedef struct _BdbListStore        BdbListStore;
typedef struct _BdbListStoreClass   BdbListStoreClass;
typedef struct _BdbListStorePrivate BdbListStorePrivate;

/**
 * BdbSerializeFunc:
 * @list_store: A #BdbListStore
 * @value: the value to serialize
 * @output: A location to store the serialized buffer
 * @user_data: user supplied data for callback
 * @error: A location for a #GError
 *
 * A callback to serialize @value into a buffer. The buffer should
 * be assigned to @output and should be freeable with g_free().
 *
 * Return value: TRUE if @value was serialized and @output set.
 */
typedef gboolean (*BdbSerializeFunc)   (BdbListStore  *list_store,
                                        GValue        *value,
                                        gchar        **output,
                                        gpointer       user_data,
                                        GError       **error);


/**
 * BdbDeserializeFunc:
 * @list_store: A #BdbListStore
 * @value: the value to store the deserialized value
 * @input: A buffer to deserialize
 * @user_data: user supplied data for callback
 * @error: A location for a #GError
 *
 * A callback to deserialize @input into @value.
 *
 * Return value: TRUE if @input was deserialized.
 */
typedef gboolean (*BdbDeserializeFunc) (BdbListStore  *list_store,
                                        GValue        *value,
                                        const gchar   *input,
                                        gpointer       user_data,
                                        GError       **error);

struct _BdbListStore
{
  GObject parent;
  
  BdbListStorePrivate *priv;
};

struct _BdbListStoreClass
{
  GObjectClass parent_class;
};

GType         bdb_list_store_get_type             (void);
BdbListStore* bdb_list_store_new                  (void);

void          bdb_list_store_append               (BdbListStore       *list_store,
                                                   GtkTreeIter        *iter);
gboolean      bdb_list_store_remove               (BdbListStore       *list_store,
                                                   GtkTreeIter        *iter);
void          bdb_list_store_set_value            (BdbListStore       *list_store,
                                                   GtkTreeIter        *iter,
                                                   gint                column,
                                                   GValue             *value);

gboolean      bdb_list_store_set_db               (BdbListStore       *list_store,
                                                   DB                 *db,
                                                   GError            **error);
DB*           bdb_list_store_get_db               (BdbListStore       *list_store);

void          bdb_list_store_set_serialize_func   (BdbListStore       *list_store,
                                                   BdbSerializeFunc    func,
                                                   gpointer            user_data,
                                                   GDestroyNotify      destroy_func);

void          bdb_list_store_set_deserialize_func (BdbListStore       *list_store,
                                                   BdbDeserializeFunc  func,
                                                   gpointer            user_data,
                                                   GDestroyNotify      destroy_func);

G_END_DECLS

#endif /* __BDB_LIST_STORE_H__ */
