/* marina-vfs-source.h
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifndef __MARINA_VFS_SOURCE_H__
#define __MARINA_VFS_SOURCE_H__

#include <glib-object.h>

#include "marina-source-base.h"

G_BEGIN_DECLS

#define MARINA_TYPE_VFS_SOURCE (marina_vfs_source_get_type ())

#define MARINA_VFS_SOURCE(obj)            \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj),     \
  MARINA_TYPE_VFS_SOURCE, MarinaVfsSource))

#define MARINA_VFS_SOURCE_CONST(obj)      \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj),     \
  MARINA_TYPE_VFS_SOURCE, MarinaVfsSource const))

#define MARINA_VFS_SOURCE_CLASS(klass)    \
  (G_TYPE_CHECK_CLASS_CAST ((klass),      \
  MARINA_TYPE_VFS_SOURCE,                 \
  MarinaVfsSourceClass))

#define MARINA_IS_VFS_SOURCE(obj)         \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj),     \
  MARINA_TYPE_VFS_SOURCE))

#define MARINA_IS_VFS_SOURCE_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass),      \
  MARINA_TYPE_VFS_SOURCE))

#define MARINA_VFS_SOURCE_GET_CLASS(obj)  \
  (G_TYPE_INSTANCE_GET_CLASS ((obj),      \
  MARINA_TYPE_VFS_SOURCE,                 \
  MarinaVfsSourceClass))

typedef struct _MarinaVfsSource         MarinaVfsSource;
typedef struct _MarinaVfsSourceClass    MarinaVfsSourceClass;
typedef struct _MarinaVfsSourcePrivate  MarinaVfsSourcePrivate;

struct _MarinaVfsSource
{
  MarinaSourceBase parent;
  
  MarinaVfsSourcePrivate *priv;
};

struct _MarinaVfsSourceClass
{
  MarinaSourceBaseClass parent_class;
};

GType            marina_vfs_source_get_type (void) G_GNUC_CONST;
MarinaVfsSource* marina_vfs_source_new      (void);

const gchar*     marina_vfs_source_get_uri  (MarinaVfsSource *vfs_source);
void             marina_vfs_source_set_uri  (MarinaVfsSource *vfs_source,
                                             const gchar     *uri);

G_END_DECLS

#endif /* __MARINA_VFS_SOURCE_H__ */
