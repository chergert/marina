/* xml-writer.c: Cursor based XML writer API
 *
 * Copyright (C) 2009  Christian Hergert  <chris@dronelabs.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * Author:
 *   Christian Hergert  <chris@dronelabs.com>
 */

#include <glib.h>

#include <libxml/xmlwriter.h>

#include "xml-writer.h"

#define CHAR_TO_XML(s)  ((unsigned char *) (s))

G_DEFINE_TYPE (XmlWriter, xml_writer, G_TYPE_OBJECT);

struct _XmlWriterPrivate
{
  xmlTextWriterPtr writer;
};

static void
xml_writer_finalize (GObject *object)
{
  xml_writer_clear (XML_WRITER (object));

  G_OBJECT_CLASS (xml_writer_parent_class)->finalize (object);
}

static void
xml_writer_class_init (XmlWriterClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = xml_writer_finalize;

  g_type_class_add_private (object_class, sizeof (XmlWriterPrivate));
}

static void
xml_writer_init (XmlWriter *writer)
{
  writer->priv = G_TYPE_INSTANCE_GET_PRIVATE (writer, XML_TYPE_WRITER, XmlWriterPrivate);
}

/*
 * Public API
 */

/**
 * xml_writer_new:
 *
 * Creates a new #XmlWriter instance. You can use the #XmlWriter
 * instance to write an XML data stream to a file or a memory
 * buffer. (TODO, add memory support).
 *
 * Return value: the newly created #XmlWriter instance. Use
 *   g_object_unref() when done using it.
 */
XmlWriter*
xml_writer_new (void)
{
  return g_object_new (XML_TYPE_WRITER, NULL);
}

/**
 * xml_writer_new_for_file:
 * @filename: a valid local filename
 *
 * Exactly like xml_writer_new() except it sets the initial state
 * to reading from the desired @filename.
 *
 * Return value: the newly created #XmlWriter instance or NULL if
 *   there was an error. Use g_object_unref() when done using it.
 */
XmlWriter*
xml_writer_new_for_file (const gchar *filename)
{
  XmlWriter *writer;
  
  writer = xml_writer_new ();
  
  if (!writer)
    return NULL;
  
  if (!xml_writer_set_file (writer, filename, NULL))
    {
      g_object_unref (writer);
      return NULL;
    }
  
  return writer;
}

/**
 * xml_writer_clear:
 * @writer: A #XmlWriter
 *
 * Clears the current state of the #XmlWriter. This allows you
 * to reuse the same writer for multiple files or buffers.
 */
void
xml_writer_clear (XmlWriter *writer)
{
  XmlWriterPrivate *priv;
  
  g_return_if_fail (XML_IS_WRITER (writer));
  
  priv = writer->priv;

  if (priv->writer)
    xmlFreeTextWriter (priv->writer);
    
  priv->writer = NULL;
}

/**
 * xml_writer_set_file:
 * @writer: A #XmlWriter
 * @file: A valid local filename
 * @error: A location for a GError*
 *
 * Sets the current state of the writer to use @file.  The current state
 * must be new or cleared with xml_writer_clear().
 *
 * Return value: TRUE if the operation was successful. @error will be
 *   set in the case of an error.
 */
gboolean
xml_writer_set_file (XmlWriter    *writer,
                     const gchar  *file,
                     GError      **error)
{
  XmlWriterPrivate *priv;
  
  g_return_val_if_fail (XML_IS_WRITER (writer), FALSE);
  
  priv = writer->priv;
  
  if (writer->priv->writer)
    {
      g_set_error (error, XML_WRITER_ERROR,
                   XML_WRITER_ERROR_INVALID,
                   "Writer is not clear");
      
      return FALSE;
    }
  
  writer->priv->writer = xmlNewTextWriterFilename (file, 0);
  
  /* TODO: Check the result for a proper error message */
  
  return (writer->priv->writer != NULL);
}

/**
 * xml_writer_flush:
 * @writer: A #XmlWriter
 *
 * Flushes the current state to the storage medium.
 */
void
xml_writer_flush (XmlWriter *writer)
{
  XmlWriterPrivate *priv;
  
  g_return_if_fail (XML_IS_WRITER (writer));
  
  priv = writer->priv;
  
  if (priv->writer)
    xmlTextWriterFlush (priv->writer);
}

/**
 * xml_writer_write_start_document:
 * @writer: A #XmlWriter
 * @version: the xml version ("1.0") or NULL for default ("1.0")
 * @encoding: the encoding or NULL for default
 * @standalone: yes" or "no" or NULL for default
 *
 * Starts a new xml document. You must be at the begging of a
 * stream for this to be successful.
 */
void
xml_writer_write_start_document (XmlWriter   *writer,
                                 const gchar *version,
                                 const gchar *encoding,
                                 const gchar *standalone)
{
  XmlWriterPrivate *priv;
  
  g_return_if_fail (XML_IS_WRITER (writer));
  
  priv = writer->priv;
  
  if (priv->writer)
    xmlTextWriterStartDocument (priv->writer, version, encoding, standalone);
}

/**
 * xml_writer_write_end_document:
 * @writer: A #XmlWriter
 *
 * End an xml document. All open elements are closed, and
 * the content is flushed to the output.
 *
 * Return value: the number of bytes written or -1 in case of error
 */
gsize
xml_writer_write_end_document (XmlWriter *writer)
{
  XmlWriterPrivate *priv;
  
  g_return_val_if_fail (XML_IS_WRITER (writer), -1);
  
  priv = writer->priv;
  
  if (priv->writer)
    return xmlTextWriterEndDocument (priv->writer);
  
  return -1;
}

/**
 * xml_writer_write_start_element:
 * @writer: A #XmlWriter
 * @name: element name
 *
 * Start an xml element.
 */
void
xml_writer_write_start_element (XmlWriter   *writer,
                                const gchar *name)
{
  XmlWriterPrivate *priv;
  
  g_return_if_fail (XML_IS_WRITER (writer));
  
  priv = writer->priv;
  
  if (priv->writer)
    xmlTextWriterStartElement (priv->writer, CHAR_TO_XML (name));
}

/**
 * xml_writer_write_start_element_with_ns:
 * @writer: A #XmlWriter
 * @name: element name
 * @prefix: namespace prefix or NULL
 * @ns: namespace uri or NULL
 *
 * Start an xml element with namespace support.
 */
void
xml_writer_write_start_element_with_ns (XmlWriter   *writer, 
                                        const gchar *name,
                                        const gchar *prefix,
                                        const gchar *ns)
{
  XmlWriterPrivate *priv;
  
  g_return_if_fail (XML_IS_WRITER (writer));
  
  priv = writer->priv;
  
  if (priv->writer)
    xmlTextWriterStartElementNS (priv->writer,
                                 CHAR_TO_XML (prefix),
                                 CHAR_TO_XML (name),
                                 CHAR_TO_XML (ns));
}

/**
 * xml_writer_write_end_element:
 * @writer: A #XmlWriter
 *
 * End the current xml element.
 *
 * Return value: the number of bytes written or -1 in case of error.
 */
gsize
xml_writer_write_end_element (XmlWriter *writer)
{
  XmlWriterPrivate *priv;
  
  g_return_val_if_fail (XML_IS_WRITER (writer), -1);
  
  priv = writer->priv;
  
  if (priv->writer)
    return xmlTextWriterEndElement (priv->writer);
  
  return -1;
}

/**
 * xml_writer_write_element_string:
 * @writer: A #XmlWriter
 * @name: element name
 * @value: element inner xml
 *
 * Writes an entire element including start, content, and end element.
 *
 * This is implemented by calling:
 *   xml_writer_write_start_element()
 *   xml_writer_write_string()
 *   xml_writer_write_end_element()
 */
void
xml_writer_write_element_string (XmlWriter *writer,
                                 const gchar *name,
                                 const gchar *value)
{
  g_return_if_fail (XML_IS_WRITER (writer));
  
  xml_writer_write_start_element (writer, name);
  xml_writer_write_string (writer, value);
  xml_writer_write_end_element (writer);
}

/**
 * xml_writer_write_start_attribute:
 * @writer: A #XmlWriter
 * @name: attribute name
 *
 * Starts an xml attribute on the current element
 */
void
xml_writer_write_start_attribute (XmlWriter   *writer,
                                  const gchar *name)
{
  XmlWriterPrivate *priv;
  
  g_return_if_fail (XML_IS_WRITER (writer));
  
  priv = writer->priv;
  
  if (priv->writer)
    xmlTextWriterStartAttribute (priv->writer, CHAR_TO_XML (name));
}

/**
 * xml_writer_write_end_attribute:
 * @writer: A #XmlWriter
 *
 * Ends the current xml attribute.
 *
 * Return value: the number of bytes written (may be 0 from buffering) or -1.
 */
gsize
xml_writer_write_end_attribute (XmlWriter *writer)
{
  XmlWriterPrivate *priv;
  
  g_return_val_if_fail (XML_IS_WRITER (writer), -1);
  
  priv = writer->priv;
  
  if (priv->writer)
    return xmlTextWriterEndAttribute (priv->writer);
  
  return -1;
}

/**
 * xml_writer_write_attribute_string:
 * @writer: A #XmlWriter
 * @name: attribute name
 * @value: attribute value
 *
 * Writes a new attribute for the current element.
 *
 * This is implemented by calling:
 *   xml_writer_write_start_attribute()
 *   xml_writer_write_string()
 *   xml_writer_write_end_attribute()
 */
void
xml_writer_write_attribute_string (XmlWriter   *writer,
                                   const gchar *name,
                                   const gchar *value)
{
  xml_writer_write_start_attribute (writer, name);
  xml_writer_write_string (writer, value);
  xml_writer_write_end_attribute (writer);
}

/**
 * xml_writer_write_start_comment:
 * @writer: A #XmlWriter
 *
 * Starts an xml comment.
 */
void
xml_writer_write_start_comment (XmlWriter *writer)
{
  XmlWriterPrivate *priv;
  
  g_return_if_fail (XML_IS_WRITER (writer));
  
  priv = writer->priv;
  
  if (priv->writer)
    xmlTextWriterStartComment (priv->writer);
}

/**
 * xml_writer_write_end_comment:
 * @writer: A #XmlWriter
 *
 * Ends an xml comment.
 *
 * Return value: the number of bytes written (can be 0 from buffering) or -1.
 */
gsize
xml_writer_write_end_comment (XmlWriter *writer)
{
  XmlWriterPrivate *priv;
  
  g_return_val_if_fail (XML_IS_WRITER (writer), -1);
  
  priv = writer->priv;
  
  if (priv->writer)
    return xmlTextWriterEndComment (priv->writer);
  
  return -1;
}

/**
 * xml_writer_write_comment_string:
 * @writer: A #XmlWriter
 * @value: comment string
 *
 * Writes an xml comment.
 *
 * This is implemented with the following methods:
 *   xml_writer_write_start_comment()
 *   xml_writer_write_string()
 *   xml_writer_write_end_comment()
 */
void
xml_writer_write_comment_string (XmlWriter   *writer,
                                 const gchar *value)
{
  xml_writer_write_start_comment (writer);
  xml_writer_write_string (writer, value);
  xml_writer_write_end_comment (writer);
}

/**
 * xml_writer_write_attributes:
 * @writer: A #XmlWriter
 * @reader: A #XmlReader
 *
 * Writes the attributes found at the current position of @reader.
 */
void
xml_writer_write_attributes (XmlWriter *writer,
                             XmlReader *reader)
{
  XmlWriterPrivate *priv;
  const gchar *name;
  const gchar *value;
  
  g_return_if_fail (XML_IS_WRITER (writer));
  g_return_if_fail (XML_IS_READER (reader));
  
  priv = writer->priv;
  
  if (xml_reader_move_to_first_attribute (reader))
    {
      do
        {
          name = xml_reader_get_name (reader);
          value = xml_reader_get_value (reader);
          xml_writer_write_attribute_string (writer, name, value);
        }
      while (xml_reader_move_to_next_attribute (reader));
    }
}

/**
 * xml_writer_write_string:
 * @writer: A #XmlWriter
 * @value: a string
 *
 * Writes @string to the output
 */
void
xml_writer_write_string (XmlWriter   *writer,
                         const gchar *value)
{
  XmlWriterPrivate *priv;
  
  g_return_if_fail (XML_IS_WRITER (writer));
  
  priv = writer->priv;
  
  if (priv->writer)
    xmlTextWriterWriteString (priv->writer, CHAR_TO_XML (value));
}

/**
 * xml_writer_write_boolean:
 * @writer: A #XmlWriter
 * @value: a gboolean
 *
 * Writes @value as a string to the output.
 */
void
xml_writer_write_boolean (XmlWriter *writer,
                          gboolean   value)
{
  xml_writer_write_string (writer, value ? "true" : "false");
}

/**
 * xml_writer_write_int:
 * @writer: A #XmlWriter
 * @value: a gint
 *
 * Writes @value as a string to the output.
 */
void
xml_writer_write_int (XmlWriter *writer,
                      gint       value)
{
  gchar *str = g_strdup_printf ("%i", value);
  xml_writer_write_string (writer, str);
  g_free (str);
}

/**
 * xml_writer_write_double:
 * @writer: A #XmlWriter
 * @value: a gdouble
 *
 * Writes @value as a string to the output.
 */
void
xml_writer_write_double (XmlWriter *writer,
                         gdouble    value)
{
  gchar *str = g_strdup_printf ("%g", value);
  xml_writer_write_string (writer, str);
  g_free (str);
}

/**
 * xml_writer_write_float:
 * @writer: A #XmlWriter
 * @value: a gfloat
 *
 * Writes @value as a string to the output.
 */
void
xml_writer_write_float (XmlWriter *writer,
                        float      value)
{
  gchar *str = g_strdup_printf ("%g", value);
  xml_writer_write_string (writer, str);
  g_free (str);
}

/**
 * xml_writer_write_timeval:
 * @writer: A #XmlWriter
 * @value: a #GTimeVal
 *
 * Writes @value as an iso8601 formatted string.
 */
void
xml_writer_write_timeval (XmlWriter *writer,
                          GTimeVal  *timeval)
{
  gchar *str = g_time_val_to_iso8601 (timeval);
  xml_writer_write_string (writer, str);
  g_free (str);
}

/**
 * xml_writer_write_long:
 * @writer: A #XmlWriter
 * @value: A #glong
 *
 * Writes @value as a string.
 */
void
xml_writer_write_long (XmlWriter *writer,
                       glong      value)
{
  gchar *str = g_strdup_printf ("%li", value);
  xml_writer_write_string (writer, str);
  g_free (str);
}

/**
 * xml_writer_write_ulong:
 * @writer: A #XmlWriter
 * @value: A #gulong
 *
 * Writes @value as a string.
 */
void
xml_writer_write_ulong (XmlWriter *writer,
                        gulong     value)
{
  gchar *str = g_strdup_printf ("%lu", value);
  xml_writer_write_string (writer, str);
  g_free (str);
}

/**
 * xml_writer_write_int64:
 * @writer: A #XmlWriter
 * @value: A #gint64
 *
 * Writes @value as a string.
 */
void
xml_writer_write_int64 (XmlWriter *writer,
                        gint64     value)
{
  gchar *str = g_strdup_printf ("%" G_GINT64_FORMAT, value);
  xml_writer_write_string (writer, str);
  g_free (str);
}

/**
 * xml_writer_write_uint64:
 * @writer: A #XmlWriter
 * @value: A #guint64
 *
 * Writes @value as a string.
 */
void
xml_writer_write_uint64 (XmlWriter *writer,
                         guint64    value)
{
  gchar *str = g_strdup_printf ("%" G_GUINT64_FORMAT, value);
  xml_writer_write_string (writer, str);
  g_free (str);
}

GQuark
xml_writer_error_quark (void)
{
  return g_quark_from_static_string ("xml-writer-error-quark");
}
