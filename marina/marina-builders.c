/* marina-builders.c
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#include "marina-builders.h"
#include "marina-debug.h"
#include "marina-default-builder.h"

struct _MarinaBuildersPrivate
{
  GList *builders;
};

G_DEFINE_TYPE (MarinaBuilders, marina_builders, G_TYPE_OBJECT);

static void
marina_builders_finalize (GObject *object)
{
  G_OBJECT_CLASS (marina_builders_parent_class)->finalize (object);
}

static void
marina_builders_class_init (MarinaBuildersClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  
  object_class->finalize = marina_builders_finalize;

  g_type_class_add_private (object_class, sizeof (MarinaBuildersPrivate));
}

static void
marina_builders_init (MarinaBuilders *builders)
{
  builders->priv = G_TYPE_INSTANCE_GET_PRIVATE (builders,
                                               MARINA_TYPE_BUILDERS,
                                               MarinaBuildersPrivate);

  /* register our core builders */
  marina_builders_register (builders,
                            MARINA_BUILDER (marina_default_builder_new ()));
}

/**
 * marina_builders_get_default:
 *
 * Retrieves the singleton instance of #MarinaBuilders.
 *
 * Return value: The instance of #MarinaBuilders. Reference counted with
 *   g_object_ref() if you plan to keep a reference.
 */
MarinaBuilders*
marina_builders_get_default (void)
{
  static MarinaBuilders *instance = NULL;
  static GStaticMutex mutex  = G_STATIC_MUTEX_INIT;

  if (!instance)
    {
      g_static_mutex_lock (&mutex);
      
      if (!instance)
        {
          marina_debug_message (DEBUG_BUILDERS, "Creating builders singleton");
          instance = g_object_new (MARINA_TYPE_BUILDERS, NULL);
        }
      
      g_static_mutex_unlock (&mutex);
    }

  return instance;
}

/**
 * marina_builders_get_list:
 * @builders: A #MarinaBuilders
 *
 * Returns the list of registered builders within Marina. The list should
 * be freed with g_list_free().
 *
 * Return value: the list of #MarinaBuilder instances
 */
GList*
marina_builders_get_list (MarinaBuilders *builders)
{
  MarinaBuildersPrivate *priv;
  
  g_return_val_if_fail (MARINA_IS_BUILDERS (builders), NULL);
  
  priv = builders->priv;
  
  return g_list_copy (priv->builders);
}

/**
 * marina_builders_register:
 * @builders: A #MarinaBuilders
 * @builder: A #MarinaBuilder
 *
 * Registers a #MarinaBuilder to be used in the item building process.
 */
void
marina_builders_register (MarinaBuilders *builders,
                          MarinaBuilder  *builder)
{
  MarinaBuildersPrivate *priv;
  
  g_return_if_fail (MARINA_IS_BUILDERS (builders));
  g_return_if_fail (MARINA_IS_BUILDER (builder));
  
  priv = builders->priv;
  
  if (g_list_find (priv->builders, builder))
    return;
  
  priv->builders = g_list_prepend (priv->builders, g_object_ref (builder));
}

/**
 * marina_builders_unregister:
 * @builders: A #MarinaBuilders
 * @builder: A registered #MarinaBuilder
 *
 * Unregisters a previously registered builder from the list of active
 * builders.
 */
void
marina_builders_unregister (MarinaBuilders *builders,
                           MarinaBuilder *builder)
{
  MarinaBuildersPrivate *priv;
  
  g_return_if_fail (MARINA_IS_BUILDERS (builders));
  g_return_if_fail (MARINA_IS_BUILDER (builder));
  
  priv = builders->priv;

  if (g_list_find (priv->builders, builder))
    {
      priv->builders = g_list_remove (priv->builders, builder);
      g_object_unref (builder);
    }
}

