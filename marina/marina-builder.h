/* marina-builder.h
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifndef __MARINA_BUILDER_H__
#define __MARINA_BUILDER_H__

#include <glib-object.h>

#include "marina-parse-result.h"
#include "marina-source.h"
#include "marina-item.h"

G_BEGIN_DECLS

#define MARINA_TYPE_BUILDER (marina_builder_get_type())

#define MARINA_BUILDER(obj)                \
  (G_TYPE_CHECK_INSTANCE_CAST((obj),       \
  MARINA_TYPE_BUILDER, MarinaBuilder))

#define MARINA_IS_BUILDER(obj)             \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj),       \
  MARINA_TYPE_BUILDER))

#define MARINA_BUILDER_GET_INTERFACE(obj)  \
  (G_TYPE_INSTANCE_GET_INTERFACE((obj),    \
  MARINA_TYPE_BUILDER, MarinaBuilderIface))

typedef struct _MarinaBuilder       MarinaBuilder;
typedef struct _MarinaBuilderIface  MarinaBuilderIface;

struct _MarinaBuilderIface
{
  GTypeInterface parent;
  
  void (*build) (MarinaBuilder     *builder,
                 MarinaParseResult *parse_result,
                 MarinaSource      *source,
                 MarinaItem        *item);
};

GType marina_builder_get_type (void) G_GNUC_CONST;

void  marina_builder_build    (MarinaBuilder     *builder,
                               MarinaParseResult *parse_result,
                               MarinaSource      *source,
                               MarinaItem        *item);

G_END_DECLS

#endif /* __MARINA_BUILDER_H__ */
