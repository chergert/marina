/* marina-soup-source.h
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with This; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifndef __MARINA_SOUP_SOURCE_H__
#define __MARINA_SOUP_SOURCE_H__

#include <glib-object.h>

#include "marina-source.h"
#include "marina-source-base.h"

G_BEGIN_DECLS

#define MARINA_TYPE_SOUP_SOURCE (marina_soup_source_get_type ())

#define MARINA_SOUP_SOURCE(obj)              \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj),        \
  MARINA_TYPE_SOUP_SOURCE,                   \
  MarinaSoupSource))

#define MARINA_SOUP_SOURCE_CONST(obj)        \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj),        \
  MARINA_TYPE_SOUP_SOURCE,                   \
  MarinaSoupSource const))

#define MARINA_SOUP_SOURCE_CLASS(klass)      \
  (G_TYPE_CHECK_CLASS_CAST ((klass),         \
  MARINA_TYPE_SOUP_SOURCE,                   \
  MarinaSoupSourceClass))

#define MARINA_IS_SOUP_SOURCE(obj)          \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj),       \
  MARINA_TYPE_SOUP_SOURCE))

#define MARINA_IS_SOUP_SOURCE_CLASS(klass)  \
  (G_TYPE_CHECK_CLASS_TYPE ((klass),        \
  MARINA_TYPE_SOUP_SOURCE))

#define MARINA_SOUP_SOURCE_GET_CLASS(obj)   \
  (G_TYPE_INSTANCE_GET_CLASS ((obj),        \
  MARINA_TYPE_SOUP_SOURCE,                  \
  MarinaSoupSourceClass))

typedef struct _MarinaSoupSource         MarinaSoupSource;
typedef struct _MarinaSoupSourceClass    MarinaSoupSourceClass;
typedef struct _MarinaSoupSourcePrivate  MarinaSoupSourcePrivate;

struct _MarinaSoupSource
{
  MarinaSourceBase parent;
  
  MarinaSoupSourcePrivate *priv;
};

struct _MarinaSoupSourceClass
{
  MarinaSourceBaseClass parent_class;
};

GType             marina_soup_source_get_type (void) G_GNUC_CONST;
MarinaSource*     marina_soup_source_new      (void);

const gchar*      marina_soup_source_get_uri            (MarinaSoupSource *soup_source);
void              marina_soup_source_set_uri            (MarinaSoupSource *soup_source,
                                                         const gchar      *uri);

gboolean          marina_soup_source_get_override_proxy (MarinaSoupSource *soup_source);
void              marina_soup_source_set_override_proxy (MarinaSoupSource *soup_source,
                                                         gboolean          override_proxy);

const gchar*      marina_soup_source_get_proxy_uri      (MarinaSoupSource *soup_source);
void              marina_soup_source_set_proxy_uri      (MarinaSoupSource *soup_source,
                                                         const gchar      *proxy_uri);

G_END_DECLS

#endif /* __MARINA_SOUP_SOURCE_H__ */
