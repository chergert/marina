/* marina-progress.h
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with This; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifndef __MARINA_PROGRESS_H__
#define __MARINA_PROGRESS_H__

#include <glib-object.h>

#include <gtk/gtk.h>
#include <iris/iris.h>

G_BEGIN_DECLS

#define MARINA_TYPE_PROGRESS (marina_progress_get_type ())

#define MARINA_PROGRESS(obj)            \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj),   \
  MARINA_TYPE_PROGRESS, MarinaProgress))

#define MARINA_PROGRESS_CONST(obj)      \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj),   \
  MARINA_TYPE_PROGRESS,                 \
  MarinaProgress const))

#define MARINA_PROGRESS_CLASS(klass)    \
  (G_TYPE_CHECK_CLASS_CAST ((klass),    \
  MARINA_TYPE_PROGRESS,                 \
  MarinaProgressClass))

#define MARINA_IS_PROGRESS(obj)         \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj),   \
  MARINA_TYPE_PROGRESS))

#define MARINA_IS_PROGRESS_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass),    \
  MARINA_TYPE_PROGRESS))

#define MARINA_PROGRESS_GET_CLASS(obj)  \
  (G_TYPE_INSTANCE_GET_CLASS ((obj),    \
  MARINA_TYPE_PROGRESS,                 \
  MarinaProgressClass))

typedef struct _MarinaProgress        MarinaProgress;
typedef struct _MarinaProgressClass   MarinaProgressClass;
typedef struct _MarinaProgressPrivate MarinaProgressPrivate;

struct _MarinaProgress
{
  GtkVBox parent;
  
  MarinaProgressPrivate *priv;
};

struct _MarinaProgressClass
{
  GtkVBoxClass parent_class;
};

GType      marina_progress_get_type (void) G_GNUC_CONST;
GtkWidget* marina_progress_new      (void);

void       marina_progress_set_text (MarinaProgress *progress,
                                     const gchar    *text);

void       marina_progress_set_secondary_text (MarinaProgress *progress,
                                               const gchar    *secondary_text);

void       marina_progress_add_task (MarinaProgress *progress,
                                     IrisTask       *task);

void       marina_progress_cancel   (MarinaProgress *progress);

void       marina_progress_set_icon_name (MarinaProgress *progress,
                                          const gchar    *icon_name);

void       marina_progress_set_cancel_func (MarinaProgress *progress,
                                            GFunc           callback,
                                            gpointer        user_data,
                                            GDestroyNotify  destroy);

G_END_DECLS

#endif /* __MARINA_PROGRESS_H__ */
