/* marina-soup-source.c
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with This; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#include <string.h>
#include <gtk/gtk.h>
#include <libsoup/soup.h>
#include <glib/gi18n.h>

#include "dialogs/marina-add-source-dialog.h"
#include "marina-debug.h"
#include "marina-dirs.h"
#include "marina-prefs.h"
#include "marina-soup-source.h"
#include "marina-util.h"

#define GET_WIDGET(b,n) (GTK_WIDGET (gtk_builder_get_object (b,n)))

struct _MarinaSoupSourcePrivate
{
  SoupSession   *soup;
  gchar         *uri;
  
  gboolean       override_proxy;
  gchar         *proxy;
};

enum
{
  PROP_0,
  PROP_URI,
};

static void marina_source_init (MarinaSourceIface *iface);
static GtkWidget* create_add_source_widget (MarinaAddSourceDialog *dialog);
static MarinaSource* create_source_func (GtkWidget *widget, MarinaAddSourceDialog *dialog);

G_DEFINE_TYPE_EXTENDED (MarinaSoupSource,
                        marina_soup_source,
                        MARINA_TYPE_SOURCE_BASE,
                        0,
                        G_IMPLEMENT_INTERFACE (MARINA_TYPE_SOURCE,
                                               marina_source_init));

static void
marina_soup_source_finalize (GObject *object)
{
  G_OBJECT_CLASS (marina_soup_source_parent_class)->finalize (object);
}

static void
marina_soup_source_get_property (GObject    *object,
                                 guint       property_id,
                                 GValue     *value,
                                 GParamSpec *pspec)
{
  switch (property_id) {
  case PROP_URI:
    g_value_set_string (value, marina_soup_source_get_uri (MARINA_SOUP_SOURCE (object)));
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
  }
}

static void
marina_soup_source_set_property (GObject      *object,
                                 guint         property_id,
                                 const GValue *value,
                                 GParamSpec   *pspec)
{
  switch (property_id) {
  case PROP_URI:
    marina_soup_source_set_uri (MARINA_SOUP_SOURCE (object), g_value_get_string (value));
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
  }
}

static void
marina_soup_source_class_init (MarinaSoupSourceClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  
  object_class->get_property = marina_soup_source_get_property;
  object_class->set_property = marina_soup_source_set_property;
  object_class->finalize = marina_soup_source_finalize;

  g_type_class_add_private (object_class, sizeof (MarinaSoupSourcePrivate));
  
  g_object_class_install_property (object_class,
                                   PROP_URI,
                                   g_param_spec_string ("uri",
                                                        "Uri",
                                                        "The soup source URI",
                                                        NULL,
                                                        G_PARAM_READWRITE));

  /* register our widget to create a new soup source */
  marina_add_source_dialog_register (_("From the local network or interet"),
		                                 create_add_source_widget,
		                                 create_source_func);
}

static void
marina_soup_source_init (MarinaSoupSource *source)
{
  source->priv = G_TYPE_INSTANCE_GET_PRIVATE (source,
                                              MARINA_TYPE_SOUP_SOURCE,
                                              MarinaSoupSourcePrivate);
  
  source->priv->soup = soup_session_sync_new ();
}

MarinaSource*
marina_soup_source_new (void)
{
  return g_object_new (MARINA_TYPE_SOUP_SOURCE, NULL);
}

static gboolean
has_stream (MarinaSource *source)
{
  return TRUE;
}

static void
got_chunk (SoupMessage        *message,
           SoupBuffer         *buffer,
           GMemoryInputStream *stream)
{
  gchar *newbuf;
  
  g_return_if_fail (SOUP_IS_MESSAGE (message));
  g_return_if_fail (buffer != NULL);
  g_return_if_fail (G_IS_MEMORY_INPUT_STREAM (stream));
  
  /* new buffer of same size */
  newbuf = g_new0 (gchar, buffer->length);
  
  /* dup the contents of the buffer */
  memcpy (newbuf, buffer->data, buffer->length);
  
  /* store newbuf in the memory stream, use g_free to release */
  g_memory_input_stream_add_data (stream, newbuf, buffer->length, g_free);
}

static GInputStream*
get_stream (MarinaSource  *source,
            GError       **error)
{
  MarinaSoupSourcePrivate *priv;
  gchar *proxy = NULL;
  SoupMessage *msg;
  guint status;
  GInputStream *stream;
  
  g_return_val_if_fail (MARINA_IS_SOUP_SOURCE (source), NULL);
  
  priv = MARINA_SOUP_SOURCE (source)->priv;
  
  if (!priv->uri)
    return NULL;
  
  /* apply global proxy if it exists and we dont override */
  if (marina_prefs_get_network_use_proxy () && !priv->override_proxy)
    {
      proxy = marina_prefs_get_network_proxy_uri ();
      g_object_set (priv->soup, "proxy-uri", proxy, NULL);
      g_free (proxy);
    }
  
  /* build our http request message */
  msg = soup_message_new ("GET", priv->uri);
  
  /* setup authentication handlers */
  
  /* stream to contain our received data */
  stream = g_memory_input_stream_new ();
  
  /* conenct to got-chunk to fill our stream */
  g_signal_connect (msg,
                    "got-chunk",
                    G_CALLBACK (got_chunk),
                    stream);
  
  /* send our message synchronously */
  status = soup_session_send_message (priv->soup, msg);
  
  marina_debug_message (DEBUG_SOURCES, "Soup status of %d", status);
  
  /* done with our message */
  g_object_unref (msg);
  
  return stream;
}

static G_CONST_RETURN gchar*
get_icon_name (MarinaSource *source)
{
  return "marina-icon";
}

static void
marina_source_init (MarinaSourceIface *iface)
{
  iface->get_stream = get_stream;
  iface->has_stream = has_stream;
  iface->get_icon_name = get_icon_name;
}

static void
apply_proxy (MarinaSoupSource *soup_source,
             const gchar      *proxy)
{
  MarinaSoupSourcePrivate *priv;
  
  g_return_if_fail (MARINA_IS_SOUP_SOURCE (soup_source));
  
  priv = soup_source->priv;
  
  if (priv->override_proxy)
    {
      if (proxy)
        {
          g_object_set (priv->soup,
                        "proxy-uri",
                        soup_uri_new (proxy),
                        NULL);
        }
    }
  else
    {
      /* global proxy is applied during get_stream() */
      g_object_set (priv->soup, "proxy-uri", NULL, NULL);
    }
}

void
marina_soup_source_set_proxy_uri (MarinaSoupSource *soup_source,
                                  const gchar      *proxy)
{
  MarinaSoupSourcePrivate *priv;
  
  g_return_if_fail (MARINA_IS_SOUP_SOURCE (soup_source));
  
  priv = soup_source->priv;
  
  priv->proxy = g_strdup (proxy);
  
  apply_proxy (soup_source, priv->proxy);
}

const gchar*
marina_soup_source_get_proxy_uri (MarinaSoupSource *soup_source)
{
  MarinaSoupSourcePrivate *priv;
  
  g_return_val_if_fail (MARINA_IS_SOUP_SOURCE (soup_source), FALSE);
  
  priv = soup_source->priv;
  
  return priv->proxy;
}

void
marina_soup_source_set_override_proxy (MarinaSoupSource *soup_source,
                                       gboolean          override_proxy)
{
  MarinaSoupSourcePrivate *priv;
  
  g_return_if_fail (MARINA_IS_SOUP_SOURCE (soup_source));
  
  priv = soup_source->priv;
  
  priv->override_proxy = override_proxy;
  
  apply_proxy (soup_source, priv->proxy);
}

gboolean
marina_soup_source_get_override_proxy (MarinaSoupSource *soup_source)
{
  MarinaSoupSourcePrivate *priv;
  
  g_return_val_if_fail (MARINA_IS_SOUP_SOURCE (soup_source), FALSE);
  
  priv = soup_source->priv;
  
  return priv->override_proxy;
}

const gchar*
marina_soup_source_get_uri (MarinaSoupSource *soup_source)
{
  MarinaSoupSourcePrivate *priv;
  
  g_return_val_if_fail (MARINA_IS_SOUP_SOURCE (soup_source), NULL);
  
  priv = soup_source->priv;
  
  return priv->uri;
}

void
marina_soup_source_set_uri (MarinaSoupSource *soup_source,
                            const gchar      *uri)
{
  MarinaSoupSourcePrivate *priv;
  
  g_return_if_fail (MARINA_IS_SOUP_SOURCE (soup_source));
  
  priv = soup_source->priv;
  
  g_free (priv->uri);
  
  priv->uri = g_strdup (uri);
}

static gchar*
get_ui_file (const gchar *ui_name)
{
  gchar *dir,
        *ui_file;
  dir = marina_dirs_get_marina_data_dir ();
  ui_file = g_build_filename (dir, "ui", ui_name, NULL);
  g_free (dir);
  return ui_file;
}

static GtkWidget*
create_add_source_widget (MarinaAddSourceDialog *dialog)
{
  GtkBuilder *builder;
  GtkWidget *widget;
  gchar *ui_file;

  ui_file = get_ui_file ("marina-soup-source.ui");
  builder = gtk_builder_new ();
  gtk_builder_add_from_file (builder, ui_file, NULL);
  g_free (ui_file);

  widget = GET_WIDGET (builder, "vbox1");
  gtk_widget_unparent (widget);
  g_object_ref (widget);

  g_object_set_data (G_OBJECT (widget), "location", GET_WIDGET (builder, "entry1"));
  g_object_set_data (G_OBJECT (widget), "title", GET_WIDGET (builder, "entry2"));

  g_object_unref (builder);

  return widget;
}

static MarinaSource*
create_source_func (GtkWidget             *widget,
                    MarinaAddSourceDialog *dialog)
{
  MarinaSource *source;
  GtkWidget    *location_entry = NULL,
               *title_entry    = NULL;
  gchar        *location,
               *title;

  source = marina_soup_source_new ();

  location_entry = g_object_get_data (G_OBJECT (widget), "location");
  g_assert (location_entry);

  title_entry = g_object_get_data (G_OBJECT (widget), "title");
  g_assert (title_entry);

  g_object_get (G_OBJECT (location_entry), "text", &location, NULL);
  g_object_get (G_OBJECT (title_entry), "text", &title, NULL);

  g_object_set (G_OBJECT (source),
                "title", title,
                "uri", location,
                NULL);

  MARINA_SOURCE_BASE (source)->id = marina_util_uuidgen ();

  return source;
}

