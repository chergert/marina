/* marina-source.c
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#include <glib/gi18n.h>

#include "marina-source.h"

enum
{
  PROP_0,
  PROP_DESCRIPTION,
  PROP_GENERATOR,
  PROP_ICON_URI,
  PROP_LINK,
  PROP_SOURCE_ID,
  PROP_TITLE,
  PROP_UPDATED,
  PROP_UPDATE_STRATEGY,
  PROP_UPDATE_FREQUENCY,
  PROP_URI,
};

enum
{
  CHILD_ADDED,
  LAST_SIGNAL
};

static guint signals[LAST_SIGNAL];

static void
marina_source_base_init (gpointer g_class)
{
  static gboolean initialized = FALSE;
  
  if (!initialized)
    {
      signals [CHILD_ADDED] =
        g_signal_new ("child-added",
                      MARINA_TYPE_SOURCE,
                      G_SIGNAL_RUN_LAST,
                      0,
                      NULL, NULL,
                      g_cclosure_marshal_VOID__OBJECT,
                      G_TYPE_NONE,
                      1,
                      MARINA_TYPE_SOURCE);
      
      initialized = TRUE;
    }
}

GType
marina_source_get_type (void)
{
  static GType marina_source_type = 0;

  if (!marina_source_type)
    {
      const GTypeInfo marina_source_info = {
        sizeof (MarinaSourceIface),
        marina_source_base_init, /* base_init */
        NULL,                    /* base_finalize */
        NULL,
        NULL,                    /* class_finalize */
        NULL,                    /* class_data */
        0,
        0,
        NULL
      };

      marina_source_type = g_type_register_static (G_TYPE_INTERFACE, "MarinaSource",
                                                   &marina_source_info, 0);
      g_type_interface_add_prerequisite (marina_source_type, G_TYPE_OBJECT);
    }

  return marina_source_type;
}

/**
 * marina_source_get_stream:
 * @source: A #MarinaSource
 * @error: A location for a #GError
 *
 * Retrieves the content stream for the source from its potentially
 * remote location.
 *
 * Return value: The #GInputStream for the source or NULL. @error is to be
 *   set if the result is NULL.
 */
GInputStream*
marina_source_get_stream (MarinaSource  *source,
                          GError       **error)
{
  return MARINA_SOURCE_GET_INTERFACE (source)->get_stream (source, error);
}

/**
 * marina_source_get_children:
 * @source: A #MarinaSource
 *
 * Return value: A #GList of children sources. Use g_list_free() when you
 *   are done to free the list.
 */
GList*
marina_source_get_children (MarinaSource *source)
{
  return MARINA_SOURCE_GET_INTERFACE (source)->get_children (source);
}

/**
 * marina_source_observes:
 * @source: A #MarinaSource
 * @item: A #MarinaItem
 *
 * Checks to see if a source is observable by the source. This is used
 * by search sources to see if the item matches the search requirements.
 *
 * Return value: TRUE if @item is observable
 */
gboolean
marina_source_observes (MarinaSource *source,
                        MarinaItem   *item)
{
  return MARINA_SOURCE_GET_INTERFACE (source)->observes (source, item);
}

/**
 * marina_source_get_id:
 * @source: A #MarinaSource
 *
 * Return value: The unique generated id for the source.
 */
const gchar*
marina_source_get_id (MarinaSource *source)
{
  return MARINA_SOURCE_GET_INTERFACE (source)->get_id (source);
}

/**
 * marina_source_is_transient:
 * @source: A #MarinaSource
 *
 * Determines if the source is transient, and therefore does not persist
 * between restarts of the application.
 *
 * Return value: TRUE if the source should not be persisted to storage.
 */
gboolean
marina_source_is_transient (MarinaSource *source)
{
  return MARINA_SOURCE_GET_INTERFACE (source)->is_transient (source);
}

/**
 * marina_source_serialize:
 * @source: A #MarinaSource
 * @writer: A XmlWriter
 *
 * Serializes the source using the xml writer. This is how sources save
 * themselves to be deserialized at startup.
 */
void
marina_source_serialize (MarinaSource *source,
                         XmlWriter    *writer)
{
  if (MARINA_SOURCE_GET_INTERFACE (source)->serialize)
    MARINA_SOURCE_GET_INTERFACE (source)->serialize (source, writer);
}

/**
 * marina_source_deserialize:
 * @source: A #MarinaSource
 * @reader: A #XmlReader
 *
 * Used to deserialize a source from the xml storage on disk. The source
 * will also unserialize all of its children if required.
 */
void
marina_source_deserialize (MarinaSource *source,
                           XmlReader    *reader)
{
  if (MARINA_SOURCE_GET_INTERFACE (source)->deserialize)
    MARINA_SOURCE_GET_INTERFACE (source)->deserialize (source, reader);
}

const gchar*
marina_source_get_title (MarinaSource *source)
{
  return MARINA_SOURCE_GET_INTERFACE (source)->get_title (source);
}

void
marina_source_set_title (MarinaSource *source,
                         const gchar  *title)
{
  if (MARINA_SOURCE_GET_INTERFACE (source)->set_title)
    MARINA_SOURCE_GET_INTERFACE (source)->set_title (source, title);
}

gboolean
marina_source_has_stream (MarinaSource *source)
{
  if (MARINA_SOURCE_GET_INTERFACE (source)->has_stream)
    return MARINA_SOURCE_GET_INTERFACE (source)->has_stream (source);
  return FALSE;
}

G_CONST_RETURN gchar*
marina_source_get_icon_name (MarinaSource *source)
{
  return MARINA_SOURCE_GET_INTERFACE (source)->get_icon_name (source);
}

void
marina_source_child_added (MarinaSource *source,
                           MarinaSource *child)
{
  g_signal_emit (source,
                 signals [CHILD_ADDED],
                 0,
                 child);
}

/**
 * marina_source_remove:
 * @source: A #MarinaSource
 *
 * Called before a source is removed from the system. If the source
 * has children sources, it is responsible for removing those children
 * sources immediately.
 */
void
marina_source_remove (MarinaSource *source)
{
  if (MARINA_SOURCE_GET_INTERFACE (source)->remove)
    MARINA_SOURCE_GET_INTERFACE (source)->remove (source);
}
