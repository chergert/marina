/*
 * marina-plugin-loader.h
 * This file is part of marina; it was borrowed from gedit.
 *
 * Copyright (C) 2008 - Jesse van den Kieboom
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA 02111-1307, USA. 
 */

#ifndef __MARINA_PLUGIN_LOADER_H__
#define __MARINA_PLUGIN_LOADER_H__

#include <glib-object.h>
#include <marina/marina-plugin.h>
#include <marina/marina-plugin-info.h>

G_BEGIN_DECLS

#define MARINA_TYPE_PLUGIN_LOADER                (marina_plugin_loader_get_type ())
#define MARINA_PLUGIN_LOADER(obj)                (G_TYPE_CHECK_INSTANCE_CAST ((obj), MARINA_TYPE_PLUGIN_LOADER, MarinaPluginLoader))
#define MARINA_IS_PLUGIN_LOADER(obj)             (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MARINA_TYPE_PLUGIN_LOADER))
#define MARINA_PLUGIN_LOADER_GET_INTERFACE(inst) (G_TYPE_INSTANCE_GET_INTERFACE ((inst), MARINA_TYPE_PLUGIN_LOADER, MarinaPluginLoaderInterface))

typedef struct _MarinaPluginLoader MarinaPluginLoader; /* dummy object */
typedef struct _MarinaPluginLoaderInterface MarinaPluginLoaderInterface;

struct _MarinaPluginLoaderInterface {
	GTypeInterface parent;

	const gchar *(*get_id)		(void);

	MarinaPlugin *(*load) 		(MarinaPluginLoader 	*loader,
			     		 MarinaPluginInfo	*info,
			      		 const gchar       	*path);

	void 	     (*unload)		(MarinaPluginLoader 	*loader,
					 MarinaPluginInfo      	*info);

	void         (*garbage_collect) (MarinaPluginLoader	*loader);
};

GType marina_plugin_loader_get_type (void);

const gchar *marina_plugin_loader_type_get_id	(GType 			 type);
MarinaPlugin *marina_plugin_loader_load		(MarinaPluginLoader 	*loader,
						 MarinaPluginInfo 	*info,
						 const gchar		*path);
void marina_plugin_loader_unload		(MarinaPluginLoader 	*loader,
						 MarinaPluginInfo	*info);
void marina_plugin_loader_garbage_collect	(MarinaPluginLoader 	*loader);

/**
 * MARINA_PLUGIN_LOADER_IMPLEMENT_INTERFACE(TYPE_IFACE, iface_init):
 *
 * Utility macro used to register interfaces for gobject types in plugin loaders.
 */
#define MARINA_PLUGIN_LOADER_IMPLEMENT_INTERFACE(TYPE_IFACE, iface_init)	\
	const GInterfaceInfo g_implement_interface_info = 			\
	{ 									\
		(GInterfaceInitFunc) iface_init,				\
		NULL, 								\
		NULL								\
	};									\
										\
	g_type_module_add_interface (type_module,				\
				     g_define_type_id, 				\
				     TYPE_IFACE, 				\
				     &g_implement_interface_info);

/**
 * MARINA_PLUGIN_LOADER_REGISTER_TYPE(PluginLoaderName, plugin_loader_name, PARENT_TYPE, loader_interface_init):
 *
 * Utility macro used to register plugin loaders.
 */
#define MARINA_PLUGIN_LOADER_REGISTER_TYPE(PluginLoaderName, plugin_loader_name, PARENT_TYPE, loader_iface_init) 	\
	G_DEFINE_DYNAMIC_TYPE_EXTENDED (PluginLoaderName,			\
					plugin_loader_name,			\
					PARENT_TYPE,				\
					0,					\
					MARINA_PLUGIN_LOADER_IMPLEMENT_INTERFACE(MARINA_TYPE_PLUGIN_LOADER, loader_iface_init));	\
										\
										\
G_MODULE_EXPORT GType								\
register_marina_plugin_loader (GTypeModule *type_module)			\
{										\
	plugin_loader_name##_register_type (type_module);			\
										\
	return plugin_loader_name##_get_type();					\
}

G_END_DECLS

#endif /* __MARINA_PLUGIN_LOADER_H__ */
