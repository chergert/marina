/* this file was borrowed from gedit.
 * please see gedit for additional copyright information */

#ifndef __MARINA_MESSAGE_TYPE_H__
#define __MARINA_MESSAGE_TYPE_H__

#include <glib-object.h>
#include <stdarg.h>

#include "marina-message.h"

G_BEGIN_DECLS

#define MARINA_TYPE_MESSAGE_TYPE			(marina_message_type_get_type ())
#define MARINA_MESSAGE_TYPE(x)			((MarinaMessageType *)(x))

typedef void (*MarinaMessageTypeForeach)		(const gchar *key, 
						 GType 	      type, 
						 gboolean     required, 
						 gpointer     user_data);

typedef struct _MarinaMessageType			MarinaMessageType;

GType marina_message_type_get_type 		 (void) G_GNUC_CONST;

gboolean marina_message_type_is_supported 	 (GType type);
gchar *marina_message_type_identifier		 (const gchar *object_path,
						  const gchar *method);
gboolean marina_message_type_is_valid_object_path (const gchar *object_path);

MarinaMessageType *marina_message_type_new	 (const gchar *object_path, 
						  const gchar *method,
						  guint	      num_optional,
						  ...) G_GNUC_NULL_TERMINATED;
MarinaMessageType *marina_message_type_new_valist	 (const gchar *object_path,
						  const gchar *method,
						  guint	      num_optional,
						  va_list      va_args);

void marina_message_type_set			 (MarinaMessageType *message_type,
						  guint		   num_optional,
						  ...) G_GNUC_NULL_TERMINATED;
void marina_message_type_set_valist		 (MarinaMessageType *message_type,
						  guint		   num_optional,
						  va_list	           va_args);

MarinaMessageType *marina_message_type_ref 	 (MarinaMessageType *message_type);
void marina_message_type_unref			 (MarinaMessageType *message_type);


MarinaMessage *marina_message_type_instantiate_valist (MarinaMessageType *message_type,
				       		     va_list	      va_args);
MarinaMessage *marina_message_type_instantiate 	 (MarinaMessageType *message_type,
				       		  ...) G_GNUC_NULL_TERMINATED;

const gchar *marina_message_type_get_object_path	 (MarinaMessageType *message_type);
const gchar *marina_message_type_get_method	 (MarinaMessageType *message_type);

GType marina_message_type_lookup			 (MarinaMessageType *message_type,
						  const gchar      *key);
						 
void marina_message_type_foreach 		 (MarinaMessageType 	  *message_type,
						  MarinaMessageTypeForeach  func,
						  gpointer	   	   user_data);

G_END_DECLS

#endif /* __MARINA_MESSAGE_TYPE_H__ */

// ex:ts=8:noet:
