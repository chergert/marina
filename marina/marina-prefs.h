/* marina-prefs-manager.h
 * 
 * This file is part of marina; it was borrowed from gedit
 *
 * Copyright (C) 2002  Paolo Maggi 
 *               2009  Christian Hergert
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA 02111-1307, USA. 
 */

#ifndef __MARINA_PREFS_H__
#define __MARINA_PREFS_H__

#define MARINA_BASE_KEY "/apps/marina"

#define MPM_PREFS_DIR                    MARINA_BASE_KEY "/preferences"

/* Network */
#define MPM_NETWORK_DIR                  MPM_PREFS_DIR "/network"
#define MPM_NETWORK_REFRESH              MPM_NETWORK_DIR "/refresh"
#define MPM_NETWORK_USE_PROXY            MPM_NETWORK_DIR "/use_proxy"
#define MPM_NETWORK_PROXY_URI            MPM_NETWORK_DIR "/proxy_uri"

/* UI */
#define MPM_WINDOW_WIDTH                 MPM_PREFS_DIR "/ui/width"
#define MPM_WINDOW_HEIGHT                MPM_PREFS_DIR "/ui/height"
#define MPM_WINDOW_X_POS                 MPM_PREFS_DIR "/ui/x_pos"
#define MPM_WINDOW_Y_POS                 MPM_PREFS_DIR "/ui/y_pos"

#define MPM_COMPACT_MODE                 MPM_PREFS_DIR "/ui/compact_mode"

#define MPM_TOOLBAR_DIR                  MPM_PREFS_DIR "/ui/toolbar"
#define MPM_TOOLBAR_VISIBLE              MPM_TOOLBAR_DIR "/toolbar_visible"

#define MPM_STATUSBAR_DIR                MPM_PREFS_DIR "/ui/statusbar"
#define MPM_STATUSBAR_VISIBLE            MPM_STATUSBAR_DIR "/statusbar_visible"

#define MPM_SIDE_PANE_DIR                MPM_PREFS_DIR "/ui/side_pane"
#define MPM_SIDE_PANE_VISIBLE            MPM_SIDE_PANE_DIR "/side_pane_visible"

#define MPM_BOTTOM_PANEL_DIR             MPM_PREFS_DIR "/ui/bottom_panel"
#define MPM_BOTTOM_PANEL_VISIBLE         MPM_BOTTOM_PANEL_DIR "/bottom_panel_visible"

#define MPM_SCHEME                       MPM_PREFS_DIR "/ui/scheme"

#define MPM_USE_DEFAULT_FONT             MPM_PREFS_DIR "/ui/use_default_font"
#define MPM_FONT                         MPM_PREFS_DIR "/ui/font"

/* Plugins */
#define MPM_PLUGINS_DIR                  MARINA_BASE_KEY "/plugins"
#define MPM_ACTIVE_PLUGINS               MPM_PLUGINS_DIR "/active-plugins"

/* Defaults */
#define MPM_DEFAULT_NETWORK_REFRESH      3600 /* seconds */
#define MPM_DEFAULT_NETWORK_USE_PROXY    FALSE
#define MPM_DEFAULT_NETWORK_PROXY_URI    NULL
#define MPM_DEFAULT_STATUSBAR_VISIBLE    1 /* TRUE */
#define MPM_DEFAULT_SIDE_PANE_VISIBLE    0 /* FALSE */
#define MPM_DEFAULT_BOTTOM_PANEL_VISIBLE 0 /* FALSE */
#define MPM_DEFAULT_TOOLBAR_VISIBLE      1 /* TRUE */
#define MPM_DEFAULT_WINDOW_WIDTH         600
#define MPM_DEFAULT_WINDOW_HEIGHT        400
#define MPM_DEFAULT_WINDOW_X_POS         -1
#define MPM_DEFAULT_WINDOW_Y_POS         -1
#define MPM_DEFAULT_SCHEME               "Default"
#define MPM_DEFAULT_COMPACT_MODE         FALSE
#define MPM_DEFAULT_USE_DEFAULT_FONT     TRUE
#define MPM_DEFAULT_FONT                 "Sans 10"

/* Lifecycle management */
gboolean marina_prefs_init     (void);
void     marina_prefs_shutdown (void);

/* Network refresh */
gint                     marina_prefs_get_network_refresh          (void);
void                     marina_prefs_set_network_refresh          (gint refresh);
gboolean                 marina_prefs_network_refresh_can_set      (void);

/* Use a network proxy */
gboolean                 marina_prefs_get_network_use_proxy        (void);
void                     marina_prefs_set_network_use_proxy        (gboolean use_proxy);
gboolean                 marina_prefs_network_use_proxy_can_set    (void);

/* Network proxy uri */
gchar*                   marina_prefs_get_network_proxy_uri        (void);
void                     marina_prefs_set_network_proxy_uri        (const gchar *proxy_uri);
gboolean                 marina_prefs_network_proxy_uri_can_set    (void);

/* Statusbar visible */
gboolean                 marina_prefs_get_statusbar_visible        (void);
void                     marina_prefs_set_statusbar_visible        (gboolean sv);
gboolean                 marina_prefs_statusbar_visible_can_set    (void);

/* Side pane visible */
gboolean                 marina_prefs_get_side_pane_visible        (void);
void                     marina_prefs_set_side_pane_visible        (gboolean tv);
gboolean                 marina_prefs_side_pane_visible_can_set    (void);

/* Bottom panel visible */
gboolean                 marina_prefs_get_bottom_panel_visible     (void);
void                     marina_prefs_set_bottom_panel_visible     (gboolean tv);
gboolean                 marina_prefs_bottom_panel_visible_can_set (void);

/* Plugins */
GSList                  *marina_prefs_get_active_plugins           (void);
void                     marina_prefs_set_active_plugins           (const GSList *plugins);
gboolean                 marina_prefs_active_plugins_can_set       (void);

/* Toolbar visible */
gboolean                 marina_prefs_get_toolbar_visible          (void);
void                     marina_prefs_set_toolbar_visible          (gboolean tv);
gboolean                 marina_prefs_toolbar_visible_can_set      (void);

/* Window width */
gint                     marina_prefs_get_window_width             (void);
void                     marina_prefs_set_window_width             (gint width);
gboolean                 marina_prefs_window_width_can_set         (void);

/* Window height */
gint                     marina_prefs_get_window_height            (void);
void                     marina_prefs_set_window_height            (gint height);
gboolean                 marina_prefs_window_height_can_set        (void);

/* Window x positioning */
gint                     marina_prefs_get_window_x_pos             (void);
void                     marina_prefs_set_window_x_pos             (gint x_pos);
gboolean                 marina_prefs_window_x_pos_can_set         (void);

/* Window y positioning */
gint                     marina_prefs_get_window_y_pos             (void);
void                     marina_prefs_set_window_y_pos             (gint y_pos);
gboolean                 marina_prefs_window_y_pos_can_set         (void);

/* Color/Layout scheme */
gchar*                   marina_prefs_get_scheme                   (void);
void                     marina_prefs_set_scheme                   (const gchar *scheme);
gboolean                 marina_prefs_scheme_can_set               (void);

/* Compact Mode */
gboolean                 marina_prefs_get_compact_mode             (void);
void                     marina_prefs_set_compact_mode             (gboolean compact_mode);
gboolean                 marina_prefs_compact_mode_can_set         (void);

/* Fonts */
gboolean                 marina_prefs_get_use_default_font         (void);
void                     marina_prefs_set_use_default_font         (gboolean default_font);
gboolean                 marina_prefs_use_default_font_can_set     (void);

gchar*                   marina_prefs_get_font                     (void);
void                     marina_prefs_set_font                     (const gchar *font);
gboolean                 marina_prefs_font_can_set                 (void);



#endif /* __MARINA_PREFS_H__ */
