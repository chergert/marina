/* g-lru-cache.c
 *
 * Copyright (C) 2009 - Christian Hergert
 *
 * This is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "g-lru-cache.h"

struct _GLruCachePrivate
{
  GStaticRWLock   rw_lock;
  guint           max_size;
  gboolean        fast_get;
  
  GHashTable     *hash_table;
  GEqualFunc      key_equal_func;
  GCopyFunc       key_copy_func;
  GList          *newest;
  GList          *oldest;
  
  GLookupFunc     retrieve_func;
  
  gpointer        user_data;
  GDestroyNotify  user_destroy_func;
};

G_DEFINE_TYPE (GLruCache, g_lru_cache, G_TYPE_OBJECT);

static void
g_lru_cache_finalize (GObject *object)
{
  GLruCachePrivate *priv = G_LRU_CACHE (object)->priv;
  
  if (priv->user_data && priv->user_destroy_func)
    priv->user_destroy_func (priv->user_data);
  
  priv->user_data = NULL;
  priv->user_destroy_func = NULL;
  
  g_hash_table_destroy (priv->hash_table);
  priv->hash_table = NULL;
  
  g_list_free (priv->newest);
  priv->newest = NULL;
  priv->oldest = NULL;
  
  G_OBJECT_CLASS (g_lru_cache_parent_class)->finalize (object);
}

static void
g_lru_cache_class_init (GLruCacheClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  
  object_class->finalize = g_lru_cache_finalize;

  g_type_class_add_private (object_class, sizeof (GLruCachePrivate));
}

static void
g_lru_cache_init (GLruCache *lru_cache)
{
  lru_cache->priv = G_TYPE_INSTANCE_GET_PRIVATE (lru_cache, G_TYPE_LRU_CACHE, GLruCachePrivate);
  
  lru_cache->priv->max_size = 1024;
  lru_cache->priv->fast_get = FALSE;
  g_static_rw_lock_init (&lru_cache->priv->rw_lock);
}

static void
g_lru_cache_evict_n_oldest_locked (GLruCache *lru_cache, gint n)
{
  GLruCachePrivate *priv;
  GList *victim;
  gint   i;
  
  g_return_if_fail (G_IS_LRU_CACHE (lru_cache));
  
  priv = lru_cache->priv;
  
  for (i = 0; i < n; i++)
    {
      victim = priv->oldest;
      
      if (victim == NULL)
        return;
      
      if (victim->prev)
        victim->prev->next = NULL;
      
      priv->oldest = victim->prev;
      g_hash_table_remove (priv->hash_table, victim->data);
      
      if (priv->newest == victim)
        priv->newest = NULL;
      
      g_list_free1 (victim); /* victim->data is owned by hashtable */
    }
  
#if DEBUG
  g_assert (g_hash_table_size (priv->hash_table) == g_list_length (priv->newest));
#endif
}

/**
 * g_lru_cache_new:
 * @hash_func: A #GHashFunc for the lookup keys
 * @key_equal_func: A #GEqualFunc used to compare keys
 * @key_copy_func: A #GCopyFunc used to copy the key if needed
 * @retrieve_func: A #GLookupFunc to retrieve an item if not found in cache
 * @key_destroy_func: A #GDestroyNotify to be called when the key is removed
 * @value_destroy_func: A #GDestroyNotify to be called when the value is removed
 * @user_data: A pointer to user data or NULL
 * @user_destroy_notify: A #GDestroyNotify to be called during dipose, or NULL
 *
 * Creates a new #GLruCache.
 *
 * Return value: The new #GLruCache. Use g_unref_object() when you are done
 *   using the #GLruCache.
 */
GLruCache*
g_lru_cache_new (GHashFunc      hash_func,
                 GEqualFunc     key_equal_func,
                 GCopyFunc      key_copy_func,
                 GLookupFunc    retrieve_func,
                 GDestroyNotify key_destroy_func,
                 GDestroyNotify value_destroy_func,
                 gpointer       user_data,
                 GDestroyNotify user_destroy_func)
{
  GLruCache *lru_cache = g_object_new (G_TYPE_LRU_CACHE, NULL);
  
  lru_cache->priv->hash_table = g_hash_table_new_full (hash_func,
                                                  key_equal_func,
                                                  key_destroy_func,
                                                  value_destroy_func);
  
  lru_cache->priv->key_equal_func = key_equal_func;
  lru_cache->priv->key_copy_func = key_copy_func;
  lru_cache->priv->retrieve_func = retrieve_func;
  lru_cache->priv->user_data = user_data;
  lru_cache->priv->user_destroy_func = user_destroy_func;
  
  return lru_cache;
}

/**
 * g_lru_cache_set_max_size:
 * @lru_cache: A #GLruCache
 * @max_size: the max number of items allowed in the lru
 *
 * Sets the maximum size of allowed items within the cache. If @max_size
 * is less than the current number of items in the cache, the oldest items
 * will be evicted.
 */
void
g_lru_cache_set_max_size (GLruCache *lru_cache, guint max_size)
{
  GLruCachePrivate *priv;
  
  g_return_if_fail (G_IS_LRU_CACHE (lru_cache));
  
  priv = lru_cache->priv;
  
  guint old_max_size = priv->max_size;
  
  g_static_rw_lock_writer_lock (&(priv->rw_lock));
  
  priv->max_size = max_size;
  
  if (old_max_size > max_size)
    g_lru_cache_evict_n_oldest_locked (lru_cache, old_max_size - max_size);
  
  g_static_rw_lock_writer_unlock (&(priv->rw_lock));
}

/**
 * g_lru_cache_get_max_size:
 * @lru_cache: A #GLruCache
 *
 * Return value: the current maximum size of the cache
 */
guint
g_lru_cache_get_max_size (GLruCache *lru_cache)
{
  g_return_val_if_fail (G_IS_LRU_CACHE (lru_cache), -1);
  return lru_cache->priv->max_size;
}

/**
 * g_lru_cache_get_size:
 * @lru_cache: A #GLruCache
 *
 * Return value: The current number of items in the cache.
 */
guint
g_lru_cache_get_size (GLruCache *lru_cache)
{
  GLruCachePrivate *priv;
  
  g_return_val_if_fail (G_IS_LRU_CACHE (lru_cache), -1);
  
  priv = lru_cache->priv;
  
  return g_hash_table_size (priv->hash_table);
}

/**
 * g_lru_cache_get:
 * @lru_cache: A #GLruCache
 * @key: the key to lookup
 *
 * Retrieves the value for @key. If @key is not found in the cache,
 * the #GLookupFunc for the cache will be invoked to locate and store
 * the item.
 *
 * Return value: A pointer to the value, or NULL
 */
gpointer
g_lru_cache_get (GLruCache *lru_cache, gpointer key)
{
  GLruCachePrivate *priv;
  gpointer value;
  
  g_return_val_if_fail (G_IS_LRU_CACHE (lru_cache), NULL);
  
  priv = lru_cache->priv;
  
  g_static_rw_lock_reader_lock (&(priv->rw_lock));
  
  value = g_hash_table_lookup (priv->hash_table, key);
  
#if DEBUG
  if (value)
    g_debug ("Cache Hit!");
  else
    g_debug ("Cache miss");
#endif
  
  g_static_rw_lock_reader_unlock (&(priv->rw_lock));
  
  if (!value)
    {
      g_static_rw_lock_writer_lock (&(priv->rw_lock));
      
      if (!g_hash_table_lookup (priv->hash_table, key))
        {
          if (g_hash_table_size (priv->hash_table) >= priv->max_size)
            {
#if DEBUG
              g_debug ("We are at capacity, must evict oldest");
#endif
              g_lru_cache_evict_n_oldest_locked (lru_cache, 1);
            }

#if DEBUG
          g_debug ("Retrieving value from external resource");
#endif

          value = priv->retrieve_func (key, priv->user_data);
          
          if (priv->key_copy_func)
            g_hash_table_insert (priv->hash_table,
              priv->key_copy_func (key, priv->user_data),
              value);
          else
            g_hash_table_insert (priv->hash_table, key, value);
          
          priv->newest = g_list_prepend (priv->newest, key);
          
          if (priv->oldest == NULL)
            priv->oldest = priv->newest;
        }
#if DEBUG
      else g_debug ("Lost storage race with another thread");
#endif
      
      g_static_rw_lock_writer_unlock (&(priv->rw_lock));
    }

  /* A fast_get means that we do not reposition the item to the head
   * of the list. this means it really isnt an LRU. but if you are
   * only concerned with reducing load on say a bdb database, it helps
   * mitigate that problem while not adding the overhead of serious
   * accounting.
   */

  else if (!priv->fast_get &&
           !priv->key_equal_func (key, priv->newest->data))
    {
#if DEBUG
      g_debug ("Making item most recent");
#endif

      g_static_rw_lock_writer_lock (&(priv->rw_lock));

      GList *list = priv->newest;
      GList *tmp;
      GEqualFunc equal = priv->key_equal_func;

      for (tmp = list; tmp; tmp = tmp->next)
        {
          if (equal (key, tmp->data))
            {
              GList *tmp1 = g_list_remove_link (list, tmp);
              priv->newest = g_list_prepend (tmp1, tmp);
              break;
            }
        }

      g_static_rw_lock_writer_unlock (&(priv->rw_lock));
    }
  
  return value;
}

/**
 * g_lru_cache_evict:
 * @lru_cache: A #GLruCache
 * @key: The item key
 *
 * Evicts an item from cache.
 */
void
g_lru_cache_evict (GLruCache *lru_cache, gpointer key)
{
  g_return_if_fail (G_IS_LRU_CACHE (lru_cache));
  
  GEqualFunc  equal = lru_cache->priv->key_equal_func;
  GList      *list  = NULL;
  
  g_static_rw_lock_writer_lock (&(lru_cache->priv->rw_lock));
  
  if (equal (key, lru_cache->priv->oldest))
    {
      g_lru_cache_evict_n_oldest_locked (lru_cache, 1);
    }
  else
    {
      g_hash_table_remove (lru_cache->priv->hash_table, key);
      
      for (list = lru_cache->priv->newest; list; list = list->next)
        {
          if (equal (key, list->data))
            {
              lru_cache->priv->newest = g_list_remove_link (lru_cache->priv->newest, list);
              g_list_free (list);
              break;
            }
        }
    }
  
  g_static_rw_lock_writer_unlock (&(lru_cache->priv->rw_lock));
}

/**
 * g_lru_cache_clear:
 * @lru_cache: A #GLruCache
 *
 * Clears all items from the cache.
 */
void
g_lru_cache_clear (GLruCache *lru_cache)
{
  g_return_if_fail (G_IS_LRU_CACHE (lru_cache));
  
  g_static_rw_lock_writer_lock (&(lru_cache->priv->rw_lock));
  
  g_hash_table_remove_all (lru_cache->priv->hash_table);
  g_list_free (lru_cache->priv->newest);
  
  lru_cache->priv->oldest = NULL;
  lru_cache->priv->newest = NULL;
  
  g_static_rw_lock_writer_unlock (&(lru_cache->priv->rw_lock));
}

/**
 * g_lru_cache_set_fast_get:
 * @lru_cache: A #GLruCache
 * @fast_get: TRUE if we should use fast_get
 *
 * Enables or disables the fast_get option. If the fast_get option
 * is enabled, we will not reposition an item to the top of the most
 * recently used list on every call to g_lru_cache_get().  This means
 * the cache is more of a quick access to a FIFO queue.
 */
void
g_lru_cache_set_fast_get (GLruCache *lru_cache, gboolean fast_get)
{
  g_return_if_fail (G_IS_LRU_CACHE (lru_cache));
  lru_cache->priv->fast_get = fast_get;
}

/**
 * g_lru_cache_get_fast_get:
 * @lru_cache: A #GLruCache
 *
 * Return value: TRUE if fast_get is enabled.
 */
gboolean
g_lru_cache_get_fast_get (GLruCache *lru_cache)
{
  g_return_val_if_fail (G_IS_LRU_CACHE (lru_cache), FALSE);
  return lru_cache->priv->fast_get;
}

