/* marina-folder-source.c
 *
 * Copyright (C) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with This; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#include <gtk/gtk.h>

#include "marina-debug.h"
#include "marina-folder-source.h"

struct _MarinaFolderSourcePrivate
{
  GList *children;
};

static void marina_source_init (MarinaSourceIface *iface);

G_DEFINE_TYPE_EXTENDED (MarinaFolderSource,
                        marina_folder_source,
                        MARINA_TYPE_SOURCE_BASE,
                        0,
                        G_IMPLEMENT_INTERFACE (MARINA_TYPE_SOURCE,
                                               marina_source_init));

static void
marina_folder_source_finalize (GObject *object)
{
  G_OBJECT_CLASS (marina_folder_source_parent_class)->finalize (object);
}

static void
marina_folder_source_class_init (MarinaFolderSourceClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  
  object_class->finalize = marina_folder_source_finalize;

  g_type_class_add_private (object_class, sizeof (MarinaFolderSourcePrivate));
}

static void
marina_folder_source_init (MarinaFolderSource *source)
{
  source->priv = G_TYPE_INSTANCE_GET_PRIVATE (source,
                                              MARINA_TYPE_FOLDER_SOURCE,
                                              MarinaFolderSourcePrivate);
}

MarinaSource*
marina_folder_source_new (void)
{
  return g_object_new (MARINA_TYPE_FOLDER_SOURCE, NULL);
}

static gboolean
has_stream (MarinaSource *source)
{
  return FALSE;
}

static GInputStream*
get_stream (MarinaSource  *source,
            GError       **error)
{
  return NULL;
}

static G_CONST_RETURN gchar*
get_icon_name (MarinaSource *source)
{
  return GTK_STOCK_DIRECTORY;
}

static void
serialize (MarinaSource *source,
           XmlWriter    *writer)
{
  MarinaFolderSourcePrivate *priv;
  GList *iter;
  
  g_return_if_fail (MARINA_IS_FOLDER_SOURCE (source));
  
  priv = MARINA_FOLDER_SOURCE (source)->priv;
  
  /* start <source> */
  xml_writer_write_start_element (writer, "source");
  
  /* type="MarinaFolderSource" */
  xml_writer_write_attribute_string (writer, "type",
                                     g_type_name (G_TYPE_FROM_INSTANCE (source)));
  
  /* id="DEADBEEF" */
  xml_writer_write_attribute_string (writer, "id",
                                     marina_source_get_id (source));

  /* title="" */
  xml_writer_write_attribute_string (writer, "title",
                                     marina_source_get_title (source));
  
  /* start <children> */
  xml_writer_write_start_element (writer, "children");
  
  /* children sources */
  for (iter = priv->children; iter; iter = iter->next)
    marina_source_serialize (iter->data, writer);
  
  /* end </children> */
  xml_writer_write_end_element (writer);
  
  /* finish </source> */
  xml_writer_write_end_element (writer);
}

static void
deserialize (MarinaSource *folder,
             XmlReader    *reader)
{
  MarinaFolderSourcePrivate *priv;
  GList *list = NULL;
  
  g_return_if_fail (MARINA_IS_FOLDER_SOURCE (folder));
  
  priv = MARINA_FOLDER_SOURCE (folder)->priv;
  
  /* deserialize the basic stuff */
  MARINA_SOURCE_GET_INTERFACE (marina_folder_source_parent_class)->deserialize (folder, reader);
  
  /* look for the inner <children> element */
  if (xml_reader_read_start_element (reader, "children"))
    {
      while (xml_reader_read (reader))
        if (g_str_equal ("children", xml_reader_get_name (reader)))
          return;
        else if (g_str_equal ("source", xml_reader_get_name (reader)))
          break;
      
      do
        {
          MarinaSource *source;
          const gchar *type;
          GType gtype;
          
          if (g_str_equal (xml_reader_get_name (reader), "children"))
            break;
          
          if ((type = xml_reader_get_attribute (reader, "type")) != NULL)
            {
              /* get the desired GType to instantiate */
              gtype = g_type_from_name (type);
              
              if (!gtype)
                continue;
              
              /* instantiate the source */
              source = g_object_new (gtype, NULL);
              
              /* make sure the type was resolved and created */
              if (source == NULL)
                continue;
              
              /* allow the source to deserialize itself */
              marina_source_deserialize (source, reader);
              
              list = g_list_prepend (list, source);
            }
        }
      while (xml_reader_read_to_next (reader));
        
      list = g_list_reverse (list);
      
      priv->children = list;
    }
}

static GList*
get_children (MarinaSource *source)
{
  MarinaFolderSourcePrivate *priv;
  
  g_return_val_if_fail (MARINA_IS_FOLDER_SOURCE (source), NULL);
  
  priv = MARINA_FOLDER_SOURCE (source)->priv;
  
  return g_list_copy (priv->children);
}

static gboolean
observes (MarinaSource *source,
          MarinaItem   *item)
{
  MarinaFolderSourcePrivate *priv;
  GList *iter;
  
  g_return_val_if_fail (MARINA_IS_FOLDER_SOURCE (source), FALSE);
  
  priv = MARINA_FOLDER_SOURCE (source)->priv;
  
  for (iter = priv->children; iter; iter = iter->next)
    if (marina_source_observes (iter->data, item))
      return TRUE;
  
  return FALSE;
}

static void
marina_source_init (MarinaSourceIface *iface)
{
  iface->get_stream = get_stream;
  iface->has_stream = has_stream;
  iface->get_icon_name = get_icon_name;
  iface->serialize = serialize;
  iface->deserialize = deserialize;
  iface->get_children = get_children;
  iface->observes = observes;
}

void
marina_folder_source_append_child (MarinaFolderSource *source,
                                   MarinaSource       *child)
{
  MarinaFolderSourcePrivate *priv;
  
  g_return_if_fail (MARINA_IS_FOLDER_SOURCE (source));
  
  priv = source->priv;
  
  if (g_list_find (priv->children, child))
    return;
    
  priv->children = g_list_append (priv->children, child);
}

void
marina_folder_source_remove_child (MarinaFolderSource *source,
                                   MarinaSource       *child)
{
  MarinaFolderSourcePrivate *priv;
  
  g_return_if_fail (MARINA_IS_FOLDER_SOURCE (source));
  
  priv = source->priv;
  
  priv->children = g_list_remove (priv->children, child);
}
