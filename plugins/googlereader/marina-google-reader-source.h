#ifndef __MARINA_GOOGLE_READER_SOURCE_H__
#define __MARINA_GOOGLE_READER_SOURCE_H__

#include <glib-object.h>
#include <marina/marina-source-base.h>

G_BEGIN_DECLS

#define MARINA_TYPE_GOOGLE_READER_SOURCE              (marina_google_reader_source_get_type ())
#define MARINA_GOOGLE_READER_SOURCE(obj)              (G_TYPE_CHECK_INSTANCE_CAST ((obj), MARINA_TYPE_GOOGLE_READER_SOURCE, MarinaGoogleReaderSource))
#define MARINA_GOOGLE_READER_SOURCE_CONST(obj)        (G_TYPE_CHECK_INSTANCE_CAST ((obj), MARINA_TYPE_GOOGLE_READER_SOURCE, MarinaGoogleReaderSource const))
#define MARINA_GOOGLE_READER_SOURCE_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass),  MARINA_TYPE_GOOGLE_READER_SOURCE, MarinaGoogleReaderSourceClass))
#define MARINA_IS_GOOGLE_READER_SOURCE(obj)           (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MARINA_TYPE_GOOGLE_READER_SOURCE))
#define MARINA_IS_GOOGLE_READER_SOURCE_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE ((klass),  MARINA_TYPE_GOOGLE_READER_SOURCE))
#define MARINA_GOOGLE_READER_SOURCE_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj),  MARINA_TYPE_GOOGLE_READER_SOURCE, MarinaGoogleReaderSourceClass))

typedef struct _MarinaGoogleReaderSource          MarinaGoogleReaderSource;
typedef struct _MarinaGoogleReaderSourceClass     MarinaGoogleReaderSourceClass;
typedef struct _MarinaGoogleReaderSourcePrivate   MarinaGoogleReaderSourcePrivate;

struct _MarinaGoogleReaderSource
{
  MarinaSourceBase parent;
  
  MarinaGoogleReaderSourcePrivate *priv;
};

struct _MarinaGoogleReaderSourceClass
{
  MarinaSourceBaseClass parent_class;
};

GType         marina_google_reader_source_get_type (void) G_GNUC_CONST;
MarinaSource* marina_google_reader_source_new      (void);

G_CONST_RETURN gchar* marina_google_reader_source_get_username (MarinaGoogleReaderSource *source);
void                  marina_google_reader_source_set_username (MarinaGoogleReaderSource *source,
                                                                const gchar              *username);

G_END_DECLS

#endif /* __MARINA_GOOGLE_READER_SOURCE_H__ */
