/*
 * googlereader-plugin.c - Access your Google Reader account from Marina
 *
 * Copyright (C) 2009 - Christian Hergert
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif


#include <glib/gi18n-lib.h>
#include <marina/marina-debug.h>
#include <marina/marina-dirs.h>
#include <marina/marina-util.h>
#include <marina/dialogs/marina-add-source-dialog.h>

#include "googlereader-plugin.h"
#include "marina-google-reader-source.h"

#define WINDOW_DATA_KEY	"GooglereaderPluginWindowData"

#define GOOGLEREADER_PLUGIN_GET_PRIVATE(object)	\
  (G_TYPE_INSTANCE_GET_PRIVATE ((object),       \
  TYPE_GOOGLEREADER_PLUGIN,                     \
  GooglereaderPluginPrivate))

#define GET_WIDGET(b,n) (GTK_WIDGET (gtk_builder_get_object (b,n)))

struct _GooglereaderPluginPrivate
{
  GType    g_type;
	gpointer dummy;
};

MARINA_PLUGIN_REGISTER_TYPE (GooglereaderPlugin, googlereader_plugin)


static void
googlereader_plugin_init (GooglereaderPlugin *plugin)
{
	plugin->priv = GOOGLEREADER_PLUGIN_GET_PRIVATE (plugin);

	marina_debug_message (DEBUG_PLUGINS,
                        "GooglereaderPlugin initializing");
}

static void
googlereader_plugin_finalize (GObject *object)
{
	marina_debug_message (DEBUG_PLUGINS,
                        "GooglereaderPlugin finalizing");

	G_OBJECT_CLASS (googlereader_plugin_parent_class)->finalize (object);
}

static gchar*
get_ui_file (const gchar *name)
{
  gchar *dir,
        *ui_file;

  dir = marina_dirs_get_marina_data_dir ();
  ui_file = g_build_filename (dir, "ui", name, NULL);
  g_free (dir);
  return ui_file;
}

static GtkWidget*
create_add_source_widget (MarinaAddSourceDialog *dialog)
{
  GtkBuilder *builder;
  GtkWidget  *widget;
  gchar      *ui_file;

  ui_file = get_ui_file ("marina-google-reader-source.ui");
  builder = gtk_builder_new ();
  gtk_builder_add_from_file (builder, ui_file, NULL);
  g_free (ui_file);

  widget = GTK_WIDGET (gtk_builder_get_object (builder, "vbox1"));
  g_assert (widget);
  gtk_widget_unparent (widget);

  g_object_set_data (G_OBJECT (widget), "username", GET_WIDGET (builder, "username"));

  g_object_ref (widget);
  g_object_unref (builder);

  return widget;
}

static MarinaSource*
create_source_func (GtkWidget             *widget,
                    MarinaAddSourceDialog *dialog)
{               
  MarinaSource *source;
  GtkWidget    *username_entry = NULL;
  gchar        *username;

  source = marina_google_reader_source_new (); 

  username_entry = g_object_get_data (G_OBJECT (widget), "username");
  g_assert (username_entry);   

  g_object_get (G_OBJECT (username_entry), "text", &username, NULL);
  g_object_set (G_OBJECT (source), "username", username, NULL);

  MARINA_SOURCE_BASE (source)->id = marina_util_uuidgen ();

  return source;
}

static void
impl_activate (MarinaPlugin *plugin,
               MarinaWindow *window)
{
  GooglereaderPluginPrivate *priv;
  
	marina_debug (DEBUG_PLUGINS);
	
	g_return_if_fail (IS_GOOGLEREADER_PLUGIN (plugin));
	
	priv = GOOGLEREADER_PLUGIN (plugin)->priv;

	// Make sure our source type is registered so that MarinaSources can deserialize
	// any sources in sources.xml from their type name.
	priv->g_type = MARINA_TYPE_GOOGLE_READER_SOURCE;

  marina_add_source_dialog_register (_("From a Google Reader account"),
                                     create_add_source_widget,
                                     create_source_func);
}

static void
impl_deactivate (MarinaPlugin *plugin,
		 MarinaWindow *window)
{

	marina_debug (DEBUG_PLUGINS);
}

static void
impl_update_ui (MarinaPlugin *plugin,
		MarinaWindow *window)
{
	marina_debug (DEBUG_PLUGINS);
}

static void
googlereader_plugin_class_init (GooglereaderPluginClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);
	MarinaPluginClass *plugin_class = MARINA_PLUGIN_CLASS (klass);

	object_class->finalize = googlereader_plugin_finalize;

	plugin_class->activate = impl_activate;
	plugin_class->deactivate = impl_deactivate;
	plugin_class->update_ui = impl_update_ui;

	g_type_class_add_private (object_class, 
                            sizeof (GooglereaderPluginPrivate));
}
