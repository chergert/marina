#include <libsoup/soup.h>
#include <glib/gi18n.h>
#include <gnome-keyring.h>
#include <iris/iris.h>
#include <gtk/gtk.h>

#include <marina/marina-dirs.h>
#include <marina/marina-debug.h>
#include <marina/marina-soup-source.h>
#include <marina/marina-sources.h>
#include <marina/marina-sync.h>

#include "marina-google-reader-source.h"

#define MARINA_GOOGLE_READER_SOURCE_GET_PRIVATE(object) \
  (G_TYPE_INSTANCE_GET_PRIVATE((object),                \
  MARINA_TYPE_GOOGLE_READER_SOURCE,                     \
  MarinaGoogleReaderSourcePrivate))

#define GOOGLE_READER_ERROR_QUARK (g_quark_from_string ("google-reader-source-error-quark"))

#define LOGIN_URI_FORMAT                                \
  "https://www.google.com/accounts/ClientLogin?"        \
  "service=reader&Email=%s&Passwd=%s&source=marina&"    \
  "continue=http://www.google.com"

#define OPML_URI "http://www.google.com/reader/subscriptions/export"

enum
{
  PROP_0,
  PROP_USERNAME,
};

struct _MarinaGoogleReaderSourcePrivate
{
  GList       *children;
  gchar       *username;
  
  GMutex      *session_lock;
  SoupSession *session;
  gchar       *cookie;
};

static void marina_source_init (MarinaSourceIface *iface);

G_DEFINE_TYPE_EXTENDED (MarinaGoogleReaderSource,
                        marina_google_reader_source,
                        MARINA_TYPE_SOURCE_BASE,
                        0,
                        G_IMPLEMENT_INTERFACE (MARINA_TYPE_SOURCE,
                                               marina_source_init));

static void
marina_google_reader_source_finalize (GObject *object)
{
  G_OBJECT_CLASS (marina_google_reader_source_parent_class)->finalize (object);
}

static void
get_property (GObject    *object,
              guint       property_id,
              GValue     *value,
              GParamSpec *pspec)
{
  switch (property_id) {
  case PROP_USERNAME:
    g_value_set_string (value, marina_google_reader_source_get_username (MARINA_GOOGLE_READER_SOURCE (object)));
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
  }
}

static void
set_property (GObject      *object,
              guint         property_id,
              const GValue *value,
              GParamSpec   *pspec)
{
  switch (property_id) {
  case PROP_USERNAME:
    marina_google_reader_source_set_username (MARINA_GOOGLE_READER_SOURCE (object), g_value_get_string (value));
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
  }
}

static void
marina_google_reader_source_class_init (MarinaGoogleReaderSourceClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  
  object_class->finalize = marina_google_reader_source_finalize;
  object_class->get_property = get_property;
  object_class->set_property = set_property;

  g_type_class_add_private (object_class, sizeof (MarinaGoogleReaderSourcePrivate));
  
  g_object_class_install_property (object_class,
                                   PROP_USERNAME,
                                   g_param_spec_string ("username",
                                                        "Username",
                                                        "Google Username",
                                                        NULL,
                                                        G_PARAM_READWRITE));
}

static void
marina_google_reader_source_init (MarinaGoogleReaderSource *source)
{
  source->priv = MARINA_GOOGLE_READER_SOURCE_GET_PRIVATE (source);
  source->priv->session_lock = g_mutex_new ();
}

MarinaSource*
marina_google_reader_source_new ()
{
  return g_object_new (MARINA_TYPE_GOOGLE_READER_SOURCE, NULL);
}

static void
get_pass_dialog (GtkWidget **dialog,
                 GtkWidget **password)
{
  GtkBuilder *builder;
  gchar *dir, *uifile;
  
  dir = marina_dirs_get_marina_data_dir ();
  uifile = g_build_filename (dir, "ui", "googlereader.ui", NULL);
  
  builder = gtk_builder_new ();
  gtk_builder_add_from_file (builder, uifile, NULL);
  
  *dialog = GTK_WIDGET (gtk_builder_get_object (builder, "dialog1"));
  *password = GTK_WIDGET (gtk_builder_get_object (builder, "password"));
  
  gtk_window_set_title (GTK_WINDOW (*dialog), "");
  
  g_free (dir);
  g_free (uifile);
  
  g_object_unref (builder);
}

static void
find_password_cb (GnomeKeyringResult  res,
                  const gchar        *password,
                  gpointer            user_data)
{
  IrisTask *task = IRIS_TASK (user_data);
  
  if (res == GNOME_KEYRING_RESULT_OK)
    IRIS_TASK_RETURN_VALUE (task, G_TYPE_STRING, password);
  else
    IRIS_TASK_THROW_NEW (task, GOOGLE_READER_ERROR_QUARK, 0,
                         "No password found in keyring");

  iris_task_complete (task);
}

static void
fetch_keyring_pass (IrisTask *task,
                    gpointer  user_data)
{
  MarinaGoogleReaderSourcePrivate *priv;
  
  g_return_if_fail (MARINA_IS_GOOGLE_READER_SOURCE (user_data));
  
  priv = MARINA_GOOGLE_READER_SOURCE (user_data)->priv;
  
  gnome_keyring_find_password (GNOME_KEYRING_NETWORK_PASSWORD,
                               find_password_cb,
                               g_object_ref (task), g_object_unref,
                               "user", priv->username,
                               "server", "www.google.com",
                               NULL);
}

static void
ask_user_pass (IrisTask *task,
               gpointer  user_data)
{
  GtkWidget   *dialog = NULL;
  GtkWidget   *entry  = NULL;
  const gchar *passwd;
  
  /* no more error, we are handling */
  IRIS_TASK_CATCH (task, NULL);
  
  get_pass_dialog (&dialog, &entry);

  g_debug ("Bringing up dialog to ask instead");
  
  /* show user dialog */
  if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_OK)
    {
      passwd = gtk_entry_get_text (GTK_ENTRY (entry));
      IRIS_TASK_RETURN_VALUE (task, G_TYPE_STRING, passwd);
      g_debug ("Got a password and stored it");
    }
  else
    {
      IRIS_TASK_THROW_NEW (task, GOOGLE_READER_ERROR_QUARK, 0,
                           "No password for google account");
      g_debug ("Couldn't get password from dialog");
    }
  
  gtk_widget_destroy (dialog);
}

static void
get_cookie (IrisTask *task,
            gpointer  user_data)
{
  MarinaGoogleReaderSourcePrivate *priv;
  SoupMessage *msg;
  gchar       *uri;
  GValue       value = {0,};
  
  g_debug ("get_cookie here");

  g_return_if_fail (IRIS_IS_TASK (task));
  g_return_if_fail (MARINA_IS_GOOGLE_READER_SOURCE (user_data));

  priv = MARINA_GOOGLE_READER_SOURCE (user_data)->priv;

  iris_task_get_result (task, &value);

  if (!G_VALUE_HOLDS_STRING (&value))
    goto cleanup;

  if (!priv->cookie)
    {
g_debug ("%d", __LINE__);
      uri = g_markup_printf_escaped (LOGIN_URI_FORMAT,
                                     priv->username,
                                     g_value_get_string (&value));
      msg = soup_message_new ("GET", uri);
      g_free (uri);
      
      if (soup_session_send_message (priv->session, msg) == 200)
        {
          gchar *tmp = strstr (msg->response_body->data, "SID=");
          if (!tmp)
            ; // set error
          
          gchar *sid = tmp;
          gchar *end = strstr (sid, "\n");
          
          if (!end)
            ; // set error
          
          *end = '\0';
          
          priv->cookie = g_strdup (sid);
          
          g_object_unref (msg);
        }
      else
        {
          // set error
        }
    }
  
  if (priv->cookie)
    IRIS_TASK_RETURN_VALUE (task, G_TYPE_STRING, priv->cookie);

cleanup:
  g_value_unset (&value);
}

static void
fetch_opml (IrisTask *task,
            gpointer  user_data)
{
  MarinaGoogleReaderSourcePrivate *priv;
  const gchar *cookie;
  SoupMessage *msg;
  GValue       value = {0,};
  
  g_return_if_fail (MARINA_IS_GOOGLE_READER_SOURCE (user_data));
  
  priv = MARINA_GOOGLE_READER_SOURCE (user_data)->priv;

  iris_task_get_result (task, &value);
  cookie =  g_value_get_string (&value);
  g_value_unset (&value);
  
  msg = soup_message_new ("GET", OPML_URI);
  soup_message_headers_append (msg->request_headers, "Cookie", cookie);
  
  if (soup_session_send_message (priv->session, msg) == 200)
    IRIS_TASK_RETURN_VALUE (task, G_TYPE_STRING, msg->response_body->data);
  else
    IRIS_TASK_THROW_NEW (task, GOOGLE_READER_ERROR_QUARK, 0,
                         "Error retrieving OPML from google");
}

static void
parse_opml (IrisTask *task,
            gpointer  user_data)
{
  MarinaGoogleReaderSource *google_source;
  XmlReader                *reader;
  GList                    *sources = NULL;
  MarinaSource             *source;
  gchar                    *id;
  GValue                    value = {0,};

  g_return_if_fail (MARINA_IS_GOOGLE_READER_SOURCE (user_data));
  
  google_source = MARINA_GOOGLE_READER_SOURCE (user_data);
  reader = xml_reader_new ();

  iris_task_get_result (task, &value);
  
  if (!xml_reader_load_from_data (reader,
                                  g_value_get_string (&value),
                                  -1, OPML_URI, NULL))
    goto cleanup;
  
  if (!xml_reader_read_start_element (reader, "opml"))
    // set error
    goto cleanup;
    
  if (!xml_reader_read_start_element (reader, "body"))
    // set error
    goto cleanup;
    
  while (xml_reader_read_start_element (reader, "outline"))
    {
      source = marina_soup_source_new ();
      id = g_strconcat (marina_source_get_id (MARINA_SOURCE (google_source)),
                        "+",
                        xml_reader_get_attribute (reader, "xmlUrl"),
                        NULL);
      MARINA_SOURCE_BASE (source)->id = g_strdup (id);
      g_object_set (source,
                    "title", xml_reader_get_attribute (reader, "title"),
                    "uri", xml_reader_get_attribute (reader, "xmlUrl"),
                    NULL);
      sources = g_list_prepend (sources, source);
      g_free (id);
    }
  
  sources = g_list_reverse (sources);
  
  if (!google_source->priv->children)
    {
      google_source->priv->children = sources;
      
      GList *iter;
      for (iter = sources; iter; iter = iter->next)
        {
          marina_source_child_added (MARINA_SOURCE (google_source), iter->data);
          marina_sync_source (iter->data);
        }
    }
  else
    {
      g_list_foreach (sources, (GFunc)g_object_unref, NULL);
      g_list_free (sources);
      sources = NULL;
      // set error
    }
  
  // update the unread counts
  marina_sources_stat (marina_sources_get_default ());
  
cleanup:
  g_object_unref (reader);
}

static gboolean
has_stream (MarinaSource *source)
{
  MarinaGoogleReaderSourcePrivate *priv;
  IrisTask *task;
  
  g_return_val_if_fail (MARINA_IS_GOOGLE_READER_SOURCE (source), FALSE);
  
  priv = MARINA_GOOGLE_READER_SOURCE (source)->priv;
  
  /* if they are asking if we have a stream, we can at this point
   * go ahead and lookup our opml from google if necessary */
  
  if (priv->username && !priv->children)
    {
      /* go ahead and start fetching the OPML from google */
      if (!priv->session)
        {
          g_mutex_lock (priv->session_lock);
          if (!priv->session)
            priv->session = soup_session_async_new ();
          g_mutex_unlock (priv->session_lock);
        }
      
      /* use gtask to get our remote items */
      
      /* get the credentials from gnome-keyring if we can */
      task = iris_task_new_full (fetch_keyring_pass,
                                 g_object_ref (source),
                                 g_object_unref,
                                 TRUE,
                                 NULL,
                                 g_main_context_default ());
      
      /* if that fails, ask the user. do this in an errback so we are in main thread */
      iris_task_add_errback (task, ask_user_pass, g_object_ref (source), g_object_unref);
      
      /* get our google cookie */
      iris_task_add_callback (task, get_cookie, g_object_ref (source), g_object_unref);
      
      /* fetch the OMPL from the google */
      iris_task_add_callback (task, fetch_opml, g_object_ref (source), g_object_unref);
      
      /* parse the OPML */
      iris_task_add_callback (task, parse_opml, g_object_ref (source), g_object_unref);
      
      iris_task_run (task);
    }
  
  return FALSE;
}

static GInputStream*
get_stream (MarinaSource  *source,
            GError       **error)
{
  return NULL;
}

static G_CONST_RETURN gchar*
get_icon_name (MarinaSource *source)
{
  return "google-reader";
}

static GList*
get_children (MarinaSource *source)
{
  MarinaGoogleReaderSourcePrivate *priv;
  
  g_return_val_if_fail (MARINA_IS_GOOGLE_READER_SOURCE (source), NULL);
  
  priv = MARINA_GOOGLE_READER_SOURCE (source)->priv;
  
  return g_list_copy (priv->children);
}

static gboolean
observes (MarinaSource *source,
          MarinaItem   *item)
{
  MarinaGoogleReaderSourcePrivate *priv;
  GList *iter;
  
  g_return_val_if_fail (MARINA_IS_GOOGLE_READER_SOURCE (source), FALSE);
  
  priv = MARINA_GOOGLE_READER_SOURCE (source)->priv;
  
  for (iter = priv->children; iter; iter = iter->next)
    if (marina_source_observes (iter->data, item))
      return TRUE;
  
  return FALSE;
}

static const gchar*
get_title (MarinaSource *source)
{
  return _("Google Reader");
}

static void
marina_source_init (MarinaSourceIface *iface)
{
  marina_debug (DEBUG_PLUGINS);
  
  iface->get_stream = get_stream;
  iface->has_stream = has_stream;
  iface->get_title = get_title;
  iface->set_title = NULL;
  iface->get_icon_name = get_icon_name;
  iface->get_children = get_children;
  iface->observes = observes;
}

void
marina_google_reader_source_set_username (MarinaGoogleReaderSource *source,
                                          const gchar              *username)
{
  MarinaGoogleReaderSourcePrivate *priv;
  
  g_return_if_fail (MARINA_IS_GOOGLE_READER_SOURCE (source));
  
  priv = source->priv;
  
  g_free (priv->username);
  priv->username = g_strdup (username);
}

G_CONST_RETURN gchar*
marina_google_reader_source_get_username (MarinaGoogleReaderSource *source)
{
  MarinaGoogleReaderSourcePrivate *priv;
  
  g_return_val_if_fail (MARINA_IS_GOOGLE_READER_SOURCE (source), NULL);
  
  priv = source->priv;
  
  return priv->username;
}
