/*
 * googlereader-plugin.h - Access your Google Reader account from Marina
 *
 * Copyright (C) 2009 - Christian Hergert
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GOOGLEREADER_PLUGIN_H__
#define __GOOGLEREADER_PLUGIN_H__

#include <glib.h>
#include <glib-object.h>
#include <marina/marina-plugin.h>

G_BEGIN_DECLS

/*
 * Type checking and casting macros
 */
#define TYPE_GOOGLEREADER_PLUGIN          (googlereader_plugin_get_type ())
#define GOOGLEREADER_PLUGIN(o)            (G_TYPE_CHECK_INSTANCE_CAST ((o), TYPE_GOOGLEREADER_PLUGIN, GooglereaderPlugin))
#define GOOGLEREADER_PLUGIN_CLASS(k)      (G_TYPE_CHECK_CLASS_CAST((k), TYPE_GOOGLEREADER_PLUGIN, GooglereaderPluginClass))
#define IS_GOOGLEREADER_PLUGIN(o)         (G_TYPE_CHECK_INSTANCE_TYPE ((o), TYPE_GOOGLEREADER_PLUGIN))
#define IS_GOOGLEREADER_PLUGIN_CLASS(k)   (G_TYPE_CHECK_CLASS_TYPE ((k), TYPE_GOOGLEREADER_PLUGIN))
#define GOOGLEREADER_GET_CLASS(o)         (G_TYPE_INSTANCE_GET_CLASS ((o), TYPE_GOOGLEREADER_PLUGIN, GooglereaderPluginClass))

/* Private structure type */
typedef struct _GooglereaderPluginPrivate	GooglereaderPluginPrivate;

/*
 * Main object structure
 */
typedef struct _GooglereaderPlugin		GooglereaderPlugin;

struct _GooglereaderPlugin
{
	MarinaPlugin parent_instance;

	/*< private >*/
	GooglereaderPluginPrivate *priv;
};

/*
 * Class definition
 */
typedef struct _GooglereaderPluginClass	GooglereaderPluginClass;

struct _GooglereaderPluginClass
{
	MarinaPluginClass parent_class;
};

/*
 * Public methods
 */
GType	googlereader_plugin_get_type	(void) G_GNUC_CONST;

/* All the plugins must implement this function */
G_MODULE_EXPORT GType register_marina_plugin (GTypeModule *module);

G_END_DECLS

#endif /* __GOOGLEREADER_PLUGIN_H__ */
