/*
 * sample-plugin.c - Show storage statistics
 * This file is part of marina.
 *
 * Copyright (C) 2009 - Christian Hergert
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib/gi18n-lib.h>

#include <marina/marina-debug.h>
#include <marina/marina-dirs.h>
#include <marina/marina-app.h>
#include <marina/marina-sources.h>
#include <marina/marina-items.h>

#include "sample-plugin.h"

#define WINDOW_DATA_KEY  "SamplePluginWindowData"

#define SAMPLE_PLUGIN_GET_PRIVATE(object)  \
  (G_TYPE_INSTANCE_GET_PRIVATE ((object),  \
  TYPE_SAMPLE_PLUGIN, SamplePluginPrivate))

struct _SamplePluginPrivate
{
  gpointer dummy;
};

MARINA_PLUGIN_REGISTER_TYPE (SamplePlugin, sample_plugin)

static void show_statistics (void);

static const gchar *ui_str = 
  "<ui>"
  "  <menubar name='MenuBar'>"
  "    <menu name='ViewMenu' action='View'>"
  "      <placeholder name='ViewOps_1'/>"
  "      <menuitem name='ShowDatabaseMenu' action='ShowDatabase'/>"
  "    </menu>"
  "  </menubar>"
  "</ui>";

static const GtkActionEntry action_entries[] =
{
  { "ShowDatabase", NULL, N_("_Database Statistics"), NULL,
    N_("Show current storage statistics"), G_CALLBACK (show_statistics) },
};

typedef struct
{
  GtkActionGroup *action_group;
  guint           ui_id;
} WindowData;

static void
on_close_clicked (GtkWidget *widget,
                  GtkWidget *window)
{
  gtk_widget_hide (window);
}

static gboolean
delete_event_cb (GtkWidget *widget)
{
  gtk_widget_hide (widget);
  return TRUE;
}

static void
increment (gpointer data,
           gpointer user_data)
{
  gint *d = user_data;
  *d = *d + 1;
}

typedef struct
{
  gint total;
  gint unread;
} IterInfo;

static gboolean
model_increment (GtkTreeModel *model,
                 GtkTreePath  *path,
                 GtkTreeIter  *iter,
                 gpointer      data)
{
  IterInfo *info = data;
  MarinaItem *item;
  
  info->total++;
  
  gtk_tree_model_get (model, iter, 0, &item, -1);
  
  if (!marina_item_get_read (item))
    info->unread++;
  
  return FALSE;
}

static GtkWidget*
create_window (void)
{
  MarinaApp     *marina_app    = marina_app_get_default ();
  MarinaWindow  *marina_window = marina_app_get_window (marina_app);
  GtkWidget     *widget;
  gchar         *file;
  
  GtkWidget     *n_articles_label;
  GtkWidget     *n_sources_label;
  GtkWidget     *unread_articles_label;
  
  file = marina_dirs_get_ui_file ("sample-plugin.ui");

  GtkBuilder *builder = gtk_builder_new ();
  if (!gtk_builder_add_from_file (builder, file, NULL))
  {
    g_warning ("Could not load the dialog ui.");
    return NULL;
  }

  widget                = GTK_WIDGET (gtk_builder_get_object (builder, "dialog"));
  n_articles_label      = GTK_WIDGET (gtk_builder_get_object (builder, "n_articles_label"));
  n_sources_label       = GTK_WIDGET (gtk_builder_get_object (builder, "n_sources_label"));
  unread_articles_label = GTK_WIDGET (gtk_builder_get_object (builder, "unread_articles_label"));
  
  g_object_set_data (G_OBJECT (widget), "n-articles", n_articles_label);
  g_object_set_data (G_OBJECT (widget), "n-sources", n_sources_label);
  g_object_set_data (G_OBJECT (widget), "unread", unread_articles_label);
  
  /* make sure window closes with parent */
  gtk_window_set_transient_for (GTK_WINDOW (widget),
                                GTK_WINDOW (marina_window));
  
  g_signal_connect (gtk_builder_get_object (builder, "close"),
                    "clicked",
                    G_CALLBACK (on_close_clicked),
                    widget);
  
  g_signal_connect (widget,
                    "delete-event",
                    G_CALLBACK (delete_event_cb),
                    NULL);
  
  g_signal_connect (gtk_builder_get_object (builder, "refresh"),
                    "clicked",
                    G_CALLBACK (show_statistics),
                    NULL);
  
  return widget;
}

static void
show_statistics (void)
{
  static GtkWidget *window  = NULL;
  MarinaItems      *items   = marina_items_get_default ();
  MarinaSources    *sources = marina_sources_get_default ();
  IterInfo          info    = {0, 0};
  gchar            *text    = NULL;
  gint              count   = 0;
  GtkTreeModel     *model;
  
  GtkWidget        *articles_label;
  GtkWidget        *sources_label;
  GtkWidget        *unread_label;
  
  if (!window)
    window = create_window ();
  
  articles_label = g_object_get_data (G_OBJECT (window), "n-articles");
  sources_label = g_object_get_data (G_OBJECT (window), "n-sources");
  unread_label = g_object_get_data (G_OBJECT (window), "unread");
  
  /* set the number of sources label */
  marina_sources_foreach (sources, increment, &count);
  text = g_markup_printf_escaped ("%d", count);
  gtk_label_set_text (GTK_LABEL (sources_label), text);
  g_free (text);
  
  /* set the number of items label */
  model = marina_items_get_model (items);
  count = 0;
  
  gtk_tree_model_foreach (model, model_increment, &info);
  text = g_markup_printf_escaped ("%d", info.total);
  gtk_label_set_text (GTK_LABEL (articles_label), text);
  g_free (text);
  
  /* update the number of unread items */
  text = g_markup_printf_escaped ("%d", info.unread);
  gtk_label_set_text (GTK_LABEL (unread_label), text);
  g_free (text);
  
  gtk_widget_show (window);
  gtk_window_present (GTK_WINDOW (window));
}

static void
sample_plugin_init (SamplePlugin *plugin)
{
  plugin->priv = SAMPLE_PLUGIN_GET_PRIVATE (plugin);

  marina_debug_message (DEBUG_PLUGINS,
                        "SamplePlugin initializing");
}

static void
sample_plugin_finalize (GObject *object)
{
  marina_debug_message (DEBUG_PLUGINS, "SamplePlugin finalizing");

  G_OBJECT_CLASS (sample_plugin_parent_class)->finalize (object);
}

static void
free_window_data (WindowData *data)
{
  g_return_if_fail (data != NULL);

  g_object_unref (data->action_group);
  g_free (data);
}

static void
impl_activate (MarinaPlugin *plugin,
               MarinaWindow *window)
{
  GtkUIManager *manager;
  WindowData *data;

  marina_debug (DEBUG_PLUGINS);

  data = g_new (WindowData, 1);
  manager = marina_window_get_ui_manager (window);

  data->action_group = gtk_action_group_new ("SamplePluginActions");
  gtk_action_group_set_translation_domain (data->action_group,
                                           GETTEXT_PACKAGE);
  gtk_action_group_add_actions (data->action_group,
                                action_entries,
                                G_N_ELEMENTS (action_entries),
                                window);

  gtk_ui_manager_insert_action_group (manager, data->action_group, -1);

  data->ui_id = gtk_ui_manager_add_ui_from_string (manager, ui_str,
                                                   -1, NULL);

  g_object_set_data_full (G_OBJECT (window), 
                          WINDOW_DATA_KEY, 
                          data,
                          (GDestroyNotify) free_window_data);
}

static void
impl_deactivate (MarinaPlugin *plugin,
                 MarinaWindow *window)
{
  GtkUIManager *manager;
  WindowData *data;

  marina_debug (DEBUG_PLUGINS);

  manager = marina_window_get_ui_manager (window);

  data = (WindowData *) g_object_get_data (G_OBJECT (window),
                                           WINDOW_DATA_KEY);
  g_return_if_fail (data != NULL);

  gtk_ui_manager_remove_ui (manager, data->ui_id);
  gtk_ui_manager_remove_action_group (manager, data->action_group);

  g_object_set_data (G_OBJECT (window), WINDOW_DATA_KEY, NULL);
}

static void
impl_update_ui (MarinaPlugin *plugin,
                MarinaWindow *window)
{
  marina_debug (DEBUG_PLUGINS);
}


static void
sample_plugin_class_init (SamplePluginClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  MarinaPluginClass *plugin_class = MARINA_PLUGIN_CLASS (klass);

  object_class->finalize = sample_plugin_finalize;

  plugin_class->activate = impl_activate;
  plugin_class->deactivate = impl_deactivate;
  plugin_class->update_ui = impl_update_ui;

  g_type_class_add_private (object_class, 
                            sizeof (SamplePluginPrivate));
}
