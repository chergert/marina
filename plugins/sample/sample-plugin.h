/*
 * sample-plugin.h - Show storage statistics
 *
 * Copyright (C) 2009 - Christian Hergert
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __SAMPLE_PLUGIN_H__
#define __SAMPLE_PLUGIN_H__

#include <glib.h>
#include <glib-object.h>
#include <marina/marina-plugin.h>

G_BEGIN_DECLS

/*
 * Type checking and casting macros
 */
#define TYPE_SAMPLE_PLUGIN		(sample_plugin_get_type ())
#define SAMPLE_PLUGIN(o)		(G_TYPE_CHECK_INSTANCE_CAST ((o), TYPE_SAMPLE_PLUGIN, SamplePlugin))
#define SAMPLE_PLUGIN_CLASS(k)		(G_TYPE_CHECK_CLASS_CAST((k),     TYPE_SAMPLE_PLUGIN, SamplePluginClass))
#define IS_SAMPLE_PLUGIN(o)		(G_TYPE_CHECK_INSTANCE_TYPE ((o), TYPE_SAMPLE_PLUGIN))
#define IS_SAMPLE_PLUGIN_CLASS(k)	(G_TYPE_CHECK_CLASS_TYPE ((k),    TYPE_SAMPLE_PLUGIN))
#define SAMPLE_GET_CLASS(o)		(G_TYPE_INSTANCE_GET_CLASS ((o),  TYPE_SAMPLE_PLUGIN, SamplePluginClass))

/* Private structure type */
typedef struct _SamplePluginPrivate	SamplePluginPrivate;

/*
 * Main object structure
 */
typedef struct _SamplePlugin		SamplePlugin;

struct _SamplePlugin
{
	MarinaPlugin parent_instance;

	/*< private >*/
	SamplePluginPrivate *priv;
};

/*
 * Class definition
 */
typedef struct _SamplePluginClass	SamplePluginClass;

struct _SamplePluginClass
{
	MarinaPluginClass parent_class;
};

/*
 * Public methods
 */
GType	sample_plugin_get_type	(void) G_GNUC_CONST;

/* All the plugins must implement this function */
G_MODULE_EXPORT GType register_marina_plugin (GTypeModule *module);

G_END_DECLS

#endif /* __SAMPLE_PLUGIN_H__ */
