/*
 * statusicon-plugin.c - A plugin providing a notification area icon
 *
 * Copyright (C) 2009 - Christian Hergert
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "statusicon-plugin.h"

#include <glib/gi18n-lib.h>

#include <marina/marina-debug.h>
#include <marina/marina-app.h>

#define WINDOW_DATA_KEY	"StatusiconPluginWindowData"

#define STATUSICON_PLUGIN_GET_PRIVATE(object)	\
  (G_TYPE_INSTANCE_GET_PRIVATE ((object),     \
  TYPE_STATUSICON_PLUGIN,                     \
  StatusiconPluginPrivate))

struct _StatusiconPluginPrivate
{
	GtkStatusIcon *status_icon;
};

MARINA_PLUGIN_REGISTER_TYPE (StatusiconPlugin, statusicon_plugin);

static const gchar *ui_str = 
  "<ui>"
  "  <popup name='StatusIconPopup'>"
  "    <menuitem action='MarinaNew'/>"
  "    <menuitem action='RefreshAll'/>"
  "    <separator/>"
  "    <menuitem action='EditPreferences'/>"
  "    <separator/>"
  "    <menuitem action='MarinaQuit'/>"
  "  </popup>"
  "</ui>";

static const GtkActionEntry action_entries[] =
{
};

typedef struct
{
  GtkActionGroup *action_group;
  guint           ui_id;
} WindowData;

static void
statusicon_plugin_init (StatusiconPlugin *plugin)
{
	plugin->priv = STATUSICON_PLUGIN_GET_PRIVATE (plugin);

	marina_debug_message (DEBUG_PLUGINS, "StatusiconPlugin initializing");
}

static void
statusicon_plugin_finalize (GObject *object)
{
	marina_debug_message (DEBUG_PLUGINS, "StatusiconPlugin finalizing");

	G_OBJECT_CLASS (statusicon_plugin_parent_class)->finalize (object);
}

static void
popup_menu_cb (GtkStatusIcon *status_icon,
               guint          button,
               guint          activate_time,
               MarinaWindow  *window)
{
  GtkUIManager *manager;
  GtkWidget *menu;
  
  manager = marina_window_get_ui_manager (window);
  g_return_if_fail (manager != NULL);

  menu = gtk_ui_manager_get_widget (manager, "/StatusIconPopup");
  g_return_if_fail (menu != NULL);
  
  gtk_menu_popup (GTK_MENU (menu), NULL, NULL,
                  gtk_status_icon_position_menu, status_icon,
                  button, activate_time);
}

static void
activate_cb (GtkStatusIcon *status_icon,
             MarinaWindow  *window)
{
  static gint x_pos = 0;
  static gint y_pos = 0;
  
  g_return_if_fail (GTK_IS_STATUS_ICON (status_icon));
  g_return_if_fail (MARINA_IS_WINDOW (window));
  
  if (GTK_WIDGET_VISIBLE (window))
    {
      /* store current location */
      gtk_window_get_position (GTK_WINDOW (window), &x_pos, &y_pos);
      
      gtk_widget_hide (GTK_WIDGET (window));
    }
  else
    {
      /* restore location */
      gtk_window_move (GTK_WINDOW (window), x_pos, y_pos);
      
      gtk_widget_show (GTK_WIDGET (window));
    }
}

static gboolean
app_quit_cb (MarinaApp *app)
{
  GtkWindow *window;
 
  window = GTK_WINDOW (marina_app_get_window (app));
  gtk_widget_hide (GTK_WIDGET (window));

  return TRUE;
}

static void
free_window_data (WindowData *data)
{
  g_return_if_fail (data != NULL);

  g_object_unref (data->action_group);
  g_free (data);
}

static void
impl_activate (MarinaPlugin *plugin,
               MarinaWindow *window)
{
  StatusiconPluginPrivate *priv;
  GtkUIManager *manager;
  WindowData *data;
  
	marina_debug (DEBUG_PLUGINS);
	
	g_return_if_fail (IS_STATUSICON_PLUGIN (plugin));
	
	priv = STATUSICON_PLUGIN (plugin)->priv;
	
	
  data = g_new (WindowData, 1);
  manager = marina_window_get_ui_manager (window);

  data->action_group = gtk_action_group_new ("StatusiconPluginActions");
  gtk_action_group_set_translation_domain (data->action_group,
                                           GETTEXT_PACKAGE);
  gtk_action_group_add_actions (data->action_group,
                                action_entries,
                                G_N_ELEMENTS (action_entries),
                                window);

  gtk_ui_manager_insert_action_group (manager, data->action_group, -1);
  data->ui_id = gtk_ui_manager_add_ui_from_string (manager, ui_str, -1, NULL);
  if (!data->ui_id)
    g_warning ("Could not load the ui definition for statusicon plugin");

  g_object_set_data_full (G_OBJECT (window), 
                          WINDOW_DATA_KEY, 
                          data,
                          (GDestroyNotify) free_window_data);
	
	priv->status_icon = gtk_status_icon_new_from_icon_name ("marina-icon");
	
	g_signal_connect (G_OBJECT (priv->status_icon),
	                  "activate",
	                  G_CALLBACK (activate_cb),
	                  window);

  g_signal_connect (G_OBJECT (priv->status_icon),
	                  "popup-menu",
	                  G_CALLBACK (popup_menu_cb),
	                  window);

  g_signal_connect (G_OBJECT (marina_app_get_default ()),
                    "quit",
                    G_CALLBACK (app_quit_cb),
                    NULL);
	
	gtk_status_icon_set_visible (priv->status_icon, TRUE);
}

static void
impl_deactivate (MarinaPlugin *plugin,
                 MarinaWindow *window)
{
  StatusiconPluginPrivate *priv;
	GtkUIManager *manager;
  WindowData *data;
  
	marina_debug (DEBUG_PLUGINS);
	
	g_return_if_fail (IS_STATUSICON_PLUGIN (plugin));
	
	priv = STATUSICON_PLUGIN (plugin)->priv;
	
	gtk_status_icon_set_visible (priv->status_icon, FALSE);
	
	g_object_unref (priv->status_icon);
	
	priv->status_icon = NULL;
	
  manager = marina_window_get_ui_manager (window);

  data = (WindowData *) g_object_get_data (G_OBJECT (window),
                                           WINDOW_DATA_KEY);
  g_return_if_fail (data != NULL);

  gtk_ui_manager_remove_ui (manager, data->ui_id);
  gtk_ui_manager_remove_action_group (manager, data->action_group);

  g_object_set_data (G_OBJECT (window), WINDOW_DATA_KEY, NULL);
}

static void
impl_update_ui (MarinaPlugin *plugin,
		MarinaWindow *window)
{
	marina_debug (DEBUG_PLUGINS);
}


static void
statusicon_plugin_class_init (StatusiconPluginClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);
	MarinaPluginClass *plugin_class = MARINA_PLUGIN_CLASS (klass);

	object_class->finalize = statusicon_plugin_finalize;

	plugin_class->activate = impl_activate;
	plugin_class->deactivate = impl_deactivate;
	plugin_class->update_ui = impl_update_ui;

	g_type_class_add_private (object_class, 
				  sizeof (StatusiconPluginPrivate));
}
