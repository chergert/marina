/*
 * marina-plugin-loader-python.h
 * This file is part of marina; it was borrowed from gedit.
 *
 * Copyright (C) 2008 - Jesse van den Kieboom
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA 02111-1307, USA. 
 */

#ifndef __MARINA_PLUGIN_LOADER_PYTHON_H__
#define __MARINA_PLUGIN_LOADER_PYTHON_H__

#include <marina/marina-plugin-loader.h>

G_BEGIN_DECLS

#define MARINA_TYPE_PLUGIN_LOADER_PYTHON		(marina_plugin_loader_python_get_type ())
#define MARINA_PLUGIN_LOADER_PYTHON(obj)		(G_TYPE_CHECK_INSTANCE_CAST ((obj), MARINA_TYPE_PLUGIN_LOADER_PYTHON, MarinaPluginLoaderPython))
#define MARINA_PLUGIN_LOADER_PYTHON_CONST(obj)	(G_TYPE_CHECK_INSTANCE_CAST ((obj), MARINA_TYPE_PLUGIN_LOADER_PYTHON, MarinaPluginLoaderPython const))
#define MARINA_PLUGIN_LOADER_PYTHON_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST ((klass), MARINA_TYPE_PLUGIN_LOADER_PYTHON, MarinaPluginLoaderPythonClass))
#define MARINA_IS_PLUGIN_LOADER_PYTHON(obj)		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), MARINA_TYPE_PLUGIN_LOADER_PYTHON))
#define MARINA_IS_PLUGIN_LOADER_PYTHON_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((klass), MARINA_TYPE_PLUGIN_LOADER_PYTHON))
#define MARINA_PLUGIN_LOADER_PYTHON_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS ((obj), MARINA_TYPE_PLUGIN_LOADER_PYTHON, MarinaPluginLoaderPythonClass))

typedef struct _MarinaPluginLoaderPython		MarinaPluginLoaderPython;
typedef struct _MarinaPluginLoaderPythonClass		MarinaPluginLoaderPythonClass;
typedef struct _MarinaPluginLoaderPythonPrivate	MarinaPluginLoaderPythonPrivate;

struct _MarinaPluginLoaderPython {
	GObject parent;
	
	MarinaPluginLoaderPythonPrivate *priv;
};

struct _MarinaPluginLoaderPythonClass {
	GObjectClass parent_class;
};

GType marina_plugin_loader_python_get_type (void) G_GNUC_CONST;
MarinaPluginLoaderPython *marina_plugin_loader_python_new(void);

/* All the loaders must implement this function */
G_MODULE_EXPORT GType register_marina_plugin_loader (GTypeModule * module);

G_END_DECLS

#endif /* __MARINA_PLUGIN_LOADER_PYTHON_H__ */

