/*
 * marina-plugin-python.h
 * This file is part of marina; it was borrowed from gedit.
 *
 * Copyright (C) 2005 - Raphael Slinckx
 * Copyright (C) 2008 - Jesse van den Kieboom
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA 02111-1307, USA. 
 */

#ifndef __MARINA_PLUGIN_PYTHON_H__
#define __MARINA_PLUGIN_PYTHON_H__

#define NO_IMPORT_PYGOBJECT

#include <glib-object.h>
#include <pygobject.h>

#include <marina/marina-plugin.h>

G_BEGIN_DECLS

/*
 * Type checking and casting macros
 */
#define MARINA_TYPE_PLUGIN_PYTHON		(marina_plugin_python_get_type())
#define MARINA_PLUGIN_PYTHON(obj)		(G_TYPE_CHECK_INSTANCE_CAST((obj), MARINA_TYPE_PLUGIN_PYTHON, MarinaPluginPython))
#define MARINA_PLUGIN_PYTHON_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST((klass), MARINA_TYPE_PLUGIN_PYTHON, MarinaPluginPythonClass))
#define MARINA_IS_PLUGIN_PYTHON(obj)		(G_TYPE_CHECK_INSTANCE_TYPE((obj), MARINA_TYPE_PLUGIN_PYTHON))
#define MARINA_IS_PLUGIN_PYTHON_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((klass), MARINA_TYPE_PLUGIN_PYTHON))
#define MARINA_PLUGIN_PYTHON_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS((obj), MARINA_TYPE_PLUGIN_PYTHON, MarinaPluginPythonClass))

/* Private structure type */
typedef struct _MarinaPluginPythonPrivate MarinaPluginPythonPrivate;

/*
 * Main object structure
 */
typedef struct _MarinaPluginPython MarinaPluginPython;

struct _MarinaPluginPython 
{
	MarinaPlugin parent;
	
	/*< private > */
	MarinaPluginPythonPrivate *priv;
};

/*
 * Class definition
 */
typedef struct _MarinaPluginPythonClass MarinaPluginPythonClass;

struct _MarinaPluginPythonClass 
{
	MarinaPluginClass parent_class;
};

/*
 * Public methods
 */
GType	 marina_plugin_python_get_type 		(void) G_GNUC_CONST;


/* 
 * Private methods
 */
void	  _marina_plugin_python_set_instance	(MarinaPluginPython *plugin, 
						 PyObject 	   *instance);
PyObject *_marina_plugin_python_get_instance	(MarinaPluginPython *plugin);

G_END_DECLS

#endif  /* __MARINA_PLUGIN_PYTHON_H__ */

